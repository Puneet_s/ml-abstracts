
1. J Appl Physiol (1985). 1996 May;80(5):1475-84.

Ventilatory dynamics during transient arousal from NREM sleep: implications for
respiratory control stability.

Khoo MC(1), Koh SS, Shin JJ, Westbrook PR, Berry RB.

Author information: 
(1)Biomedical Engineering Department, University of Southern California, Los
Angeles 90089-1451, USA.

The polysomnographic and ventilatory patterns of nine normal adults were measured
during non-rapid-eye-movement (NREM) stage 2 sleep before and after repeated
administrations of a tone (40-72 dB) lasting 5 s. The ventilatory response to
arousal (VRA) was determined in data sections showing electrocortical arousal
following the start of the tone. Mean inspiratory flow and tidal volume increased
significantly above control levels in the first seven breaths after the start of 
arousal, with peak increases (64.2% > control) occurring on the second breath.
Breath-to-breath occlusion pressure 100 ms after the start of inspiration showed 
significant increases only on the second and third postarousal breaths, whereas
upper airway resistance declined immediately and remained below control for > or 
= 7 consecutive breaths. These results suggest that the first breath and latter
portion of the VRA are determined more by upper airway dynamics than by changes
in the neural drive to breathe. Computer model simulations comparing different
VRA time courses show that sustained periodic apnea is more likely to occur when 
the fall in the postarousal increase in ventilation is more abrupt.


PMID: 8727529  [PubMed - indexed for MEDLINE]

