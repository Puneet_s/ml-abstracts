
1. J Appl Physiol (1985). 2013 Feb;114(3):316-28. doi:
10.1152/japplphysiol.00895.2012. Epub 2012 Nov 29.

Phasic bursts of the antagonistic jaw muscles during REM sleep mimic a
coordinated motor pattern during mastication.

Kato T(1), Nakamura N, Masuda Y, Yoshida A, Morimoto T, Yamamura K, Yamashita S, 
Sato F.

Author information: 
(1)Osaka University Graduate School of Dentistry, Department of Oral Anatomy and 
Neurobiology, Osaka, Japan. takafumi@dent.osaka-u.ac.jp

Sleep-related movement disorders are characterized by the specific phenotypes of 
muscle activities and movements during sleep. However, the state-specific
characteristics of muscle bursts and movement during sleep are poorly understood.
In this study, jaw-closing and -opening muscle electromyographic (EMG) activities
and jaw movements were quantified to characterize phenotypes of motor patterns
during sleep in freely moving and head-restrained guinea pigs. During non-rapid
eye movement (NREM) sleep, both muscles were irregularly activated in terms of
duration, activity, and intervals. During rapid eye movement (REM) sleep,
clusters of phasic bursts occurred in the two muscles. Compared with NREM sleep, 
burst duration, activity, and intervals were less variable during REM sleep for
both muscles. Although burst activity was lower during the two sleep states than 
during chewing, burst duration and intervals during REM sleep were distributed
within a similar range to those during chewing. A trigger-averaged analysis of
muscle bursts revealed that the temporal association between the bursts of the
jaw-closing and -opening muscles during REM sleep was analogous to the temporal
association during natural chewing. The burst characteristics of the two muscles 
reflected irregular patterns of jaw movements during NREM sleep and repetitive
alternating bilateral movements during REM sleep. The distinct patterns of jaw
muscle bursts and movements reflect state-specific regulations of the jaw motor
system during sleep states. Phasic activations in the antagonistic jaw muscles
during REM sleep are regulated, at least in part, by the neural networks
involving masticatory pattern generation, demonstrating that waking jaw motor
patterns are replayed during sleep periods.

DOI: 10.1152/japplphysiol.00895.2012 
PMID: 23195628  [PubMed - indexed for MEDLINE]

