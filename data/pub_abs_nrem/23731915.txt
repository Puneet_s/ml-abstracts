
1. Funct Neurol. 2013 Jan-Mar;28(1):47-53.

Sleep in ring chromosome 20 syndrome: a peculiar electroencephalographic pattern.

Zambrelli E(1), Vignoli A, Nobili L, Didato G, Mastrangelo M, Turner K, Canevini 
MP.

Author information: 
(1)San Paolo Hospital, Milan, Italy. elena.zambrelli@ao-sanpaolo.it

Ring chromosome 20 [r(20)] syndrome is a chromosomal disorder characterized by
epilepsy and intellectual disability. Distinctive electroclinical features and
wakefulness EEG patterns have been described. The EEG features of sleep have not 
yet been evaluated. We studied the pattern of sleep in six patients aged 2-59
years who underwent at least one polysomnographic recording. Their sleep pattern 
evolution is described as deterioration ranging from normal to destructured
NREM/REM sleep. NREM sleep alterations were observed from childhood and were more
evident in adulthood. EEG abnormalities detected during wakefulness persisted,
with morphological changes, during sleep. During NREM sleep all the subjects
presented high amplitude delta sequences with a sharply contoured or notched
appearance, prevalent over frontal regions. The theta rhythm of wakefulness was
seen to persist during REM sleep. Ring chromosome 20 syndrome shows sleep
alterations that seem to be age-related. A potential role of cortical and
thalamocortical dysfunction is discussed.


PMCID: PMC3812720
PMID: 23731915  [PubMed - indexed for MEDLINE]

