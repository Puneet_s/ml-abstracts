
1. Mol Brain. 2013 Dec 26;6:59. doi: 10.1186/1756-6606-6-59.

Analysis of sleep disorders under pain using an optogenetic tool: possible
involvement of the activation of dorsal raphe nucleus-serotonergic neurons.

Ito H, Yanase M, Yamashita A, Kitabatake C, Hamada A, Suhara Y, Narita M, Ikegami
D, Sakai H, Yamazaki M(1), Narita M.

Author information: 
(1)Department of Pharmacology, Hoshi University School of Pharmacy and
Pharmaceutical Sciences, 2-4-41 Ebara, Shinagawa-ku, Tokyo 142-8501, Japan.
myama@med.u-toyama.ac.jp.

BACKGROUND: Several etiological reports have shown that chronic pain
significantly interferes with sleep. Inadequate sleep due to chronic pain may
contribute to the stressful negative consequences of living with pain. However,
the neurophysiological mechanism by which chronic pain affects sleep-arousal
patterns is as yet unknown. Although serotonin (5-HT) was proposed to be
responsible for sleep regulation, whether the activity of 5-HTergic neurons in
the dorsal raphe nucleus (DRN) is affected by chronic pain has been studied only 
infrequently. On the other hand, the recent development of optogenetic tools has 
provided a valuable opportunity to regulate the activity in genetically targeted 
neural populations with high spatial and temporal precision. In the present
study, we investigated whether chronic pain could induce sleep dysregulation
while changing the activity of DRN-5-HTergic neurons. Furthermore, we sought to
physiologically activate the DRN with channelrhodopsin-2 (ChR2) to identify a
causal role for the DRN-5-HT system in promoting and maintaining wakefulness
using optogenetics.
RESULTS: We produced a sciatic nerve ligation model by tying a tight ligature
around approximately one-third to one-half the diameter of the sciatic nerve. In 
mice with nerve ligation, we confirmed an increase in wakefulness and a decrease 
in non-rapid eye movement (NREM) sleep as monitored by electroencephalogram
(EEG). Microinjection of the retrograde tracer fluoro-gold (FG) into the
prefrontal cortex (PFC) revealed several retrogradely labeled-cells in the DRN.
The key finding of the present study was that the levels of 5-HT released in the 
PFC by the electrical stimulation of DRN neurons were significantly increased in 
mice with sciatic nerve ligation. Using optogenetic tools in mice, we found a
causal relationship among DRN neuron firing, cortical activity and sleep-to-wake 
transitions. In particular, the activation of DRN-5-HTergic neurons produced a
significant increase in wakefulness and a significant decrease in NREM sleep. The
duration of NREM sleep episodes was significantly decreased during
photostimulation in these mice.
CONCLUSIONS: These results suggest that neuropathic pain accelerates the activity
of DRN-5-HTergic neurons. Although further loss-of-function experiments are
required, we hypothesize that this activation in DRN neurons may, at least in
part, correlate with sleep dysregulation under a neuropathic pain-like state.

DOI: 10.1186/1756-6606-6-59 
PMCID: PMC3879646
PMID: 24370235  [PubMed - indexed for MEDLINE]

