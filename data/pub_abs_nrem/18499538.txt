
1. Respir Physiol Neurobiol. 2008 Jun 30;162(1):63-72. doi:
10.1016/j.resp.2008.04.002. Epub 2008 Apr 11.

NMDA receptor-mediated processes in the Parabrachial/Kölliker fuse complex
influence respiratory responses directly and indirectly via changes in cortical
activation state.

Boon JA(1), Milsom WK.

Author information: 
(1)Biology/Physical Geography Unit, University of British Columbia Okanagan, 3333
University Way, Kelowna, B.C., Canada. Joyce.Boon@ubc.ca

We tested the hypothesis that glutamate, acting via NMDA-type receptors (NMDAr)
in the Parabrachial/Kölliker fuse (PBrKF) nucleus of the pons, is involved both
directly and indirectly (via changes in cortical activation state) in modulating 
breathing and ventilatory responses to hypoxia. To this end we examined the
effects of MK-801, injected either systemically or directly into the PBrKF, on
the breathing patterns of urethane-anaesthetized rats breathing air or an hypoxic
gas mixture as electroencephalographic (EEG) activity alternated between State I 
(awake-like) and State III (NREM sleep-like) EEG patterns. Regardless of EEG
state, systemic MK-801 reduced ventilation primarily by reducing tidal volume
while microinjection of MK-801 into the PBrKF reduced ventilation by reducing
breathing frequency. With both injections, EEG pattern changed from State I to
III mimicking the change from wakefulness to NREM sleep that occurs in
unanaesthetized rats given MK-801 systemically. Systemic injection of MK-801
delayed and reduced the response to hypoxia while microinjection of MK-801 into
the PBrKF did not reduce the HVR but sustained the hypoxic increase in tidal
volume well into the post-hypoxic recovery period. Thus, while NMDAr in the PBrKF
complex of the pons play a role in modulating sleep/wake-like states as well as
changes in breathing pattern associated with changes in cortical activation
state, they are neither involved in the hypoxic ventilatory response nor in the
change in hypoxic sensitivity associated with the changes in cortical activation 
state.

DOI: 10.1016/j.resp.2008.04.002 
PMID: 18499538  [PubMed - indexed for MEDLINE]

