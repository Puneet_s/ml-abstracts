
1. J Clin Sleep Med. 2016 Apr 15;12(4):607-16. doi: 10.5664/jcsm.5702.

Respiratory Variability during Sleep in Methadone Maintenance Treatment Patients.

Nguyen CD(1,)(2), Kim JW(1), Grunstein RR(1,)(3), Thamrin C(1), Wang
D(1,)(3,)(4).

Author information: 
(1)Woolcock Institute of Medical Research and Sydney Medical School, University
of Sydney, Glebe, New South Wales, Australia. (2)Neuroscience Research Australia 
(NeuRA), Randwick, New South Wales, Australia. (3)Department of Respiratory and
Sleep Medicine, Royal Prince Alfred Hospital, Sydney Local Health District,
Central Clinical School, University of Sydney, Camperdown, New South Wales,
Australia. (4)Department of Respiratory and Sleep Disorders Medicine, Western
Hospital, University of Melbourne, Victoria, Australia.

STUDY OBJECTIVES: Methadone maintenance treatment (MMT) patients have a high
prevalence of central sleep apnea and ataxic breathing related to damage to
central respiratory rhythm control. However, the quantification of sleep apnea
indices requires laborious manual scoring, and ataxic breathing pattern is
subjectively judged by visual pattern recognition. This study proposes a
semi-automated technique to characterize respiratory variability in MMT patients.
METHODS: Polysomnography, blood, and functional outcomes of sleep questionnaire
(FOSQ) from 50 MMT patients and 20 healthy subjects with matched age, sex, and
body mass index, were analyzed. Inter-breath intervals (IBI) were extracted from 
the nasal cannula pressure signal. Variability of IBI over 100 breaths was
quantified by standard deviation (SD), coefficient of variation (CV), and scaling
exponent (α) from detrended fluctuation analysis. The relationships between these
variability measures and blood methadone concentration, central sleep apnea index
(CAI), apnea-hypopnea index (AHI), and clinical outcome (FOSQ), were then
examined.
RESULTS: MMT patients had significantly higher SD and CV during all sleep stages.
During NREM sleep, SD and CV were correlated with blood methadone concentration
(Spearman R = 0.52 and 0.56, respectively; p < 0.01). SD and CV were also
correlated with CAI (R = 0.63 and 0.71, p < 0.001, respectively), and AHI (R =
0.45 and 0.58, p < 0.01, respectively). Only α showed significant correlation
with FOSQ (R = -0.33, p < 0.05).
CONCLUSIONS: MMT patients have a higher respiratory variability during sleep than
healthy controls. Semi-automated variability measures are related to apnea
indices obtained by manual scoring and may provide a new approach to quantify
opioid-related sleep-disordered breathing.

© 2016 American Academy of Sleep Medicine.

DOI: 10.5664/jcsm.5702 
PMCID: PMC4795289 [Available on 2016-10-15]
PMID: 26943710  [PubMed - in process]

