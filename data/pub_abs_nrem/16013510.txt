
1. Lin Chuang Er Bi Yan Hou Ke Za Zhi. 2005 Mar;19(6):262-4.

[Research about sleep structure of OSAHS].

[Article in Chinese]

Zhang B(1), Li X, Wang T.

Author information: 
(1)Department of Otolaryngology, General Hospital of Air Force, Beijing, 100036, 
China. zhang.bao.lin@sohu.com

OBJECTIVE: Research the sleep structure of the OSAHS and snores, explore the
reason that patients feel drowsy in daytime.
METHOD: Monitor the sleep of 46 OSAHS, 16 snores and 20 normal control, calculate
the percent of stages of NREM and REM, count the RDI, AI, HI, total MI, MI
associated with leg movement, MI associated with RDI, MI associated with snore,
spontaneous MI.
RESULT: OSAHS has sleep structure disturbance obviously. The light sleep (stage
I) increase obviously, but deep one decrease (stage II + V) obviously. The sleep 
structure is insufficient. The patients are deprived of a number of the REM
sleep. The waking time is longer than normal control group. During the light
sleep, the microarousal that is associated with both apnea and leg movement
increase obviously.
CONCLUSION: During the sleep, OSAHS has obvious sleep deprivation, frequent
arousal, disturbance sleep structure and blood desaturation, etc. These resulted 
in disturbances of brain metabolize. This is the reason that patients feel
drowsy, feeble decline, etc.


PMID: 16013510  [PubMed - indexed for MEDLINE]

