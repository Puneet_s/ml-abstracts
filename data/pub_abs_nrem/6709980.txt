
1. Respir Physiol. 1984 Jan;55(1):103-20.

Effects of slow wave sleep on ventilatory compensation to inspiratory elastic
loading.

Wilson PA, Skatrud JB, Dempsey JA.

We determined the effects of slow wave sleep on ventilatory compensation to
inspiratory elastic loads (18 cm H2O/L). Multiple loading trials of variable
duration were applied in three healthy adult humans in wakefulness and during
NREM sleep. During wakefulness, ventilatory response over 5 loaded breaths were
highly variable. Tidal volume (VT), mean inspiratory flow (VT/TI), and minute
ventilation (VE) were preserved or increased in 2 of the 3 subjects in whom mouth
occlusion pressure (P0.1) was augmented in the immediate (second breath) response
to the load. In the third subject who showed no change in P0.1, VE was not
preserved during loading. During NREM sleep, the loading response was highly
consistent in all trials and in all 3 subjects. P0.1 on the second loaded breath 
was not increased; thus VE, VT and VT/TI were reduced over five loaded breaths.
This absence of immediate load compensation during NREM sleep was similar during 
normoxia, hyperoxia, and hypercapnia. During sustained loading in NREM sleep VE
and VT returned toward control levels coincident with an increase in end tidal
CO2. We conclude that augmentation of inspiratory neural drive sufficient for
immediate compensation to elastic loads requires wakefulness. Compensatory
responses to loading do not occur during NREM sleep until inspiratory effort is
augmented by chemical stimuli.


PMID: 6709980  [PubMed - indexed for MEDLINE]

