
1. Sleep. 2008 Jul;31(7):1035-42.

Contextual fear extinction ameliorates sleep disturbances found following fear
conditioning in rats.

Wellman LL(1), Yang L, Tang X, Sanford LD.

Author information: 
(1)Sleep Research Laboratory, Department of Pathology and Anatomy, Eastern
Virginia Medical School, Norfolk, VA 23507, USA.

STUDY OBJECTIVE: To examine the effects of fear extinction on subsequent sleep in
rats and to compare it with the effects seen following contextual reminders of
fear.
DESIGN: Habituation of the rats to handling and baseline recordings were obtained
over 2 consecutive days. Afterward, the rats were subjected to shock training
(ST; day 1), context reexposure (CR; either 30 or 60 min; day 2), and fear recall
(R; day 3). Percentage time spent in freezing (FT%) was observed during ST, CR,
and R exposures. Sleep was recorded for 20 h (8-h light and 12-h dark period)
following ST, CR, and R.
SETTING: NA SUBJECTS: The subjects were outbred Wistar rats randomly assigned to 
one of two groups: contextual fear (FR; n = 7) or contextual extinction (EXT; n =
7).
INTERVENTIONS: The rats were surgically implanted with electrodes for recording
the electroencephalogram and electromyogram for determining arousal state.
MEASUREMENTS AND RESULTS: There were no differences between groups on FT% during 
ST or the first 30 min of CR; however, during R, the FR group had greater FT%
than EXT. Sleep did not differ between groups following ST. Following CR, EXT
exhibited significantly more total sleep, NREM, and REM than FR. After R, there
were no differences between groups.
CONCLUSIONS: Rats that exhibit extinction of contextual fear show significantly
increased sleep compared to rats who continue to exhibit contextual fear. This
suggests that sleep disturbances normally experienced in humans following
traumatic events or reminders may be ameliorated by therapies that address and
eliminate the associated fear.


PMCID: PMC2491499
PMID: 18652099  [PubMed - indexed for MEDLINE]

