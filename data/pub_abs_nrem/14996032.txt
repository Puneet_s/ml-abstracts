
1. J Sleep Res. 2004 Mar;13(1):31-6.

Corticospinal excitability and sleep: a motor threshold assessment by
transcranial magnetic stimulation after awakenings from REM and NREM sleep.

Bertini M(1), Ferrara M, De Gennaro L, Curcio G, Fratello F, Romei V, Pauri F,
Rossini PM.

Author information: 
(1)Dipartimento di Psicologia, Università di Roma La Sapienza, Rome, Italy.

Transcranial magnetic stimulation (TMS) is a recently established technique in
the neurosciences that allows the non-invasive assessment, among other
parameters, of the excitability of motor cortex. Up to now, its application to
sleep research has been very scarce and because of technical problems it provided
contrasting results. In fact delivering one single suprathreshold magnetic
stimulus easily awakes subjects, or lightens their sleep. For this reason, in the
present study we assessed motor thresholds (MTs) upon rapid eye movement (REM)
and non-rapid eye movement (NREM) sleep awakenings, both in the first and in the 
last part of the night. Taking into account that a full re-establishment of wake 
regional brain activity patterns upon awakening from sleep needs up to 20-30 min,
it is possible to make inferences about the neurophysiological characteristics of
the different sleep stages by analyzing the variables of interest immediately
after provoked awakenings. Ten female volunteers slept in the lab for four
consecutive nights. During the first night the MTs were collected, following a
standardized procedure: 5 min before lights off, upon stage 2 awakening (second
NREM period), upon REM sleep awakening (second REM period), upon the final
morning awakening (always from stage 2). Results showed that MTs increased
linearly from presleep wakefulness to REM sleep awakenings, and from the latter
to stage 2 awakenings. There was also a time-of-night effect on MTs upon
awakening from stage 2, indicating that MTs decreased from the first to the
second part of the night. The increase in corticospinal excitability across the
night, which parallels the fulfillment of sleep need, is consistent with the
linear decrease of auditory arousal thresholds during the night. The maximal
reduction of corticospinal excitability during early NREM sleep can be related to
the hyperpolarization of thalamocortical neurons, and is in line with the
decreased metabolic activity of motor cortices during this sleep stage. On the
contrary, the increase of MTs upon REM sleep awakenings should reflect peripheral
factors. We conclude that our findings legitimate the introduction of the TMS
technique as a new proper tool in sleep research.


PMID: 14996032  [PubMed - indexed for MEDLINE]

