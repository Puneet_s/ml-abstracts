
1. Anesthesiology. 2003 Feb;98(2):428-36.

The alpha2-adrenoceptor agonist dexmedetomidine converges on an endogenous
sleep-promoting pathway to exert its sedative effects.

Nelson LE(1), Lu J, Guo T, Saper CB, Franks NP, Maze M.

Author information: 
(1)Department of Anaesthetics, Imperial College of Science, Technology and
Medicine, London, United Kingdom.

BACKGROUND: The authors investigated whether the sedative, or hypnotic, action of
the general anesthetic dexmedetomidine (a selective alpha -adrenoceptor agonist) 
activates endogenous nonrapid eye movement (NREM) sleep-promoting pathways.
METHODS: c-Fos expression in sleep-promoting brain nuclei was assessed in rats
using immunohistochemistry and hybridization. Next, the authors perturbed these
pathways using (1) discrete lesions induced by ibotenic acid, (2) local and
systemic administration of gamma-aminobutyric acid receptor type A (GABA )
receptor antagonist gabazine, or (3) alpha2-adrenoceptor antagonist atipamezole
in rats, and (4) genetic mutation of the alpha -adrenoceptor in mice.
RESULTS: Dexmedetomidine induced a qualitatively similar pattern of c-Fos
expression in rats as seen during normal NREM sleep, a decrease in the locus
ceruleus (LC) and tuberomammillary nucleus (TMN) and an increase in the
ventrolateral preoptic nucleus (VLPO). These changes were attenuated by
atipamezole and were not seen in mice lacking functional alpha2a-adrenoceptors,
which do not show a sedative response to dexmedetomidine. Bilateral VLPO lesions 
attenuated the sedative response to dexmedetomidine, and the dose-response curve 
to dexmedetomidine was shifted right by gabazine administered systemically or
directly into the TMN. VLPO lesions and gabazine pretreatment altered c-Fos
expression in the TMN but in not the LC after dexmedetomidine administration,
indicating a hierarchical sequence of changes.
CONCLUSIONS: The authors propose that endogenous sleep pathways are causally
involved in dexmedetomidine-induced sedation; dexmedetomidine's sedative
mechanism involves inhibition of the LC, which disinhibits VLPO firing. The
increased release of GABA at the terminals of the VLPO inhibits TMN firing, which
is required for the sedative response.


PMID: 12552203  [PubMed - indexed for MEDLINE]

