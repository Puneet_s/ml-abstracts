
1. Clin Neurophysiol. 2011 Feb;122(2):311-9. doi: 10.1016/j.clinph.2010.06.019. Epub
2010 Jul 15.

Neurocognitive assessment and sleep analysis in children with sleep-disordered
breathing.

Miano S(1), Paolino MC, Urbano A, Parisi P, Massolo AC, Castaldo R, Villa MP.

Author information: 
(1)Department of Paediatrics, Sleep Disease Centre, La Sapienza University of
Rome - Sant'Andrea Hospital, Rome, Italy.

OBJECTIVE: To assess possible correlations between intelligence quotient (IQ) and
attention deficit hyperactive disorder (ADHD) rating scale values and sleep
(including cyclic alternating patterns analysis) and respiratory parameters in
children with sleep-disordered breathing (SDB).
METHODS: Thirteen children who satisfied the criteria for primary snoring and 31 
children for obstructive sleep apnea syndrome (OSAS) underwent polysomnography in
a standard laboratory setting and a neurocognitive assessment. Sixty normal
controls recruited from two schools underwent the neurocognitive assessment.
RESULTS: The IQ estimates of controls were higher and the ADHD rating scale
scores lower than those of children with SDB. Children with OSAS had a higher REM
sleep latency and arousal index as well as a lower N3 and A mean duration than
children who snored. In our sample of children with SDB, the percentage of
wakefulness after sleep onset, of N1, of A2, of arousal and A2 index correlated
positively with global intelligence. Total and hyperactivity scores correlated
positively with the A2 index. Regression analysis mostly confirmed the
correlations between neurocognitive measures and sleep parameters and further
demonstrated a negative correlation between the hyperactivity rating score and
oxygen saturation during the night.
CONCLUSIONS: Our results support the hypothesis that arousal is a defensive
mechanism that may preserve cognitive function by counteracting the respiratory
events, at the expense of sleep maintenance and NREM sleep instability.
SIGNIFICANCE: We believe that our study makes an interesting contribution to
research on the relationship between sleep fragmentation and cognitive function.

Copyright Â© 2010 International Federation of Clinical Neurophysiology. Published
by Elsevier Ireland Ltd. All rights reserved.

DOI: 10.1016/j.clinph.2010.06.019 
PMID: 20637692  [PubMed - indexed for MEDLINE]

