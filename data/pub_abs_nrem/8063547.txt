
1. Int J Neurosci. 1993 Jun;70(3-4):285-98.

The ultradian rhythm of alternating cerebral hemispheric activity.

Shannahoff-Khalsa D(1).

Author information: 
(1)Khalsa Foundation for Medical Science, Del Mar, California 92014-5708.

This review covers cognitive and electroencephalographic studies of the ultradian
rhythm of alternating cerebral hemispheric activity found in humans and animals. 
This endogenous alternation of right and left dominance ranges in periodicity
from about 25 to 300 min with peaks between 90-200 min during waking and around
100 min during sleep. Studies of lateralized EEG activity during sleep are
reported as correlates of the REM-NREM sleep cycle. Studies of lateralized
ultradian rhythms of EEG during wakefulness reveal a correlation between
hemispheric dominance and the nasal cycle. The rhythm of cerebral dominance has
also been identified with tests of lateralized cognitive performance using left
and right hemisphere dependent tasks. Awakening from REM or NREM sleep is
associated with different effects on left or right hemispheric dominance. This
rhythm plays an important role in cognitive performance, memory processes, visual
perception, levels of arousal and performance, mood, and individual and social
behavior.


PMID: 8063547  [PubMed - indexed for MEDLINE]

