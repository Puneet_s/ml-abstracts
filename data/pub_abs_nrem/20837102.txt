
1. Neurosci Lett. 2010 Nov 26;485(3):173-7. doi: 10.1016/j.neulet.2010.09.004. Epub 
2010 Sep 15.

Significant thalamocortical coherence of sleep spindle, theta, delta, and slow
oscillations in NREM sleep: recordings from the human thalamus.

Tsai YT(1), Chan HL, Lee ST, Tu PH, Chang BL, Wu T.

Author information: 
(1)Department of Neurology, Chang Gung Memorial Hospital and University, Taoyuan 
33305, Taiwan.

The electrophysiological studies of thalamocortical oscillations were mostly done
in animal models. Placement of stimulation electrodes at the anterior nucleus of 
the thalamus (ANT) for seizure reduction enables the study of the thalamocortical
interplay in human subjects. Nocturnal sleep electroencephalograms (EEGs) and
local field potentials (LFPs) of the left and right thalamus (LT, RT) were
recorded in three subjects receiving ANT stimulation. Sleep stages were scored
according to American Academy of Sleep Medicine criteria. The whole-night
time-frequency coherence maps between EEG (C3, C4) and LFP (LT, RT) showed
specific coherence patterns during non-rapid eye movement (NREM) sleep. Pooled
coherence in the NREM stage was significant in slow, delta, theta and spindle
frequency ranges. The spindle oscillations had the highest coherence (0.17-0.58) 
in the homolateral hemisphere. Together, these observations indicate that the
oscillations were related to thalamocortical circuitry.

Copyright © 2010 Elsevier Ireland Ltd. All rights reserved.

DOI: 10.1016/j.neulet.2010.09.004 
PMID: 20837102  [PubMed - indexed for MEDLINE]

