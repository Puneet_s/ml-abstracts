
1. Electroencephalogr Clin Neurophysiol. 1980 Oct;50(1-2):71-80.

The relationship of alpha and delta EEG frequencies to pain and mood in
'fibrositis' patients treated with chlorpromazine and L-tryptophan.

Moldofsky H, Lue FA.

Aspects of sleep stage evaluation and analysis of alpha and delta EEG frequencies
in sleep were shown to be related to musculo-skeletal pain and mood disturbance
in patients with 'fibrositis syndrome'. Patients were treated at bedtime for 3
weeks with either chlorpromazine, 100 mg (8 patients), or L-tryptophan, 5 g (7
patients). Chlorpromazine, but not L-tryptophan, was associated with increased
slow wave sleep and amelioration of pain and mood symptoms. Mean percent time/min
or mean percent power/min of alpha frequency during NREM and REM sleep corrlated 
with overnight increase in pain measures, hostility, and decrease in energy. On
the other hand, mean percent time/min of delta in NREM sleep was related to
overnight decrease in pain and mean percent delta power/min was associated with
decreased anxiety and hostility, and increased energy.


PMID: 6159193  [PubMed - indexed for MEDLINE]

