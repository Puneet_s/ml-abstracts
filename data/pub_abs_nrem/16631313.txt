
1. Neuroscience. 2006 Jul 21;140(4):1395-9. Epub 2006 Apr 21.

Homeostatic behavior of fast Fourier transform power in very low frequency
non-rapid eye movement human electroencephalogram.

Campbell IG(1), Higgins LM, Darchia N, Feinberg I.

Author information: 
(1)Department of Psychiatry and Behavioral Sciences, University of California
Davis, Davis, CA 95616, USA. igcampbell@ucdavis.edu

Basic research shows that the physiological and molecular mechanisms of very low 
frequency (<1 Hz) electroencephalogram (EEG) waves of non-rapid eye movement
(NREM) sleep differ from those of the higher (1-4 Hz) delta frequencies. Human
studies show that the across-NREM period dynamics of very low frequency and 1-4
Hz EEG also differ. These differences and the reported failure of very low
frequency EEG power to increase after a night of total sleep deprivation raise
the question of whether very low frequency EEG shows the other homeostatic
properties established for higher delta frequencies. Here we tested the relation 
of very low frequency EEG power density to prior waking duration across a normal 
day and whether these low frequencies meet another criterion for homeostatic
sleep EEG: conservation of power across a late nap and post-nap sleep. Data from 
19 young adults recorded in four separate sessions of baseline, daytime nap and
post-nap sleep were analyzed. Power density in very low frequency NREM EEG
increased linearly when naps were taken later in the day (i.e. were preceded by
longer waking durations). In the night following an 18:00 h nap, very low
frequency power was reduced by roughly the amount of power in the nap. Thus, very
low frequency EEG meets two major homeostatic criteria. We hypothesize that these
low frequencies reflect the executive rather than the functional processes by
which NREM sleep reverses the effects of waking brain activity.

DOI: 10.1016/j.neuroscience.2006.03.005 
PMID: 16631313  [PubMed - indexed for MEDLINE]

