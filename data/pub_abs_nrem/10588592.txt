
1. Am J Respir Crit Care Med. 1999 Dec;160(6):1824-9.

Mirtazapine, a mixed-profile serotonin agonist/antagonist, suppresses sleep apnea
in the rat.

Carley DW(1), Radulovacki M.

Author information: 
(1)Department of Medicine, University of Illinois at Chicago, Chicago, Illinois, 
USA.

Serotonin enhancing drugs, including L-tryptophan and, more recently, fluoxetine 
and paroxetine, have been tested as pharmacologic treatments for sleep apnea
syndrome. Although some patients have demonstrated reduced apnea expression after
treatment with these compounds, this improvement has been restricted to nonrapid 
eye movement (NREM) sleep, with some patients showing no improvement. This study 
reports the effects of mirtazapine, an antidepressant with 5-HT(1) agonist as
well as 5-HT(2) and 5-HT(3) antagonist effects, on sleep and respiration in an
established animal model of central apnea. We studied nine adult male
Sprague-Dawley rats chronically instrumented for sleep staging. In random order
on separate days, rats were recorded after intraperitoneal injection of: (1)
saline, (2) 0.1 mg/kg +/- mirtazapine (labeled as Remeron), (3) 1 mg/kg
mirtazapine, or (4) 5 mg/ kg mirtazapine. With respect to saline injections,
mirtazapine at all three doses reduced apnea index during NREM sleep by more than
50% (p < 0.0001) and during REM sleep by 60% (p < 0.0001) for at least 6 h. In
association with this apnea suppression normalized inspiratory minute ventilation
increased during all wake/sleep states (p < 0.001 for each state). The duration
of NREM sleep was unaffected by any dose of mirtazapine (p = 0.42), but NREM EEG 
delta power was increased by more than 30% at all doses (p = 0.04), indicating
improved NREM sleep consolidation after mirtazapine injection. We conclude that
mirtazapine, over a 50-fold dose range, significantly reduces central apnea
expression during NREM and REM sleep in the rat. The efficacy of this compound to
suppress apnea in all sleep stages most probably arises from its mixed
agonist/antagonist profile at serotonin receptors. The implications of these
findings for the management of sleep apnea syndrome must be verified by
appropriate clinical trials.

DOI: 10.1164/ajrccm.160.6.9902090 
PMID: 10588592  [PubMed - indexed for MEDLINE]

