
1. Hokkaido Igaku Zasshi. 1993 Sep;68(5):630-45.

[Changes in epileptiform activities during sleep and sleep structures in temporal
lobe epilepsy].

[Article in Japanese]

Kohsaka M(1).

Author information: 
(1)Department of Psychiatry and Neurology, Hokkaido University School of
Medicine, Sapporo, Japan.

We investigated the distribution of epileptiform activities (EA) and ictal
discharges, and sleep characteristics in 23 patients with temporal lobe epilepsy 
(TLE) using polysomnography (PSG) for two consecutive nights. The cassette EEG
system devised by our group was employed throughout the study to minimize th
untoward effects of overnight recording. The patients consisted of nine men and
fourteen women, aged from 17 to 60. Fourteen of them had been taking
antiepileptic drugs for long periods of time. The remaining 9 were the untreated 
patients and examined before and after the commencement of drug therapy. Sleep
characteristics of patients were compared to those of age and sex matched healty 
subjects. The following findings were obtained: 1) EA were found more often
during NREM sleep than REM sleep in both treated and untreated patients. The
frequency of EA increased in stage 3 + 4 compared to stage 1 or 2. 2) Changes of 
EA numbers during the consecutive sleep cycles were investigated. Twelve of 14
patients in the treated group showed curves which had decreasing tendency
throughout the night. 3) Ictal discharges were observed exclusively in the stage 
REM or within 10 minutes before and after the REM stage. These phenomena were
recognized in both treated and untreated groups. 4) Sleep characteristics of both
treated and untreated patients indicated significantly decreased sleep efficiency
and increase of % awakening. Moreover, % stage 4 significantly increased in
untreated patients compared to healthy subjects. These findings were discussed in
terms of the clinical characteristics with TLE and antiepileptic drugs on
epileptic activities during sleep as well as sleep structures.


PMID: 8225171  [PubMed - indexed for MEDLINE]

