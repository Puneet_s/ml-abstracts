
1. Adv Exp Med Biol. 2006;588:65-73.

Control of cerebral blood flow during sleep and the effects of hypoxia.

Corfield DR(1), Meadows GE.

Author information: 
(1)Institute of Science and Technology in Medicine, School of Life Sciences,
Keele University, Keele, UK. d.corfield@keele.ac.uk

During wakefulness, cerebral blood flow (CBF) is closely coupled to regional
cerebral metabolism; however CBF is also strongly modulated by breathing,
increasing in response to both hypercapnia and hypoxia. During stage III/IV
non-rapid eye (NREM) sleep, cerebral metabolism and CBF decrease whilst the
partial pressure of arterial CO2 increases due to a reduction in alveolar
ventilation. The reduction in CBF during NREM sleep therefore occurs despite a
relative state of hypercapnia. We have used transcranial Doppler ultrasound to
determine middle cerebral artery velocity, as an index of CBF, and have
determined that NREM sleep is associated with a reduction in the cerebrovascular 
response to hypercapnia. This reduction in reactivity would, at least in part,
allow the observed reductions in CBF in this state. Similarly, we have observed
that the CBF response to hypoxia is absent during stage III/IV NREM sleep.
Nocturnal hypoxia and hypercapnia are major pathogenic factor associated with
cardio-respiratory diseases. These marked changes in cerebrovascular control that
occur during sleep suggest that the cerebral circulation may be particularly
vulnerable to cardio-respiratory insults during this period.


PMID: 17089880  [PubMed - indexed for MEDLINE]

