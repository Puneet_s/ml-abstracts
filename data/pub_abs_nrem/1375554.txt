
1. Electroencephalogr Clin Neurophysiol. 1992 Jun;82(6):458-68.

Varying expressions of alerting mechanisms in wakefulness and across sleep
states.

Sanford LD(1), Morrison AR, Ball WA, Ross RJ, Mann GL.

Author information: 
(1)Laboratory of Anatomy, School of Veterinary Medicine, University of
Pennsylvania, Philadelphia 19104-6045.

Alerting stimuli, such as intense tones, presented to cats in wakefulness (W)
elicit the orienting response (OR) and/or the acoustic startle reflex (ASR) in
conjunction with elicited ponto-geniculo-occipital waves (PGOE) from the lateral 
geniculate body (LGB) and elicited waves from the thalamic central lateral
nucleus (CLE). Alerting stimuli presented during rapid eye movement sleep (REM)
and non-rapid eye movement sleep (NREM) also elicit PGOE. We presented tones in
W, REM and NREM to determine whether CLE could be obtained in sleep and to
examine the patterns of responsiveness of PGOE and CLE across behavioral states. 
Also, we recorded ASR and OR and compared the response patterns of behavioral and
central correlates of alerting. The subjects were 7 cats; all exhibited
spontaneously occurring waves in LGB and CL. All cats exhibited PGOE and 5 cats
exhibited CLE in W, REM and NREM. PGOE and CLE showed less evidence of
habituation than did ASR and OR. The pattern of responsiveness of CLE across
behavioral states was different from that found for PGOE, and spontaneous CL
waves were much rarer than the LGB waves. ASR was elicited in 5 cats during W
trials, and in 3 cats during REM trials. OR habituated rapidly in W and did not
occur in REM and NREM. The data indicate that central mechanisms of alerting
function in sleep states as well as in W and suggest that CLE and PGOE reflect
activity in mechanisms underlying cortical desynchronization and visual processes
which may act in concert during alerting.


PMID: 1375554  [PubMed - indexed for MEDLINE]

