
1. Eur Respir J. 1998 Feb;11(2):434-9.

Heart block in patients with obstructive sleep apnoea: pathogenetic factors and
effects of treatment.

Koehler U(1), Fus E, Grimm W, Pankow W, Schäfer H, Stammnitz A, Peter JH.

Author information: 
(1)Dept of Internal Medicine, Schlafmedizinisches Labor, Philipps-University
Marburg, Germany.

Heart block during sleep has been described in up to 10% of patients with
obstructive sleep apnoea. The aim of this study was to determine the relationship
between sleep stage, oxygen desaturation and apnoea-associated bradyarrhythmias
as well as the effect of nasal continuous positive airway pressure (nCPAP)/nasal 
bi-level positive airway pressure (nBiPAP) therapy on these arrhythmias in
patients without electrophysiological abnormalities. Sixteen patients (14 males
and two females, mean age 49.6+/-10.4 yrs) with sleep apnoea and nocturnal heart 
block underwent polysomnography after exclusion of electrophysiological
abnormalities of the sinus node function and atrioventricular (AV) conduction
system by invasive electrophysiological evaluation. During sleep, 651 episodes of
heart block were recorded, 572 (87.9%) occurred during rapid eye movement (REM)
sleep and 79 (12.1%) during nonrapid eye movement (NREM) sleep stages 1 and 2.
During REM sleep, the frequency of heart block was significantly higher than
during NREM sleep: 0.69+/-0.99 versus 0.02+/-0.04 episodes of heart block x
min(-1) of the respective sleep stage (p<0.001). During apnoeas or hypopnoeas,
609 bradyarrhythmias (93.5%) occurred with a desaturation of at least 4%. With
nCPAP/ nBiPAP therapy, apnoea/hypopnoea index (AHI) decreased from 75.5+/-39.6 x 
h(-1) to 3.0+/-6.6 x h(-1) (p<0.01) and the number of arrhythmias from 651 to 72 
(p<0.01). We conclude that: 1) 87.9% of apnoea-associated bradyarrhythmias occur 
during rapid eye movement sleep; 2) the vast majority of heart block episodes
occur during a desaturation of at least 4% without a previously described
threshold value of 72%; and 3) nasal continuous positive airway pressure or nasal
bi-level positive airway pressure is the therapy of choice in patients with
apnoea-associated bradyarrhythmias.


PMID: 9551750  [PubMed - indexed for MEDLINE]

