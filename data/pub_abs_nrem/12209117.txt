
1. Nat Rev Neurosci. 2002 Sep;3(9):679-93.

The cognitive neuroscience of sleep: neuronal systems, consciousness and
learning.

Hobson JA(1), Pace-Schott EF.

Author information: 
(1)Laboratory of Neurophysiology, Department of Psychiatry, Harvard Medical
School, Massachusetts Mental Health Center, 74 Fenwood Road, Boston,
Massachusetts 02115, USA.

Sleep can be addressed across the entire hierarchy of biological organization. We
discuss neuronal-network and regional forebrain activity during sleep, and its
consequences for consciousness and cognition. Complex interactions in
thalamocortical circuits maintain the electroencephalographic oscillations of
non-rapid eye movement (NREM) sleep. Functional neuroimaging affords views of the
human brain in both NREM and REM sleep, and has informed new concepts of the
neural basis of dreaming during REM sleep -- a state that is characterized by
illogic, hallucinosis and emotionality compared with waking. Replay of waking
neuronal activity during sleep in the rodent hippocampus and in functional images
of human brains indicates possible roles for sleep in neuroplasticity. Different 
forms and stages of learning and memory might benefit from different stages of
sleep and be subserved by different forebrain regions.

DOI: 10.1038/nrn915 
PMID: 12209117  [PubMed - indexed for MEDLINE]

