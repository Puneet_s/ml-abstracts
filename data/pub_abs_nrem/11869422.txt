
1. J Sleep Res. 2002 Mar;11(1):17-28.

Long-term vs. short-term processes regulating REM sleep.

Franken P(1).

Author information: 
(1)Institute of Pharmacology, University of Zürich, Zürich, Switzerland.
pfranken@stanford.edu

Comment in
    J Sleep Res. 2002 Mar;11(1):29-31; discussion 31-3.
    J Sleep Res. 2003 Sep;12(3):259-62.

In cats, rats, and mice, the amount of rapid eye movement sleep (REMS) lost
during a sleep deprivation (SD) predicts the subsequent REMS rebound during
recovery sleep. This suggests that REMS is homeostatically regulated and that a
need or pressure for REMS accumulates in its absence, i.e. during both
wakefulness and non-rapid eye movement sleep (NREMS). Conversely, it has been
proposed that REMS pressure accumulates exclusively during NREMS [Benington and
Heller, Am. J. Physiol. 266 (1994) R1992; Prog. Neurobiol. 44 (1994b) 433]. This 
hypothesis is based on the analysis of the duration of successive NREMS and REMS 
episodes and of electroencephalogram (EEG) events preceding REMS. Pre-REMS events
(PREs) do not always result in sustained REMS and can thus be regarded as REMS
attempts that increase as NREMS progresses. It is assumed that two processes
regulating REMS can resolve the apparent contradiction between these two
concepts: a 'long-term' process that homeostatically regulates the daily REMS
amount and a 'short-term' process that regulates the NREM--REMS cycle. These
issues were addressed in two SD experiments in rats. The two SDs varied in length
(12 and 24 h) and resulted in very similar compensatory changes in NREMS but
evoked very different changes for all REMS parameters studied. The large REMS
increase observed after 24-h SD was accompanied by a reduction in unsuccessful
PREs and an increase in sustained REMS episodes, together resulting in a
threefold increase in the success-rate to enter REMS. Changes in success-rate
matched those of a theoretically derived long-term REMS pressure. The SD induced 
changes in sleep architecture could be reproduced by assuming that the increased 
long-term REMS pressure interacts with the short-term process by increasing the
probability to enter and remain in REMS.


PMID: 11869422  [PubMed - indexed for MEDLINE]

