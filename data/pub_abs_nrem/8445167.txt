
1. J Child Neurol. 1993 Jan;8(1):19-26.

Abnormal behavioral patterns in the human fetus at term: correlation with lesion 
sites in the central nervous system after birth.

Koyanagi T(1), Horimoto N, Maeda H, Kukita J, Minami T, Ueda K, Nakano H.

Author information: 
(1)Maternity and Perinatal Care Unit, Kyushu University, Fukuoka, Japan.

We describe three fetuses at term that demonstrated abnormal behavioral patterns 
in utero when observed using real-time ultrasound. The abnormal patterns turned
out to have neurologic correlations after birth. In case 1, despite a normal
breathing pattern, no movement in any of the four extremities was observed, thus 
suggesting the existence of a spinal cord lesion at the rostral end, located at
C4 or C5. In case 2 a lack of breathing movement was noted repeatedly, together
with the abnormal finding that alternation of periods of rapid eye movement (REM)
sleep with those of non-rapid eye movement (NREM) sleep was not present. These
findings implied a lesion involving the pons and/or medulla oblongata. In case 3 
movement of the four extremities, breathing movement, and alternating periods of 
REM sleep and NREM sleep were found to be within the normal range. The
concurrence of regular mouthing and the NREM sleep period was not observed,
however, suggesting impairment in the brain function responsible for NREM sleep
located from the pons through the thalamocortical connection to the cerebral
hemisphere.


PMID: 8445167  [PubMed - indexed for MEDLINE]

