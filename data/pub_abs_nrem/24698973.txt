
1. Brain Stimul. 2014 Jul-Aug;7(4):508-15. doi: 10.1016/j.brs.2014.03.001. Epub 2014
Mar 12.

Transcranial slow oscillation stimulation during sleep enhances memory
consolidation in rats.

Binder S(1), Berg K(1), Gasca F(2), Lafon B(3), Parra LC(3), Born J(4), Marshall 
L(5).

Author information: 
(1)University of Lübeck, Department of Neuroendocrinology, Lübeck, Germany.
(2)University of Lübeck, Graduate School for Computing in Medicine and Life
Sciences, Lübeck, Germany; University of Lübeck, Institute for Robotics and
Cognitive Systems, Lübeck, Germany. (3)The City College of The City University of
New York, Department of Biomedical Engineering, New York, USA. (4)University of
Lübeck, Department of Neuroendocrinology, Lübeck, Germany; University of
Tübingen, Institute of Medical Psychology and Behavioral Neurobiology, Tübingen, 
Germany. (5)University of Lübeck, Department of Neuroendocrinology, Lübeck,
Germany; University of Lübeck, Graduate School for Computing in Medicine and Life
Sciences, Lübeck, Germany. Electronic address: marshall@uni-luebeck.de.

BACKGROUND: The importance of slow-wave sleep (SWS), hallmarked by the occurrence
of sleep slow oscillations (SO), for the consolidation of hippocampus-dependent
memories has been shown in numerous studies. Previously, the application of
transcranial direct current stimulation, oscillating at the frequency of
endogenous slow oscillations, during SWS enhanced memory consolidation for a
hippocampus dependent task in humans suggesting a causal role of slowly
oscillating electric fields for sleep dependent memory consolidation.
OBJECTIVE: Here, we aimed to replicate and extend these findings to a rodent
model.
METHODS: Slow oscillatory direct transcranial current stimulation (SO-tDCS) was
applied over the frontal cortex of rats during non-rapid eye movement (NREM)
sleep and its effects on memory consolidation in the one-trial object-place
recognition task were examined. A retention interval of 24 h was used to
investigate the effects of SO-tDCS on long-term memory.
RESULTS: Animals' preference for the displaced object was significantly greater
than chance only when animals received SO-tDCS. EEG spectral power indicated a
trend toward a transient enhancement of endogenous SO activity in the SO-tDCS
condition.
CONCLUSIONS: These results support the hypothesis that slowly oscillating
electric fields causal affect sleep dependent memory consolidation, and
demonstrate that oscillatory tDCS can be a valuable tool to investigate the
function of endogenous cortical network activity.

Copyright © 2014 Elsevier Inc. All rights reserved.

DOI: 10.1016/j.brs.2014.03.001 
PMID: 24698973  [PubMed - indexed for MEDLINE]

