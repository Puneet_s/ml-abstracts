
1. Can J Psychiatry. 2003 Jun;48(5):318-23.

A case-control study on psychological symptoms in sleep apnea-hypopnea syndrome.

Yue W(1), Hao W, Liu P, Liu T, Ni M, Guo Q.

Author information: 
(1)Mental Health Institute, Second Xiangya Hospital, Central South University,
86, Middle Renmin Road, 410011, Changsha, Hunan, People's Republic of China.
dryue@163.com

Comment in
    Can J Psychiatry. 2004 Aug;49(8):576; author reply 576.

OBJECTIVES: To investigate the psychological status of patients with sleep
apnea-hypopnea syndrome (SAHS) and to evaluate the association of SAHS with
psychological symptoms, using the Symptom Checklist-90 (SCL-90) scale.
METHODS: The study comprised 30 SAHS patients (25 men, 5 women) and 30 matched,
healthy control subjects. They all completed the SCL-90 and the Epworth Sleep
Scale (ESS) and underwent a whole-night polysomnographic (PSG) examination. We
used t-tests for group comparisons of nocturnal PSG characteristics, daytime
sleepiness, and psychological symptoms. We employed Spearman's rank correlation
analysis to indicate the effects of several nocturnal PSG variables (for example,
total sleep time, percentage of wake at sleep, Apnea and Hypopnea Index [AHI],
and oxygen desaturation) or subjective daytime sleepiness on psychological
symptoms in SAHS.
RESULTS: SAHS patients suffered from fragmented sleep and decreased arterial
oxygen saturations, compared with healthy control subjects. The General Severity 
Index (GSI) of SCL-90 was significantly higher in SAHS patients than in healthy
control subjects, as were measures of somatization, obsession-compulsion,
depression, anxiety, and hostility (P < 0.05). The severity of psychological
symptoms in SAHS patients was negatively related to total sleep time and
percentage of stage 2 nonrapid eye movement (NREM) sleep; it was positively
related to percentage of wake time after sleep onset, percentage of stage 1 NREM 
sleep, and ESS scores.
CONCLUSION: In our study population, SAHS patients had decreased psychological
well-being, which could be explained by fragmented sleep or excessive daytime
sleepiness.


PMID: 12866337  [PubMed - indexed for MEDLINE]

