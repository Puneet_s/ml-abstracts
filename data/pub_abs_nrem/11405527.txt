
1. Eur Respir J. 2001 Mar;17(3):467-73.

Proportional positive airway pressure: a new concept to treat obstructive sleep
apnoea.

Juhász J(1), Becker H, Cassel W, Rostig S, Peter JH.

Author information: 
(1)Klinik für Schalfstörungen Bayerisch Gmain, Germany.

Proportional positive airway pressure (PPAP) was designed to optimize airway
pressure for the therapy of obstructive sleep apnoea (OSA). In a randomized
crossover prospective study, the clinical feasibility of PPAP and its immediate
effects on the breathing disorder and sleep in comparison with continuous
positive airway pressure (CPAP) was evaluated. Twelve patients requiring CPAP
therapy underwent CPAP and PPAP titration in a random order. Obstructive and
mixed respiratory events could be completely abolished with both forms of
treatment. This efficacy could be achieved at a significantly lower mean mask
pressure during PPAP titration (8.45+/-2.42 cmH2O) compared to CPAP (9.96+/-2.7
cmH2O) (p=0.002). The mean minimal arterial oxygen saturation (Sa,O2)
(82.8+/-6.5%) on the diagnostic night increased significantly (p<0.001) to an
average Sa,O2 of 93.35+/-1.71% and 93.19+/-2.9% during CPAP and PPAP titration.
Total sleep time, slow wave sleep and rapid eye movement (REM) sleep increased
significantly by the same amount during both CPAP and PPAP titration (p<0.001),
while sleep stage nonrapid eye movement (NREM) 1 and 2 decreased. Six patients
preferred the PPAP titration night, four patients did not have a preference, and 
two patients preferred CPAP. The present data show that proportional positive
airway pressure is as effective as continuous positive airway pressure in
eliminating obstructive events and has the same immediate effect on sleep. The
lower average mask pressure during proportional positive airway pressure implies 
potential advantages compared to continuous positive airway pressure.
Proportional positive airway pressure presents a new effective therapeutic
approach to obstructive sleep apnoea.


PMID: 11405527  [PubMed - indexed for MEDLINE]

