
1. Br J Psychiatry Suppl. 1989 Oct;(6):61-5.

Effect of a reversible monoamine oxidase-A inhibitor (moclobemide) on sleep of
depressed patients.

Monti JM(1).

Author information: 
(1)Department of Pharmacology & Therapeutics, School of Medicine, Clinics,
Hospital, Montevideo, Uruguay.

The effect of moclobemide, a short-acting, reversible, preferential monoamine
oxidase-A inhibitor in a 4-week therapeutic trial, on the sleep of ten depressed 
patients, was assessed by polysomnographic recordings. Compared with their time
on placebo, patients receiving moclobemide showed improved sleep continuity,
particularly during the intermediate and late stages of drug administration. The 
total increase in sleep time was comprised of larger amounts of stage 2 non-rapid
eye movement (NREM) sleep and rapid eye movement (REM) sleep. Withdrawal of
moclobemide was followed by a further increase of REM sleep, although values did 
not surpass those sometimes observed in adults with normal sleep. In these
patients, the symptoms of depression were rated as being significantly improved
during the study period.


PMID: 2619974  [PubMed - indexed for MEDLINE]

