
1. Ann Neurol. 1980 Mar;7(3):277-80.

Absence of REM and altered NREM sleep in patients with spinocerebellar
degeneration and slow saccades.

Osorio I, Daroff RB.

The pontine tegmentum contains the neurons responsible for generation of saccadic
eye movements and certain phases of sleep. We studied two genetically unrelated
patients with spinocerebellar degeneration and slow saccadic eye movements.
Multiple all-night sleep studies in both patients disclosed absence of REM and
stage 4 sleep with an extremely short stage 3 and long stage 2. Both patients had
a sleep stage (X) not previously reported. These are the first awake and
ambulatory humans in whom consistent absence of REM sleep has been demonstrated. 
Both behaved appropriately during wakefulness and showed no overt psychological
abnormalities.

DOI: 10.1002/ana.410070312 
PMID: 7425560  [PubMed - indexed for MEDLINE]

