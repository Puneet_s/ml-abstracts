
1. Behav Brain Res. 1995 Jul-Aug;69(1-2):23-7.

Sleep for development or development for waking?--some speculations from a human 
perspective.

Salzarulo P(1), Fagioli I.

Author information: 
(1)Dipartimento di Psicologia generale, dei processi di sviluppo e
socializzazione, Università di Firenze, Italy.

The issue of the relationship between sleep and development could be posed in the
following terms: (1) does sleep have a function for development? and (2) which is
the specificity of sleep function during development? Is it possible to assess
critical ages of emergence and decline of specific sleep functions? The results
of recent investigations related to the so-called ontogenetic hypothesis for the 
function of rapid eye movement (REM) sleep will be reviewed; suggestions are put 
forward concerning the possible role of non-rapid eye movement (NREM) sleep.
Because of the difficulties to provoke long-lasting sleep deprivation in humans
during development, two different approaches were used. The results of one set of
analyses concerned the secretion of growth hormone during sleep under normal and 
pathological conditions and the relationship between sleep organization and
nutritional supply utilisation in infants and children. The second approach aimed
at investigating the long-term development of children suffering from sleep
abnormalities at earlier ages. Furthermore, the role of dreaming during
development will be discussed. The data summarized here only partly support the
function of sleep during development; we would like to underscore the difficulty 
to dissociate the function of sleep from that of waking.


PMID: 7546314  [PubMed - indexed for MEDLINE]

