
1. Epilepsia. 2006 Jan;47(1):82-5.

Effects of levetiracetam on nocturnal sleep and daytime vigilance in healthy
volunteers.

Cicolin A(1), Magliola U, Giordano A, Terreni A, Bucca C, Mutani R.

Author information: 
(1)Sleep Disorder Center, Department of Neurosciences, University of Turin,
Turin, Italy. alessandro.cicolin@unito.it

PURPOSE: Individuals with epilepsy commonly report daytime sleepiness, attributed
to sleep disruption (frequent arousals, awakenings, and stage shifts) induced by 
ictal and interictal activity or antiepileptic drugs (AEDs) or both. To study the
effect of levetiracetam (LEV) on sleep, at full doses but without the
interference of epilepsy, we investigated the sleep architecture and daytime
vigilance in healthy adults after 3 weeks of treatment.
METHODS: The study was of a double-blind crossover design with random allocation 
of multiple doses of two different treatments (randomly first LEV <or=2,000
mg/day or placebo for 3 weeks, washout for 4 weeks, and then the alternative
treatment for another 3 weeks). Fourteen healthy volunteers were studied with
polysomnography (PSG) and the Multiple Sleep Latency Test (MSLT). Epworth
Sleepiness Scale (ESS) and sleep log also were evaluated.
RESULTS: After treatment with LEV, statistically significant increases were
observed in total sleep time, sleep efficiency, and time spent in non-rapid eye
movement (NREM) sleep stages 2 and 4. Stage shifts and wake after sleep onset
were significantly decreased. Sleep latency was normal at PSG and MSLT in all
subjects and did not statistically differ between placebo and LEV. No changes
were found in the ESS.
CONCLUSIONS: Our findings show that in healthy volunteers, LEV consolidates sleep
and does not modify vigilance, two appreciated qualities in epilepsy patients
with sleep disturbance and daytime sleepiness.

DOI: 10.1111/j.1528-1167.2006.00376.x 
PMID: 16417535  [PubMed - indexed for MEDLINE]

