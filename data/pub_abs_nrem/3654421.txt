
1. J Appl Physiol (1985). 1987 Aug;63(2):609-22.

Maturation of spontaneous fetal diaphragmatic activity and fetal response to
hypercapnia and hypoxemia.

Ioffe S(1), Jansen AH, Chernick V.

Author information: 
(1)Department of Pediatrics, University of Manitoba, Winnipeg, Canada.

The electromyogram (EMG) of the diaphragm, lateral rectus, and nuchal and
hindlimb muscles were studied during spontaneous activity and during hypercapnia 
or hypoxemia in eight fetal sheep from 0.5 to 0.8 gestation (73-128 days). At the
earliest gestational age, diaphragmatic EMG activity was mainly tonic and
associated with tonic activity of somatic muscles. The stimulus for the
diaphragmatic activity originated centrally. Brief periods of a
rapid-eye-movement (REM) state characterized by phasic lateral rectus and
diaphragmatic activity and absence of nuchal activity were recognized.
Furthermore, from 0.5 to 0.7 gestation onward, activity of all muscles increased.
Thereafter increased specificity of activity in relation to the apparent REM and 
non-rapid-eye-movement (NREM) state occurred. With maturation, phasic
diaphragmatic activity increased at the expense of tonic activity. The most
striking effect of maturation on apnea was a greater proportion of apnea lasting 
greater than 1 min, but the total duration of apnea as a percent of a total
recording remained unchanged. The quantitative response to hypercapnia during
maturation was independent of the pattern of spontaneous diaphragmatic activity. 
Hypercapnia at 0.5 gestation changed the pattern of diaphragmatic EMG activity
from mainly tonic to phasic. Thus the central chemoreceptors and appropriate
neuronal pathways are present and functional as early as 0.5 gestation.
Hypercapnia at 0.5 gestation caused a shift in diaphragmatic EMG power to lower
frequencies similar to that found during control conditions in the older fetus.
This might suggest that during maturation there is increased recruitment of
phrenic motoneurons. Hypoxemia abolished tonic somatic activity at 0.5 gestation 
and decreased phasic diaphragmatic activity at more advanced gestational ages.
Therefore the central inhibitory mechanisms of hypoxemia are developed by 0.5
gestation.


PMID: 3654421  [PubMed - indexed for MEDLINE]

