
1. J Clin Sleep Med. 2016 Apr 15;12(4):513-7. doi: 10.5664/jcsm.5678.

Effect of Sleep State and Position on Obstructive Respiratory Events Distribution
in Adolescent Children.

El-Kersh K(1), Cavallazzi R(1), Patel PM(1), Senthilvel E(2).

Author information: 
(1)Department of Medicine, Division of Pulmonary, Critical Care and Sleep
Disorders Medicine, University of Louisville, Louisville, KY. (2)Department of
Pediatrics, Division of Sleep Medicine, University of Louisville, Louisville, KY.

STUDY OBJECTIVES: This study aimed to examine the effect of sleep state (rapid
eye movement [REM] versus non-rapid eye movement [NREM]) and position (supine
versus non-supine position) on obstructive respiratory events distribution in
adolescent population (ages 12 to 18 y).
METHODS: This was a retrospective study that included 150 subjects between the
ages of 12 to 18 y with an apnea-hypopnea index (AHI) > 1/h. Subjects using REM
sleep-suppressant medications and subjects with history of genetic anomalies or
craniofacial syndromes were excluded.
RESULTS: The median age was 14 y with interquartile range (IQR) of 13 to 16 y,
56% of patients were males and the median body mass index (BMI) z-score was 2.35 
(IQR: 1.71-2.59) with 77.3% of patients fulfilling obesity criteria. Respiratory 
obstructive events were more common in REM sleep. The median REM obstructive AHI 
(OAHI) was 8.9 events per hour (IQR: 2.74-22.8), whereas the median NREM OAHI was
3.2 events per hour (IQR: 1.44-8.29; p < 0.001). African American adolescents had
more REM obstructive events with median REM OAHI of 13.2 events per hour (IQR:
4.88-30.6), which was significantly higher than median REM OAHI of 4.94 (IQR:
2.05-11.36; p = 0.004) in white adolescents. Obstructive events were more common 
in supine position with higher median supine OAHI of 6.55 (IQR: 4-17.73) when
compared to median non-supine OAHI of 2.94 (IQR: 1-6.54; p < 0.001).
CONCLUSIONS: This study shows that sleep related obstructive respiratory events
in the adolescents (12 to 18 y of age) occur predominantly in REM sleep and in
supine position.

© 2016 American Academy of Sleep Medicine.

DOI: 10.5664/jcsm.5678 
PMCID: PMC4795277 [Available on 2016-10-15]
PMID: 26612510  [PubMed - in process]

