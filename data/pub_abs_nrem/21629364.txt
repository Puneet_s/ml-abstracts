
1. Sleep. 2011 Jun 1;34(6):759-71. doi: 10.5665/SLEEP.1044.

Effects of stressor predictability and controllability on sleep, temperature, and
fear behavior in mice.

Yang L(1), Wellman LL, Ambrozewicz MA, Sanford LD.

Author information: 
(1)Sleep Research Laboratory, Department of Pathology and Anatomy, Eastern
Virginia Medical School, Norfolk, VA 23501, USA.

STUDY OBJECTIVES: Predictability and controllability are important factors in the
persisting effects of stress. We trained mice with signaled, escapable shock
(SES) and with signaled, inescapable shock (SIS) to determine whether shock
predictability can be a significant factor in the effects of stress on sleep.
DESIGN: Male BALB/cJ mice were implanted with transmitters for recording EEG,
activity, and temperature via telemetry. After recovery from surgery, baseline
sleep recordings were obtained for 2 days. The mice were then randomly assigned
to SES (n = 9) and yoked SIS (n = 9) conditions. The mice were presented cues (90
dB, 2 kHz tones) that started 5.0 sec prior to and co-terminated with footshocks 
(0.5 mA; 5.0 sec maximum duration). SES mice always received shock but could
terminate it by moving to the non-occupied chamber in a shuttlebox. SIS mice
received identical tones and shocks, but could not alter shock duration. Twenty
cue-shock pairings (1.0-min interstimulus intervals) were presented on 2 days
(ST1 and ST2). Seven days after ST2, SES and SIS mice, in their home cages, were 
presented with cues identical to those presented during ST1 and ST2.
SETTING: NA.
PATIENTS OR PARTICIPANTS: NA.
INTERVENTIONS: NA.
MEASUREMENTS AND RESULTS: On each training and test day, EEG, activity and
temperature were recorded for 20 hours. Freezing was scored in response to the
cue alone. Compared to SIS mice, SES mice showed significantly increased REM
after ST1 and ST2. Compared to SES mice, SIS mice showed significantly increased 
NREM after ST1 and ST2. Both groups showed reduced REM in response to cue
presentation alone. Both groups showed similar stress-induced increases in
temperature and freezing in response to the cue alone.
CONCLUSIONS: These findings indicate that predictability (modeled by signaled
shock) can play a significant role in the effects of stress on sleep.

DOI: 10.5665/SLEEP.1044 
PMCID: PMC3099497
PMID: 21629364  [PubMed - indexed for MEDLINE]

