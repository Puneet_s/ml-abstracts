
1. Behav Brain Res. 1991 Nov 26;45(2):105-15.

Behavioral, sleep-waking and EEG power spectral effects following the two
specific 5-HT uptake inhibitors zimeldine and alaproclate in cats.

Sommerfelt L(1), Ursin R.

Author information: 
(1)Department of Physiology, University of Bergen, Norway.

Sleep, waking and EEG power spectra were studied in cats for 15 h following
peroral administration of placebo or 10 mg/kg and 20 mg/kg of the 5-HT reuptake
inhibitors zimeldine and alaproclate. Behavior was also observed during the
initial period following drug administration. Both drugs had effects on motor
behavior and initiated hallucinatory like behavior. Zimeldine increased latency
to stable sleep and to SWS-2. Alaproclate increased latency to SWS-1. Both drugs 
increased SWS (NREM sleep) and particularly SWS-2. REM sleep latency was
increased and REM sleep was reduced following both drugs. EEG slow wave activity 
was increased following zimeldine. It is concluded that the 5-HT stimulation
caused by the drugs yields complex effects on the sleep-waking axis, both sleep
incompatible and sleep promoting effects.


PMID: 1838688  [PubMed - indexed for MEDLINE]

