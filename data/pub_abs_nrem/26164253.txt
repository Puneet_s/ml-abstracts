
1. Conscious Cogn. 2015 Nov;36:196-205. doi: 10.1016/j.concog.2015.06.012. Epub 2015
Jul 9.

Daydreams and nap dreams: Content comparisons.

Carr M(1), Nielsen T(2).

Author information: 
(1)Dream & Nightmare Laboratory, Center for Advanced Research in Sleep Medicine, 
Hôpital du Sacré-Coeur de Montréal, Montréal, Canada; Dept. Biomedical Sciences, 
Université de Montréal, Canada. (2)Dream & Nightmare Laboratory, Center for
Advanced Research in Sleep Medicine, Hôpital du Sacré-Coeur de Montréal,
Montréal, Canada; Dept. Psychiatry, Université de Montréal, Canada. Electronic
address: tore.nielsen@umontreal.ca.

Differences between nighttime REM and NREM dreams are well-established but only
rarely are daytime REM and NREM nap dreams compared with each other or with
daydreams. Fifty-one participants took daytime naps (with REM or NREM awakenings)
and provided both waking daydream and nap dream reports. They also provided
ratings of their bizarreness, sensory experience, and emotion intensity. Recall
rates for REM (96%) and NREM (89%) naps were elevated compared to typical recall 
rates for nighttime dreams (80% and 43% respectively), suggesting an enhanced
circadian influence. All attribute ratings were higher for REM than for NREM
dreams, replicating findings for nighttime dreams. Compared with daydreams, NREM 
dreams had lower ratings for emotional intensity and sensory experience while REM
dreams had higher ratings for bizarreness and sensory experience. Results support
using daytime naps in dream research and suggest that there occurs selective
enhancement and inhibition of specific dream attributes by REM, NREM and waking
state mechanisms.

Copyright © 2015 Elsevier Inc. All rights reserved.

DOI: 10.1016/j.concog.2015.06.012 
PMID: 26164253  [PubMed - indexed for MEDLINE]

