
1. PLoS Comput Biol. 2016 Sep 1;12(9):e1005022. doi: 10.1371/journal.pcbi.1005022.
eCollection 2016.

A Thalamocortical Neural Mass Model of the EEG during NREM Sleep and Its Response
to Auditory Stimulation.

Schellenberger Costa M(1,)(2), Weigenand A(1,)(3), Ngo HV(2), Marshall L(3,)(4), 
Born J(2,)(5), Martinetz T(1,)(3), Claussen JC(1,)(6).

Author information: 
(1)Institute for Neuro- and Bioinformatics, University of Lübeck, Lübeck,
Germany. (2)Institute for Medical Psychology and Behavioral Neurobiology,
University of Tübingen, Tübingen, Germany. (3)Graduate School for Computing in
Medicine and Life Science, University of Lübeck, Lübeck, Germany. (4)Institute of
Experimental and Clinical Pharmacology and Toxicology, University of Lübeck,
Lübeck, Germany. (5)Center for Integrative Neuroscience, University of Tübingen, 
Tübingen, Germany. (6)Computational Systems Biology, Jacobs University Bremen,
Bremen, Germany.

Few models exist that accurately reproduce the complex rhythms of the
thalamocortical system that are apparent in measured scalp EEG and at the same
time, are suitable for large-scale simulations of brain activity. Here, we
present a neural mass model of the thalamocortical system during natural non-REM 
sleep, which is able to generate fast sleep spindles (12-15 Hz), slow
oscillations (<1 Hz) and K-complexes, as well as their distinct temporal
relations, and response to auditory stimuli. We show that with the inclusion of
detailed calcium currents, the thalamic neural mass model is able to generate
different firing modes, and validate the model with EEG-data from a recent sleep 
study in humans, where closed-loop auditory stimulation was applied. The model
output relates directly to the EEG, which makes it a useful basis to develop new 
stimulation protocols.

DOI: 10.1371/journal.pcbi.1005022 
PMID: 27584827  [PubMed - in process]

