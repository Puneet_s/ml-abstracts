
1. Sleep Med. 2006 Dec;7(8):634-40. Epub 2006 Nov 13.

The effects of dietary iron deprivation on murine circadian sleep architecture.

Dean T Jr(1), Allen RP, O'Donnell CP, Earley CJ.

Author information: 
(1)The University of Pennsylvania School of Medicine, Department of Neuroscience,
USA. terrydeanjr@gmail.com

OBJECTIVES: Iron deficiency is considered a putative cause for restless legs
syndrome (RLS), a human sensorimotor disorder characterized by a circadian
presentation of symptoms during the evening hours that disrupts one's ability to 
sleep. We sought to evaluate the sleep-wake effects of diet-induced iron
deficiency in mice as an animal model of RLS. To this end, we hypothesized that
the iron-deprived mice would exhibit a sleep-wake circadian pattern
characteristic of the human syndrome: increased wakefulness during the hours
immediately preceding the sleep-predominant period.
METHODS: Following weaning at post-natal day (PND) 21, C57BL/6J mice were
assigned to one of two dietary treatments: iron-deficient (ID, n=7) or
iron-adequate (i.e., control, CTL, n=6). At PND 44, the mice were surgically
instrumented for polysomnographic (PSG) recording, and data were collected at
young adulthood: PNDs 59 and 60. Sleep-wake architecture was characterized for
the 12-h light and dark periods and also for six consecutive 4-h blocks
comprising a 24-h day.
RESULTS: The ID mice showed marked increases in wake time in the 4-h period prior
to lights-on; both non-rapid eye movement (NREM) and rapid eye movement (REM)
sleep were reduced. In contrast, sleep-wake activity did not differ across the
12-h light period.
CONCLUSIONS: Dietary iron deficiency in mice elicited increases in wakefulness
during a particular circadian time point that corresponds to the period during
which RLS symptoms would maximally disturb sleep onset and progression in humans.
These data indicate that iron-deficient mice may provide a potentially useful
animal model for RLS.

DOI: 10.1016/j.sleep.2006.07.002 
PMID: 17098470  [PubMed - indexed for MEDLINE]

