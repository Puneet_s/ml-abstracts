
1. Front Hum Neurosci. 2015 Jun 5;9:328. doi: 10.3389/fnhum.2015.00328. eCollection 
2015.

Altered sleep composition after traumatic brain injury does not affect
declarative sleep-dependent memory consolidation.

Mantua J(1), Mahan KM(2), Henry OS(2), Spencer RM(3).

Author information: 
(1)Neuroscience and Behavior Program, University of Massachusetts Amherst, MA,
USA. (2)Commonwealth Honors College, University of Massachusetts Amherst, MA,
USA. (3)Neuroscience and Behavior Program, University of Massachusetts Amherst,
MA, USA ; Department of Psychological and Brain Sciences, University of
Massachusetts Amherst, MA, USA.

Individuals with a history of traumatic brain injury (TBI) often report sleep
disturbances, which may be caused by changes in sleep architecture or reduced
sleep quality (greater time awake after sleep onset, poorer sleep efficiency, and
sleep stage proportion alterations). Sleep is beneficial for memory formation,
and herein we examine whether altered sleep physiology following TBI has
deleterious effects on sleep-dependent declarative memory consolidation.
Participants learned a list of word pairs in the morning or evening, and recall
was assessed 12-h later, following an interval awake or with overnight sleep.
Young adult participants (18-22 years) were assigned to one of four experimental 
groups: TBI Sleep (n = 14), TBI Wake (n = 12), non-TBI Sleep (n = 15), non-TBI
Wake (n = 15). Each TBI participant was >1 year post-injury. Sleep physiology was
measured with polysomnography. Memory consolidation was assessed by comparing
change in word-pair recall over 12-h intersession intervals. The TBI group spent 
a significantly greater proportion of the night in SWS than the non-TBI group at 
the expense of NREM1. The TBI group also had marginally lower EEG delta power
during SWS in the central region. Intersession changes in recall were greater for
intervals with sleep than without sleep in both groups. However, despite abnormal
sleep stage proportions for individuals with a TBI history, there was no
difference in the intersession change in recall following sleep for the TBI and
non-TBI groups. In both Sleep groups combined, there was a positive correlation
between Intersession Change and the proportion of the night in NREM2 + SWS.
Overall, sleep composition is altered following TBI but such deficits do not
yield insufficiencies in sleep-dependent memory consolidation.

DOI: 10.3389/fnhum.2015.00328 
PMCID: PMC4456580
PMID: 26097451  [PubMed]

