
1. Annu Rev Pharmacol Toxicol. 2016;56:577-603. doi:
10.1146/annurev-pharmtox-010715-103801. Epub 2015 Nov 2.

Sleep Pharmacogenetics: Personalized Sleep-Wake Therapy.

Holst SC(1), Valomon A(1), Landolt HP(1).

Author information: 
(1)Institute of Pharmacology and Toxicology and Zürich Center for
Interdisciplinary Sleep Research, University of Zürich, CH-8057 Zürich,
Switzerland; email: landolt@pharma.uzh.ch.

Research spanning (genetically engineered) animal models, healthy volunteers, and
sleep-disordered patients has identified the neurotransmitters and
neuromodulators dopamine, serotonin, norepinephrine, histamine, hypocretin,
melatonin, glutamate, acetylcholine, γ-amino-butyric acid, and adenosine as
important players in the regulation and maintenance of sleep-wake-dependent
changes in neuronal activity and the sleep-wake continuum. Dysregulation of these
neurochemical systems leads to sleep-wake disorders. Most currently available
pharmacological treatments are symptomatic rather than causal, and their
beneficial and adverse effects are often variable and in part genetically
determined. To evaluate opportunities for evidence-based personalized medicine
with present and future sleep-wake therapeutics, we review here the impact of
known genetic variants affecting exposure of and sensitivity to drugs targeting
the neurochemistry of sleep-wake regulation and the pathophysiology of sleep-wake
disturbances. Many functional polymorphisms modify drug response phenotypes
relevant for sleep. To corroborate the importance of these and newly identified
variants for personalized sleep-wake therapy, human sleep pharmacogenetics should
be complemented with pharmacogenomic investigations, research about
sleep-wake-dependent pharmacological actions, and studies in mice lacking
specific genes. These strategies, together with future knowledge about epigenetic
mechanisms affecting sleep-wake physiology and treatment outcomes, may lead to
potent and safe novel therapies for the increasing number of sleep-disordered
patients (e.g., in aged populations).

DOI: 10.1146/annurev-pharmtox-010715-103801 
PMID: 26527070  [PubMed - in process]

