
1. Brain Res Bull. 2009 Mar 30;78(6):270-5. doi: 10.1016/j.brainresbull.2008.12.006.
Epub 2008 Dec 31.

Directional information flows between brain hemispheres across waking, non-REM
and REM sleep states: an EEG study.

Bertini M(1), Ferrara M, De Gennaro L, Curcio G, Moroni F, Babiloni C, Infarinato
F, Rossini PM, Vecchio F.

Author information: 
(1)Dipartimento di Psicologia Sapienza Università di Roma, Via dei Marsi 78,
00185 Roma, Italy. mario.bertini@uniroma1.it

The present electroencephalographic (EEG) study evaluated the hypothesis of a
preferred directionality of communication flows between brain hemispheres across 
24 h (i.e., during the whole daytime and nighttime), as an extension of a recent 
report showing changes in preferred directionality from pre-sleep wake to early
sleep stages. Scalp EEGs were recorded in 10 normal volunteers during daytime
wakefulness (eyes closed; first period: from 10:00 to 13:00 h; second period:
from 14:00 to 18:00 h; third period: from 19:00 to 22:00 h) and nighttime sleep
(four NREM-REM cycles). EEG rhythms of interest were delta (1-4 Hz), theta (5-7
Hz), alpha (8-11 Hz), sigma (12-15 Hz) and beta (16-28 Hz). The direction of the 
inter-hemispheric information flow was evaluated by computing the directed
transfer function (DTF) from these EEG rhythms. Inter-hemispheric directional
flows varied as a function of the state of consciousness (wake, NREM sleep, REM
sleep) and in relation to different cerebral areas. During the daytime, alpha and
beta rhythms conveyed inter-hemispheric signals with preferred Left-to-Right
hemisphere direction in parietal and central areas, respectively. During the NREM
sleep periods of nighttime, the direction of inter-hemispheric DTF information
flows conveyed by central beta rhythms was again preponderant from Left-to-Right 
hemisphere in the stage 2, independent of cortical areas. No preferred direction 
emerged across the REM periods. These results support the hypothesis that
specific directionality of communication flows between brain hemispheres is
associated with wakefulness, NREM (particularly stage 2) and REM states during
daytime and nighttime. They also reinforce the suggestive hypothesis of a
relationship between inter-hemispheric directionality of EEG functional coupling 
and frequency of the EEG rhythms.

DOI: 10.1016/j.brainresbull.2008.12.006 
PMID: 19121373  [PubMed - indexed for MEDLINE]

