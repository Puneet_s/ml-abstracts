
1. Epilepsia. 2014 Apr;55(4):584-91. doi: 10.1111/epi.12576. Epub 2014 Mar 20.

Spike wave location and density disturb sleep slow waves in patients with CSWS
(continuous spike waves during sleep).

Bölsterli Heinzle BK(1), Fattinger S, Kurth S, Lebourgeois MK, Ringli M, Bast T, 
Critelli H, Schmitt B, Huber R.

Author information: 
(1)Pediatric Sleep Disorders Center, University Children's Hospital Zurich,
Zurich, Switzerland; Division of Clinical Neurophysiology, University Children's 
Hospital Zurich, Zurich, Switzerland; Children's Research Center, University
Children's Hospital Zurich, Zurich, Switzerland.

OBJECTIVE: In CSWS (continuous spike waves during sleep) activation of spike
waves during slow wave sleep has been causally linked to neuropsychological
deficits, but the pathophysiologic mechanisms are still unknown. In healthy
subjects, the overnight decrease of the slope of slow waves in NREM (non-rapid
eye movement) sleep has been linked to brain recovery to regain optimal cognitive
performance. Here, we investigated whether the electrophysiologic hallmark of
CSWS, the spike waves during sleep, is related to an alteration in the overnight 
decrease of the slope, and if this alteration is linked to location and density
of spike waves.
METHODS: In a retrospective study, the slope of slow waves (0.5-2 Hz) in the
first hour and last hour of sleep (19 electroencephalography [EEG] electrodes) of
14 patients with CSWS (3.1-13.5 years) was calculated. The spike wave "focus" was
determined as the location of highest spike amplitude and the density of spike
waves as spike wave index (SWI).
RESULTS: There was no overnight change of the slope of slow waves in the "focus."
Instead, in "nonfocal" regions, the slope decreased significantly. This
difference in the overnight course resulted in a steeper slope in the "focus"
compared to "nonfocal" electrodes during the last hour of sleep. Spike wave
density was correlated with the impairment of the overnight slope decrease: The
higher the SWI, the more hampered the slope decrease.
SIGNIFICANCE: Location and density of spike waves are related to an alteration of
the physiologic overnight decrease of the slow wave slope. This overnight
decrease of the slope was shown to be closely related to the recovery function of
sleep. Such recovery is necessary for optimal cognitive performance during
wakefulness. Therefore we propose the impairment of this process by spike waves
as a potential mechanism leading to neuropsychological deficits in CSWS. A
PowerPoint slide summarizing this article is available for download in the
Supporting Information section here.

Wiley Periodicals, Inc. © 2014 International League Against Epilepsy.

DOI: 10.1111/epi.12576 
PMID: 24650120  [PubMed - indexed for MEDLINE]

