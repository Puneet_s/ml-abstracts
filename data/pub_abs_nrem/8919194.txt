
1. Arch Ital Biol. 1995 Dec;134(1):81-99.

Amygdaloid control of alerting and behavioral arousal in rats: involvement of
serotonergic mechanisms.

Sanford LD(1), Tejani-Butt SM, Ross RJ, Morrison AR.

Author information: 
(1)Laboratory for Study of the Brain in Sleep, School of Veterinary Medicine,
University of Pennsylvania, Philadelphia, USA.

The role of 5-HT mechanisms in the amygdala in the modulation of sleep and
arousal states and PGO waves was examined. Studies of the amygdala suggest that
it provides a neural mechanism by which emotionally-relevant or significant
stimuli may influence behavioral state and alerting mechanisms. The amygdala
projects massively (via the central nucleus) into brainstem regions involved in
alerting and in the generation of REM and PGO waves. Serotonergic innervation of 
the amygdala comes from DRN and to a lesser degree MRN. Microinjections of 5-HT
into the amygdala produced short-latency changes of state from NREM and REM with 
the effect being relatively greater in REM. Microinjections of the 5-HT
antagonist, methysergide, increased sleep efficiency and increased PGO wave
frequency in waking and NREM. These results demonstrate an important role for the
amygdala in the control of behavioral state and alerting mechanisms and suggest
that 5-HT exerts some of its regulatory effects via an influence on forebrain
regions.


PMID: 8919194  [PubMed - indexed for MEDLINE]

