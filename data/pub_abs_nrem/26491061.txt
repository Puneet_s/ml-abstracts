
1. J Pharmacol Exp Ther. 2016 Jan;356(1):64-73. doi: 10.1124/jpet.115.227819. Epub
2015 Oct 21.

Paeoniflorin Promotes Non-rapid Eye Movement Sleep via Adenosine A1 Receptors.

Chen CR(1), Sun Y(1), Luo YJ(1), Zhao X(1), Chen JF(1), Yanagawa Y(1), Qu WM(2), 
Huang ZL(2).

Author information: 
(1)Department of Pharmacology, State Key Laboratory of Medical Neurobiology,
Institutes of Brain Science and Collaborative Innovation Center for Brain
Science, Shanghai Medical College, Fudan University, Shanghai, People's Republic 
of China (C.-R.C., Y.S., Y.-J.L., W.-M.Q., Z.-L.H.); Department of Pharmacology, 
School of Medical Science, Ningbo University, Ningbo, Zhejiang, People's Republic
of China (X.Z.); Department of Neurology, School of Medicine, Boston University, 
Boston, Massachusetts (J.-F.C.); Department of Genetic and Behavioral
Neuroscience, Gunma University Graduate School of Medicine, Maebashi, Japan
(Y.Y.). (2)Department of Pharmacology, State Key Laboratory of Medical
Neurobiology, Institutes of Brain Science and Collaborative Innovation Center for
Brain Science, Shanghai Medical College, Fudan University, Shanghai, People's
Republic of China (C.-R.C., Y.S., Y.-J.L., W.-M.Q., Z.-L.H.); Department of
Pharmacology, School of Medical Science, Ningbo University, Ningbo, Zhejiang,
People's Republic of China (X.Z.); Department of Neurology, School of Medicine,
Boston University, Boston, Massachusetts (J.-F.C.); Department of Genetic and
Behavioral Neuroscience, Gunma University Graduate School of Medicine, Maebashi, 
Japan (Y.Y.) huangzl@fudan.edu.cn quweimin@fudan.edu.cn.

Paeoniflorin (PF, C23H28O11), one of the principal active ingredients of Paeonia 
Radix, exerts depressant effects on the central nervous system. We determined
whether PF could modulate sleep behaviors and the mechanisms involved.
Electroencephalogram and electromyogram recordings in mice showed that
intraperitoneal PF administered at a dose of 25 or 50 mg/kg significantly
shortened the sleep latency and increased the amount of non-rapid eye movement
(NREM). Immunohistochemical study revealed that PF decreased c-fos expression in 
the histaminergic tuberomammillary nucleus (TMN). The sleep-promoting effects and
changes in c-fos induced by PF were reversed by
8-cyclopentyl-1,3-dimethylxanthine (CPT), an adenosine A1 receptor antagonist,
and PF-induced sleep was not observed in adenosine A1 receptor knockout mice.
Whole-cell patch clamping in mouse brain slices showed that PF significantly
decreased the firing frequency of histaminergic neurons in TMN, which could be
completely blocked by CPT. These results indicate that PF increased NREM sleep by
inhibiting the histaminergic system via A1 receptors.

Copyright © 2015 by The American Society for Pharmacology and Experimental
Therapeutics.

DOI: 10.1124/jpet.115.227819 
PMID: 26491061  [PubMed - indexed for MEDLINE]

