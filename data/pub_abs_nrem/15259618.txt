
1. Clin EEG Neurosci. 2004 Jul;35(3):125-31.

Visual assessment of selected high amplitude frontopolar slow waves of sleep:
differences between healthy subjects and apnea patients.

Himanen SL(1), Joutsen A, Virkkala J.

Author information: 
(1)Department of Clinical Neurophysiology, Tampere University Hospital, Tampere, 
Finland. sari-leena.himanen@pshp.fi

Slow wave sequences with individually defined amplitude criterion were selected
manually from frontopolar sleep EEG of eight healthy control subjects and eight
patients with sleep apnea syndrome. Healthy subjects had clearly more time
occupied by slow wave sequences in all night sleep. Closer examination revealed
that the difference in the amount of slow wave sequence time between the groups
was statistically significant only in the first NREM sleep episode. In other
words, healthy subjects had more time with slow wave sequences in the first NREM 
sleep episode, where sleep pressure is supposed to be highest. The lower amount
of slow wave sequences in the apnea patients might reflect the fragmented sleep
of the patients, inhibiting cortical synchronization.


PMID: 15259618  [PubMed - indexed for MEDLINE]

