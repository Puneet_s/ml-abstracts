
1. Sleep. 2010 Dec;33(12):1681-6.

Utility of sleep stage transitions in assessing sleep continuity.

Laffan A(1), Caffo B, Swihart BJ, Punjabi NM.

Author information: 
(1)Department of Epidemiology, Johns Hopkins University, Baltimore, MD 21224,
USA.

STUDY OBJECTIVES: Sleep continuity is commonly assessed with polysomnographic
measures such as sleep efficiency, sleep stage percentages, and the arousal
index. The aim of this study was to examine whether the transition rate between
different sleep stages could be used as an index of sleep continuity to predict
self-reported sleep quality independent of other commonly used metrics.
DESIGN AND SETTING: Analysis of the Sleep Heart Health Study polysomnographic
data.
PARTICIPANTS: A community cohort.
MEASUREMENTS AND RESULTS: Sleep recordings on 5,684 participants were deemed to
be of sufficient quality to allow visual scoring of NREM and REM sleep. For each 
participant, we tabulated the frequency of transitions between wake, NREM sleep, 
and REM sleep. An overall transition rate was determined as the number of all
transitions per hour sleep. Stage-specific transition rates between wake, NREM
sleep, and REM sleep were also determined. A 5-point Likert scale was used to
assess the subjective experience of restless and light sleep the morning after
the sleep study. Multivariable regression models showed that a high overall sleep
stage transition rate was associated with restless and light sleep independent of
several covariates including total sleep time, percentages of sleep stages, wake 
time after sleep onset, and the arousal index. Compared to the lowest quartile of
the overall transition rate (<7.76 events/h), the odds ratios for restless sleep 
were 1.27, 1.42, and 1.38, for the second (7.77-10.10 events/h), third
(10.11-13.34 events/h), and fourth (≥13.35 events/h) quartiles, respectively.
Analysis of stage-specific transition rates showed that transitions between wake 
and NREM sleep were also independently associated with restless and light sleep.
CONCLUSIONS: Assessing overall and stage-specific transition rates provides a
complementary approach for assessing sleep continuity. Incorporating such
measures, along with conventional metrics, could yield useful insights into the
significance of sleep continuity for clinical outcomes.


PMCID: PMC2982738
PMID: 21120130  [PubMed - indexed for MEDLINE]

