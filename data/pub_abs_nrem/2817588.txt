
1. Am Rev Respir Dis. 1989 Nov;140(5):1274-8.

Obstructive sleep apnea with severe chronic airflow limitation. Comparison of
hypercapnic and eucapnic patients.

Chan CS(1), Grunstein RR, Bye PT, Woolcock AJ, Sullivan CE.

Author information: 
(1)Department of Thoracic Medicine, Royal Prince Alfred Hospital, Sydney, New
South Wales, Australia.

The mechanism of sustained awake hypercapnia in the obstructive sleep apnea
syndrome (OSA) is unknown. Recent work has implicated coexisting chronic airflow 
limitation (CAL) as an important contributing factor. We approached this question
by studying consecutive patients with both OSA syndrome and severe CAL in detail 
and comparing those with and without retention of CO2 while awake. Of 28 patients
with both severe OSA (mean NREM apnea index = 48 +/- 9, SEM) and severe CAL (mean
FEV1 = 1.07 +/- 0.07 L), 14 had persistent awake hypercapnia (mean PaCO2 = 50 +/-
1 mm Hg), and 14 were normocapnic (mean PaCO2 = 40 +/- 1 mm Hg). When separated
according to their PaCO2 level, there was no difference in the apnea indices in
both non-rapid-eye-movement (NREM) sleep, or rapid-eye-movement (REM) sleep,
although the hypercapnic group had lower average levels of oxyhemoglobin
saturation in both NREM (SaO2 = 77 +/- 2% versus 85 +/- 3%, p less than 0.05) and
REM (SaO2 = 60 +/- 4% versus 82 +/- 3%, p less than 0.001) sleep. The mean values
for FEV1, VC, lung volumes, and diffusing capacity for CO measured while awake
did not differ. The hypercapnic group had lower awake PaO2 levels (p less than
0.001), were heavier (p less than 0.05), had narrower upper airway size on CT
scan measurements (p less than 0.01), and gave a history of much heavier alcohol 
intake (p less than 0.05). Our results demonstrate that some patients with severe
OSA and severe CAL can maintain normal awake arterial CO2 levels.(ABSTRACT
TRUNCATED AT 250 WORDS)

DOI: 10.1164/ajrccm/140.5.1274 
PMID: 2817588  [PubMed - indexed for MEDLINE]

