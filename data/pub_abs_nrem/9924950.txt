
1. Technol Health Care. 1998 Nov;6(4):231-6.

Home polysomnography norms for children.

Stores G(1), Crawford C, Selman J, Wiggs L.

Author information: 
(1)University of Oxford Department of Psychiatry, Park Hospital for Children,
Headington, UK.

Home polysomnography (PSG) by means of ambulatory monitoring systems has distinct
advantages over sleep laboratory recordings, especially for children. However,
normative data have been lacking. Norms for conventional PSG variables were
compiled for 60 children age 5-16 years using the Oxford Medilog ambulatory
monitoring system. Recordings were confined to a single night in view of previous
demonstrations that the sleep on the first night is not significantly affected by
the recording procedure. The results are presented in 5 age subgroups. Broad
comparisons with published laboratory PSG norms for children of the same ages
suggest that in home recordings sleep duration is longer, and slow wave sleep is 
much more pronounced with a commensurate reduction in stage 2 NREM sleep. That
is, sleep quality and quantity appears better at home even when adaptation to the
laboratory situation has been promoted. These new normative data are considered
valuable for both clinical and research purposes where physiological sleep
studies in children are required.


PMID: 9924950  [PubMed - indexed for MEDLINE]

