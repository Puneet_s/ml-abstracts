
1. J Math Biol. 2010 May;60(5):615-44. doi: 10.1007/s00285-009-0276-5. Epub 2009 Jun
26.

A mathematical model of the sleep/wake cycle.

Rempe MJ(1), Best J, Terman D.

Author information: 
(1)Mathematical Biosciences Institute, Ohio State University, Columbus, OH 43210,
USA. mrempe@mbi.ohio-state.edu

We present a biologically-based mathematical model that accounts for several
features of the human sleep/wake cycle. These features include the timing of
sleep and wakefulness under normal and sleep-deprived conditions, ultradian
rhythms, more frequent switching between sleep and wakefulness due to the loss of
orexin and the circadian dependence of several sleep measures. The model
demonstrates how these features depend on interactions between a circadian
pacemaker and a sleep homeostat and provides a biological basis for the
two-process model for sleep regulation. The model is based on previous
"flip-flop" conceptual models for sleep/wake and REM/NREM and we explore whether 
the neuronal components in these flip-flop models, with the inclusion of a
sleep-homeostatic process and the circadian pacemaker, are sufficient to account 
for the features of the sleep/wake cycle listed above. The model is minimal in
the sense that, besides the sleep homeostat and constant cortical drives, the
model includes only those nuclei described in the flip-flop models. Each of the
cell groups is modeled by at most two differential equations for the evolution of
the total population activity, and the synaptic connections are consistent with
those described in the flip-flop models. A detailed analysis of the model leads
to an understanding of the mathematical mechanisms, as well as insights into the 
biological mechanisms, underlying sleep/wake dynamics.

DOI: 10.1007/s00285-009-0276-5 
PMID: 19557415  [PubMed - indexed for MEDLINE]

