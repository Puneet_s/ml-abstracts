
1. Pharmacol Ther. 2014 Mar;141(3):300-34. doi: 10.1016/j.pharmthera.2013.10.012.
Epub 2013 Nov 1.

Neuroscience-driven discovery and development of sleep therapeutics.

Dresler M(1), Spoormaker VI(1), Beitinger P(1), Czisch M(1), Kimura M(1), Steiger
A(1), Holsboer F(2).

Author information: 
(1)Max Planck Institute of Psychiatry, Munich, Germany. (2)Max Planck Institute
of Psychiatry, Munich, Germany. Electronic address: holsboer@mpipsykl.mpg.de.

Until recently, neuroscience has given sleep research and discovery of better
treatments of sleep disturbances little attention, despite the fact that
disturbed sleep has overwhelming impact on human health. Sleep is a complex
phenomenon in which specific psychological, electrophysiological, neurochemical, 
endocrinological, immunological and genetic factors are involved. The brain as
both the generator and main object of sleep is obviously of particular interest, 
which makes a neuroscience-driven view the most promising approach to evaluate
clinical implications and applications of sleep research. Polysomnography as the 
gold standard of sleep research, complemented by brain imaging, neuroendocrine
testing, genomics and other laboratory measures can help to create composite
biomarkers that allow maximizing the effects of individualized therapies while
minimizing adverse effects. Here we review the current state of the neuroscience 
of sleep, sleep disorders and sleep therapeutics and will give some leads to
promote the discovery and development of sleep medicines that are better than
those we have today.

Copyright © 2013 Elsevier Inc. All rights reserved.

DOI: 10.1016/j.pharmthera.2013.10.012 
PMID: 24189488  [PubMed - indexed for MEDLINE]

