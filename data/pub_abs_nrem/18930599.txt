
1. Psychoneuroendocrinology. 2009 Feb;34(2):212-9. doi:
10.1016/j.psyneuen.2008.09.002. Epub 2008 Oct 19.

Sleep architecture in Sheehan's syndrome before and 6 months after growth hormone
replacement therapy.

Ismailogullari S(1), Tanriverdi F, Kelestimur F, Aksu M.

Author information: 
(1)Erciyes University Medical Faculty, Neurology Department, 38039 Kayseri,
Turkey.

OBJECTIVE: To characterize the sleep parameters in patients with growth hormone
(GH) deficiency in Sheehan's syndrome adults and to assess the effects of 6-month
GH replacement therapy (GHRT).
METHODS: Twenty-two women with Sheehan's syndrome, (mean age; 49.1+/-2.2 years), 
and 12 women with similar age (mean age; 51.3+/-3.8 years) and body mass index as
control subjects were included in the study. Under baseline conditions, women
received adequate hormone replacement therapy for all hormonal deficiencies other
than GH. Twelve patients received recombinant GH (Genotropin; Pfizer Stockholm,
Sweden) (treatment group) and eight patients received placebo (placebo group) for
6 months. Two patients had only baseline evaluation and were not followed up
prospectively. Two polysomnography (PSG) recordings were performed on the
patients group, one in the baseline period and the other at the sixth month of
treatment (either GH or placebo). Control group had only baseline PSG.
RESULTS: GH deficient females with Sheehan's syndrome have more NREM (95.9+/-1.5%
and 88.6+/-0.9%, respectively; p<0.05), particularly in stage 4 sleep
(11.4+/-1.9% and 4.9+/-1.6, respectively; p<0.05), less REM sleep (4.2+/-1.5% and
11.4+/-0.9, respectively; p<0.05) and also less sleep efficiency (69.7+/-3.4% and
81.1+/-2.8%, respectively; p<0.05) when compared to healthy controls. After 6
months of GHRT there was no significant difference in sleep parameters.
CONCLUSION: GH deficiency has sleep disturbing effects on Sheehan's syndrome
patients under baseline conditions.

DOI: 10.1016/j.psyneuen.2008.09.002 
PMID: 18930599  [PubMed - indexed for MEDLINE]

