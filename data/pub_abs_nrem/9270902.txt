
1. Biol Psychiatry. 1997 Aug 15;42(4):260-6.

Effects of clozapine on sleep: a longitudinal study.

Hinze-Selch D(1), Mullington J, Orth A, Lauer CJ, Pollmächer T.

Author information: 
(1)Max Planck Institute of Psychiatry, Clinical Institute, Munich, Germany.

Polysomnographic studies on the effects of clozapine, an atypical antipsychotic
agent with strong sedative properties, on night sleep report inconsistent
results. Most of these studies did not include baseline recordings and were not
controlled for clozapine-induced fever, which is known to alter nocturnal sleep. 
We conducted a 2-week longitudinal polysomnographic investigation in 10 long-term
drug-free schizophrenic patients prior to and at the end of the first and second 
weeks of clozapine treatment. Rectal temperature was measured daily and patients 
with fever (> 37.9 degrees C) were excluded. Clozapine significantly improved
sleep continuity. In addition, non-rapid eye movement (NREM) sleep and in
particular stage 2 sleep increased significantly, while the amounts of stage 4
and slow-wave sleep decreased significantly. Clozapine increased significantly
REM density, but it did not affect the amount of REM sleep. We conclude that in
patients who do not experience clozapine-induced fever, clozapine has strong
sleep consolidating effects resulting from an increase in stage 2 NREM sleep.

DOI: 10.1016/S0006-3223(96)00347-2 
PMID: 9270902  [PubMed - indexed for MEDLINE]

