
1. Nat Neurosci. 2010 Jan;13(1):9-17. doi: 10.1038/nn.2445. Epub 2009 Dec 6.

The slow (<1 Hz) rhythm of non-REM sleep: a dialogue between three cardinal
oscillators.

Crunelli V(1), Hughes SW.

Author information: 
(1)School of Biosciences, Cardiff University, Cardiff, UK. crunelli@cardiff.ac.uk

The slow (<1 Hz) rhythm, the most important electroencephalogram (EEG) signature 
of non-rapid eye movement (NREM) sleep, is generally viewed as originating
exclusively from neocortical networks. Here we argue that the full manifestation 
of this fundamental sleep oscillation in a corticothalamic module requires the
dynamic interaction of three cardinal oscillators: one predominantly synaptically
based cortical oscillator and two intrinsic, conditional thalamic oscillators.
The functional implications of this hypothesis are discussed in relation to other
EEG features of NREM sleep, with respect to coordinating activities in local and 
distant neuronal assemblies and in the context of facilitating cellular and
network plasticity during slow-wave sleep.

DOI: 10.1038/nn.2445 
PMCID: PMC2980822
PMID: 19966841  [PubMed - indexed for MEDLINE]

