
1. Sleep. 1997 May;20(5):370-6.

Generalized anxiety and sleep architecture: a polysomnographic investigation.

Fuller KH(1), Waters WF, Binks PG, Anderson T.

Author information: 
(1)Department of Psychology, Louisiana State University, Baton Rouge, USA.

The sleep of 15 adult subjects who reported heightened generalized anxiety in the
absence of other psychiatric syndromes and a 15-adult contrast group were studied
by means of nocturnal polysomnography. Analysis of polysomnography variables
revealed a significant discriminant function that accounted for 79% of the
variance between groups, indicating that high-anxiety/worry subjects took longer 
to fall asleep, had a smaller percentage of deep (slow-wave) sleep, and more
frequent transitions into light sleep [stage 1 nonrapid eye movement (NREM)].
Additional analyses indicated that high-anxiety/worry subjects had a greater
percentage of light sleep, more early microarousals, a lower rapid eye movement
(REM) density relative to low-anxiety subjects. These subjects also showed more
electrodermal storming when slow-wave sleep and REM sleep variables were
covaried. Results indicated disrupted sleep depth and continuity similar to that 
documented in clinical anxiety disorder patients and distinct from that of
depressed patients. These results indicate that generalized anxiety and worry in 
otherwise healthy individuals may act to produce a clinically significant sleep
disturbance in the absence of other psychiatric symptoms.


PMID: 9381061  [PubMed - indexed for MEDLINE]

