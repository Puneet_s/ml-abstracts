
1. Hokkaido Igaku Zasshi. 1994 Sep;69(5):1162-77.

[Polysomnographic study in epilepsy with nocturnal seizures].

[Article in Japanese]

Ito M(1).

Author information: 
(1)Department of Psychiatry, Hokkaido University School of Medicine, Sapporo,
Japan.

Polysomnography was performed in 26 patients with nocturnal seizures to
investigate the relationship between sleep stage and epileptic discharges and the
sleep characteristics. We found three different patterns in interictal discharges
(IID): Pattern A is characterized by an increase frequency of IID during slow
wave sleep and a decrease of IID during REM (rapid eye movement) sleep, Pattern B
is characterized by an increase of IID during stages 1 and REM, and Pattern C
showed no definite correlation of IID with sleep stages. In almost all of the
patients with pattern A, IID were localized to the temporal regions, whereas IID 
were found in the frontal or central regions in patients with patterns B and C.
Further, patients with pattern A had seizures which usually occurred during the
first half of the night in contrast to patients with patterns B and C, whose
seizures usually occurred soon after falling asleep and/or early in the morning. 
Ictal discharges (ID) were observed in 18 patients. Again, there were three
patterns found, :In group a, ID occurred only in NREM (non-rapid eye movement)
sleep, in group b, ID occurred during NREM and REM sleep, and in group c, ID
clustered in shallow, NREM sleep. In patients in group a, ID occurred from the
temporal regions. In almost all of the patients in groups b and c, ID occurred
from the frontal regions. Compared to normal controls, the patients with and
without seizures exhibited a significant increase of stage W on polysomnography. 
These results suggest a relationship between the location of IID and ID and the
sleep stage of expression of these discharges. The results also indicate that
polygraphic sleep alterations are seen in epileptic patients.


PMID: 7868056  [PubMed - indexed for MEDLINE]

