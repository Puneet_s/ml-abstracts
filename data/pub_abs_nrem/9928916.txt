
1. Eur Neuropsychopharmacol. 1998 Dec;8(4):273-8.

Conventional and spectral power analysis of all-night sleep EEG after subchronic 
treatment with paroxetine in healthy male volunteers.

Schlösser R(1), Röschke J, Rossbach W, Benkert O.

Author information: 
(1)Department of Psychiatry, University of Mainz, Germany.
schloess@mail.uni-mainz.de

Paroxetine is a selective and potent serotonin reuptake inhibitor with reported
antidepressant properties. Since changes in the regular sleeping pattern were
described as side effects under treatment with paroxetine, the impact of the drug
on the sleep architecture is of major interest. The present study addressed the
question of subchronic effects of paroxetine medication (30 mg/day) in eight
healthy male volunteers in a double blind, placebo-controlled crossover-design.
Conventional sleep EEG parameters and additionally computed spectral power
analysis based on FFT of 20-s time epochs in the delta, theta, alpha, beta and
gamma frequency range for different sleep stages after 4 weeks of treatment were 
investigated. Subchronic paroxetine administration in healthy subjects led to a
prolonged REM latency and a decrease in the number of REM phases, whereas sleep
efficiency, total sleep time, sleep onset latency, number of awakenings, and
awake during sleep period time were not altered by paroxetine medication.
Moreover, we could not detect any alterations of the spectral power values in
certain frequency bands during NREM or REM sleep following subchronic paroxetine 
medication.


PMID: 9928916  [PubMed - indexed for MEDLINE]

