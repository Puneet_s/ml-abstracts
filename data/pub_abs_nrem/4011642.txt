
1. Pharmacol Biochem Behav. 1985 May;22(5):889-91.

p-Chloroamphetamine: effect on sleep and respiration in the rat.

DeMesquita S.

Rats, fitted with chronic EEG and EMG electrodes and a thoracic pneumograph, were
monitored electrophysiologically for three successive days before and after an IP
injection of p-chloroamphetamine (PCA) (2 mg/kg). During the 12 hours post PCA
treatment, sleep onset was delayed, the percentage of Rapid Eye Movement (REM)
sleep was decreased and the breathing rate during both the Non-REM (NREM) and REM
sleep states was reduced. By 24 and 48 hours after the PCA injection, the sleep
pattern and NREM respiratory rate had returned to control values; however,
respiratory rate during REM sleep still tended to be decreased. The results
suggest that PCA, at this dose, is capable of inducing insomnia and reducing REM 
sleep acutely without chronically altering the sleep pattern. The data also
suggest that respiratory rate during sleep may decrease following PCA treatment.


PMID: 4011642  [PubMed - indexed for MEDLINE]

