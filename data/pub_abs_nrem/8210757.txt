
1. Respir Physiol. 1993 Aug;93(2):175-87.

Effect of sleep on changes in breathing pattern accompanying sigh breaths.

Issa FG(1), Porostocky S.

Author information: 
(1)Faculty of Medicine, University of Calgary Health Science Centre, Alberta,
Canada.

We studied the effect of sleep on the characteristics of sigh breaths and the
associated changes in breathing pattern in breaths following spontaneous sighs in
4 unrestrained dogs with an intact upper airway. The sigh breath was
characterized by its large tidal volume (VT), long TI and TE in comparison with
the control breath. The volume of the sigh breath was larger in awake sighs than 
in those recorded during non-REM (NREM) and REM sleep. The strength of
Hering-Breuer reflex as determined by duration of the post-sigh apnea was similar
in NREM and REM sleep. Sighs occurring during wakefulness, NREM and REM sleep
were associated with augmented activity of the parasternal muscles during
inspiration, and a persistent tonic abdominal muscle activity during the
expiratory period. Breathing pattern in the post-sigh period was characterized by
a smaller VT and longer TE in the first post-sigh breath in all sleep states
(compared with the control breath), but the pattern returned to control level
within the second or third post-sigh breath in both NREM and REM sleep. Sighs did
not precipitate periodic breathing or other forms of abnormal breathing patterns 
in either wakefulness or sleep. We conclude that the respiratory control
mechanisms stabilizing breathing after a sigh in the awake dog are intact in NREM
and REM sleep.


PMID: 8210757  [PubMed - indexed for MEDLINE]

