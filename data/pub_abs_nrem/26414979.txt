
1. J Clin Sleep Med. 2016 Mar;12(3):327-32. doi: 10.5664/jcsm.5576.

Expiratory Time Constant and Sleep Apnea Severity in the Overlap Syndrome.

Wiriyaporn D(1), Wang L(2), Aboussouan LS(3).

Author information: 
(1)Division of Respiratory Disease, Department of Medicine, Bhumibol Adulyadej
Hospital Royal Thai Air Force, Thailand. (2)Department of Quantitative Health
Sciences, Cleveland Clinic, Cleveland, OH. (3)Sleep Disorders Center,
Neurological Institute, and Department of Pulmonary, Allergy, and Critical Care
Medicine, Respiratory Institute, Cleveland Clinic, Cleveland, OH.

STUDY OBJECTIVES: Lung mechanics in the overlap of COPD and sleep apnea impact
the severity of sleep apnea. Specifically, increased lung compliance with
hyperinflation protects against sleep apnea, whereas increased airway resistance 
worsens sleep apnea. We sought to assess whether the expiratory time constant,
which reflects lung mechanics, is associated with sleep apnea severity in such
patients.
METHODS: Polysomnographies in 34 subjects with the overlap syndrome were
reviewed. Three time constants were measured for each of up to 5 stages (wake,
NREM stages, and REM). The time constants were derived by fitting time and
pressure coordinates on the expiratory portion of a nasal pressure signal along
an exponentially decaying equation, and solving for the time constant.
Demographics, morphometrics, wake end-tidal CO2, right diaphragmatic arc on a
chest radiograph, and the apnea-hypopnea index (AHI) were recorded.
RESULTS: The time constant was not associated with age, gender, body mass index, 
right diaphragmatic arc, or wake end-tidal CO2, and was not significantly
different between sleep stages. A mean time constant (TC) was therefore obtained.
Subjects with a TC > 0.5 seconds had a greater AHI than those with a TC ≤ 0.5
seconds (median AHI 58 vs. 18, respectively, p = 0.003; Odds ratio of severe
sleep apnea 10.6, 95% CI 3.9-51.1, p = 0.005).
CONCLUSIONS: A larger time constant in the overlap syndrome is associated with
increased odds of severe sleep apnea, suggesting a greater importance of airway
resistance relative to lung compliance in sleep apnea causation in these
subjects.

© 2016 American Academy of Sleep Medicine.

DOI: 10.5664/jcsm.5576 
PMCID: PMC4773622
PMID: 26414979  [PubMed - in process]

