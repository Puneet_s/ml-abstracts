
1. Aust Fam Physician. 2002 Nov;31(11):991-4.

Parasomnias. Things that go thump in the night.

King P(1).

Author information: 
(1)Royal Prince Alfred and Westmead Hospitals, Hornsby Sleep Disorders Centre,
Sydney, New South Wales. pking9@bigpond.net.au

BACKGROUND: General practitioners are well versed with patients presenting to the
surgery with sleep symptoms, however, the approach to evaluating these symptoms
is often haphazard. Insomnia is the commonest presenting complaint. Sleep apnoea,
although readily treatable, carries with it significant morbidity and mortality.
OBJECTIVE: This article aims to highlight those disorders that occur infrequently
during the night but which interrupt sleep--the parasomnias.
DISCUSSION: Knowledge of the spectrum of parasomnias and their symptoms usually
allows a reasonably accurate clinical diagnosis and the assistance of a sleep
physician or sleep laboratory is not often required. Parasomnias may be
classified by the sleep phase during which they occur. Nonrapid eye movement
(NREM) parasomnias are most likely to occur during the first episode of stages 3 
and 4 of NREM sleep (slow wave sleep) which is approximately one hour after sleep
onset. Rapid eye movement (REM) sleep density is usually greatest in the last few
hours of sleep, therefore REM sleep parasomnias are most likely to occur during
this time.


PMID: 12471954  [PubMed - indexed for MEDLINE]

