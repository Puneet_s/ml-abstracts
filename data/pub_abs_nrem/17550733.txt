
1. Zhonghua Yi Xue Za Zhi. 2007 Mar 6;87(9):619-21.

[Changes of sleep architecture in patients with narcolepsy].

[Article in Chinese]

Li J(1), Xu Y, Dong XS, Han X, He ZM, Lü YH, Wang L, He QY, Han F.

Author information: 
(1)Department of Pulmonary Medicine, People's Hospital, Beijing University,
Beijing 100044, China.

OBJECTIVE: To investigate the sleep architectures of patients with narcolepsy.
METHODS: 38 drug-naive narcoleptic patients, 25 males and 13 females, aged 21 +/-
6.5, and 44 age-, sex ratio-, and BMI-matched normal persons underwent
polysomnography (PSG) and multiple sleep latency test (MSLT) during one night
sleep. Conventional visual scoring of the polysomnograms was performed according 
to the international.
RESULTS: The sleep latency of the patients was 5.6 min, however, 30 patients
(79%) complained of fragmented nocturnal sleep and difficulty to fall asleep
again. The sleep efficiency of the narcoleptics was 81.7% +/- 12.5%,
significantly lower than that of the normal persons (87.1% +/- 7.9%, P = 0.029). 
The non-rapid eye movement (NREM) I sleep accounted for (21.5 +/- 12.2)% in the
patients, a proportion significantly higher than that of the normal persons
[(10.3 +/- 6.3)%, P = 0.000]). The AHI of the patients was 0.6 +/- 1.3 times/h,
not significantly different from that of the normal persons (0.5 +/- 1.1
times/h). Although the rapid eye movement (REM) period and eye movement density
of the narcoleptics were significantly increased, their REM period duration was
not significantly different from that of the normal subjects (17.7% +/- 6.9% vs
18.9% +/- 5.5%, P = 0.23), probably due to the interruption of REM sleep by more 
frequent arousals in narcoleptics. PSG did not show significant periodic leg
movements in these 2 groups.
CONCLUSION: One of the important symptoms of narcolepsy, night sleep disturbance 
may contribute to the pathological sleepiness of narcolepsy during daytime.


PMID: 17550733  [PubMed - indexed for MEDLINE]

