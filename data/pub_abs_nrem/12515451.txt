
1. Clin Electroencephalogr. 2003 Jan;34(1):32-8.

Epileptic nystagmus and its possible relationship with PGO spikes.

Hughes JR(1), Fino JJ.

Author information: 
(1)Epilepsy Clinic, University of Illinois Medical Center, M/C 796, 912 S. Wood
St., Chicago, IL 96612, USA.

A simultaneous video-EEG on a waking 6-year-old male revealed rapid horizontal
and then vertical eye movements and 10 sec later showed ictal rhythms maximal on 
the occipital areas, quickly spreading to all other areas. A second ictal event
during wakefulness was very similar to the first. During sleep interictal
discharges were seen from the right frontal-temporal area and one more ictal
event was noted. This latter seizure in the NREM sleep record did not show any
eye movements, but showed ictal activity on the right frontal-temporal area,
which later became generalized. We propose that the interictal discharges on the 
right frontal-temporal area likely arose from the amygdala, which activated the
pontine nuclei responsible for PGO (ponto-geniculo-occipital) spikes and the
rapid eye movements seen in our patient. The PGO spikes activated the occipital
areas, which then showed clear ictal rhythms to complete the sequence of events. 
This case demonstrates a sequence of rapid eye movements without ictal patterns, 
followed by seizure rhythms, but may still be an example of "epileptic
nystagmus," assuming that the eye movements arose from an ictal activation of the
deep subcortical portion of the PGO system.


PMID: 12515451  [PubMed - indexed for MEDLINE]

