
1. Neuroimage. 2015 Dec;123:229-44. doi: 10.1016/j.neuroimage.2015.07.057. Epub 2015
Jul 26.

Heterogeneity of arousals in human sleep: A stereo-electroencephalographic study.

Peter-Derex L(1), Magnin M(2), Bastuji H(3).

Author information: 
(1)Lyon Neuroscience Research Center, Central Integration of Pain Lab, INSERM,
U1028, CNRS, UMR5292, Université Claude Bernard, Bron, F-69677, France; Hospices 
Civils de Lyon, Centre Hospitalier Lyon Sud, Service de Neurologie-Sommeil, 165
chemin du grand Revoyet, 69495 Pierre-Benite, France; Hospices civils de Lyon,
Hôpital Neurologique, Unité d'hypnologie, Service de Neurologie fonctionnelle et 
d'épileptologie, Groupement Hospitalier Est, 59 boulevard Pinel, 69677 Bron,
France. Electronic address: laure.peter-derex@chu-lyon.fr. (2)Lyon Neuroscience
Research Center, Central Integration of Pain Lab, INSERM, U1028, CNRS, UMR5292,
Université Claude Bernard, Bron, F-69677, France. (3)Lyon Neuroscience Research
Center, Central Integration of Pain Lab, INSERM, U1028, CNRS, UMR5292, Université
Claude Bernard, Bron, F-69677, France; Hospices civils de Lyon, Hôpital
Neurologique, Unité d'hypnologie, Service de Neurologie fonctionnelle et
d'épileptologie, Groupement Hospitalier Est, 59 boulevard Pinel, 69677 Bron,
France.

Wakefulness, non-rapid eye movement (NREM), and rapid eye movement (REM) sleep
are characterized by specific brain activities. However, recent experimental
findings as well as various clinical conditions (parasomnia, sleep inertia) have 
revealed the presence of transitional states. Brief intrusions of wakefulness
into sleep, namely, arousals, appear as relevant phenomena to characterize how
brain commutes from sleep to wakefulness. Using intra-cerebral recordings in 8
drug-resistant epileptic patients, we analyzed electroencephalographic (EEG)
activity during spontaneous or nociceptive-induced arousals in NREM and REM
sleep. Wavelet spectral analyses were performed to compare EEG signals during
arousals, sleep, and wakefulness, simultaneously in the thalamus, and primary,
associative, or high-order cortical areas. We observed that 1) thalamic activity 
during arousals is stereotyped and its spectral composition corresponds to a
state in-between wakefulness and sleep; 2) patterns of cortical activity during
arousals are heterogeneous, their manifold spectral composition being related to 
several factors such as sleep stages, cortical areas, arousal modality
("spontaneous" vs nociceptive-induced), and homeostasis; 3) spectral compositions
of EEG signals during arousal and wakefulness differ from each other. Thus,
stereotyped arousals at the thalamic level seem to be associated with different
patterns of cortical arousals due to various regulation factors. These results
suggest that the human cortex does not shift from sleep to wake in an abrupt
binary way. Arousals may be considered more as different states of the brain than
as "short awakenings." This phenomenon may reflect the mechanisms involved in the
negotiation between two main contradictory functional necessities, preserving the
continuity of sleep, and maintaining the possibility to react.

Copyright © 2015 Elsevier Inc. All rights reserved.

DOI: 10.1016/j.neuroimage.2015.07.057 
PMID: 26220744  [PubMed - indexed for MEDLINE]

