
1. J Sleep Res. 2000 Mar;9(1):27-34.

Age differences in the spontaneous termination of sleep.

Murphy PJ(1), Rogers NL, Campbell SS.

Author information: 
(1)Laboratory of Human Chronobiology, Department of Psychiatry, Weill Medical
College of Cornell University, White Plains, NY 10605, USA.
pjmurphy@med.cornell.edu

The stage from which the spontaneous ending of sleep occurred was investigated in
138 sleep episodes obtained from 14 younger (19-28 years) and 11 older (60-82
years) individuals. The possible influences of circadian phase and quality of the
preceding sleep period, as well as the impact of aging on characteristics of
sleep termination were examined. Under experimental conditions in which subjects 
were isolated from time cues, and behavioral options to sleep were limited, no
age-associated differences in the duration of sleep periods, or in the number or 
duration of REM episodes were observed. Despite similar percentages of NREM
(stages 2-4) and REM sleep across age groups, younger subjects awakened
preferentially from REM while older subjects did not. Of the sleep episodes
obtained from older subjects, those with sleep efficiencies higher than the
median were more likely to terminate from REM than those with lower sleep
efficiencies. For all subjects, the REM episodes from which sleep termination
occurred were truncated relative to those that did not end the sleep period. In
addition, nonterminating REM episodes that were interrupted by a stage shift were
most often interrupted by brief arousals to stage 0. Such arousals within
nonterminating REM episodes occurred, on average, after a similar duration as the
terminating point of sleep-ending REM episodes. The results from this study
demonstrate that there are age-related differences in the sleep stage from which 
spontaneous awakenings occur, and that these differences may be due in part to
the quality of the sleep period preceding termination. Findings regarding the
characteristics of both terminating and nonterminating REM episodes are
consistent with the notion that the neural and biochemical context of REM sleep
may facilitate a smooth transition to wakefulness. It is speculated that
age-associated changes in sleep continuity may render unnecessary the putative
role of REM sleep in providing a 'gate' to wakefulness.


PMID: 10733686  [PubMed - indexed for MEDLINE]

