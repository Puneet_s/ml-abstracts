
1. Front Psychol. 2015 Sep 7;6:1354. doi: 10.3389/fpsyg.2015.01354. eCollection
2015.

Labile sleep promotes awareness of abstract knowledge in a serial reaction time
task.

Kirov R(1), Kolev V(2), Verleger R(3), Yordanova J(2).

Author information: 
(1)Cognitive Psychophysiology, Institute of Neurobiology, Bulgarian Academy of
Sciences Sofia, Bulgaria. (2)Cognitive Psychophysiology, Institute of
Neurobiology, Bulgarian Academy of Sciences Sofia, Bulgaria ; Department of
Neurology, University of Lübeck Lübeck, Germany. (3)Department of Neurology,
University of Lübeck Lübeck, Germany ; Institute of Psychology II, University of 
Lübeck Lübeck, Germany.

Sleep has been identified as a critical brain state enhancing the probability of 
gaining insight into covert task regularities. Both non-rapid eye movement (NREM)
and rapid eye movement (REM) sleep have been implicated with oﬄine re-activation 
and reorganization of memories supporting explicit knowledge generation.
According to two-stage models of sleep function, oﬄine processing of information 
during sleep is sequential requiring multiple cycles of NREM and REM sleep
stages. However, the role of overnight dynamic sleep macrostructure for
insightfulness has not been studied so far. In the present study, we test the
hypothesis that the frequency of interactions between NREM and REM sleep stages
might be critical for awareness after sleep. For that aim, the rate of sleep
stage transitions was evaluated in 53 participants who learned implicitly a
serial reaction time task (SRTT) in which a determined sequence was inserted. The
amount of explicit knowledge about the sequence was established by verbal recall 
after a night of sleep following SRTT learning. Polysomnography was recorded in
this night and in a control night before and was analyzed to compare the rate of 
sleep-stage transitions between participants who did or did not gain awareness of
task regularity after sleep. Indeed, individual ability of explicit knowledge
generation was strongly associated with increased rate of transitions between
NREM and REM sleep stages and between light sleep stages and slow wave sleep.
However, the rate of NREM-REM transitions specifically predicted the amount of
explicit knowledge after sleep in a trait-dependent way. These results
demonstrate that enhanced lability of sleep goes along with individual ability of
knowledge awareness. Observations suggest that facilitated dynamic interactions
between sleep stages, particularly between NREM and REM sleep stages play a role 
for oﬄine processing which promotes rule extraction and awareness.

DOI: 10.3389/fpsyg.2015.01354 
PMCID: PMC4561346
PMID: 26441730  [PubMed]

