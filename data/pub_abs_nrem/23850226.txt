
1. Psychoneuroendocrinology. 2013 Nov;38(11):2618-27. doi:
10.1016/j.psyneuen.2013.06.005. Epub 2013 Jul 10.

Autonomic regulation across phases of the menstrual cycle and sleep stages in
women with premenstrual syndrome and healthy controls.

de Zambotti M(1), Nicholas CL, Colrain IM, Trinder JA, Baker FC.

Author information: 
(1)Human Sleep Research Program, SRI International, 333 Ravenswood Avenue, Menlo 
Park, CA 94025, USA.

To investigate the influence of menstrual cycle phase and the presence of severe 
premenstrual symptoms on cardiac autonomic control during sleep, we performed
heart rate variability (HRV) analysis during stable non-rapid eye movement (NREM)
and REM sleep in 12 women with severe premenstrual syndrome and 14 controls in
the mid-follicular, mid-luteal, and late-luteal phases of the menstrual cycle.
Heart rate was higher, along with lower high frequency (HF) power, reflecting
reduced vagal activity, and a higher ratio of low frequency (LF) to high
frequency power, reflecting a shift to sympathetic dominance, in REM sleep
compared with NREM sleep in both groups of women. Both groups of women had higher
heart rate during NREM and REM sleep in the luteal phase recordings compared with
the mid-follicular phase. HF power in REM sleep was lowest in the mid-luteal
phase, when progesterone was highest, in both groups of women. The mid-luteal
phase reduction in HF power was also evident in NREM sleep in control women but
not in women with PMS, suggesting some impact of premenstrual syndrome on
autonomic responses to the hormone environment of the mid-luteal phase. In
addition, mid-luteal phase progesterone levels correlated positively with HF
power and negatively with LF/HF ratio in control women in NREM sleep and with the
LF/HF ratio during REM sleep in both groups of women. Our findings suggest the
involvement of female reproductive steroids in cardiac autonomic control during
sleep in women with and without premenstrual syndrome.

Copyright © 2013 Elsevier Ltd. All rights reserved.

DOI: 10.1016/j.psyneuen.2013.06.005 
PMCID: PMC3812396
PMID: 23850226  [PubMed - indexed for MEDLINE]

