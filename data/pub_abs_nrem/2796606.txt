
1. Life Sci. 1989;45(15):1349-56.

Regional cerebral glucose metabolic rate in human sleep assessed by positron
emission tomography.

Buchsbaum MS(1), Gillin JC, Wu J, Hazlett E, Sicotte N, Dupont RM, Bunney WE Jr.

Author information: 
(1)Department of Psychiatry, UC Irvine 92717.

The cerebral metabolic rate of glucose was measured during nighttime sleep in 36 
normal volunteers using positron emission tomography and fluorine-18-labeled
2-deoxyglucose (FDG). In comparison to waking controls, subjects given FDG during
non-rapid eye movement (NREM) sleep (primarily stages 2 and 3) showed about a 23%
reduction in metabolic rate across the entire brain. This decrease was greater
for the frontal than temporal or occipital lobes, and greater for basal ganglia
and thalamus than cortex. Subjects in rapid eye movement (REM) sleep tended to
have higher cortical metabolic rates than waking subjects. The cingulate gyrus
was the only cortical structure to show a significant increase in glucose
metabolic rate in REM sleep in comparison to waking. The basal ganglia were
relatively more active on the right in REM sleep and symmetrical in NREM sleep.


PMID: 2796606  [PubMed - indexed for MEDLINE]

