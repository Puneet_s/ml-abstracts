
1. Neurophysiol Clin. 2015 May;45(2):167-75. doi: 10.1016/j.neucli.2014.10.004. Epub
2015 Apr 15.

Does more sleep matter? Differential effects of NREM- and REM-dominant sleep on
sleepiness and vigilance.

Neu D(1), Mairesse O(2), Newell J(3), Verbanck P(4), Peigneux P(5), Deliens G(6).

Author information: 
(1)Brugmann University Hospital, U.L.B./V.U.B, Sleep Laboratory & Unit for
Chronobiology U78, Arthur Van Gehuchten Square, Building Hh, 1020 Brussels,
Belgium; UNI, ULB Neurosciences Institute, Faculty of Medicine, Laboratory for
Medical Psychology ULB312, Université Libre de Bruxelles (U.L.B.), Brussels,
Belgium; Center for the Study of Sleep Disorders, Edith Cavell Medical Institute,
CHIREC, Brussels, Belgium. Electronic address: daniel.neu@chu-brugmann.be.
(2)Brugmann University Hospital, U.L.B./V.U.B, Sleep Laboratory & Unit for
Chronobiology U78, Arthur Van Gehuchten Square, Building Hh, 1020 Brussels,
Belgium; UNI, ULB Neurosciences Institute, Faculty of Medicine, Laboratory for
Medical Psychology ULB312, Université Libre de Bruxelles (U.L.B.), Brussels,
Belgium; Department of Experimental and Applied Psychology (EXTO), Vrije
Universiteit Brussel (V.U.B.), Brussels, Belgium; Royal Military Academy
(R.M.A.), Department LIFE, Brussels, Belgium. (3)Brugmann University Hospital,
U.L.B./V.U.B, Sleep Laboratory & Unit for Chronobiology U78, Arthur Van Gehuchten
Square, Building Hh, 1020 Brussels, Belgium. (4)Brugmann University Hospital,
U.L.B./V.U.B, Sleep Laboratory & Unit for Chronobiology U78, Arthur Van Gehuchten
Square, Building Hh, 1020 Brussels, Belgium; UNI, ULB Neurosciences Institute,
Faculty of Medicine, Laboratory for Medical Psychology ULB312, Université Libre
de Bruxelles (U.L.B.), Brussels, Belgium. (5)UR2NF, Neuropsychology and
Functional Neuroimaging Research Group at CRCN - Center for Research in Cognition
and Neurosciences, Université Libre de Bruxelles (ULB) and UNI - ULB
Neurosciences Institute, Brussels, Belgium. (6)Brugmann University Hospital,
U.L.B./V.U.B, Sleep Laboratory & Unit for Chronobiology U78, Arthur Van Gehuchten
Square, Building Hh, 1020 Brussels, Belgium; UR2NF, Neuropsychology and
Functional Neuroimaging Research Group at CRCN - Center for Research in Cognition
and Neurosciences, Université Libre de Bruxelles (ULB) and UNI - ULB
Neurosciences Institute, Brussels, Belgium.

We investigated effects of NREM and REM predominant sleep periods on sleepiness
and psychomotor performances measured with visual analog scales and the
psychomotor vigilance task, respectively. After one week of stable sleep-wake
rhythms, 18 healthy sleepers slept 3hours of early sleep and 3hours of late
sleep, under polysomnographic control, spaced by two hours of sustained
wakefulness between sleep periods in a within subjects split-night, sleep
interruption protocol. Power spectra analysis was applied for sleep EEG
recordings and sleep phase-relative power proportions were computed for six
different frequency bands (delta, theta, alpha, sigma, beta and gamma). Both
sleep periods presented with similar sleep duration and efficiency. As expected, 
phasic NREM and REM predominances were obtained for early and late sleep
conditions, respectively. Albeit revealing additive effects of total sleep
duration, our results showed a systematic discrepancy between psychomotor
performances and sleepiness levels. In addition, sleepiness remained stable
throughout sustained wakefulness during both conditions, whereas psychomotor
performances even decreased after the second sleep period. Disregarding exchanges
for frequency bands in NREM or stability in REM, correlations between outcome
measures and EEG power proportions further evidenced directional divergence with 
respect to sleepiness and psychomotor performances, respectively. Showing that
the functional correlation pattern changed with respect to early and late sleep
condition, the relationships between EEG power and subjective or behavioral
outcomes might however essentially be related to total sleep duration rather than
to the phasic predominance of REM or NREM sleep.

Copyright © 2015 Elsevier Masson SAS. All rights reserved.

DOI: 10.1016/j.neucli.2014.10.004 
PMID: 25890785  [PubMed - indexed for MEDLINE]

