
1. Behav Brain Res. 2015 Sep 15;291:72-9. doi: 10.1016/j.bbr.2015.05.015. Epub 2015 
May 18.

Heart rate variability during carbachol-induced REM sleep and cataplexy.

Torterolo P(1), Castro-Zaballa S(2), Cavelli M(2), Velasquez N(2), Brando V(3),
Falconi A(2), Chase MH(4), Migliaro ER(3).

Author information: 
(1)Laboratorio de Neurobiología del Sueño, Departamento de Fisiología, Facultad
de Medicina, Universidad de la República, Montevideo, Uruguay. Electronic
address: ptortero@fmed.edu.uy. (2)Laboratorio de Neurobiología del Sueño,
Departamento de Fisiología, Facultad de Medicina, Universidad de la República,
Montevideo, Uruguay. (3)Laboratorio de Fisiología Cardiovascular, Departamento de
Fisiología, Facultad de Medicina, Universidad de la República, Montevideo,
Uruguay. (4)WebSciences International and UCLA School of Medicine, Los Angeles,
CA 90095, USA.

The nucleus pontis oralis (NPO) exerts an executive control over REM sleep.
Cholinergic input to the NPO is critical for REM sleep generation. In the cat, a 
single microinjection of carbachol (a cholinergic agonist) into the NPO produces 
either REM sleep (REMc) or wakefulness with muscle atonia (cataplexy, CA). In
order to study the central control of the heart rate variability (HRV) during
sleep, we conducted polysomnographic and electrocardiogram recordings from
chronically prepared cats during REMc, CA as well as during sleep and
wakefulness. Subsequently, we performed statistical and spectral analyses of the 
HRV. The heart rate was greater during CA compared to REMc, NREM or REM sleep.
Spectral analysis revealed that the low frequency band (LF) power was
significantly higher during REM sleep in comparison to REMc and CA. Furthermore, 
we found that during CA there was a decrease in coupling between the RR intervals
plot (tachogram) and respiratory activity. In contrast, compared to natural
behavioral states, during REMc and CA there were no significant differences in
the HRV based upon the standard deviation of normal RR intervals (SDNN) and the
mean squared difference of successive intervals (rMSSD). In conclusion, there
were differences in the HRV during naturally-occurring REM sleep compared to
REMc. In addition, in spite of the same muscle atonia, the HRV was different
during REMc and CA. Therefore, the neuronal network that controls the HRV during 
REM sleep can be dissociated from the one that generates the muscle atonia during
this state.

Copyright © 2015 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.bbr.2015.05.015 
PMID: 25997581  [PubMed - indexed for MEDLINE]

