
1. Ann N Y Acad Sci. 2008;1129:330-4. doi: 10.1196/annals.1417.024.

Why does consciousness fade in early sleep?

Tononi G(1), Massimini M.

Author information: 
(1)Department of Psychiatry, University of Wisconsin, 6001 Research, Park Blvd., 
Madison, WI 53719, USA. gtononi@wisc.edu

Consciousness fades during deep nonrapid eye movement (NREM) sleep early in the
night, yet cortical neurons remain active, keep receiving sensory inputs, and can
display patterns of synchronous activity. Why then does consciousness fade?
According to the integrated information theory of consciousness, what is critical
for consciousness is not firing rates, sensory input, or synchronization per se, 
but rather the ability of a system to integrate information. If consciousness is 
the capacity to integrate information, then the brain should be able to generate 
consciousness to the extent that it has a large repertoire of available states
(information), yet it cannot be decomposed into a collection of causally
independent subsystems (integration). A key prediction stemming from this
hypothesis is that such ability should be greatly reduced in deep NREM sleep; the
dreamless brain either breaks down into causally independent modules, shrinks its
repertoire of possible responses, or both. In this article, we report the results
of a series of experiments in which we employed a combination of transcranial
magnetic stimulation and high-density electroencephalography (TMS/hd-EEG) to
directly test this prediction in humans. Altogether, TMS/hdEEG measurements
suggest that the sleeping brain, despite being active and reactive, loses its
ability of entering states that are both integrated and differentiated; it either
breaks down in causally independent modules, responding to TMS with a short and
local activation, or it bursts into an explosive and aspecific response,
producing a full-fledged slow wave.

DOI: 10.1196/annals.1417.024 
PMID: 18591492  [PubMed - indexed for MEDLINE]

