
1. J Neuroimmunol. 2003 Apr;137(1-2):59-66.

Interleukin-6 alters sleep of rats.

Hogan D(1), Morrow JD, Smith EM, Opp MR.

Author information: 
(1)Department of Psychiatry and Behavioral Sciences, University of Texas Medical 
Branch, Galveston, TX, USA.

Although it is well established that the cytokines tumor necrosis factor (TNF)
and interleukin (IL)-1 regulate sleep, there is no direct evidence implicating
IL-6 in the regulation/modulation of sleep. We tested the hypotheses that central
administration of rat recombinant IL-6 increases non-rapid eye movements (NREM)
sleep of rats, and that central administration of anti-IL-6 antibodies reduces
NREM sleep. Effective doses of IL-6 (100 and 500 ng) initially enhance NREM
sleep, after which NREM sleep may be suppressed. IL-6 induces febrile responses
at doses lower (50 ng) than those required to alter sleep. Rapid eye movements
(REM) sleep is not altered by the doses of IL-6 tested. Central administration of
monoclonal or polyclonal anti-rat IL-6 antibodies does not alter any of the
parameters determined in this study. Collectively, these results support the
hypothesis that IL-6 possesses sleep modulatory properties. However, this
cytokine may not be involved in the regulation of spontaneous sleep in healthy
animals because antagonizing the IL-6 system using antibodies does not alter
sleep. The interpretation of these data is consistent with those of previous
studies demonstrating correlations between increased IL-6 and excessive daytime
sleepiness during some pathophysiological conditions.


PMID: 12667648  [PubMed - indexed for MEDLINE]

