
1. PLoS One. 2013 Oct 14;8(10):e75823. doi: 10.1371/journal.pone.0075823.
eCollection 2013.

Essential roles of GABA transporter-1 in controlling rapid eye movement sleep and
in increased slow wave activity after sleep deprivation.

Xu XH(1), Qu WM, Bian MJ, Huang F, Fei J, Urade Y, Huang ZL.

Author information: 
(1)Department of Pharmacology, Shanghai Medical College, Fudan University,
Shanghai, China.

GABA is the major inhibitory neurotransmitter in the mammalian central nervous
system that has been strongly implicated in the regulation of sleep. GABA
transporter subtype 1 (GAT1) constructs high affinity reuptake sites for GABA and
regulates GABAergic transmission in the brain. However, the role of GAT1 in
sleep-wake regulation remains elusive. In the current study, we characterized the
spontaneous sleep-wake cycle and responses to sleep deprivation in GAT1 knock-out
(KO) mice. GAT1 KO mice exhibited dominant theta-activity and a remarkable
reduction of EEG power in low frequencies across all vigilance stages. Under
baseline conditions, spontaneous rapid eye movement (REM) sleep of KO mice was
elevated both during the light and dark periods, and non-REM (NREM) sleep was
reduced during the light period only. KO mice also showed more state transitions 
from NREM to REM sleep and from REM sleep to wakefulness, as well as more number 
of REM and NREM sleep bouts than WT mice. During the dark period, KO mice
exhibited more REM sleep bouts only. Six hours of sleep deprivation induced
rebound increases in NREM and REM sleep in both genotypes. However, slow wave
activity, the intensity component of NREM sleep was briefly elevated in WT mice
but remained completely unchanged in KO mice, compared with their respective
baselines. These results indicate that GAT1 plays a critical role in the
regulation of REM sleep and homeostasis of NREM sleep.

DOI: 10.1371/journal.pone.0075823 
PMCID: PMC3796508
PMID: 24155871  [PubMed - indexed for MEDLINE]

