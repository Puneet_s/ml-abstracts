
1. Rev Port Pneumol. 2009 Sep-Oct;15(5):847-57.

Clinical and polysomnographic characteristics of patients with REM sleep
disordered breathing.

[Article in English, Portuguese]

Loureiro CC(1), Drummond M, Winck JC, Almeida J.

Author information: 
(1)Pulmonology Unit, Hospitais da Universidade de Coimbra.
cl_loureiro@hotmail.com

There is a 10 -36% rate of obstructive sleep apnoea syndrome (OSAS) associated
with rapid eye movement (REM) in the OSAS population. Prior studies have
suggested an increased prevalence of psychiatric disorders and an effect of
gender and age on these patients. Our aim was to study the clinical and
polysomnograph (PSG) characteristics of our patients with REM- -related sleep
disordered breathing (REM SDB). Inclusion criteria was the identification of REM 
SDB detected by PSG defined as apnea -hypopnea index (AHI) in REM sleep > or =
5h, AHI in non -REM sleep (NREM) < or = 15h and REM/NREM AHI > or = 2. Several
Sleep Disorders Questionnaire (SDQ) version 1.02 parameters were analysed. The
study comprised 19 patients with a mean age of 54.0 (SD+/-13.97), a mean BMI of
29.01 (SD +/- 4.10) and a 0.58 female / male ratio. The mean Epworth Sleepiness
Scale score was 12.74 (SD +/-4.86). Mean AHI was 9.16/h (SD 4.09); mean AHI in
REM sleep 37.08/h (SD 25.87) and mean REM -AHI/NREM- -AHI 8.86 (SD 8.63). The
anxiety disorder rate was 33.3%; 44.4% in females, 16.7% in males. The average
deep sleep was 20.7% (SD 10.42) and REM sleep 15.45% (SD 9.96), with a sleep
efficiency of 85.3 (SD 8.70). No significant statistical correlation was found
between the REM/NREM AHI index and anxiety symptoms, daytime sleepiness and sleep
quality (REM and deep sleep percentages). These patients differ from the general 
OSAS population: on average, they are not obese, there are a greater number of
females affected and they do not present a very significant diurnal hypersomnia. 
Reduced deep sleep and increased REM sleep were also present versus general
population data, and sleep efficiency was just below the normal limit. Anxiety
disorders were more prevalent in this group than described for the general
population (3%) and OSAS patients.


PMID: 19649543  [PubMed - indexed for MEDLINE]

