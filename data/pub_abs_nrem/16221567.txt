
1. Clin Neurophysiol. 2005 Nov;116(11):2675-84. Epub 2005 Oct 10.

NREM sleep alterations in narcolepsy/cataplexy.

Ferri R(1), Miano S, Bruni O, Vankova J, Nevsimalova S, Vandi S, Montagna P,
Ferini-Strambi L, Plazzi G.

Author information: 
(1)Department of Neurology IC, Sleep Research Centre, Oasi Institute (IRCCS),
Troina, Enna, Italy. rferri@oasi.en.it

OBJECTIVE: NREM sleep patterns of narcoleptic patients with cataplexy were
studied, focusing on their sleep 'microstructure', by analyzing the cyclic
alternating pattern (CAP).
METHODS: Forty-nine HLA DQB1*0602-positive patients with narcolepsy/cataplexy (32
men and 17 women, aged 18-46 years) were included together with 37 age-matched
normal controls. Each subject underwent one polysomnographic night recording
after an adaptation night. Sleep stages were scored following standard criteria
and CAP A phases were detected and classified into 3 subtypes (A1, A2, and A3).
Power spectra for frequencies between 0.5 and 25 Hz were obtained for each CAP
condition, separately in sleep stage 2 and SWS.
RESULTS: Narcoleptic patients displayed reduced total CAP rate. A selective
reduction in the number of A1 subtypes/hour and a reduced A3 index were found in 
narcoleptics who had also a smaller average number of CAP sequences. Narcoleptic 
patients had higher power spectra for fast frequencies mostly during SWS, while
REM sleep power spectra showed significantly higher power density for frequency
bins 0.5-1.5, 8.5-9.5, and 17.5-25 Hz. Similarly, CAP A1 subtypes and NCAP epochs
during SWS displayed significantly higher power density for fast frequency bins.
CONCLUSIONS: The main finding of this study is that the occurrence of the A1 CAP 
subtypes is impaired during NREM sleep in narcoleptic patients. Thus, narcolepsy 
seems to be accompanied not only by alterations of REM but also NREM sleep which 
is subtly but significantly impaired, as reflected by CAP and the corresponding
EEG spectral analysis.
SIGNIFICANCE: Our findings might indicate that in narcolepsy very-slow
oscillation processes less effective than normal might be present, with a subtly 
impaired capability of grouping the other sleep EEG activities; this aspect
deserves further insight in order to obtain a better understanding of its
functional meaning.

DOI: 10.1016/j.clinph.2005.08.004 
PMID: 16221567  [PubMed - indexed for MEDLINE]

