
1. Rev Neurol. 1998 Nov;27(159):841-3.

[Sleep-arousal dissociation in a case of hypersomnia].

[Article in Spanish]

Rodríguez-Gómez J(1).

Author information: 
(1)Servicio de Neurofisiología Clínica, Hospital del Bierzo, Fuentesnuevas, León,
España.

INTRODUCTION: Dissociated sleep-arousal states are clinical and experimental
phenomena which represent mixed forms of the three stages of this cycle (arousal,
NREM sleep and REM sleep).
CLINICAL CASE: We describe the case of a man who presented with a history of
excessive diurnal somnolence for the previous 5 years. He also had symptoms of
sleep paralysis, hypnagogic hallucinations, automatic behavior and excessive
movements during sleep. He had had no episodes of loss of muscle tone; MR was
normal; HL DR2 and DQwl antigens were negative; two polisomnographic and a
Multiple Latency Sleep test showed: 1. Absence of respiratory disorders. 2.
Normal latency of REM sleep. 3. Periods of dissociated sleep (REM without atonia 
and arousal with atonia), and 4. An average sleep latency of 3.2 minutes and
absence of REM periods.
CONCLUSION: This case adds a new type of dissociated sleep state which may
accompany the disorder known as hypersomnia without REM periods.


PMID: 9859165  [PubMed - indexed for MEDLINE]

