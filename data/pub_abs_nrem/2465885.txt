
1. Electroencephalogr Clin Neurophysiol. 1989 Mar-Apr;74(2):105-11.

The effects of sleep on median nerve short latency somatosensory evoked
potentials.

Addy RO(1), Dinner DS, Lüders H, Lesser RP, Morris HH, Wyllie E.

Author information: 
(1)Emory University School of Medicine, Atlanta, GA 30335.

The effects of sleep on median nerve short latency somatosensory evoked
potentials were studied in 7 subjects made up of 6 patients being evaluated for
seizure disorders by all-night electroencephalograms and 1 normal healthy
volunteer. The median nerve was stimulated at the wrist, and the peripheral (N9),
subcortical (P13) and early cortical (N1, P2) evoked potentials were recorded
during full wakefulness and natural night-time sleep. Sleep-wake state was
monitored by the simultaneously obtained polysomnogram. The latencies of the
cortical responses were prolonged during non-rapid eye movement (NREM) sleep. In 
3 of the subjects P2 was consistently bifid during NREM sleep only. The second
component of the bifid potential, 3-4 msec longer in latency than the first,
appeared to be selectively enhanced during NREM sleep whereas the first component
tended to become less prominent or even disappear. This suggests that the 2 peaks
have different generators that are affected differently by NREM sleep. These are 
clinically relevant findings for interpretation of routine clinical studies.


PMID: 2465885  [PubMed - indexed for MEDLINE]

