
1. J Sleep Res. 1993 Sep;2(3):156-162.

Changes in EEG power density of NREM sleep in depressed patients during treatment
with citalopram.

Van Bemmel AL(1), Beersma DG, Van Den Hoofdakker RH.

Author information: 
(1)Department of Psychiatry, University of Limburg, The Netherlands.

According to a recent hypothesis the therapeutic effects of antidepressants might
be related to acute or cumulative suppression of NREM sleep intensity. This
intensity has been proposed to be expressed in the EEG power density in NREM
sleep. In the present study the relationship was examined between the changes of 
EEG power density in NREM sleep and the changes in clinical state in 16 depressed
patients during treatment with citalopram, a highly specific serotonin uptake
inhibitor. A one-week wash-out period was followed by 1 week of placebo
administration, a medication period of 5 weeks, and a one-week placebo period. In
order to minimize systematic influences of sleep duration and NREM-REM sleep
alterations, EEG power was measured over the longest common amount of NREM sleep 
stages 2, 3 and 4 (91.5 min). During the last treatment week and the week after
withdrawal, a significant decrease of EEG power as compared to baseline was found
in the 8-9 Hz frequency range. No clear-cut change, however, was observed in the 
EEG power of the delta frequency range (1-4 Hz), which is considered to be the
principle manifestation of NREMS intensity. Furthermore, no relationship between 
changes in EEG power density and changes in clinical state could be demonstrated.


PMID: 10607088  [PubMed - as supplied by publisher]

