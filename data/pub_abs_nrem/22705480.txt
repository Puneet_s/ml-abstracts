
1. Biol Psychol. 2012 Sep;91(1):150-5. doi: 10.1016/j.biopsycho.2012.05.012. Epub
2012 Jun 13.

Consolidation of temporal order in episodic memories.

Griessenberger H(1), Hoedlmoser K, Heib DP, Lechinger J, Klimesch W, Schabus M.

Author information: 
(1)Laboratory for Sleep and Consciousness Research, University of Salzburg,
Department of Psychology, Division of Physiological Psychology,
Hellbrunnerstrasse 34, 5020 Salzburg, Austria.

Even though it is known that sleep benefits declarative memory consolidation, the
role of sleep in the storage of temporal sequences has rarely been examined. Thus
we explored the influence of sleep on temporal order in an episodic memory task
followed by sleep or sleep deprivation. Thirty-four healthy subjects (17 men)
aged between 19 and 28 years participated in the randomized, counterbalanced,
between-subject design. Parameters of interests were NREM/REM cycles, spindle
activity and spindle-related EEG power spectra. Participants of both groups
(sleep group/sleep deprivation group) performed retrieval in the evening, morning
and three days after the learning night. Results revealed that performance in
temporal order memory significantly deteriorated over three days only in sleep
deprived participants. Furthermore our data showed a positive relationship
between the ratios of the (i) first NREM/REM cycle with more REM being associated
with delayed temporal order recall. Most interestingly, data additionally
indicated that (ii) memory enhancers in the sleep group show more fast spindle
related alpha power at frontal electrode sites possibly indicating access to a
yet to be consolidated memory trace. We suggest that distinct sleep mechanisms
subserve different aspects of episodic memory and are jointly involved in
sleep-dependent memory consolidation.

Copyright © 2012 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.biopsycho.2012.05.012 
PMCID: PMC3427018
PMID: 22705480  [PubMed - indexed for MEDLINE]

