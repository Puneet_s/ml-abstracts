
1. Electroencephalogr Clin Neurophysiol. 1994 Apr;90(4):291-7.

Extended sleep in humans in 14 hour nights (LD 10:14): relationship between REM
density and spontaneous awakening.

Barbato G(1), Barker C, Bender C, Giesen HA, Wehr TA.

Author information: 
(1)Clinica Psichiatrica, II Facoltà di Medicina, Università di Napoli, Italy.

The sleep patterns of 8 normal subjects living in a winter-type photoperiod (10 h
light and 14 h darkness; LD 10:14) for 4 weeks were characterized by the presence
of periods of spontaneous wakefulness alternating with periods of spontaneous
sleep. Transitions from sleep to wakefulness occurred much more frequently out of
REM sleep than out of NREM sleep (P < 0.002). REM periods that terminated in
wakefulness showed shorter REM durations (P < 0.0005) and higher REM densities (P
< 0.0005) than REM periods that did not terminate in wakefulness. The authors
discuss these results in terms of a possible relationship between REM density and
arousal level. The higher REM density preceding wakefulness and the increased
number of REM periods terminating in spontaneous awakenings could reflect an
enhanced level of a brain arousing process, resulting from reduced sleep pressure
in the extended nights.


PMID: 7512910  [PubMed - indexed for MEDLINE]

