
1. J Clin Sleep Med. 2014 Jan 15;10(1):65-72. doi: 10.5664/jcsm.3362.

Sleep disordered breathing in chronic spinal cord injury.

Sankari A(1), Bascom A(1), Oomman S(1), Badr MS(1).

Author information: 
(1)Sleep Research Laboratory, John D. Dingell Veterans Affairs Medical Center,
Wayne State University, Detroit, MI.

STUDY OBJECTIVES: Spinal cord injury (SCI) is associated with 2-5 times greater
prevalence of sleep disordered breathing (SDB) than the general population. The
contribution of SCI on sleep and breathing at different levels of injury using
two scoring methods has not been assessed. The objectives of this study were to
characterize the sleep disturbances in the SCI population and the associated
physiological abnormalities using quantitative polysomnography and to determine
the contribution of SCI level on the SDB mechanism.
METHODS: We studied 26 consecutive patients with SCI (8 females; age 42.5 ± 15.5 
years; BMI 25.9 ± 4.9 kg/m2; 15 cervical and 11 thoracic levels) by spirometry, a
battery of questionnaires and by attended polysomnography with flow and
pharyngeal pressure measurements. Inclusion criteria for SCI: chronic SCI (> 6
months post injury), level T6 and above and not on mechanical ventilation.
Ventilation, end-tidal CO2 (PETCO2), variability in minute ventilation (VI-CV)
and upper airway resistance (RUA) were monitored during wakefulness and NREM
sleep in all subjects. Each subject completed brief history and exam, Epworth
Sleepiness Scale (ESS), Pittsburgh Sleep Quality Index (PSQI), Berlin
questionnaire (BQ) and fatigue severity scale (FSS). Sleep studies were scored
twice, first using standard 2007 American Academy of Sleep Medicine (AASM)
criteria and second using new 2012 recommended AASM criteria.
RESULTS: Mean PSQI was increased to 10.3 ± 3.7 in SCI patients and 92% had poor
sleep quality. Mean ESS was increased 10.4 ± 4.4 in SCI patients and excessive
daytime sleepiness (ESS ≥ 10) was present in 59% of the patients. Daytime fatigue
(FSS > 20) was reported in 96% of SCI, while only 46% had high-risk score of SDB 
on BQ. Forced vital capacity (FVC) in SCI was reduced to 70.5% predicted in
supine compared to 78.5% predicted in upright positions (p < 0.05). Likewise
forced expiratory volume in first second (FEV1) was 64.9% predicted in supine
compared to 74.7% predicted in upright positions (p < 0.05). Mean AHI in SCI
patients was 29.3 ± 25.0 vs. 20.0 ± 22.8 events/h using the new and conventional 
AASM scoring criteria, respectively (p < 0.001). SCI patients had SDB (AHI > 5
events/h) in 77% of the cases using the new AASM scoring criteria compared to 65%
using standard conventional criteria (p < 0.05). In cervical SCI, VI decreased
from 7.2 ± 1.6 to 5.5 ± 1.3 L/min, whereas PETCO2 and VI-CV, increased during
sleep compared to thoracic SCI.
CONCLUSION: The majority of SCI survivors have symptomatic SDB and poor sleep
that may be missed if not carefully assessed. Decreased VI and increased PETCO2
during sleep in patients with cervical SCI relative to thoracic SCI suggests that
sleep related hypoventilation may contribute to the pathogenesis SDB in patients 
with chronic cervical SCI.

DOI: 10.5664/jcsm.3362 
PMCID: PMC3869071
PMID: 24426822  [PubMed - indexed for MEDLINE]

