
1. Epilepsia. 2014 Dec;55(12):1986-95. doi: 10.1111/epi.12851. Epub 2014 Dec 3.

The spatial and signal characteristics of physiologic high frequency
oscillations.

Alkawadri R(1), Gaspard N, Goncharova II, Spencer DD, Gerrard JL, Zaveri H,
Duckrow RB, Blumenfeld H, Hirsch LJ.

Author information: 
(1)Department of Neurology, Yale Comprehensive Epilepsy Center, New Haven,
Connecticut, U.S.A.

OBJECTIVES: To study the incidence, spatial distribution, and signal
characteristics of high frequency oscillations (HFOs) outside the epileptic
network.
METHODS: We included patients who underwent invasive evaluations at Yale
Comprehensive Epilepsy Center from 2012 to 2013, had all major lobes sampled, and
had localizable seizure onsets. Segments of non-rapid eye movement (NREM) sleep
prior to the first seizure were analyzed. We implemented a semiautomated process 
to analyze oscillations with peak frequencies >80 Hz (ripples 80-250 Hz; fast
ripples 250-500 Hz). A contact location was considered epileptic if it exhibited 
epileptiform discharges during the intracranial evaluation or was involved
ictally within 5 s of seizure onset; otherwise it was considered nonepileptic.
RESULTS: We analyzed recordings from 1,209 electrode contacts in seven patients. 
The nonepileptic contacts constituted 79.1% of the total number of contacts.
Ripples constituted 99% of total detections. Eighty-two percent of all HFOs were 
seen in 45.2% of the nonepileptic contacts (82.1%, 47%, 34.6%, and 34% of the
occipital, parietal, frontal, and temporal nonepileptic contacts, respectively). 
The following sublobes exhibited physiologic HFOs in all patients: Perirolandic, 
basal temporal, and occipital subregions. The ripples from nonepileptic sites had
longer duration, higher amplitude, and lower peak frequency than ripples from
epileptic sites. A high HFO rate (>1/min) was seen in 110 nonepileptic contacts, 
of which 68.2% were occipital. Fast ripples were less common, seen in
nonepileptic parietooccipital regions only in two patients and in the epileptic
mesial temporal structures.
CONCLUSIONS: There is consistent occurrence of physiologic HFOs over vast areas
of the neocortex outside the epileptic network. HFOs from nonepileptic regions
were seen in the occipital lobes and in the perirolandic region in all patients. 
Although duration of ripples and peak frequency of HFOs are the most effective
measures in distinguishing pathologic from physiologic events, there was
significant overlap between the two groups.

Wiley Periodicals, Inc. © 2014 International League Against Epilepsy.

DOI: 10.1111/epi.12851 
PMID: 25470216  [PubMed - indexed for MEDLINE]

