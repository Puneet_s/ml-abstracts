
1. Arq Neuropsiquiatr. 1993 Mar;51(1):41-5.

Primary sleep enuresis in childhood. Polysomnographic evidences of sleep stage
and time modulation.

Reimão R(1), Pachelli LC, Carneiro R, Faiwichow G.

Author information: 
(1)Sleep Disorders Center, Hospital Israelita Albert Einstein, São Paulo, Brasil.

The objective of this study was to evaluate enuretic events and its relations to 
sleep stages, sleep cycles and time durations in a selected group of children
with primary essential sleep enuresis. We evaluated 18 patients with mean age of 
8.2 years old (ranging from 5 to 12 years); 10 were males and 8 females (n.s.).
They were referred to the Sleep Disorders Center with the specific complaint of
enuresis since the first years of life (primary). Pediatric, urologic and
neurologic workup did not show objective abnormalities (essential). The standard 
all-night polysomnography including an enuresis sensor attached to the shorts in 
the crotch area was performed. Only enuretic events nights were included. All
were drug free patients for two weeks prior to polysomnography. In this report,
only one polysomnography per patient was considered. The enuretic events were
phase related, occurring predominantly in non-REM (NREM) sleep (p < 0.05). There 
was no predominance of enuretic events among the NREM stages (n.s.). A tendency
of these events to occur in the first two sleep cycles was detected but may be
due to the longer duration of these cycles. The events were time modulated,
adjusted to a normal distribution with a mean of 213.4 min of recording time.


PMID: 8215929  [PubMed - indexed for MEDLINE]

