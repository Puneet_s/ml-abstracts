
1. Sleep. 2010 Jan;33(1):19-28.

Genetic evidence for a role for protein kinase A in the maintenance of sleep and 
thalamocortical oscillations.

Hellman K(1), Hernandez P, Park A, Abel T.

Author information: 
(1)Department of Neurobiology, University of Chicago, Chicago, IL, USA.

STUDY OBJECTIVES: Genetic manipulation of cAMP-dependent protein kinase A (PKA)
in Drosophila has implicated an important role for PKA in sleeplwake state
regulation. Here, we characterize the role of this signaling pathway in the
regulation of sleep using electroencephalographic (EEG) and electromyographic
(EMG) recordings in R(AB) transgenic mice that express a dominant negative form
of the regulatory subunit of PKA in neurons within cortex and hippocampus.
Previous studies have revealed that these mutant mice have reduced PKA activity
that results in the impairment of hippocampus-dependent long-term memory and
long-lasting forms of hippocampal synaptic plasticity.
DESIGN: PKA assays, in situ hybridization, immunoblots, and sleep studies were
performed in R(AB) transgenic mice and wild-type control mice.
MEASUREMENTS AND RESULTS: We have found that R(AB) transgenic mice have reduced
PKA activity within cortex and reduced Ser845 phosphorylation of the glutamate
receptor subunit GluR1. R(AB) transgenic mice exhibit non-rapid eye movement
(NREM) sleep fragmentation and increased amounts of rapid eye movement (REM)
sleep relative to wild-type mice. Further, R(AB) transgenic mice have more delta 
power but less sigma power during NREM sleep relative to wild-type mice. After
sleep deprivation, the amounts of NREM and REM sleep were comparable between
wild-type and R(AB) transgenic mice. However, the homeostatic rebound of sigma
power in R(AB) transgenic mice was reduced.
CONCLUSIONS: Alterations in cortical synaptic receptors, impairments in sleep
continuity, and alterations in sleep oscillations in R(AB) mice imply that PKA is
involved not only in synaptic plasticity and memory storage but also in the
regulation of sleep/wake states.


PMCID: PMC2802244
PMID: 20120617  [PubMed - indexed for MEDLINE]

