
1. Sleep Med. 2015 Apr;16(4):534-9. doi: 10.1016/j.sleep.2014.12.002. Epub 2015 Jan 
9.

Are NREM sleep characteristics associated to subjective sleep complaints after
mild traumatic brain injury?

Arbour C(1), Khoury S(2), Lavigne GJ(3), Gagnon K(4), Poirier G(5), Montplaisir
JY(6), Carrier J(1), Gosselin N(7).

Author information: 
(1)Center for Advanced Research in Sleep Medicine (CARSM), Hôpital du Sacré-Coeur
de Montréal, Montreal, Quebec, Canada; Department of Psychology, Université de
Montréal, Montreal, Quebec, Canada. (2)Center for Advanced Research in Sleep
Medicine (CARSM), Hôpital du Sacré-Coeur de Montréal, Montreal, Quebec, Canada;
Department of Physiology, Université de Montréal, Montreal, Quebec, Canada.
(3)Center for Advanced Research in Sleep Medicine (CARSM), Hôpital du Sacré-Coeur
de Montréal, Montreal, Quebec, Canada; Faculty of Dental Medicine, Université de 
Montréal, Montreal, Quebec, Canada. (4)Center for Advanced Research in Sleep
Medicine (CARSM), Hôpital du Sacré-Coeur de Montréal, Montreal, Quebec, Canada;
Department of Psychology, Université du Québec à Montréal, Montreal, Quebec,
Canada. (5)Center for Advanced Research in Sleep Medicine (CARSM), Hôpital du
Sacré-Coeur de Montréal, Montreal, Quebec, Canada. (6)Center for Advanced
Research in Sleep Medicine (CARSM), Hôpital du Sacré-Coeur de Montréal, Montreal,
Quebec, Canada; Department of Psychiatry, Université de Montréal, Montreal,
Quebec, Canada. (7)Center for Advanced Research in Sleep Medicine (CARSM),
Hôpital du Sacré-Coeur de Montréal, Montreal, Quebec, Canada; Department of
Psychology, Université de Montréal, Montreal, Quebec, Canada. Electronic address:
nadia.gosselin@umontreal.ca.

INTRODUCTION: Sleep complaints are common after mild traumatic brain injury
(mTBI). While recent findings suggest that sleep macro-architecture is preserved 
in mTBI, features of non-rapid eye movement (NREM) sleep micro-architecture
including electroencephalography (EEG) spectral power, slow waves (SW), and sleep
spindles could be affected. This study aimed to compare NREM sleep in mTBI and
healthy controls, and explore whether NREM sleep characteristics correlate with
sleep complaints in these groups.
METHODS: Thirty-four mTBI participants (mean age: 34.2 ± 11.9 yrs; post-injury
delay: 10.5 ± 10.4 weeks) and 29 age-matched controls (mean age: 32.4 ± 8.2 yrs) 
were recruited for two consecutive nights of polysomnographic (PSG) recording.
Spectral power was computed and SW and spindles were automatically detected in
three derivations (F3, C3, O1) for the first three sleep cycles. Subjective sleep
quality was assessed with the Pittsburgh Sleep Quality Index (PSQI).
RESULTS: mTBI participants reported significant poorer sleep quality than
controls on the PSQI and showed significant increases in beta power during NREM
sleep at the occipital derivation only. Conversely, no group differences were
found in SW and spindle characteristics. Interestingly, changes in NREM sleep
characteristics were not associated with mTBI estimation of sleep quality.
CONCLUSIONS: Compared to controls, mTBI were found to have enhanced NREM beta
power. However, these changes were not found to be associated with the subjective
evaluation of sleep. While increases in beta bands during NREM sleep may be
attributable to the occurrence of a brain injury, they could also be related to
the presence of pain and anxiety as suggested in one prior study.

Crown Copyright © 2014. Published by Elsevier B.V. All rights reserved.

DOI: 10.1016/j.sleep.2014.12.002 
PMID: 25747335  [PubMed - indexed for MEDLINE]

