
1. Sleep. 2002 Dec;25(8):850-5.

Arterial stiffness increases during obstructive sleep apneas.

Jelic S(1), Bartels MN, Mateika JH, Ngai P, DeMeersman RE, Basner RC.

Author information: 
(1)Department of Medicine, College of Physicians and Surgeons of Columbia
University, New York, NY 10032, USA.

STUDY OBJECTIVES: Obstructive sleep apnea (OSA) appears to be an independent risk
factor for diurnal systemic hypertension, but the specific biologic markers for
this association have not been well established. Increased arterial stiffness is 
an important measure of increased left ventricular load and a predictor of
cardiovascular morbidity and may precede the onset of systemic hypertension in
humans. However, arterial stiffness has not been measured in association with
obstructive apneas in patients with OSA, nor related to systemic blood pressure
(BP) activity in this setting. Our objective was to test the hypothesis that
arterial stiffness may be utilized as a sensitive measure of arterial vasomotor
perturbation during obstructive events in patients with OSA, by demonstrating
that (1) arterial stiffness increases acutely in association with obstructive
apnea and hypopnea, and that (2) such increased stiffness may occur in the
absence of acute BP increase.
DESIGN: Prospective, cross-sectional.
SETTING: A tertiary-care university-based sleep and ventilatory disorders center.
PATIENTS: Forty-four normo- and hypertensive adult patients (11 women, 33 men)
with polysomnographically diagnosed moderate to severe OSA.
INTERVENTIONS: N/A.
MEASUREMENTS AND RESULTS: Beat-to-beat BP was recorded from the radial artery by 
applanation tonometry during nocturnal polysomnography. Arterial augmentation
index (AAI), a measure of arterial stiffness, was calculated as the ratio of
augmented systolic BP (SBP) to pulse pressure and expressed as a percentage for
the following conditions: awake, the first 10 ("early apnea") and last 10 ("late 
apnea") cardiac cycles of obstructive events, and the first 15 cardiac cycles
following apnea termination ("post apnea"). Mean AAI (+/-SD) for the group was
significantly increased during NREM sleep from early apnea to late apnea (12.02
+/- 2.70% vs 13.35 +/- 3.54%, p<0.05, ANOVA). During REM (analyzed in 20
patients), MI again significantly increased from early apnea to late apnea (11.75
+/- 2.81% vs 13.43 +/- 4.97%). Conversely, neither mean SBP nor mean arterial BP 
was significantly changed from early apnea to late apnea in NREM (SBP 130 +/- 14 
mmHg vs 129 +/- 14 mmHg) or REM (SBP 128 +/- 22 mmHg vs 127 +/- 21 mmHg).
CONCLUSIONS: Arterial stiffness increases acutely during obstructive apneas in
both NREM and REM sleep, in the absence of measurable BP change. These data
suggest that arterial stiffness may be a sensitive measure of acute arterial
vasomotor perturbation in this setting and may have implications concerning
cardiovascular sequelae in patients with OSA.


PMID: 12489890  [PubMed - indexed for MEDLINE]

