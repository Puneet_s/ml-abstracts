
1. Ann Neurol. 1998 May;43(5):661-4.

Sleep apnea in patients with postpolio syndrome.

Dean AC(1), Graham BA, Dalakas M, Sato S.

Author information: 
(1)EEG Section, NINDS, NIH, Bethesda, MD, USA.

We studied sleep architecture and sleep apnea pattern in patients with postpolio 
syndrome (PPS). Ten patients with clinical signs of PPS underwent
polysomnographic recording for two consecutive nights. Although sleep efficiency 
and proportions of sleep stages were within the normal range, sleep architecture 
was disrupted owing to sleep apnea. Patients with bulbar involvement had more
frequent sleep apnea (mean sleep apnea index, 11.09) than patients without (apnea
index, 5.88). The former also had significantly more central apnea, which
occurred more commonly during non-rapid-eye-movement (NREM) than
rapid-eye-movement (REM) sleep, than those without bulbar signs. This finding
suggests reduction in forebrain control of compromised bulbar respiratory centers
during NREM sleep in PPS.

DOI: 10.1002/ana.410430516 
PMID: 9585362  [PubMed - indexed for MEDLINE]

