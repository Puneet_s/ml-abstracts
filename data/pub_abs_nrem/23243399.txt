
1. J Clin Sleep Med. 2012 Dec 15;8(6):655-66A. doi: 10.5664/jcsm.2258.

The impact of body posture and sleep stages on sleep apnea severity in adults.

Eiseman NA(1), Westover MB, Ellenbogen JM, Bianchi MT.

Author information: 
(1)Neurology Department, Massachusetts General Hospital, Boston, MA 02114, USA.

STUDY OBJECTIVES: Determining the presence and severity of obstructive sleep
apnea (OSA) is based on apnea and hypopnea event rates per hour of sleep. Making 
this determination presents a diagnostic challenge, given that summary metrics do
not consider certain factors that influence severity, such as body position and
the composition of sleep stages.
METHODS: We retrospectively analyzed 300 consecutive diagnostic PSGs performed at
our center to determine the impact of body position and sleep stage on sleep
apnea severity.
RESULTS: The median percent of REM sleep was 16% (reduced compared to a normal
value of ~25%). The median percent supine sleep was 65%. Fewer than half of PSGs 
contained > 10 min in each of the 4 possible combinations of REM/NREM and
supine/non-supine. Half of patients had > 2-fold worsening of the apnea-hypopnea 
index (AHI) in REM sleep, and 60% had > 2-fold worsening of AHI while supine.
Adjusting for body position had greater impact on the AHI than adjusting for
reduced REM%. Misclassification--specifically underestimation of OSA severity--is
attributed more commonly to body position (20% to 40%) than to sleep stage
(~10%).
CONCLUSIONS: Supine-dominance and REM-dominance commonly contribute to AHI
underestimation in single-night PSGs. Misclassification of OSA severity can be
mitigated in a patient-specific manner by appropriate consideration of these
variables. The results have implications for the interpretation of single-night
measurements in clinical practice, especially with trends toward home testing
devices that may not measure body position or sleep stage.

DOI: 10.5664/jcsm.2258 
PMCID: PMC3501662
PMID: 23243399  [PubMed - indexed for MEDLINE]

