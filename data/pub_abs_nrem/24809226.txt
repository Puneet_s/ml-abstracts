
1. Brain Dev. 2015 Jan;37(1):59-65. doi: 10.1016/j.braindev.2014.03.006. Epub 2014
May 5.

Asymmetrical generalized paroxysmal fast activities in children with intractable 
localization-related epilepsy.

Mohammadi M(1), Okanishi T(2), Okanari K(2), Baba S(2), Sumiyoshi H(2), Sakuma
S(2), Ochi A(2), Widjaja E(3), Go CY(2), Snead OC 3rd(2), Otsubo H(4).

Author information: 
(1)Division of Neurology, The Hospital for Sick Children, Toronto, ON, Canada;
Department of Pediatric Neurology, Children's Medical Center, Tehran, Iran.
(2)Division of Neurology, The Hospital for Sick Children, Toronto, ON, Canada.
(3)Diagnostic Imaging, The Hospital for Sick Children, Toronto, ON, Canada.
(4)Division of Neurology, The Hospital for Sick Children, Toronto, ON, Canada.
Electronic address: hiroshi.otsubo@sickkids.ca.

BACKGROUND: Generalized paroxysmal fast activity (GPFA) consists of burst of
generalized rhythmic discharges; 100-200 μV; 1-9s; 8-26 Hz; with frontal
predominance; appearing during NREM sleep. GPFA was originally described as an
electrographic feature of Lennox-Gastaut Syndrome (LGS). We analyzed GPFA on
scalp video EEG (VEEG) in children to evaluate that GPFA presents in patients
with intractable localization-related epilepsy.
METHODS: We collected cases with GPFA with intractable localization-related
epilepsy who underwent scalp VEEG, MRI, and magnetoencephalography (MEG) prior to
intracranial video EEG (IVEEG) and surgical resection. We collected 50 epochs of 
GPFA per patient during the first night during scalp VEEG. We analyzed amplitude,
duration and frequency of GPFA over the bilateral frontal region between surgical
resection side with grid placement and non-resection side.
RESULTS: We identified 14 (14%) patients with GPFA on scalp VEEG. The mean
amplitude ranged from 145 to 589 μV (mean 293 μV). The mean duration ranged from 
1.18 to 2.31s (mean 1.6s). The mean frequencies ranged from 9.3 to 14.7 Hz (mean 
11.1 Hz). The amplitude (307 ± 156 μV) and duration (1.62 ± 0.8s) of GPFAs in all
the patients over the resection side were significantly higher than those (279 ± 
141 μV, 1.58 ± 0.8s) of the non-resection side (p<0.001). All nine patients who
showed significant duration differences between two hemispheres (p<0.05) had
longer duration of GPFA over the resection side. Eight of 12 patients who showed 
significant amplitude differences between two hemispheres (p<0.05) had higher
amplitude of GPFA over the resection side. Four of six patients who showed
significant frequency differences between two hemispheres (p<0.05) had higher
frequency of GPFA over the resection side. Nine (64%) patients became seizure
free after surgical resection including multilobar resections in eight patients.
CONCLUSIONS: GPFA can exist in localization-related epilepsy with secondary
bilateral synchrony. Although EEG shows GPFA on scalp VEEG, the precise
localization of the epileptogenic zone using IVEEG could achieve the successful
surgical resection.

Copyright © 2014 The Japanese Society of Child Neurology. Published by Elsevier
B.V. All rights reserved.

DOI: 10.1016/j.braindev.2014.03.006 
PMID: 24809226  [PubMed - indexed for MEDLINE]

