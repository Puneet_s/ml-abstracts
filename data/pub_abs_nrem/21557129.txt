
1. Laterality. 2012;17(1):1-17. doi: 10.1080/1357650X.2010.517849. Epub 2011 May 9.

Asymmetric sleep in rats.

Gene L(1), Esteban S, González J, Akâarir M, Gamundí A, Rial RV, Llobera MC.

Author information: 
(1)a Universitat de les Illes Balears , Palma de Mallorca , Spain.

Five Wistar rats were surgically implanted with cortical and parietal electrodes 
for conventional polysomnography to test for sleep-related EEG asymmetries during
48 hours of continuous recording. When the animals were grouped not according to 
right-left dominance (which would represent a population bias) but instead
according to preferred vs non-preferred hemisphere, significant light/dark
circadian changes in side dominance were found in delta power during NREM; in
theta and beta power during REM; and in alpha 1, alpha 2, and theta power during 
wakefulness. The changes have been interpreted as a response to temporal
variations in the capability to respond to environmental challenges.

DOI: 10.1080/1357650X.2010.517849 
PMID: 21557129  [PubMed - indexed for MEDLINE]

