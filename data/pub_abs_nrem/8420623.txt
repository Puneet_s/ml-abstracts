
1. Brain Res Bull. 1993;30(1-2):143-7.

Dissociation of delta EEG amplitude and incidence in rat NREM sleep.

Campbell IG(1), Feinberg I.

Author information: 
(1)Psychiatry Service, VA Medical Center, Martinez, CA 94553.

The delta (1-4 Hz) EEG of nonREM (NREM) sleep was subjected to period/amplitude
analysis in 10 Sprague-Dawley rats. During NREM sleep in the 12-h light period,
average delta wave amplitude and delta wave incidence (halfwaves/min) both
declined; the curves were biphasic with a plateau across hours 4-6. In contrast, 
the behavior of amplitude and incidence was strikingly different in dark period
NREM sleep. At dark onset, amplitude increased sharply and remained at this
elevated level without any significant trend across the 12 hours. Delta incidence
was low at dark onset and increased with a strong linear trend. These data point 
to several experiments to test the mechanisms mediating the behavior of delta
wave amplitude at the light-dark transition; they also bear on the homeostatic
model of delta sleep.


PMID: 8420623  [PubMed - indexed for MEDLINE]

