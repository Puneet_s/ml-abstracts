
1. Neuroimage. 2012 Mar;60(1):497-504. doi: 10.1016/j.neuroimage.2011.11.093. Epub
2011 Dec 8.

Slow EEG rhythms and inter-hemispheric synchronization across sleep and
wakefulness in the human hippocampus.

Moroni F(1), Nobili L, De Carli F, Massimini M, Francione S, Marzano C, Proserpio
P, Cipolli C, De Gennaro L, Ferrara M.

Author information: 
(1)Department of Psychology, Sapienza University of Rome, Roma, Italy.

Converging data that attribute a central role to sleep in memory consolidation
have increased the interest to understand the characteristics of the hippocampal 
sleep and their relations with the processing of new information. Neural
synchronization between different brain regions is thought to be implicated in
long-term memory consolidation by facilitating neural communication and by
promoting neural plasticity. However, the majority of studies have focused their 
interest on intra-hippocampal, rhinal-hippocampal or cortico-hippocampal
synchronization, while inter-hemispheric synchronization has been so far
neglected. To clarify the features of spontaneous human hippocampal activity and 
to investigate inter-hemispheric hippocampal synchronization across vigilance
states, pre-sleep wakefulness and nighttime sleep were recorded from right and
left homologous hippocampal loci using stereo-EEG techniques. Hence, quantitative
and inter-hemispheric coherence analyses of hippocampal activity across sleep and
waking states were carried out. The results showed the presence of delta activity
in human hippocampal spontaneous EEG also during wakefulness. The activity in the
delta range exhibited a peculiar bimodal distribution, namely a low frequency
non-oscillatory activity (up to 2 Hz) synchronized between hemispheres mainly
during wake and REM sleep, and a faster oscillatory rhythm (2-4 Hz). The latter
was less synchronized between the hippocampi and seemed reminiscent of animal RSA
(rhythmic slow activity). Notably, the low-delta activity showed high
inter-hemispheric hippocampal coherence during REM sleep and, to a lesser extent,
during wakefulness, paralleled by a (unexpected) decrease of coherence during
NREM sleep. Therefore, low-delta hippocampal state-dependent synchronization
starkly contrasts with neocortical behavior in the same frequency range. Further 
studies might shed light on the role of these low frequency rhythms in the
encoding processes during wakefulness and in the consolidation processes during
subsequent sleep.

Copyright © 2011 Elsevier Inc. All rights reserved.

DOI: 10.1016/j.neuroimage.2011.11.093 
PMID: 22178807  [PubMed - indexed for MEDLINE]

