
1. J Clin Sleep Med. 2010 Jun 15;6(3):256-63.

Increased neck soft tissue mass and worsening of obstructive sleep apnea after
growth hormone treatment in men with abdominal obesity.

Karimi M(1), Koranyi J, Franco C, Peker Y, Eder DN, Angelhed JE, Lönn L, Grote L,
Bengtsson BA, Svensson J, Hedner J, Johannsson G.

Author information: 
(1)Sleep Laboratory, Department of Pulmonary Medicine, Sahlgrenska University
Hospital, Göteborg, Sweden. mahssa.karimi@lungall.gu.se

BACKGROUND: Risk factors for obstructive sleep apnea (OSA) are male gender,
obesity and abnormalities in neck soft tissue mass. OSA is associated with both
growth hormone (GH) excess and severe GH deficiency in adults. Adults with
abdominal obesity have markedly suppressed GH secretion.
AIM: To study the effect of GH treatment on OSA in abdominally obese men with
impaired glucose tolerance.
PATIENTS AND METHODS: Forty men with abdominal obesity and glucose intolerance
were randomized in a prospective, 12-month double-blind trial to receive either
GH or placebo. The treatment groups had similar BMI and waist circumference.
Overnight polysomnography and computed tomography to assess muscle and fat
distribution in the neck and abdomen were performed at baseline and after 12
months.
RESULTS: GH treatment increased insulin-like growth-factor-1i from (mean [SD])
168 (72) to 292 (117) microg/L, the apnea-hypopnea index from (n/h) 31 (20) to 43
(25) and oxygen-desaturation index from (n/h) 18 (14) to 29 (21) (p = 0.0001,
0.001, 0.002). Neck transverse diameter, circumference and total cross-sectional 
area (p = 0.007, 0.01, 0.02) increased, while abdominal visceral adipose tissue
(p = 0.007) was reduced. No between-group differences in total sleep time, REM
sleep, NREM sleep, and time spent in supine position were found. The Epworth
sleepiness scale score was unchanged.
CONCLUSIONS: GH treatment increased the severity of OSA in abdominally obese men.
The possible mechanism appears to be reflected by the GH-induced increase of
measures of neck volume. The present results, to some extent, argue against that 
low GH/IGF-I activity is a primary cause of OSA in abdominally obese men.


PMCID: PMC2883037
PMID: 20572419  [PubMed - indexed for MEDLINE]

