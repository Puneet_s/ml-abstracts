
1. Electroencephalogr Clin Neurophysiol. 1980 Oct;50(1-2):141-50.

Sleep during neuromuscular blockade in cats.

Glenn LL, Foutz AS, Dement WC.

The purpose of the experiment was to determine whether normal sleep patterns can 
occur during neuromuscular blockade. Electrographic variables for determining the
states of sleep and wakefulness, the electrocorticogram, lateral geniculate
nucleus potentials, and dorsal hippocampal potentials, were recorded before,
during and after the administration of gallamine triethiodide to cats with
chronically implanted electrodes. When respiratory muscles became paralyzed,
artificial ventilation commenced through a chronic tracheal fistula. The
electrographic wave forms of the states (wakefulness, NREM sleep and REM sleep)
in paralyzed cats were indistinquishable by visual observation from those of
freely moving animals. As compared to freely moving cats, paralyzed cats had more
wakefulness at the expense of both states of sleep (about 33% NREM and 3% REM
compared to 45% NREM and 15% REM respectively). REM sleep wasdemonstrated to
occur, albeit increase across repeated session in the same cats nor was the
distribution uneven within the average session. Large percentages of REM sleep
with respect to total recording time were associated with large percentages of
NREM sleep (correlation coefficient = 0.58). The sequence of sleep states was
like that of freely behaving animals. The main conclusion is that this
preparation, depsite low amounts of REM sleep, is useful in neural studies of
sleep and wakefulness.


PMID: 6159182  [PubMed - indexed for MEDLINE]

