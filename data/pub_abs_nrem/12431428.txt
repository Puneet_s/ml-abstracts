
1. Neurobiol Learn Mem. 2002 Sep;78(2):441-57.

Sleep-dependent hippocampal slow activity correlates with waking memory
performance in humans.

Bódizs R(1), Békésy M, Szucs A, Barsi P, Halász P.

Author information: 
(1)Epilepsy Center, National Institute of Psychiatry and Neurology, Hüvösvölgyi
út 116, H-1021 Budapest, Hungary. rbodizs@opni.hu

The positive effect of postlearning sleep on memory consolidation as well as the 
relationship between sleep-related memory processes and the hippocampal formation
are increasingly clarified topics in neurobiology. However, the possibility of a 
stable relationship between waking mnemonic performance and sleep-dependent
hippocampal electric activity is unexplored. Here we report a correlative
analysis between sleep-dependent parahippocampal-hippocampal (pHip-Hip) electric 
activity recorded by foramen ovale (FO) electrodes and different types of memory 
performances in epileptic patients. Psychological testing was performed days or
weeks before electrophysiological recordings. The relative spectral power of the 
slow activity (below 1.25 Hz) during deep non-REM (NREM) sleep at the right
pHip-Hip region correlated positively with the visual memory performance
according to Rey-Osterrieth Complex Figure Test (ROCFT). Along the
posterior-anterior direction of the hippocampal formation a linear increasing of 
correlations was observed. The relative power of the activity below 1.25 Hz at
the left pHip-Hip during phasic REM sleep correlated positively with verbal
learning performance and mnemonic retention values according to ROCFT. It is
concluded that the pHip-Hip structures' capacity of producing high amplitude and 
synchronized slow (< 1 Hz) oscillation during deep NREM sleep is related to the
functional power of these structures. We hypothesize that the asymmetric
(side-specific) propagation of ponto-geniculo-occipital (PGO) activity to the
pHip-Hip region is related to the memory correlates of phasic REM sleep.


PMID: 12431428  [PubMed - indexed for MEDLINE]

