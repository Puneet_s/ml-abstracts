
1. Electroencephalogr Clin Neurophysiol. 1975 Apr;38(4):403-13.

A syndrome of hypersomnia with automatic behavior.

Guilleminault C, Phillips R, Dement WC.

Five male patients, complaining of daytime sleep attacks, and hav ing a history
of automatic behavior, were recorded continously by polygraph for 108 h under two
different protocols: (1ents were given specific tests to perform during Phase 1. 
Six normal males were run as controls during Phase . At the end of the
recordings, patients had measurements of homovanillic acid (HVA) and 5
Hydroxy-indol Acetic Acid (5HIAA) in CSF before and after Probenecid testing.
Patients appeared to present an abnormal sleep structure, with lack of Stage 3
and 4 NREM sleep, but had no significant increase in total sleep time during 24 h
compared to normals. They also had repetitive very short periods of "micro-sleep"
which account for their automatic behavior and were responsible for their great
decrement in performance compared to normal controls. No CSF HVA and 5HIAA
abnormality could be detected. This syndrome, which greatly impairs the social
life and working ability of the patients, may be more related to an impairment of
the "wakefulness" structures than to a dysfunction of the "sleep" structures.


PMID: 46820  [PubMed - indexed for MEDLINE]

