
1. J Neurol Neurosurg Psychiatry. 1977 Jul;40(7):718-25.

Sleep apnoea syndrome: states of sleep and autonomic dysfunction.

Gulleminault C, Lehrman AT, Forno L, Dement WC.

Eleven patients with upper airway apnoea during sleep (one with SHY-Drager
syndrome) were monitored polygraphically for wakefulness, sleep, and
cardiovascular variables. Systemic hypertension and most of the severe
arrhythmias recorded during sleep were secondary to repetitive obstructive
apnonea and were mediated through the autonomic nervous system. Sleep related
elevations of pulmonary arterial pressure were not influenced by atropine or
impaired autonomic functions. Upper airway sleep apnoea is sleep related; the
type of sleep (REM or NREM) is critical in the appearance of abnormalities. The
distinction between two patient subgroups (total sleep dependent and NREM sleep
dependent) has haemodynamic, and possibly long-term, implications. Sleep apnoea
syndrome should be looked for in pateints with the Shy-Drager syndrome.


PMCID: PMC492815
PMID: 199709  [PubMed - indexed for MEDLINE]

