
1. Zhonghua Yi Xue Za Zhi. 2013 Apr 23;93(16):1238-42.

[Clinical analyses of polysomnogram in snoring children].

[Article in Chinese]

Gao F(1), Gu QL, Jiang ZD.

Author information: 
(1)Department of Otorhinolaryngology, Capital Institute of Pediatrics Affiliated 
Children's Hospital, Beijing 100020, China.

OBJECTIVE: To analyze the characteristics of sleep structure, heart rate and
arousal index (ArI) in children with primary snoring (PS) and mild, moderate or
severe obstructive sleep apnea hypopnea syndrome (OSAHS).
METHODS: A total of 113 children with sleep disorders were enrolled from January 
2010 to March 2012 at Affiliated Children's Hospital, Capital Institute of
Pediatrics. All of them underwent polysomnogram (PSG) and the data were analyzed 
statistically by SPSS 19.0.
RESULTS: (1) No statistical difference existed in age, sleeping time or sleeping 
efficacy between PS and all OSAHS groups (all P > 0.05). (2) The proportion of
stage I sleeping was 2.6% ± 1.4% in PS group, 5.4% ± 3.2% in mild OSAHS group,
4.7% ± 1.9% in moderate OSAHS group and 8.9% ± 4.0% in severe OSAHS group (F =
6.542, P = 0.000). The proportion of stage IV sleeping was 25.3% ± 5.6% in PS
group, 32.4% ± 11.1% in mild OSAHS group, 30.6% ± 9.0% in moderate OSAHS group
and 21.4% ± 10.8% in severe OSAHS group (F = 7.544, P = 0.000).The proportion of 
stage rapid eye movement (REM) sleeping was 21.1% ± 8.6% in PS group, 13.9% ±
4.0% in mild OSAHS group, 14.5% ± 4.9% in moderate OSAHS group and 12.3% ± 6.9%
in severe OSAHS group (F = 11.204, P = 0.000).The proportion of stage II and III 
sleeping had no statistical difference among four groups. (3) The average heart
rate in stage REM sleeping of four groups was (85 ± 11), (90 ± 14), (95 ± 10) and
(101 ± 18) beats per minute(F = 6.452, P = 0.000) and (79 ± 10), (84 ± 14), (86 ±
7) and (93 ± 16) beats per minute in stage NREM sleeping(F = 5.369, P = 0.002).
(4) In four groups, the difference of total count of spontaneous arousal, the
spontaneous arousal count in stage REM and non-rapid eye movement (NREM) sleeping
were all statistically significant (F = 56.379, 60.781, 44.061, all P = 0.000).
And the difference of total count of respiratory arousal, the median of
respiratory arousal count in stage REM and NREM sleeping were all statistically
significant (F = 79.250, 36.137, 65.239, all P = 0.000).
CONCLUSIONS: Heart rate is affected more obviously in moderate-severe OSAHS
children. As compared with PS counterparts, OSAHS children had a reduction of
spontaneous arousal and an increase of respiratory arousal. But the occurrence of
spontaneous arousal of OSAHS children does not decrease with the progress of
OSAHS in either stage REM or stage NREM.


PMID: 23902615  [PubMed - indexed for MEDLINE]

