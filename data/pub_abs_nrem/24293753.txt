
1. Sleep. 2013 Dec 1;36(12):1793-8. doi: 10.5665/sleep.3202.

Testosterone conversion blockade increases breathing stability in healthy men
during NREM sleep.

Chowdhuri S(1), Bascom A, Mohan D, Diamond MP, Badr MS.

Author information: 
(1)Medical Service, Sleep Medicine Section, John D. Dingell Veterans Affairs
Medical Center, Detroit, MI ; Division of Pulmonary/Critical Care and Sleep
Medicine, Department of Medicine, Wayne State University School of Medicine,
Detroit, MI.

STUDY OBJECTIVES: Gender differences in the prevalence of sleep apnea/hypopnea
syndrome may be mediated via male sex hormones. Our objective was to determine
the exact pathway for a testosterone-mediated increased propensity for central
sleep apnea via blockade of the 5α-reductase pathway of testosterone conversion
by finasteride.
DESIGN: Randomization to oral finasteride vs. sham, single-center study.
SETTING: Sleep research laboratory.
PARTICIPANTS: Fourteen healthy young males without sleep apnea.
INTERVENTION: Hypocapnia was induced via brief nasal noninvasive positive
pressure ventilation during stable NREM sleep. Cessation of mechanical
ventilation resulted in hypocapnic central apnea or hypopnea.
MEASUREMENTS AND RESULTS: The apnea threshold (AT) was defined as the end-tidal
CO₂(P(ET)CO₂) that demarcated the central apnea closest to the eupneic P(ET)CO₂. 
The CO₂ reserve was defined as the difference in P(ET)CO₂ between eupnea and AT. 
The apneic threshold and CO₂ reserve were measured at baseline and repeated after
at a minimum of 1 month. Administration of finasteride resulted in decreased
serum dihydrotestosterone. In the finasteride group, the eupneic ventilatory
parameters were unchanged; however, the AT was decreased (38.9 ± 0.6 mm Hg
vs.37.7 ± 0.9 mm Hg, P = 0.02) and the CO₂ reserve was increased (-2.5 ± 0.3 mm
Hg vs. -3.8 ± 0.5 mm Hg, P = 0.003) at follow-up, with a significantly lower
hypocapnic ventilatory response, thus indicating increased breathing stability
during sleep. No significant changes were noted in the sham group on follow-up
study.
CONCLUSIONS: Inhibition of testosterone action via the 5α-reductase pathway may
be effective in alleviating breathing instability during sleep, presenting an
opportunity for novel therapy for central sleep apnea in selected populations.

DOI: 10.5665/sleep.3202 
PMCID: PMC3825428
PMID: 24293753  [PubMed - indexed for MEDLINE]

