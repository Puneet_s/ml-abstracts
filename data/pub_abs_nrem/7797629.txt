
1. J Clin Neurophysiol. 1995 Mar;12(2):147-54.

Arousal fluctuations in non-rapid eye movement parasomnias: the role of cyclic
alternating pattern as a measure of sleep instability.

Zucconi M(1), Oldani A, Ferini-Strambi L, Smirne S.

Author information: 
(1)Department of Neurology, State University, Milan, Italy.

Some non-rapid eye movement (NREM) parasomnias, such as sleep-walking (SW), sleep
terror (ST) and, in some aspects, sleep enuresis (SE), are considered "arousal
disorders" without significant polysomnographic changes in classic sleep
macrostructure. The aim of our study was to evaluate sleep microstructure and
oscillations of arousal level by cyclic alternating pattern (CAP) scoring in some
NREM parasomnias. Nocturnal polysomnography and videotape recording was used to
study 21 patients with motor and behavioral phenomena during sleep: 13 in Group A
(seven SW, six ST) with delta sleep-related episodes, eight in Group B with other
parasomnias (six sleep bruxism and two SE), and six healthy controls. Classic
sleep macrostructural parameters were no different in the parasomniacs and
controls. Compared with the controls, our patients' sleep microstructure, scored 
by CAP analysis, showed increases in CAP rate (a measure of NREM instability with
high level of arousal oscillation), in number of the CAP cycles, and in arousals 
with EEG synchronization, the increases being more significant in Group A than in
Group B. An increase in sleep instability and in arousal oscillation seems to be 
a typical microstructural feature of delta sleep-related parasomnias and probably
plays a role in triggering abnormal motor episodes during sleep in these
patients.


PMID: 7797629  [PubMed - indexed for MEDLINE]

