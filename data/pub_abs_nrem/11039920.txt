
1. J Biol Rhythms. 2000 Oct;15(5):429-36.

Long photoperiod restores the 24-h rhythm of sleep and EEG slow-wave activity in 
the Djungarian hamster (Phodopus sungorus).

Deboer T(1), Vyazovskiy VV, Tobler I.

Author information: 
(1)Institute of Pharmacology and Toxicology, University of Zürich, Switzerland.

Photoperiod influences the distribution of sleep and waking and
electroencephalogram (EEG) power density in the Djungarian hamster. In an
experimental procedure combining short photoperiod (SP) and low ambient
temperature, the light-dark difference in the amount of sleep was decreased, and 
the changes in slow-wave activity (SWA) (mean EEG power density between 0.75 and 
4.0 Hz) in nonrapid eye movement (NREM) sleep within 24 h were abolished. These
findings, obtained in three different groups of animals, suggested that at the
lower ambient temperature, the influence of the circadian clock on sleep-wake
behavior was diminished. However, it remained unclear whether the changes were
due to the photoperiod, ambient temperature, or both. Here, the authors show that
EEG and electromyogram recordings in a single group of animals sequentially
adapted to a short and long photoperiod (LP) at low ambient temperature
(approximately 15 degrees C) confirm that EEG power is reduced in SP. Moreover,
the nocturnal sleep-wake behavior and the changes in SWA in NREM sleep over 24 h 
were restored by returning the animals to LP and retaining ambient temperature at
15 degrees C. Therefore, the effects cannot be attributed to ambient temperature 
alone but are due to a combined effect of temperature and photoperiod. When the
Djungarian hamster adapts to winter conditions, it appears to uncouple sleep
regulation from the circadian clock.


PMID: 11039920  [PubMed - indexed for MEDLINE]

