
1. Neurology. 2001 Jun 12;56(11):1453-9.

Distribution of partial seizures during the sleep--wake cycle: differences by
seizure onset site.

Herman ST(1), Walczak TS, Bazil CW.

Author information: 
(1)Department of Neurology, State University of New York, Brooklyn, USA.
susan_herman@netmail.hscbklyn.edu

OBJECTIVE: To evaluate the effects of sleep on partial seizures arising from
various brain regions.
METHODS: The authors prospectively studied 133 patients with localization-related
epilepsy undergoing video-EEG monitoring over a 2-year period. Seizure type, site
of onset, sleep/wake state at onset, duration, and epilepsy syndrome diagnosis
were recorded. Periorbital, chin EMG, and scalp/sphenoidal electrodes were used. 
A subset of 34 patients underwent all-night polysomnography with scoring of sleep
stages.
RESULTS: The authors analyzed 613 seizures in 133 patients. Forty-three percent
(264 of 613) of all partial seizures began during sleep. Sleep seizures began
during stages 1 (23%) and 2 (68%) but were rare in slow-wave sleep; no seizures
occurred during REM sleep. Temporal lobe complex partial seizures were more
likely to secondarily generalize during sleep (31%) than during wakefulness
(15%), but frontal lobe seizures were less likely to secondarily generalize
during sleep (10% versus 26%; p < 0.005).
CONCLUSIONS: Partial-onset seizures occur frequently during NREM sleep,
especially stage 2 sleep. Frontal lobe seizures are most likely to occur during
sleep. Patients with temporal lobe seizures have intermediate sleep seizure
rates, and patients with seizures arising from the occipital or parietal lobes
have rare sleep-onset seizures. Sleep, particularly stage 2 sleep, promotes
secondary generalization of temporal and occipitoparietal, but not frontal,
seizures. These findings suggest that the hypersynchrony of sleep facilitates
both initiation and propagation of partial seizures, and that effects of sleep
depend in part on the location of the epileptic focus.


PMID: 11402100  [PubMed - indexed for MEDLINE]

