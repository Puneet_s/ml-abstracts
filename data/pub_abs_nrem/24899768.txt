
1. Sleep. 2014 Apr 1;37(4):821-4. doi: 10.5665/sleep.3598.

Rapid tolerance development to the NREM sleep promoting effect of alcohol.

Sharma R(1), Sahota P(1), Thakkar MM(1).

Author information: 
(1)Harry S. Truman Memorial Veterans Hospital and Department of Neurology,
University of Missouri, Columbia, MO.

STUDY OBJECTIVES: Alcohol tolerance is a major contributor towards the
development of alcohol dependence. Does alcohol intake result in rapid tolerance 
development to alcohol induced NREM sleep promotion? This has never been
examined. Our objective was to examine whether two bouts of alcohol consumption
on consecutive days results in rapid tolerance development to alcohol-induced
NREM sleep promotion.
DESIGN: N/A.
SETTING: N/A.
PATIENTS OR PARTICIPANTS: C57BL/6J mice.
INTERVENTIONS: Mice (N = 5) were implanted with sleep electrodes using standard
surgical conditions. Following postoperative recovery and habituation, the
experiment was begun. On baseline day, water bottle changes were performed at
10:00 (3 h after dark onset) and 14:00 to mimic conditions during alcohol
consumption days. On next 2 days, (Days 1 and 2) mice were allowed to
self-administer alcohol (20% v/v) for 4 h beginning at 10:00 and ending at 14:00.
Sleep-wakefulness was continuously recorded from 10:00 to 18:00 (8 h; 4 h during 
alcohol + 4 h post-alcohol) on all 3 days.
MEASUREMENTS AND RESULTS: Although mice consumed comparable amounts of alcohol on
Days 1 and 2, NREM sleep and wakefulness were significantly and differentially
affected during 4 h post-alcohol period. A robust alcohol-induced NREM sleep
promotion was observed on Day 1. However, no such sleep promotion was observed on
Day 2, suggesting rapid tolerance development.
CONCLUSIONS: Our study is the first to demonstrate that alcohol consumption for
two consecutive days results in development of rapid tolerance to alcohol-induced
sleep promotion.

DOI: 10.5665/sleep.3598 
PMCID: PMC4044753
PMID: 24899768  [PubMed - indexed for MEDLINE]

