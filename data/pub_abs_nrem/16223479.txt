
1. Eur J Pharmacol. 2005 Oct 17;522(1-3):63-71. Epub 2005 Oct 11.

Antidepressants and REM sleep in Wistar-Kyoto and Sprague-Dawley rats.

Ivarsson M(1), Paterson LM, Hutson PH.

Author information: 
(1)Merck Sharp & Dohme Research Laboratories, Neuroscience Research Centre,
Harlow, Essex, UK. Magnus_Ivarsson@merck.com

Compared to other rat strains, the Wistar-Kyoto rats show increased amount of REM
sleep, one of the characteristic sleep changes observed in depressed patients.
The aims of this study were firstly to validate a simple sleep stage
discriminator and then compare the effect of antidepressants on suppression of
rapid eye movement (REM) sleep in Wistar-Kyoto rats and an outbred rat strain
(Sprague-Dawley). Rats were implanted with telemetry transmitters with
electroencephalogram/electromyogram electrodes. Following recovery, the animals
were orally dosed at light onset with either desipramine (20 mg/kg), fluoxetine
(10 mg/kg), citalopram (10 or 40 mg/kg) or vehicle in a cross-over design. Every 
12-s epoch was automatically scored as WAKE, NREM or REM sleep. Results confirm
that Wistar-Kyoto rats show increased amount of REM sleep and decreased REM
latency compared with Sprague-Dawley rats. All antidepressants significantly
suppressed REM sleep in Sprague-Dawley rats, but only the high dose of citalopram
suppressed REM sleep in Wistar-Kyoto rats. These findings suggest that the
enhanced REM activity in Wistar-Kyoto rats is less sensitive to the effect of
antidepressants and therefore does not provide any additional predictive validity
for assessing antidepressant efficacy.

DOI: 10.1016/j.ejphar.2005.08.050 
PMID: 16223479  [PubMed - indexed for MEDLINE]

