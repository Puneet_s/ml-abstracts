
1. Electroencephalogr Clin Neurophysiol. 1979 May;46(5):592-600.

Long duration EEG studies in the case of a psychotic child.

Findji F, Harrison-Covello A, Lairy GC.

Three biotelemetric examinations and a whole night sleep recording were carried
out in an 8-year-old child whose behaviour alternated between excitation and
autism with stereotypes. The EEG showed 5 c/sec temporo-parietal sharp wave
discharges lasting from 1 sec to 20 min. These discharges were at times
unilateral and predominantly right sided, at other times bilateral, without any
clinical sign of epilepsy. The chronological distribution of right, left and
bilateral discharges during the successive 1 min epochs was computed and related 
to corresponding 'behavioural states' of the child. The paroxysmal discharges
predominated when the child was awake but not involved in any relational
activity; during sleep, they mostly appeared during light NREM sleep (stage I)
and paradoxical sleep. The significance of these paroxysmal discharges is
discussed in relation to stereotyped behaviour, vigilance and early
disorganization of biological rhythms.


PMID: 88347  [PubMed - indexed for MEDLINE]

