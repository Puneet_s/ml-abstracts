
1. Sleep Med Rev. 1999 Sep;3(3):241-55.

Myocardial ischemia during sleep.

Bonsignore MR(1), Smirne S, Marrone O, Insalaco G, Salvaggio A, Bonsignore G.

Author information: 
(1)Istituto di Fisiopatologia Respiratoria del Consiglio Nazionale delle
Ricerche, Palermo, Italy.

The role of sleep in the pathogenesis of coronary ischaemic events such as
myocardial infarction, transient myocardial ischaemia, or cardiac sudden death,
is unclear. This review will analyse the available data on the subject according 
to: (i) the autonomic and cardiovascular changes during sleep that may
potentially favour myocardial ischaemia; (ii) the evidence of a circadian
distribution of coronary events; and (iii) the factors possibly involved in the
pathogenesis of nocturnal angina. Available data suggest that myocardial
ischaemia may occur by different mechanisms in non-rapid eye movement (NREM)
(decreased coronary perfusion pressure) and rapid eye movement (REM) sleep
(increased myocardial oxygen demand). Coronary events show a major peak of
occurrence between 6.00 a.m. and noon; however, the myocardial ischaemic
threshold, defined as the heart rate value at which myocardial ischaemia
develops, may be lower at night than during the daytime, suggesting an
unexpectedly higher susceptibility to myocardial ischaemia during sleep than
during wakefulness. These data warrant further study on the pathophysiology of
coronary circulation during sleep. Finally, some evidence is available that sleep
disordered breathing may precipitate nocturnal angina especially in REM sleep,
through decreased arterial oxygen content secondary to hypoventilation or true
apnoeas. More data are needed to better understand the effects of sleep on the
coronary circulation, and to improve the therapeutic approach of nocturnal
angina.


PMID: 15310478  [PubMed]

