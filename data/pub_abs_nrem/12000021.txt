
1. Ann N Y Acad Sci. 2001 Mar;933:201-10.

Mediators of inflammation and their interaction with sleep: relevance for chronic
fatigue syndrome and related conditions.

Mullington JM(1), Hinze-Selch D, Pollmächer T.

Author information: 
(1)Department of Neurology, Beth Israel Deaconess Medical Center and Harvard
Medical School, Boston, Massachusetts 02215, USA. jmulling@caregroup.harvard.edu

In humans, activation of the primary host defense system leads to increased or
decreased NREM sleep quality, depending on the degree of early immune activation.
Modest elevations of certain inflammatory cytokines are found during experimental
sleep loss in humans and, in addition, relatively small elevations of cytokines
are seen following commencement of pharmacological treatments with clozapine, a
CNS active antipsychotic agent, known to have immunomodulatory properties.
Cytokines such as TNF-alpha, its soluble receptors, and IL-6, present in the
periphery and the CNS, comprise a link between peripheral immune stimulation and 
CNS-mediated behaviors and experiences such as sleep, sleepiness, and fatigue.
The debilitating fatigue experienced in chronic fatigue syndrome and related
diseases may also be related to altered cytokine profiles.


PMID: 12000021  [PubMed - indexed for MEDLINE]

