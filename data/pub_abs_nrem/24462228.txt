
1. Epilepsy Res. 2014 Mar;108(3):459-67. doi: 10.1016/j.eplepsyres.2013.12.007. Epub
2013 Dec 30.

Altered polysomnographic profile in juvenile myoclonic epilepsy.

Krishnan P(1), Sinha S(2), Taly AB(3), Ramachandraiah CT(4), Rao S(5),
Satishchandra P(6).

Author information: 
(1)National Institute of Mental Health and Neuro Sciences, Bangalore 560029,
Karnataka, India. Electronic address: drpramodkrishnan@gmail.com. (2)National
Institute of Mental Health and Neuro Sciences, Hosur Road, Bangalore 560029,
Karnataka, India. Electronic address: sanjib_sinha2004@yahoo.co.in. (3)National
Institute of Mental Health and Neuro Sciences, Bangalore, Karnataka, India.
Electronic address: abtaly@yahoo.com. (4)Department of Neurology, National
Institute of Mental Health and Neuro Sciences, Bangalore, Karnataka, India.
Electronic address: drchaitratr@yahoo.com. (5)National Institute of Mental Health
and Neuro Sciences, Bangalore, Karnataka, India. Electronic address:
shivajirao.nimhans@gmail.com. (6)National Institute of Mental Health and Neuro
Sciences, Bangalore, Karnataka, India. Electronic address:
drpsatishchandra@yahoo.com.

PURPOSE: To study the spectra of sleep profile using PSG in a cohort of patients 
with JME attending a University hospital.
METHODOLOGY: This prospective cross-sectional case-control study involved 25
patients of JME (age: 22.0±6.3 years; M:F=13:12) on valproic acid (VPA) and 25
matched healthy controls (age: 23.2±3.04 years; M:F=16:9) were recruited. All
patients underwent clinical assessment, electroencephalogram (EEG), and
evaluation with sleep questionnaire and PSG.
RESULTS: PSG analysis revealed significant alterations in sleep architecture in
the JME group in the form of reduced mean sleep efficiency (p=<0.035) and number 
of patients with reduced sleep efficiency (p=0.001), increased mean sleep onset
latency (p=0.04) and number of patients with increased sleep latency (p=0.023),
reduced mean N2 sleep percentage (p=0.005) and reduced mean total NREM (non-rapid
eye movement) sleep (p=0.001) and increased mean wake percentage (p=0.001). The
frequency of arousals, involuntary limb movements, and event related arousals in 
the JME groups was not different from the controls. Patients >20 years had
reduced total sleep time compared to those <20 years (p=0.012). Patients with
seizures for >5 years had reduced NREM sleep percentage (p=0.042) and those on
VPA therapy >1 year had a longer stage 2 (p=0.03) and N3 latency (p=0.03).
Patients on ≤600mg/day of VPA had a higher prevalence of isolated limb movements 
(p=0.01).
CONCLUSIONS: PSG revealed significant alterations in sleep architecture in JME
despite adequate seizure control. There was variable degree of PSG-phenotypic
correlation.

Copyright © 2013 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.eplepsyres.2013.12.007 
PMID: 24462228  [PubMed - indexed for MEDLINE]

