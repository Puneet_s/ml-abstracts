
1. Zhongguo Dang Dai Er Ke Za Zhi. 2008 Jun;10(3):322-4.

[Interictal epileptiform discharges in children with epilepsy].

[Article in Chinese]

Xiao YH(1), Liao JX, Huang J, Mai JN.

Author information: 
(1)Department of Neurology, Shenzhen Children's Hospital, Shenzhen, Guangdong
518026, China.

OBJECTIVE: To study the features of interictal epileptiform discharges (IED)
during sleep and wakefulness in children with epilepsy.
METHODS: The polysomnography, active EEG and video EEG were performed on 48
children with epilepsy during the whole night, and wakefulness of pre- and
post-sleep. The epileptiform sharp/spike discharge indexes during sleep and
wakefulness were recorded. The positive rate of IED in focal and generalized
epilepsy was compared.
RESULTS: Of the 48 patients, 25 showed IED, including 9 cases (36.0%) in the
generalized seizure group and 16 cases (64.0%) in the focal seizure group
(P<0.05). The epileptiform sharp/spike discharge indexes in the whole non-rapid
eye movement (NREM) sleep stage (stages S1-S4: 21.13+/-19.96, 19.59+/-17.76,
22.85+/-18.99, and 20.37+/-16.63) were significantly higher than that in the
wakefulness stage (8.20+/-6.21) (P<0.05). The discharge index in the S3 stage
during NREM sleep was higher than that during the rapid eye movement (REM) sleep 
(22.85+/-18.99 vs 12.91+/-10.95; P<0.05).
CONCLUSIONS: The positive rate of IED in the focal seizure group was higher than 
that in the generalized seizure group. Sleep, especially NREM sleep, facilitates 
IED in children with epilepsy.


PMID: 18554459  [PubMed - indexed for MEDLINE]

