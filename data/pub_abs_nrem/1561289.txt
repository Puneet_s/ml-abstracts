
1. Psychiatry Res. 1992 Jan;41(1):65-72.

Comparison of the delta EEG in the first and second non-REM periods in depressed 
adults and normal controls.

Armitage R(1), Calhoun JS, Rush AJ, Roffwarg HP.

Author information: 
(1)Dept. of Psychiatry, University of Texas Southwestern Medical Center, Dallas
75235-9070.

The distribution of period-analyzed delta activity in the first and second
non-rapid eye movement (NREM) periods was compared in nine symptomatic depressed 
outpatients and nine normal controls. The groups did not differ in ratios of
delta zero-cross or delta power in the first to the second NREM periods. Further,
neither group showed a systematic change in delta count or delta power across the
first two NREM periods. Our findings suggest that ratios of delta activity in the
first two NREM periods may not systematically differentiate depressed adults from
normal subjects.


PMID: 1561289  [PubMed - indexed for MEDLINE]

