
1. Sleep Med. 2007 Nov;8(7-8):773-8. Epub 2007 May 23.

Comparison of sleep parameters at titration and subsequent compliance between
CPAP-pretreated and non-CPAP-pretreated patients with obstructive sleep apnea.

Suzuki M(1), Saigusa H, Furukawa T.

Author information: 
(1)Department of Otolaryngology, Teikyo University School of Medicine, 2-11-1
Kaga, Itabashi-ku, 173-8605 Tokyo, Japan. suzukima@med.teikyo-u.ac.jp

BACKGROUND AND PURPOSE: Obstructive sleep apnea-hypopnea syndrome (OSAHS)
patients undergo continuous positive airway pressure (CPAP) treatment for the
first time on titration night, and then the effect of overnight CPAP treatment is
estimated immediately. The purpose of this study is to compare the effects of
CPAP-pretreated and non-pretreated on patients with OSAHS.
METHODS: Prospective randomized, controlled parallel study was performed. Seventy
patients with OSAHS received autoadjusted CPAP treatment for 2 months and then
received the standard manual titration (CPAP-pretreated group). The other 70 did 
not receive any CPAP treatment before receiving the standard manual titration
(non-CPAP-pretreated group).
RESULTS: The CPAP-pretreated group had significantly improved sleep efficiency
and arousal index in non-rapid eye movement (NREM) sleep compared with the
initial CPAP group at titration, whereas there were no significant differences
between the two groups in other sleep parameters. Eight patients in the
non-CPAP-pretreated group discontinued CPAP treatment 9 months after the
titration, whereas one patient in the CPAP-pretreated group discontinued
treatment.
CONCLUSIONS: A preceding CPAP treatment showed minimal effects on sleep
parameters on titration night and subsequent CPAP compliance rate, although it
was speculated that this preceding treatment might be of benefit for better
compliance in some patients.

DOI: 10.1016/j.sleep.2007.01.013 
PMID: 17512246  [PubMed - indexed for MEDLINE]

