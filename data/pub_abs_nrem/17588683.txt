
1. Behav Brain Res. 2007 Aug 22;182(1):95-102. Epub 2007 May 22.

Neonatal REM sleep is regulated by corticotropin releasing factor.

Feng P(1), Liu X, Vurbic D, Fan H, Wang S.

Author information: 
(1)Department of Physiology, School of Medicine, Zhengzhou University, Zhengzhou,
China. pfeng@yahoo.com

Sleep/wake regulation is quite different during the neonatal and adult periods.
Although cholinergic neurons have been recognized to be the major source of rapid
eye movement (REM) sleep regulation in adulthood, their effect on neonatal REM
sleep remains to be discovered. Current evidence suggests that
corticotropin-releasing factor (CRF) may play a role in REM promotion during the 
neonatal period. We conducted the following study to test our hypothesis that
blocking CRF R1 receptor would reduce REM sleep in developing rat pups. First,
rat pups were surgically implanted with electrodes on postnatal day (PN) 13. On
PN 14, six hours of polysomnographic (PSG) data were collected before and after
administration of three different doses of NBI 27914 (NBI), a CRF R1 receptor
antagonist. Compared with baseline, REM sleep was significantly reduced in all
groups treated with NBI but not with dimethyl sulfoxide/saline. The reduction of 
REM sleep was dose-related and was replaced primarily by non-REM (NREM) sleep.
Second, two groups of rat pups were given a single dose of either NBI or vehicle 
on PN 14 for quantification of ACTH and acetylcholine without PSG recording. NBI 
induced no change of either ACTH or acetylcholine. Third, the effect of
administering atropine (6 mg/kg) on sleep/wake in two-week-old rats was
investigated. Atropine suppressed REM sleep significantly and increased
wakefulness simultaneously. Our data revealed that blockage of CRF R1 receptors
deprives neonatal REM sleep. The mechanism for CRF in enhancing REM sleep may be 
associated with but not be similar to the cholinergic mechanism.

DOI: 10.1016/j.bbr.2007.05.009 
PMID: 17588683  [PubMed - indexed for MEDLINE]

