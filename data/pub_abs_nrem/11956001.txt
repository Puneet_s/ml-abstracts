
1. Clin Neurophysiol. 2002 Apr;113(4):561-70.

Cortical EEG topography of REM onset: the posterior dominance of middle and high 
frequencies.

De Gennaro L(1), Ferrara M, Curcio G, Cristiani R, Bertini M.

Author information: 
(1)Department of Psychology, Section of Neuroscience, University of Rome La
Sapienza, Via dei Marsi 78, 00185, Rome, Italy. luigi.degennaro@uniroma1.it

OBJECTIVES: To investigate the brain topography of human sleep
electroencephalography (EEG) along the antero-posterior axis during rapid eye
movement sleep (REM) onset and REM offset, by means of a quantitative analysis of
EEG changes.
METHODS: EEG power values were calculated across a 1.00-25.75 Hz frequency range 
during time intervals preceding and following REM onset of the first 4 sleep
cycles of 10 normal subjects. Topographical changes were assessed through Fpz-A1,
Fz-A1, Cz-A1, Pz-A1, Oz-A1 recordings during NREM-REM-NREM transitions.
RESULTS: The temporal dynamics of REM onset is characterized by a specific
topographical pattern of EEG changes with a relatively higher EEG activity at
posterior sites: Oz does not show any clear change within the alpha and beta
frequencies, at variance with the marked reductions of the other sites, while it 
shows reductions of power in the delta/theta and sigma frequency ranges of
smaller size as compared to the other sites. REM offset does not appear as a
mirror-image of REM onset, since the pattern of regional differences
characterizing the NREM sleep preceding REM onset is not fully reached.
CONCLUSIONS: REM onset is characterized by a general change of EEG activity
toward a relative occipital diffusion of power, specifically distinguished by a
posterior dominance of middle and high frequencies.


PMID: 11956001  [PubMed - indexed for MEDLINE]

