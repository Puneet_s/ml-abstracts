
1. Sleep Med. 2012 Sep;13(8):999-1005. doi: 10.1016/j.sleep.2012.05.006. Epub 2012
Jul 2.

Time course of EEG slow-wave activity in pre-school children with sleep
disordered breathing: a possible mechanism for daytime deficits?

Biggs SN(1), Walter LM, Nisbet LC, Jackman AR, Anderson V, Nixon GM, Davey MJ,
Trinder J, Hoffmann R, Armitage R, Horne RS.

Author information: 
(1)The Ritchie Centre, Monash Institute of Medical Research, Monash University,
Melbourne, Australia. sarah.biggs@monash.edu

BACKGROUND: Daytime deficits in children with sleep disordered breathing (SDB)
are theorized to result from hypoxic insult to the developing brain or fragmented
sleep. Yet, these do not explain why deficits occur in primary snorers (PS). The 
time course of slow wave EEG activity (SWA), a proxy of homeostatic regulation
and cortical maturation, may provide insight.
METHODS: Clinical and control subjects (N=175: mean age 4.3±0.9 y: 61% male)
participated in overnight polysomnography (PSG). Standard sleep scoring and power
spectral analyses were conducted on EEG (C4/A1; 0.5-<3.9Hz). Univariate ANOVA's
evaluated group differences in sleep stages and respiratory parameters.
Repeated-measures ANCOVA evaluated group differences in the time course of SWA.
RESULTS: Four groups were classified: controls (OAHI ≤ 1 event/h; no clinical
history); PS (OAHI ≤ 1 event/h; clinical history); mild OSA (OAHI=1-5 events/h); 
and moderate to severe OSA (MS OSA: OAHI>5 events/h). Group differences were
found in the percentage of time spent in NREM Stages 1 and 4 (p<0.001) and in the
time course of SWA. PS and Mild OSA children had higher SWA in the first NREM
period than controls (p<0.05). All SDB groups had higher SWA in the fourth NREM
period (p<0.01).
CONCLUSIONS: These results suggest enhanced sleep pressure but impaired
restorative sleep function in pre-school children with SDB, providing new
insights into the possible mechanism for daytime deficits observed in all
severities of SDB.

Copyright © 2012 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.sleep.2012.05.006 
PMID: 22763016  [PubMed - indexed for MEDLINE]

