
1. Sleep Med. 2012 Sep;13(8):1006-12. doi: 10.1016/j.sleep.2012.05.011. Epub 2012
Jul 4.

Iron deficiency anemia in infancy exerts long-term effects on the tibialis
anterior motor activity during sleep in childhood.

Peirano P(1), Algarin C, Chamorro R, Manconi M, Lozoff B, Ferri R.

Author information: 
(1)Laboratory of Sleep and Functional Neurobiology, Institute of Nutrition and
Food Technology, University of Chile, Santiago, Chile. ppeirano@inta.cl

OBJECTIVES: To explore the eventual connection between iron deficiency anemia
(IDA) in infancy and altered leg movements during sleep in a 10-year follow-up
study in children who did or did not have IDA in infancy.
SUBJECTS AND METHODS: Polysomnographic studies were performed in 32 10-year-old
children (13 females and 19 males) who had IDA in infancy and 26 peers (10
females and 16 males) who were nonanemic controls. The time structure of their
polysomnographically recorded leg movements (LM) was analyzed by means of an
approach particularly able to consider their quantity, periodicity, and
distribution during the night.
RESULTS: All LM indexes and those related to periodic LM during sleep (PLMS) were
slightly higher in the former IDA group than in the control group, but not always
significant. The Periodicity index during NREM sleep was higher and was reflected
by a small but significant increase in PLMS separated by 10-50s intervals. PLMS
index tended to be higher in former IDA children than in controls throughout the 
whole night.
CONCLUSION: The association between IDA in infancy, despite iron therapy, and
PLMS in childhood could lead to new research in this area. Indeed, transient
infantile IDA, a common nutritional problem among human infants, may turn out to 
be important for understanding the mechanisms of PLMS or restless legs syndrome, 
which are common in adulthood.

Copyright © 2012. Published by Elsevier B.V.

DOI: 10.1016/j.sleep.2012.05.011 
PMCID: PMC3863761
PMID: 22770698  [PubMed - indexed for MEDLINE]

