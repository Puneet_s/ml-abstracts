
1. Physiol Behav. 2008 Jan 28;93(1-2):50-8. Epub 2007 Aug 2.

Sleep deprivation in the pigeon using the Disk-Over-Water method.

Newman SM(1), Paletz EM, Rattenborg NC, Obermeyer WH, Benca RM.

Author information: 
(1)Neuroscience Training Program, University of Wisconsin-Madison, 6001 Research 
Park Boulevard, Madison WI 53719, USA.

A well-defined sleep deprivation (SD) syndrome has been observed in studies with 
rats under conditions of severe sleep loss on the Disk-Over-Water (DOW)
apparatus. Observation of the sleep deprivation syndrome across taxa would assist
in the elucidation of the function of sleep. In the present study, the effects of
total sleep deprivation were assessed in pigeons, a biologically relevant choice 
given that birds are the only non-mammalian taxon known to exhibit unequivocal
rapid-eye-movement (REM) sleep and non-REM (NREM) sleep. Pigeons were deprived of
sleep for 24-29 days on the DOW by rotating the disk and requiring them to walk
whenever sleep was initiated. Control (C) birds were also housed on the DOW and
required to walk only when the deprived (D) birds were required to walk due to
sleep initiation. NREM and REM sleep amounts were reduced from baseline during
the deprivation for both D and C birds, although D birds obtained less NREM sleep
than controls. Across the deprivation, D birds had their total sleep reduced by
54% of baseline (scored in 4 s epochs), whereas previous studies in rats on the
DOW reported total sleep reduction of as much as 91% (scored in 30 s epochs).
Pigeons proved to be more resistant to sleep deprivation by the DOW method and
were much more difficult to deprive over the course of the experiment. Overall,
the pigeons showed recovery sleep patterns similar to those seen in rats; i.e.,
rebound sleep during recovery was disproportionately composed of REM sleep. They 
did not, however, show the obvious external physical signs of the SD syndrome nor
the large metabolic and thermoregulatory changes associated with the syndrome.
The DOW method was thus effective in producing sleep loss in the pigeon, but was 
not as effective as it is in rats. The absence of the full SD syndrome is
discussed in the context of limitations of the DOW apparatus and the possibility 
of species-specific adaptations that birds may possess to withstand or adapt to
conditions of limited sleep opportunity.

DOI: 10.1016/j.physbeh.2007.07.012 
PMID: 17765274  [PubMed - indexed for MEDLINE]

