
1. Sleep. 1989 Apr;12(2):157-66.

Nightcap: a home-based sleep monitoring system.

Mamelak A(1), Hobson JA.

Author information: 
(1)Laboratory of Neurophysiology, Harvard Medical School, Boston, Massachusetts
02115.

In an attempt to offer a home-based adjunct to traditional sleep laboratory
methods, we developed a system to monitor sleep, and to predict algorithmically
non-rapid-eye-movement (NREM) and rapid-eye-movement (REM) sleep states, using
eye and body motility as the only parameters. Eye movement was measured using a
strain gauge transducer applied to the eyelid of subjects, while body movement
was measured using a piezo-ceramic phono cartridge. Both transducers were mounted
on a tennis headband, along with electronics that amplified, filtered, and
digitized the signals. Digital pulse signals were input to a portable computer in
minute-long epochs, and state-predicting algorithms were run based on this
motility data. Four subjects were monitored in the sleep lab with both our
headgear and standard polysomnography. Hand-scored sleep records were compared
with those predicted by computer algorithms. Algorithm-predicted states agreed
with hand-scored ones an average of 85.57% (SEM +/- 1.7%). Mean values for sleep 
onset and REM latency were within 1.6 and 10.8 min of polysomnographic records,
respectively. These results are encouraging, and suggest that this system could
provide a comfortable, subject operable, and inexpensive method for the
evaluation of sleep at home.


PMID: 2711091  [PubMed - indexed for MEDLINE]

