
1. J Neurophysiol. 1983 Nov;50(5):1098-107.

Consistency and signal strength of respiratory neuronal activity.

Orem J, Dick T.

The concerns of this study are taxonomic. We demonstrate a defining
characteristic of respiratory neuronal activity. This characteristic is the
degree or size of the respiratory component in the activity of a respiratory
cell. The essential feature of respiratory activity is that it occurs in phase
with some portion of the respiratory cycle. Therefore, neuronal activity can be
arranged within a matrix in which the columns are fractions of the respiratory
cycle and the rows are breaths and, as Netick and Orem (10) have shown, this
matrix can be analyzed with an analysis of variance to determine whether the
activity contains a respiratory component. However, the analysis of variance
indicates nothing about the size of a statistically significant respiratory
component. We hypothesized that the size of the respiratory component in the
activity of different respiratory cells differed among cells but was a stable
characteristic of any given cell. The index used to quantify the degree of
respiratory activity was eta 2, the proportion of the total variance of the
activity of a respiratory neuron consisting of the variance occurring across
fractions of the respiratory cycle. This index theoretically depends on a) the
range of activity levels across a respiratory cycle and the dispersion of
activity levels over this range (parameters signifying the strength of the
respiratory signal) and b) the variability in the activity of the cell across
breaths (a parameter signifying the consistency of the respiratory activity).
Theoretically, eta 2 values can vary from 0.0 to 1.0 indicating, respectively,
that none or all of the variability in the activity of a cell across breaths is
accounted for by a respiratory effect. eta 2 was used to analyze the size of the 
respiratory component in the activity of 32 medullary respiratory neurons
recorded during nonrapid eye movement (NREM) sleep in chronic cats. These
different respiratory cells had activity patterns with eta 2 values ranging from 
0.1 to 0.9. However, the activity of a given cell produced eta 2 values that were
consistent from sample to sample. Therefore, the eta 2 value of the activity of a
cell was a defining, stable characteristic of that cell. The eta 2 values of the 
activity of the cells were strongly correlated with the consistency of their
discharge pattern from breath to breath (r x,y = 0.975).(ABSTRACT TRUNCATED AT
400 WORDS)


PMID: 6644361  [PubMed - indexed for MEDLINE]

