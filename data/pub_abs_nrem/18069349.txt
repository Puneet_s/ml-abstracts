
1. Glas Srp Akad Nauka Med. 2007;(49):1-6.

[Normal sleep].

[Article in Serbian]

Susić V.

Sleep represents organized complex behavior necessary and vital for the survival 
of the species. It is reversible, internally regulated and homeostatically
controlled process. Sleep consists of two separate states designated as NREM and 
REM sleep. NREM sleep has four sleep states (1 through 4) easily defined by the
PSG and EEG components. REM sleep consists of tonic and phasic components. The
tonic component of REM sleep by default includes the duration while phasic
component consists of clusters of rapid eye movement, muscle twitches and PGO
activity. The two states of sleep differ fundamentally both from one another as
well as from the state of wakefulness. NREM and REM sleep is organized in sleep
cycles with a typical duration between 90 and 110 minutes. Approximately 4 to 6
cycles emerge during the night with the REM episodes being prolonged towards the 
morning. About 70-80% of sleep process belongs to NREM and 20-25% to REM sleep.
Normal aging carries the reduction in slow high-voltage activity (delta sleep)
while REM sleep is of the relatively constant duration. Overall, sleep in elderly
is characterized by the increase in the number of sleep stage shifts, increase in
the number of awakenings and a shift towards the superficial sleep stages.


PMID: 18069349  [PubMed - indexed for MEDLINE]

