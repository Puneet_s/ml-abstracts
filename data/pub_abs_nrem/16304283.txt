
1. Chest. 2005 Nov;128(5):3350-7.

Rapid eye movement-related disordered breathing: clinical and polysomnographic
features.

Haba-Rubio J(1), Janssens JP, Rochat T, Sforza E.

Author information: 
(1)Sleep Laboratory, Department of Psychiatry, University Hospital, Geneva,
Switzerland.

OBJECTIVE: The existence of a rapid eye movement (REM)-specific sleep-disordered 
breathing (SDB) has been suggested based on the finding of an association between
sleepiness and respiratory disturbances confined primarily to REM sleep. The aim 
of the study was to define the frequency and the clinical and polysomnographic
features of REM SDB in a large clinical population.
METHODS: Anthropometric, clinical, and polysomnographic characteristics of 415
patients undergoing polysomnography for SDB were examined. For all patients the
apnea-hypopnea index (AHI) during total sleep time, the AHI during REM (AHI-REM),
and the AHI during non-REM sleep (AHI-NREM) were calculated. REM SDB was defined 
as an AHI-REM/AHI-NREM ratio >2. Patients were stratified according to the
severity of disease in mild, moderate, and severe cases. Daytime sleepiness was
assessed subjectively by the Epworth sleepiness scale (ESS), and objectively, in 
a subgroup of 228 patients, by the maintenance wakefulness test (MWT).
RESULTS: Of the initial sample, 36.4% of cases (n = 151) fulfilled the REM SDB
criteria. No significant differences in subjective complaints, medical history,
and drug intake were present between REM and non-REM SDB patients, and no
significant differences were found in ESS scores and mean sleep latency of the
MWT between groups. A high occurrence of REM SDB was found in mild (73.1%) and
moderate cases (47.2%). While in the entire group and in non-REM SDB patients a
strong male prevalence was found, the incidence of REM SDB was similar in men and
women.
CONCLUSION: Our results show that neither clinical history nor daytime sleepiness
differentiate patients with REM SDB from non-REM SDB patients. The disorder is
more common in mild and moderate cases; there is an equal incidence in women and 
men. These findings may suggest that REM-related SDB is a part of the spectrum of
SDB.

DOI: 10.1378/chest.128.5.3350 
PMID: 16304283  [PubMed - indexed for MEDLINE]

