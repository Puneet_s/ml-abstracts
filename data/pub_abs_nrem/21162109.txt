
1. Synapse. 2011 Jul;65(7):668-76. doi: 10.1002/syn.20898. Epub 2011 Feb 25.

Effects of gabapentin on brain hyperactivity related to pain and sleep
disturbance under a neuropathic pain-like state using fMRI and brain wave
analysis.

Takemura Y(1), Yamashita A, Horiuchi H, Furuya M, Yanase M, Niikura K, Imai S,
Hatakeyama N, Kinoshita H, Tsukiyama Y, Senba E, Matoba M, Kuzumaki N, Yamazaki
M, Suzuki T, Narita M.

Author information: 
(1)Department of Toxicology, Hoshi University School of Pharmacy and
Pharmaceutical Sciences, 2-4-41 Ebara, Shinagawa-ku, Tokyo 142-8501, Japan.

Neuropathic pain is the most difficult pain to manage in the pain clinic, and
sleep problems are common among patients with chronic pain including neuropathic 
pain. In the present study, we tried to visualize the intensity of pain by
assessing neuronal activity and investigated sleep disturbance under a
neuropathic pain-like state in mice using functional magnetic resonance imaging
(fMRI) and electroencephalogram (EEG)/electromyogram (EMG), respectively.
Furthermore, we investigated the effect of gabapentin (GBP) on these phenomena.
In a model of neuropathic pain, sciatic nerve ligation caused a marked decrease
in the latency of paw withdrawal in response to a thermal stimulus only on the
ipsilateral side. Under this condition, fMRI showed that sciatic nerve ligation
produced a significant increase in the blood oxygenation level-dependent (BOLD)
signal intensity in the pain matrix, which was significantly decreased 2 h after 
the i.p. injection of GBP. Based on the results of an EEG/EMG analysis, sciatic
nerve-ligated animals showed a statistically significant increase in wakefulness 
and a decrease in non-rapid eye movement (NREM) sleep during the light phase, and
the sleep disturbance was almost completely alleviated by a higher dose of GBP in
nerve-ligated mice. These findings suggest that neuropathic pain associated with 
sleep disturbance can be objectively assessed by fMRI and EEG/EMG analysis in
animal models. Furthermore, GBP may improve the quality of sleep as well as
control pain in patients with neuropathic pain.

Copyright © 2010 Wiley-Liss, Inc.

DOI: 10.1002/syn.20898 
PMID: 21162109  [PubMed - indexed for MEDLINE]

