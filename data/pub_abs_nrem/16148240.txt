
1. J Neurosci. 2005 Sep 7;25(36):8322-32.

Inhibition of serotonergic neurons in the nucleus paragigantocellularis lateralis
fragments sleep and decreases rapid eye movement sleep in the piglet:
implications for sudden infant death syndrome.

Darnall RA(1), Harris MB, Gill WH, Hoffman JM, Brown JW, Niblock MM.

Author information: 
(1)Department of Physiology, Dartmouth Medical School, Lebanon, New Hampshire
03756, USA. robert.a.darnall@hitchcock.org

Serotonergic receptor binding is altered in the medullary serotonergic nuclei,
including the paragigantocellularis lateralis (PGCL), in many infants who die of 
sudden infant death syndrome (SIDS). The PGCL receives inputs from many sites in 
the caudal brainstem and projects to the spinal cord and to more rostral areas
important for arousal and vigilance. We have shown previously that local
unilateral nonspecific neuronal inhibition in this region with GABA(A) agonists
disrupts sleep architecture. We hypothesized that specifically inhibiting
serotonergic activity in the PGCL would result in less sleep and heightened
vigilance. We analyzed sleep before and after unilaterally dialyzing the 5-HT1A
agonist (+/-)-8-hydroxy-2-(dipropylamino)-tetralin (8-OH-DPAT) into the
juxtafacial PGCL in conscious newborn piglets. 8-OH-DPAT dialysis resulted in
fragmented sleep with an increase in the number and a decrease in the duration of
bouts of nonrapid eye movement (NREM) sleep and a marked decrease in amount of
rapid eye movement (REM) sleep. After 8-OH-DPAT dialysis, there were decreases in
body movements, including shivering, during NREM sleep; body temperature and
heart rate also decreased. The effects of 8-OH-DPAT were blocked by local
pretreatment with
N-[2-[4-(2-methoxyphenyl)-1-piperazinyl]ethyl]-N-2-pyridinylcyclohexane-carboxami
de, a selective 5-HT1A antagonist. Destruction of serotonergic neurons with
5,7-DHT resulted in fragmented sleep and eliminated the effects of subsequent
8-OH-DPAT dialysis on REM but not the effects on body temperature or heart rate. 
We conclude that neurons expressing 5-HT1A autoreceptors in the juxtafacial PGCL 
are involved in regulating or modulating sleep. Abnormalities in the function of 
these neurons may alter sleep homeostasis and contribute to the etiology of SIDS.

DOI: 10.1523/JNEUROSCI.1770-05.2005 
PMID: 16148240  [PubMed - indexed for MEDLINE]

