
1. J Clin Neurophysiol. 1989 Apr;6(2):191-9.

A study of the interhemispheric correlation during sleep in elderly subjects.

Barcaro U(1), Bonanni E, Denoth F, Murri L, Navona C, Stefanini A.

Author information: 
(1)Istituto di Elaborazione della Informazione, C.N.R., Pisa, Italy.

The interhemispheric relationship during sleep in elderly subjects was studied
throughout the night by a minute-by-minute computation of two linear correlation 
coefficients between right and left EEG activities. One of these coefficients (X 
delta) related to the 1-4-Hz band activity, and the other (X sigma) to the
12.5-14.5-Hz band activity. For five of the six subjects examined, it was found
that the rapid-eye-movement (REM) mean values of both coefficients were
significantly different from the nonrapid-eye-movement (NREM) values. A
comparison between this elderly group and a control group of young subjects,
examined previously, did not reveal any significant shift, either for the REM or 
for the NREM mean values of the coefficients.


PMID: 2708518  [PubMed - indexed for MEDLINE]

