
1. J Sleep Res. 2011 Mar;20(1 Pt 1):57-72. doi: 10.1111/j.1365-2869.2010.00830.x.

Human non-rapid eye movement stage II sleep spindles are blocked upon spontaneous
K-complex coincidence and resume as higher frequency spindles afterwards.

Kokkinos V(1), Kostopoulos GK.

Author information: 
(1)Department of Physiology, Medical School, University of Patras, Rion, Greece.

The purpose of this study was to investigate a potential relation between the
K-complex (KC) and sleep spindles of non-rapid eye movement (NREM) stage II of
human sleep. Using 58 electroencephalogram electrodes, plus standard
electrooculogram and electromyogram derivations for sleep staging, brain activity
during undisturbed whole-night sleep was recorded in six young adults (one of
them participated twice). NREM stage II spindles (1256 fast and 345 slow) and
1131 singular generalized KCs were selected from all sleep cycles. The negative
peak of the KC, the positive peak of the KC (where applicable), and the prominent
negative wave peak of slow and fast spindles were marked as events of reference. 
Fast Fourier transform-based time-frequency analysis was performed over the
marked events, which showed that: (a) fast spindles that happen to coincide with 
KC are interrupted (100% of 403 cases) and in their place a slower rhythmic
oscillation often (80%) appears; and (b) spindles that are usually (72% of 1131) 
following KCs always have a higher frequency (by ∼1 Hz) than both the interrupted
spindles and the individual fast spindles that are not in any way associated with
a KC. This enhancement of spindle frequency could not be correlated to any of the
KC parameters studied. The results of this study reveal a consistent interaction 
between the KC and the sleep spindle during NREM stage II in human sleep.

© 2010 European Sleep Research Society.

DOI: 10.1111/j.1365-2869.2010.00830.x 
PMID: 20477951  [PubMed - indexed for MEDLINE]

