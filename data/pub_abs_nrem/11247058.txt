
1. Sleep. 2001 Mar 15;24(2):211-7.

Cardiorespiratory and autonomic interactions during snoring related resistive
breathing.

Mateika JH(1), Mitru G.

Author information: 
(1)Department of Biobehavioral Sciences, Teachers College, Columbia University,
New York, NY 10027, USA. JM477@columbia.edu

STUDY OBJECTIVES: We hypothesized that blood pressure (BP) is less during snoring
as compared to periods of non-snoring in non-apneic individuals. Furthermore, we 
hypothesized that this reduction may be accompanied by a simultaneous decrease in
sympathetic (SNSA) and parasympathetic (PNSA) nervous system activity and an
increase in heart rate (HR).
DESIGN: N/A.
SETTING: N/A.
PATIENTS OR PARTICIPANTS: N/A.
MEASUREMENTS: The variables mentioned above in addition to breathing frequency
were measured in 9 subjects during NREM sleep. In addition, the lowest systolic
(SBP) and diastolic blood pressure (DBP) during inspiration and the highest SBP
and DBP during expiration was determined breath-by-breath from segments selected 
from each NREM cycle. Heart rate variability was used as a marker of autonomic
nervous system activity.
RESULTS: Our results showed that BP during snoring decreased compared to
non-snoring and the breath-by-breath BP analysis suggested that this difference
may have been mediated by changes in intrathoracic pressure. In conjunction with 
the decrease in BP, SNSA decreased and HR increased however PNSA remained
constant. Thus, a decrease in PNSA was likely not the primary mechanism
responsible for the HR response.
CONCLUSIONS: We conclude that BP responses and SNSA during snoring are similar to
that reported previously in non-snoring individuals. However, the causal
mechanisms maybe different and manifested in other measures such as HR. Thus,
nocturnal cardiovascular and autonomic function maybe uniquely different in
non-apneic snoring individuals.


PMID: 11247058  [PubMed - indexed for MEDLINE]

