
1. J Clin Sleep Med. 2012 Jun 15;8(3):323-32. doi: 10.5664/jcsm.1928.

The AASM Scoring Manual four years later.

Grigg-Damberger MM(1).

Author information: 
(1)University of New Mexico School of Medicine, MSC 10 5620, One University of
NM, Albuquerque, New Mexico 87131-0001, USA. MGriggD@salud.unm.edu

PURPOSE OF REVIEW: Review published studies and critiques which evaluate the
impact and effects of the American Academy of Sleep Medicine (AASM) Sleep Scoring
Manual in the four years since its publication.
FINDINGS: USING THE AASM MANUAL RULES TO SCORE SLEEP AND EVENTS IN A
POLYSOMNOGRAM (PSG) RESULTS IN: (1) very large differences in apnea-hypopnea
indexes (AHI) when using the recommended and alternative rule for scoring
hypopneas in adults; (2) increases in NREM 1 and sleep stage shifts with
compensatory decreases in NREM 2 in children and adults when following rule
5.C.b. for ending NREM 2 sleep; (3) increases in NREM 3 in adults scoring slow
wave activity in the frontal EEG derivations; (4) improved interscorer
reliability; and (5) successfully identified fragmented sleep in children with
obstructive sleep apnea (OSA) from primary snorers or normal controls because
they had more NREM 1 and stage shifts using rule 5.C.b. Criticism of the Manual
most often cited: (1) two rules for scoring hypopneas; (2) alternative EEG
montage cancellation effects; (3) scoring stages 3 and 4 as NREM 3; and (4) too
few rules for scoring arousals and REM sleep without atonia.
SUMMARY: Four years have passed since the AASM Scoring Manual was published with 
far less criticism than those who developed it feared. The AASM Manual provides a
foundation upon which we all can build rules and methods which identify the
complexity of sleep and its disorders.

DOI: 10.5664/jcsm.1928 
PMCID: PMC3365093
PMID: 22701392  [PubMed - indexed for MEDLINE]

