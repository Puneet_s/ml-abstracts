
1. Eur J Neurosci. 2010 Feb;31(3):499-507. doi: 10.1111/j.1460-9568.2009.07062.x.
Epub 2010 Jan 25.

Basal ganglia control of sleep-wake behavior and cortical activation.

Qiu MH(1), Vetrivelan R, Fuller PM, Lu J.

Author information: 
(1)Department of Neurology, Beth Israel Deaconess Medical Center and Harvard
Medical School, Boston, MA, USA.

The basal ganglia (BG) are involved in numerous neurobiological processes that
operate on the basis of wakefulness, including motor function, learning, emotion 
and addictive behaviors. We hypothesized that the BG might play an important role
in the regulation of wakefulness. To test this prediction, we made cell
body-specific lesions in the striatum and globus pallidus (GP) using ibotenic
acid. We found that rats with striatal (caudoputamen) lesions exhibited a 14.95% 
reduction in wakefulness and robust fragmentation of sleep-wake behavior, i.e. an
increased number of state transitions and loss of ultra-long wake bouts (> 120
min). These lesions also resulted in a reduction in the diurnal variation of
sleep-wakefulness. On the other hand, lesions of the accumbens core resulted in a
26.72% increase in wakefulness and a reduction in non-rapid eye movement (NREM)
sleep bout duration. In addition, rats with accumbens core lesions exhibited
excessive digging and scratching. GP lesions also produced a robust increase in
wakefulness (45.52%), and frequent sleep-wake transitions and a concomitant
decrease in NREM sleep bout duration. Lesions of the subthalamic nucleus or the
substantia nigra reticular nucleus produced only minor changes in the amount of
sleep-wakefulness and did not alter sleep architecture. Finally, power spectral
analysis revealed that lesions of the striatum, accumbens and GP slowed down the 
cortical electroencephalogram. Collectively, our results suggest that the BG, via
a cortico-striato-pallidal loop, are important neural circuitry regulating
sleep-wake behaviors and cortical activation.

DOI: 10.1111/j.1460-9568.2009.07062.x 
PMCID: PMC3928571
PMID: 20105243  [PubMed - indexed for MEDLINE]

