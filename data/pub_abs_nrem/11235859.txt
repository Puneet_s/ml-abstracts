
1. Psychiatry Clin Neurosci. 2001 Feb;55(1):57-65.

Absence of sleep spindles in human medial and basal temporal lobes.

Nakabayashi T(1), Uchida S, Maehara T, Hirai N, Nakamura M, Arakaki H, Shimisu H,
Okubo Y.

Author information: 
(1)Department of Neuropsychiatry, Tokyo Medical and Dental University, Japan.

All-night recordings from subdural electrocorticographic (ECoG) electrodes on the
human medial and basal temporal lobes were analysed to examine spindling
activities during sleep. Subjects were three males and three females who were
candidates for neurosurgical treatments of partial epilepsy. Subdural electrodes 
were attached to the medial and basal temporal lobe cortices, allowing ECoG and
electroencephalogram from the scalp vertex (Cz EEG) to be recorded simultaneously
during all night sleep. In one case, subdural electrodes were attached also on
the parietal lobe. Fast Fourier transformation (FFT) analyses were performed on
the ECoG and Cz EEG signals. No organized sleep spindles or sigma band (12-16 Hz)
peaks in FFT power spectra were observed from the medial or basal temporal lobes 
of the non-epileptogenic hemispheres during non-rapid eye movement (NREM) sleep. 
In a case with parietal electrodes, organized spindle bursts were observed in
parietal signals synchronized with Cz spindles. Although delta band (0.3-3 Hz)
power from both the medial and basal temporal lobes fluctuated across each night 
as expected, sigma activity changed little. However, 14 Hz oscillatory bursts
were observed in the medial basal temporal lobe of epileptogenic hemisphere in
two cases and bilaterally in one case during not only NREM sleep but rapid eye
movement (REM) sleep and wakefulness. From the present study we conclude that
sleep spindle activities are absent in the medial and basal temporal lobes.
Fourteen Hz oscillatory bursts observed from the medial or basal temporal lobe in
some cases were not considered to be sleep spindles since they also appeared
during REM sleep and wakefulness. These waveforms could have originated due to
epileptic pathology, since they frequently appeared in epileptic regions.

DOI: 10.1046/j.1440-1819.2001.00785.x 
PMID: 11235859  [PubMed - indexed for MEDLINE]

