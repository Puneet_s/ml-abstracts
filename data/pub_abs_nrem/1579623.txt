
1. Psychopharmacology (Berl). 1992;106(4):497-501.

Effects of zolpidem and flunitrazepam on nocturnal sleep of women subjectively
complaining of insomnia.

Declerck AC(1), Ruwe F, O'Hanlon JF, Vermeeren A, Wauquier A.

Author information: 
(1)Epilepsy Center Kemphenhaghe, Department of EEG and Clinical Neurophysiology, 
Heeze, The Netherlands.

Erratum in
    Psychopharmacology (Berl) 1992;109(1-2):254.

Eighteen non-pregnant woman complaining about insomnia were polysomnographically 
investigated for 3 nights with weekly intervals. They received placebo, 2 mg
flunitrazepam or 10 mg zolpidem according to a cross-over double blind design.
The patients were selected by general practitioners on the basis of subjective
complaints. Zolpidem is a recently introduced short-acting imidazopyridine
hypnotic, binding to a subunit of the benzodiazepine 1 receptor. Flunitrazepam is
a well-known hypnotic, binding to both the benzodiazepine 1 and 2 receptor
subtypes. Objective recording did not substantiate the subjective complaint of
insomnia. Sleep patterns during placebo differed only little from that expected
from age matched healthy persons. Both flunitrazepam and zolpidem significantly
shortened sleep onset (5 min of continuous sleep beginning with NREM 1 sleep).
The sleep composition following flunitrazepam was characterized by an increase in
NREM 2, a prolongation of the time of REM sleep, a reduction of REM sleep and an 
increase in NREM 3-4 sleep during the first 2 h of sleep. The sleep composition
following zolpidem resembled more than seen in persons without sleep complaints. 
However, as compared to placebo, there was a decrease of the time spent awake
during sleep and an increase in NREM 3-4 during the first 2 of sleep.


PMID: 1579623  [PubMed - indexed for MEDLINE]

