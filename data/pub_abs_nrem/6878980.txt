
1. Sleep. 1983;6(2):108-20.

Breathing during sleep in normal young and elderly subjects: hypopneas, apneas,
and correlated factors.

Krieger J, Turlot JC, Mangin P, Kurtz D.

Polysomnograms were obtained from two groups of normal subjects (20 medical
students and 20 elderly persons). The recordings included the usual sleep
parameters (electroencephalograph, electrooculograph, and electromyograph of chin
muscles) and respiratory data obtained by means of a pneumotachygraph (tidal
volumes and respiratory frequency) and thoracic and abdominal strain gauges. In
the older group, sleep was more disturbed and respiratory events (hypopneas and
apneas) were more frequent both in non-rapid eye movement (NREM) and in REM
sleep. The apneas were predominantly of the obstructive type. A correspondence
analysis carried out on the 40 subjects showed that a high frequency of hypopneas
and obstructive apneas was linked to old age and age-related parameters
(disturbed sleep and obesity) both during NREM and REM sleep. A high frequency of
central apneas during light slow-wave sleep was linked to the male sex; a high
frequency of central apneas during REM sleep was linked to the amount of REM
sleep. No relationship could be demonstrated between the decrease of minute
ventilation from wakefulness to steady sleep and the incidence of sleep
respiratory events.


PMID: 6878980  [PubMed - indexed for MEDLINE]

