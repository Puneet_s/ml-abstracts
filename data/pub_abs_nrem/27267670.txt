
1. J Clin Child Adolesc Psychol. 2016 Jun 6:1-10. [Epub ahead of print]

Stage 2 Sleep EEG Sigma Activity and Motor Learning in Childhood ADHD: A Pilot
Study.

Saletin JM(1,)(2), Coon WG(2,)(3), Carskadon MA(1,)(2,)(4).

Author information: 
(1)a Department of Psychiatry and Human Behavior , Alpert Medical School of Brown
University. (2)b Sleep for Science Research Laboratory , E.P. Bradley Hospital.
(3)c National Center for Adaptive Neurotechnologies , New York State Department
of Health. (4)d Centre for Sleep Research , University of South Australia.

Attention deficit hyperactivity disorder (ADHD) is associated with deficits in
motor learning and sleep. In healthy adults, overnight improvements in motor
skills are associated with sleep spindle activity in the sleep
electroencephalogram (EEG). This association is poorly characterized in children,
particularly in pediatric ADHD. Polysomnographic sleep was monitored in 7
children with ADHD and 14 typically developing controls. All children were
trained on a validated motor sequence task (MST) in the evening with retesting
the following morning. Analyses focused on MST precision (speed-accuracy
trade-off). NREM Stage 2 sleep EEG power spectral analyses focused on
spindle-frequency EEG activity in the sigma (12-15 Hz) band. The ADHD group
demonstrated a selective decrease in power within the sigma band. Evening MST
precision was lower in ADHD, yet no difference in performance was observed
following sleep. Moreover, ADHD status moderated the association between slow
sleep spindle activity (12-13.5 Hz) and overnight improvement; spindle-frequency 
EEG activity was positively associated with performance improvements in children 
with ADHD but not in controls. These data highlight the importance of sleep in
supporting next-day behavior in ADHD while indicating that differences in sleep
neurophysiology may contribute to deficits in this population.

DOI: 10.1080/15374416.2016.1157756 
PMID: 27267670  [PubMed - as supplied by publisher]

