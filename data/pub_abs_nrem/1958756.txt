
1. Biol Neonate. 1991;60 Suppl 1:30-5.

REM sleep determined using in utero penile tumescence in the human fetus at term.

Koyanagi T(1), Horimoto N, Nakano H.

Author information: 
(1)Maternity and Perinatal Care Unit, Kyushu University Hospital, Fukuoka, Japan.

To assess how REM/NREM periods can be linked to the determination of the REM/NREM
sleep and/or awake state, we have investigated the relationship between the
REM/NREM periods and penile tumescence in the human fetus. This study was made on
11 male fetuses from 36 to 41 weeks of gestation. Eye movement and penile
tumescence were simultaneously examined with an observation window of 60 min,
using two separate real-time ultrasound systems. The mean percentage of the total
duration of penile tumescence during the REM period and that during the NREM
period was 77.7 and 15.8%, respectively. In all cases, the total duration of
penile tumescence during the REM period against REM duration was greater than the
duration of tumescence during the NREM period against NREM duration with
statistical significance. This indicates that the fetal penile tumescence is
strongly associated with REM period and the REM period containing penile
tumescence in the human fetus can be considered equivalent to REM sleep in utero.
In addition, the finding that there exists a part of the REM period lacking
penile tumescence suggests the possible origin of the awake state, brought about 
by advances in fetal development at this stage of gestation.


PMID: 1958756  [PubMed - indexed for MEDLINE]

