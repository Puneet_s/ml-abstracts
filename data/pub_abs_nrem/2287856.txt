
1. Sleep. 1990 Oct;13(5):449-55.

Memory sources of REM and NREM dreams.

Cavallero C(1), Foulkes D, Hollifield M, Terry R.

Author information: 
(1)Dipartimento di Psicologia, Università di Bologna, Italy.

Sixteen male volunteers slept 4 nonconsecutive nights each in a sleep laboratory.
They were awakened for one dream report per night. Awakenings were made, in
counterbalanced order, from early-night and late-night rapid-eye movement (REM)
and non-REM (NREM) sleep. Following dream reporting, subjects were asked to
identify memory sources of their dream imagery. Two independent judges reliably
rated mentation reports for temporal units and categorized memory sources as
autobiographical episodes, abstract self-references, or semantic knowledge. We
replicated earlier findings that semantic knowledge is more frequently mentioned 
as a dream source for REM than for NREM reports. However, with controls for
length of reports, the REM-NREM difference disappeared, indicating that the stage
difference in memory sources was not independent of stage difference in report
lengths. There was a significant effect of time of night on source class, but
only in REM sleep: Both without and with controls for report length, more
semantic sources were cited for late than for early REM dreams.


PMID: 2287856  [PubMed - indexed for MEDLINE]

