
1. Neuropsychopharmacology. 1991 Nov;5(3):167-76.

Nonlinear analysis of EEG sleep states.

Ehlers CL(1), Havstad JW, Garfinkel A, Kupfer DJ.

Author information: 
(1)Department of Neuropharmacology, Research Institute of the Scripps Clinic, La 
Jolla, CA 92037.

Recent advances in the field of nonlinear dynamics have provided new conceptual
models as well as novel analytical techniques applicable to
neuropsychopharmacologic studies. One measurement technique that has been
recently developed in an attempt to characterize nonlinear systems in physics and
biology is the estimation of dimension. Dimension may be seen as a measure of the
information required to describe the current behavior of a system. We have
applied these techniques to the analysis of the sleep EEG, and have found that
the dimension of rapid eye movement (REM) sleep is significantly higher than
non-rapid-eye-movement (NREM) sleep. These data support a preliminary hypothesis 
that EEG dimension may represent the number of nonlinear modes activated in the
brain. Thus, sleep states of low arousal or low input would be envisioned as
having low dimension (e.g., slow-wave sleep) whereas increased arousal (REM)
would activate more nonlinear modes. Although more investigations will be needed 
to explore this hypothesis, these studies suggest that further development of
nonlinear approaches to the analysis of brain systems are likely to generate new 
clinical measures as well as new ways of viewing brain electrical function.


PMID: 1755932  [PubMed - indexed for MEDLINE]

