
1. Am J Physiol. 1989 Jan;256(1 Pt 2):R106-11.

Bright morning light advances the human circadian system without affecting NREM
sleep homeostasis.

Dijk DJ(1), Beersma DG, Daan S, Lewy AJ.

Author information: 
(1)Department of Biological Psychiatry, University of Groningen, The Netherlands.

Eight male subjects were exposed to either bright light or dim light between 0600
and 0900 h for 3 consecutive days each. Relative to the dim light condition, the 
bright light treatment advanced the evening rise in plasma melatonin and the time
of sleep termination (sleep onset was held constant) for an average approximately
1 h. The magnitude of the advance of the plasma melatonin rise was dependent on
its phase in dim light. The reduction in sleep duration was at the expense of
rapid-eye-movement (REM) sleep. Spectral analysis of the sleep
electroencephalogram (EEG) revealed that the advance of the circadian pacemaker
did not affect EEG power densities between 0.25 and 15.0 Hz during either non-REM
or REM sleep. The data show that shifting the human circadian pacemaker by 1 h
does not affect non-REM sleep homeostasis. These findings are in accordance with 
the predictions of the two-process model of sleep regulation.


PMID: 2912203  [PubMed - indexed for MEDLINE]

