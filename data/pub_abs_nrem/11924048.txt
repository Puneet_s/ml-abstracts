
1. Rev Neurol (Paris). 2001 Nov;157(11 Pt 2):S87-91.

[Circadian and ultradian cycles in narcolepsy].

[Article in French]

Espa F(1), Besset A.

Author information: 
(1)INSERM E-9930, Hôpital La Colombière, Montpellier Service de Neurologie B,
Hôpital Gui de Chauliac, Montpellier.

In narcolepsy, homeostatic process is preserved while sleep/wake circadian
process is impaired. Other circadian components (body temperature, endocrine
secretions, subjective sleepiness) are preserved. This circadian system weakness 
permits the occurrence of a very strong ultradian component, modulating
sleep/wake rhythm. So, a 4 hour ultradian rhythmicity of Slow Wave Activity, and 
a 2 hour NREM/REM cycle longer than in normal subjects, has been evidenced in
narcoleptic patients. These circadian and ultradian alterations can explain a
major part of the narcoleptic symptoms.


PMID: 11924048  [PubMed - indexed for MEDLINE]

