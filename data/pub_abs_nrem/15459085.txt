
1. Cereb Cortex. 2005 Jul;15(7):877-84. Epub 2004 Sep 30.

Spectral power time-courses of human sleep EEG reveal a striking discontinuity at
approximately 18 Hz marking the division between NREM-specific and
wake/REM-specific fast frequency activity.

Merica H(1), Fortune RD.

Author information: 
(1)Laboratoire de Sommeil et de Neurophysiologie, Hôpitaux Universitaires de
Genève, Belle Idée, 1225 Chêne-Bourg, Geneva, Switzerland. helli.merica@hcuge.ch

Spectral power time-courses over the ultradian cycle of the sleep
electroencephalogram (EEG) provide a useful window for exploring the temporal
correlation between cortical EEG and sub-cortical neuronal activities. Precision 
in the measurement of these time-courses is thus important, but it is hampered by
lacunae in the definition of the frequency band limits that are in the main based
on wake EEG conventions. A frequently seen discordance between the shape of the
beta power time-course across the ultradian cycle and that reported for the
sequential mean firing rate of brainstem-thalamic activating neurons invites a
closer examination of these band limits, especially since the sleep EEG
literature indicates in several studies an intriguing non-uniformity of
time-course comportment across the traditional beta band frequencies. We ascribe 
this tentatively to the sharp reversal of slope we have seen at approximately 18 
Hz in our data and that of others. Here, therefore, using data for the first four
ultradian cycles from 18 healthy subjects, we apply several criteria based on
changes in time-course comportment in order to examine this non-uniformity as we 
move in 1 Hz bins through the frequency range 14-30 Hz. The results confirm and
describe in detail the striking discontinuity of shape at around 18 Hz, with only
the upper range (18-30 Hz) displaying a time-course similar to that of the
firing-rate changes measured in brainstem activating neurons and acknowledged to 
engender states of brain activation. Fast frequencies in the lower range (15-18
Hz), on the other hand, are shown to be specific to non-rapid-eye-movement sleep.
Splitting the beta band at approximately 18 Hz therefore permits a significant
improvement in EEG measurement and a more precise correlation with cellular
activity.

DOI: 10.1093/cercor/bhh192 
PMID: 15459085  [PubMed - indexed for MEDLINE]

