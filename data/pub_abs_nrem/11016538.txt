
1. Neurobiol Aging. 2000 Sep-Oct;21(5):689-93.

Age-dependent changes in recovery sleep after 48 hours of sleep deprivation in
rats.

Mendelson WB(1), Bergmann BM.

Author information: 
(1)Department of Psychiatry, The University of Chicago, IL 60637, USA.
wmendels@yoda.bsd.uchicago.edu

To characterize possible changes in homeostatic regulation of sleep with aging,
we have examined sleep stages during recovery sleep after 48 h of sleep
deprivation in young (3 months), middle aged (12 months), and old (24 months)
rats. It was found that young and middle aged, in contrast to old rats, had large
(21-24%) increases in total sleep time during recovery sleep; the old rats
experienced a quantitatively small (8%) but significant rise in total sleep. NREM
sleep increased significantly during the recovery period in young and middle
aged, but not older rats. High voltage NREM sleep (HS2) declined by 30% during
recovery in the young animals, but remained unchanged compared to baseline in the
middle aged and old animals. The young and middle aged rats had increases in REM 
sleep during recovery compared to their baseline by 96% and 93%, respectively,
which was significantly greater than a 65% increase during recovery in the old
rats. Increases in total sleep and REM sleep during recovery were largely
confined to the first 6 h in young and middle aged rats, but maxima for the old
rats occurred in the second 6 h.


PMID: 11016538  [PubMed - indexed for MEDLINE]

