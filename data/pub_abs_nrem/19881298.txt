
1. Biol Pharm Bull. 2009 Nov;32(11):1862-5.

Effects of anti-dementia drugs on morphine-induced somnolence.

Ishida T(1), Suga A, Akagi M, Kamei C.

Author information: 
(1)Department of Medicinal Pharmacology, Okayama University Graduate School of
Medicine, Dentistry and Pharmaceutical Sciences, Tsushima-Naka, Kita-ku, Okayama 
700-8530, Japan.

The present study was undertaken to investigate the characteristics of morphine
in rat sleep patterns and also the effects of donepezil and memantine on
somnolence caused by morphine. Electrodes were chronically implanted into the
cortex and dorsal neck muscle of rats for electroencephalogram (EEG) and
electromyogram (EMG) recordings, respectively. EEG and EMG were recorded with an 
electroencephalograph. SleepSigh ver.2.0 was used to analyse the sleep-wake
state. Total times of wakefulness, non-rapid eye movement (NREM) sleep and rapid 
eye movement (REM) sleep were measured from 10:00 to 16:00. Morphine at a high
dose caused a significant decrease in sleep latency and total REM sleep time,
although the drug at low doses caused significant increases in sleep latency and 
total awake time, and a significant decrease in NREM sleep time. Donepezil,
memantine and methylphenidate antagonized the decrease in sleep latency caused by
morphine. From these findings, it can be concluded that morphine caused
somnolence, and donepezil and memantine are useful for somnolence caused by
morphine, similar to methylphenidate.


PMID: 19881298  [PubMed - indexed for MEDLINE]

