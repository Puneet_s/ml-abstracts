
1. J Physiol. 1999 Dec 15;521 Pt 3:679-90.

Adenosinergic modulation of rat basal forebrain neurons during sleep and waking: 
neuronal recording with microdialysis.

Alam MN(1), Szymusiak R, Gong H, King J, McGinty D.

Author information: 
(1)Department of Psychology, University of California, Los Angeles 90033, USA.

1. The cholinergic system of the basal forebrain (BF) is hypothesized to play an 
important role in behavioural and electrocortical arousal. Adenosine has been
proposed as a sleep-promoting substance that induces sleep by inhibiting
cholinergic neurons of the BF and brainstem. However, adenosinergic influences on
the activity of BF neurons in naturally awake and sleeping animals have not been 
demonstrated. 2. We recorded the sleep-wake discharge profile of BF neurons and
simultaneously assessed adenosinergic influences on wake- and sleep-related
activity of these neurons by delivering adenosinergic agents adjacent to the
recorded neurons with a microdialysis probe. Discharge rates of BF neurons were
recorded through two to three sleep-wake episodes during baseline (artificial
cerebrospinal fluid perfusion), and after delivering an adenosine transport
inhibitor (s-(p-nitrobenzyl)-6-thioinosine; NBTI), or exogenous adenosine, or a
selective adenosine A1 receptor antagonist (8-cyclopentyl-1, 3-dimethylxanthine; 
CPDX). 3. NBTI and adenosine decreased the discharge rate of BF neurons during
both waking and non-rapid eye movement (NREM) sleep. In contrast, CPDX increased 
the discharge rate of BF neurons during both waking and NREM sleep. These results
suggest that in naturally awake and sleeping animals, adenosine exerts tonic
inhibitory influences on BF neurons, supporting the hypothesized role of
adenosine in sleep regulation. 4. However, in the presence of exogenous
adenosine, NBTI or CPDX, BF neurons retained their wake- and sleep-related
discharge patterns, i.e. still exhibited changes in discharge rate during
transitions between waking and NREM sleep. This suggests that other
neurotransmitters/neuromodulators also contribute to the sleep-wake discharge
modulation of BF neurons.


PMCID: PMC2269685
PMID: 10601498  [PubMed - indexed for MEDLINE]

