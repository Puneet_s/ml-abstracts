
1. Arch Ital Biol. 2015 Jun-Sep;153(2-3):194-203. doi: 10.12871/0003982920152344.

Can sleep microstructure improve diagnosis of OSAS? Integrative information from 
CAP parameters.

Milioli G(1), Bosi M, Grassi A, Riccardi S, Terzano MG, Cortelli P, Poletti V,
Parrino L.

Author information: 
(1)Sleep Disorders Center, Dept of Neurosciences, University of Parma, Italy.
Email: giulia.milioli@gmail.com.

PURPOSE: The scoring of American Academy of Sleep Medicine (AASM) arousal is
mandatory for the definition of respiratory event-related arousal (RERA). However
there are other EEG activation phenomena, such as A phases of cyclic alternating 
pattern (CAP) which are associated with respiratory events in non rapid eye
movements (NREM) sleep. This study aims at quantifying the additional value of
CAP for the definition of respiratory events and sleep alterations in OSAS.
METHODS: Analysis of polysomnographic recordings from nineteen OSAS patients was 
carried out. Scoring was focused on investigation of the cerebral response to
flow limitation (FL) events. For this purpose we used both CAP rules and AASM
arousal criteria.
MAIN RESULTS: While no difference was demonstrated in the arousal index between
mild and moderate-severe OSAS patients, CAP time showed a progressive enhancement
from normal subjects (152.5±20.76) to mild (180.64±34.76) and moderate-severe
(282.27±58.02) OSAS patients. In NREM sleep, only 41.1% of FL events met the
criteria for the definition of RERA, while, 75.5% of FL events ended with a CAP A
phase and most FL CAP (69.1%) terminated with a CAP phase A3 subtype.
CONCLUSIONS: Our data indicate that the RERA scoring has a limited accuracy in
the detection of FL events. In NREM sleep, CAP rules provided more information
than AASM arousal for the definition of respiratory events and sleep alterations 
in OSAS.


PMID: 26742673  [PubMed - indexed for MEDLINE]

