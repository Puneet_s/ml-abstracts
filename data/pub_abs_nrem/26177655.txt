
1. J Pharmacol Exp Ther. 2015 Sep;354(3):471-82. doi: 10.1124/jpet.115.225466. Epub 
2015 Jul 15.

Characterization of JNJ-42847922, a Selective Orexin-2 Receptor Antagonist, as a 
Clinical Candidate for the Treatment of Insomnia.

Bonaventure P(1), Shelton J(2), Yun S(2), Nepomuceno D(2), Sutton S(2), Aluisio
L(2), Fraser I(2), Lord B(2), Shoblock J(2), Welty N(2), Chaplan SR(2), Aguilar
Z(2), Halter R(2), Ndifor A(2), Koudriakova T(2), Rizzolio M(2), Letavic M(2),
Carruthers NI(2), Lovenberg T(2), Dugovic C(1).

Author information: 
(1)Janssen Research & Development, LLC, San Diego, California
Pbonave1@its.jnj.com CDugovic@its.jnj.com. (2)Janssen Research & Development,
LLC, San Diego, California.

Dual orexin receptor antagonists have been shown to promote sleep in various
species, including humans. Emerging research indicates that selective orexin-2
receptor (OX2R) antagonists may offer specificity and a more adequate sleep
profile by preserving normal sleep architecture. Here, we characterized
JNJ-42847922
([5-(4,6-dimethyl-pyrimidin-2-yl)-hexahydro-pyrrolo[3,4-c]pyrrol-2-yl]-(2-fluoro-
6-[1,2,3]triazol-2-yl-phenyl)-methanone), a high-affinity/potent OX2R antagonist.
JNJ-42847922 had an approximate 2-log selectivity ratio versus the human orexin-1
receptor. Ex vivo receptor binding studies demonstrated that JNJ-42847922 quickly
occupied OX2R binding sites in the rat brain after oral administration and
rapidly cleared from the brain. In rats, single oral administration of
JNJ-42847922 (3-30 mg/kg) during the light phase dose dependently reduced the
latency to non-rapid eye movement (NREM) sleep and prolonged NREM sleep time in
the first 2 hours, whereas REM sleep was minimally affected. The reduced sleep
onset and increased sleep duration were maintained upon 7-day repeated dosing (30
mg/kg) with JNJ-42847922, then all sleep parameters returned to baseline levels
following discontinuation. Although the compound promoted sleep in wild-type
mice, it had no effect in OX2R knockout mice, consistent with a specific
OX2R-mediated sleep response. JNJ-42847922 did not increase dopamine release in
rat nucleus accumbens or produce place preference in mice after subchronic
conditioning, indicating that the compound lacks intrinsic motivational
properties in contrast to zolpidem. In a single ascending dose study conducted in
healthy subjects, JNJ-42847922 increased somnolence and displayed a favorable
pharmacokinetic and safety profile for a sedative/hypnotic, thus emerging as a
promising candidate for further clinical development for the treatment of
insomnia.

Copyright © 2015 by The American Society for Pharmacology and Experimental
Therapeutics.

DOI: 10.1124/jpet.115.225466 
PMID: 26177655  [PubMed - indexed for MEDLINE]

