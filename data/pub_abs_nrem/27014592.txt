
1. MethodsX. 2016 Feb 21;3:144-55. doi: 10.1016/j.mex.2016.02.003. eCollection 2016.

SegWay: A simple framework for unsupervised sleep segmentation in experimental
EEG recordings.

Yaghouby F(1), Sunderam S(1).

Author information: 
(1)Department of Biomedical Engineering, University of Kentucky, Lexington, KY,
USA.

Sleep analysis in animal models typically involves recording an
electroencephalogram (EEG) and electromyogram (EMG) and scoring vigilance state
in brief epochs of data as Wake, REM (rapid eye movement sleep) or NREM (non-REM)
either manually or using a computer algorithm. Computerized methods usually
estimate features from each epoch like the spectral power associated with
distinctive cortical rhythms and dissect the feature space into regions
associated with different states by applying thresholds, or by using
supervised/unsupervised statistical classifiers; but there are some factors to
consider when using them:•Most classifiers require scored sample data, elaborate 
heuristics or computational steps not easily reproduced by the average sleep
researcher, who is the targeted end user.•Even when prediction is reasonably
accurate, small errors can lead to large discrepancies in estimates of important 
sleep metrics such as the number of bouts or their duration.•As we show here,
besides partitioning the feature space by vigilance state, modeling transitions
between the states can give more accurate scores and metrics. An unsupervised
sleep segmentation framework, "SegWay", is demonstrated by applying the algorithm
step-by-step to unlabeled EEG recordings in mice. The accuracy of sleep scoring
and estimation of sleep metrics is validated against manual scores.

DOI: 10.1016/j.mex.2016.02.003 
PMCID: PMC4792881
PMID: 27014592  [PubMed]

