
1. Neuropsychopharmacology. 1993 Aug;9(1):41-8.

Ketamine administration during waking increases delta EEG intensity in rat sleep.

Feinberg I(1), Campbell IG.

Author information: 
(1)Psychiatry Service, VA Medical Center, Martinez, California.

Ketamine is known to increase the metabolic rate of limbic brain structures. We
exploited this action to test a hypothesis of the homeostatic model of delta
sleep: that an increase in the waking metabolic rate of plastic neuronal systems 
would increase delta electroencephalographic (EEG) intensity in subsequent
nonrapid-eye-movement (NREM) sleep. In separate experiments, we gave
intraperitoneal injections of ketamine to Sprague-Dawley rats of either 15, 25,
or 50 mg/kg (0.055, 0.091, 0.18 mmol/kg) three times, at approximately hourly
intervals, during the dark (waking) period; the last dose was given 4 to 5 hours 
before onset of the light (sleep) period. After ketamine, both NREM duration and 
delta EEG intensity (amplitude and incidence) increased significantly over
control (saline injections) levels. The magnitude of this increase places it
among the largest pharmacologically induced stimulations of delta sleep yet
observed. The interpretation of this effect is complicated by the fact that
ketamine produces widespread metabolic changes throughout the brain and it also
acts on several receptor classes. However, since ketamine's major action is
noncompetitive blockade of the cation channel gated by the N-methyl-D-aspartate
receptor, our data join recent observations that suggest that excitatory amino
acid receptor systems are involved in sleep regulation.

DOI: 10.1038/npp.1993.41 
PMID: 8397722  [PubMed - indexed for MEDLINE]

