
1. Sleep. 2010 Jul;33(7):982-9.

Prospective trial of efficacy and safety of ondansetron and fluoxetine in
patients with obstructive sleep apnea syndrome.

Prasad B(1), Radulovacki M, Olopade C, Herdegen JJ, Logan T, Carley DW.

Author information: 
(1)Center for Narcolepsy, Sleep and Health Research, Department of Medicine,
University of Illinois at Chicago, 845 South Damen Ave, Chicago, IL 60612, USA.

STUDY OBJECTIVE: Incremental withdrawal of serotonin during wake to sleep
transition is postulated as a key mechanism that renders the pharyngeal airway
collapsible. While serotonin promotion with reuptake inhibitors have demonstrated
modest beneficial effects during NREM sleep on obstructive sleep apnea (OSA),
animal studies suggest a potential therapeutic role for selective serotonin
receptor antagonists (5-HT3) in REM sleep. We aimed to test the hypothesis that a
combination of ondansetron (Ond) and fluoxetine (Fl) may effectively reduce
expression of disordered breathing during REM and NREM sleep in patients with
OSA.
DESIGN AND SETTING: A prospective, parallel-groups, single-center trial in
patients with OSA.
PARTICIPANTS: 35 adults with apnea hypopnea index (AHI) > 10; range 10-98.
INTERVENTION: Subjects were randomized to placebo, n = 7; Ond (24 mg QD), n = 9; 
Fl (5 mg QD) + Ond (12 mg QD), n = 9; and Fl (10 mg QD) + Ond (24 mg QD), n = 10.
MEASUREMENTS AND RESULTS: AHI was measured by in-lab polysomnography after a
7-day no-treatment period (Baseline) and on days 14 and 28 of treatment. The
primary endpoint was AHI reduction at days 14 and 28. OND+FL resulted in
approximately 40% reduction of baseline AHI at days 14 and 28 (unadjusted P <
0.03 for each) and improved oximetry trends. This treatment-associated relative
reduction in AHI was also observed in REM and supine sleep.
CONCLUSIONS: Combined treatment with OND+FL is well-tolerated and reduces AHI,
yielding a potentially therapeutic response in some subjects with OSA.


PMCID: PMC2894441
PMID: 20614859  [PubMed - indexed for MEDLINE]

