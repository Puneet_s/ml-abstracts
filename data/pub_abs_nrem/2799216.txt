
1. Sleep. 1989 Oct;12(5):430-8.

Sleep pattern alterations and brief airway obstructions in overweight infants.

Kahn A(1), Mozin MJ, Rebuffat E, Sottiaux M, Burniat W, Shepherd S, Muller MF.

Author information: 
(1)Pediatric Sleep Unit, University Children's Hospital, Brussels, Belgium.

The sleep characteristics of 10 overweight infants were monitored
polysomnographically and compared with those of 10 age- and sex-matched control
infants with no weight excess. The infants were selected from a well-babies
clinic. Infants were assigned to the weight excess group if their weight was
greater than 120% of ideal weight/height for age. There were six boys and four
girls in both groups, with a median age of 23.5 weeks in the weight excess group 
and 22.0 weeks in the control group. The infants with weight excess spent
significantly less time sleeping in non-rapid-eye-movement (NREM) sleep stage 3-4
and more time in indeterminate sleep than their matched controls. The infants
with weight excess had also significantly more gross body movements and more
sleep stage shifts than the control infants. Brief airway obstructions were found
significantly more frequently in the weight excess group than in the control
group. Seven overweight infants showed a total of 74 brief airway obstructions;
41 occurred in NREM sleep stage 1 or 2 and 14 in rapid-eye-movement (REM) sleep. 
The median duration of the obstructive episodes was 8 s (range 3-13 s). Of the 10
control subjects, only 2 had one obstructive episode each, lasting 3 and 4 s and 
occurring during REM sleep. Only one mixed apnea of 6.5 s was recorded in a
39-week-old overweight boy. The obstructive episodes were accompanied by a median
fall in heart rate of 9% (range 0-51%) and by a median fall in oxygen saturation 
of 0.9% (range 0-10%).(ABSTRACT TRUNCATED AT 250 WORDS)


PMID: 2799216  [PubMed - indexed for MEDLINE]

