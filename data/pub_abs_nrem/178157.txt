
1. Adv Pediatr. 1976;22:137-50.

The pathophysiology of sleep disorders in pediatrics. Part I. Sleep in infancy.

Anders TF, Guilleminault C.

In this part of the chapter we have described the characteristics of two
alternating sleep states - REM and NREM sleep in preterm and full-term infants.
We have indicated how individual physiologic measures, recorded during sleep,
mature and become synchromized into patterns that define the sleep states. We
have described abnormalities in this process and have related them to clinical
populations of deviant infants. Throughout, we have emphasized the complexity of 
the process and the methodologic sophistication required to investigate
adequately the multiple dimensions of sleep. Despite somewhat discrepant and
confusing reports in the literature, we continue to believe that sleep studies
will provide rewarding insights into the central nervous system functioning of
the developing young infant.


PMID: 178157  [PubMed - indexed for MEDLINE]

