
1. Conf Proc IEEE Eng Med Biol Soc. 2013;2013:5777-80. doi:
10.1109/EMBC.2013.6610864.

Automatic sleep staging: from young adults to elderly patients using multi-class 
support vector machine.

Kempfner J, Jennum P, Sorensen HB, Christensen JA, Nikolic M.

Aging is a process that is inevitable, and makes our body vulnerable to
age-related diseases. Age is the most consistent factor affecting the sleep
structure. Therefore, new automatic sleep staging methods, to be used in both of 
young and elderly patients, are needed. This study proposes an automatic sleep
stage detector, which can separate wakefulness, rapid-eye-movement (REM) sleep
and non-REM (NREM) sleep using only EEG and EOG. Most sleep events, which define 
the sleep stages, are reduced with age. This is addressed by focusing on the
amplitude of the clinical EEG bands, and not the affected sleep events. The
age-related influences are then reduced by robust subject-specific scaling. The
classification of the three sleep stages are achieved by a multi-class support
vector machine using the one-versus-rest scheme. It was possible to obtain a high
classification accuracy of 0.91. Validation of the sleep stage detector in other 
sleep disorders, such as apnea and narcolepsy, should be considered in future
work.

DOI: 10.1109/EMBC.2013.6610864 
PMID: 24111051  [PubMed - indexed for MEDLINE]

