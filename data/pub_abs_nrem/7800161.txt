
1. Neuropsychobiology. 1994;30(2-3):143-7.

Auditory information processing in sleep: habituation to repetitive stimuli.

van Sweden B(1), van Dijk JG, Caekebeke JF.

Author information: 
(1)Department of Clinical Neurophysiology and Sleep/Wake Research, Medical Center
St. Jozef, Bilzen, Belgium.

Habituation to evoked responses is obvious in waking but still controversial in
sleep. Single-response analysis proves short-term habituation of auditory evoked 
potentials in stage 2 NREM sleep. The data are discussed referring to the
two-system hypothesis of sensory processing in sleep and to DC instability and
sleep maintenance mechanisms in stage 2 NREM. It is suggested that information
processing might continue in sleep.


PMID: 7800161  [PubMed - indexed for MEDLINE]

