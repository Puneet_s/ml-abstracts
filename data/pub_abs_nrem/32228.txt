
1. J Neural Transm. 1978;43(2):121-32.

Kleine-Levin syndrome with periodic apnea during hypersomnic stages--E.E.G.
study.

Vardi J, Flechter S, Tupilsky M, Rabey JM, Carasso R, Streifler M.

A 33 year old male, suffering from Kleine-Levine syndrome associated with periods
of apnea during the hypersomnic attacks, is reported. Ventilatory studies negate 
the Pickwickian syndrome. The E.E.G.'s recorded during the hypersomnic attacks
and the apneic periods showed a direct correlation between high-voltage delta
waves paroxysmal E.E.G. activity, and apneic period. Medications known to improve
Kleine-Levin syndrome, in our case, had no effect upon the clinical hypersomnic
and apnea periods, nor on the correlatives E.E.G.'s pattern and spirometric
studies. Theoretical considerations let us assume that these paroxysmal E.E.G.
patterns associated with apnea are NRem-sleep serotonin dependent, and have an
inhibitory influence on the respiratory centers, by alternating the equilibrium
between the catecholamines and acetylcholine activities.


PMID: 32228  [PubMed - indexed for MEDLINE]

