
1. Sleep Med. 2004 Jul;5(4):407-12.

Changes in cerebral and autonomic activity heralding periodic limb movements in
sleep.

Ferrillo F(1), Beelke M, Canovaro P, Watanabe T, Aricò D, Rizzo P, Garbarino S,
Nobili L, De Carli F.

Author information: 
(1)Center for Sleep Medicine, DISMR, Department of Motor Sciences, University of 
Genova, Ospedale S. Martino, Largo R. Benzi 10, I-16132 Genoa, Italy.
franco.ferrillo@unige.it

BACKGROUND AND PURPOSE: Periodic limb movement disorder (PLMD) is frequently
accompanied by awakenings or signs of EEG arousal. However, it is matter of
debate whether EEG arousals trigger leg movements or both EEG arousal and leg
movements are separate expressions of a common pathophysiological mechanism.
Previous studies showed that cardiac and cerebral changes occur in association
with periodic limb movements (PLMs), and that a combining increase in delta
activity and in heart rate (HR) occurs before the onset of PLMs.
PATIENTS AND METHODS: This paper presents some preliminary data, obtained from a 
sample of 5 subjects with PLMD not associated to restless legs syndrome. To
describe the temporal pattern of cardiac and EEG activities changes concomitant
with PLMs in NREM sleep we used time frequency analysis technique.
RESULTS: PLM onset is heralded by a significant activation of HR and delta
activity power, beginning 4.25 and 3 s respectively before PLMs onset, with PLMs 
onset and arousal onset falling together.
DISCUSSION: Delta and HR variations herald PLMs and activation of fast EEG
frequencies. Such a stereotyped pattern is common in PLMs and in spontaneous or
stimuli-induced arousals. Moreover a similar pattern seems to encompass the CAP
phenomenon. The whole of these phenomena can be linked to the activity of a
common brainstem system, which receives peripheral inputs, regulating the
vascular, cardiac and respiratory activities and synchronizing them to cortical
oscillations of EEG.

DOI: 10.1016/j.sleep.2004.01.008 
PMID: 15223001  [PubMed - indexed for MEDLINE]

