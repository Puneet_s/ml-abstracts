
1. Eur J Pharmacol. 1989 Nov 21;171(2-3):207-18.

Effects of seganserin, a 5-HT2 antagonist, and temazepam on human sleep stages
and EEG power spectra.

Dijk DJ(1), Beersma DG, Daan S, van den Hoofdakker RH.

Author information: 
(1)Department of Biological Psychiatry, University of Groningen, The Netherlands.

The effects of seganserin, a specific 5HT2 antagonist, on human sleep were
assessed in two experiments and compared to the effects of temazepam and sleep
deprivation. During daytime recovery sleep after sleep deprivation, seganserin
did not significantly enhance visually scored slow wave sleep (SWS, stages 3 + 4)
or the EEG power density in the delta frequencies. Under these conditions
temazepam reduced the power density in the delta and theta frequencies. During
nighttime sleep after a nap in the evening, seganserin caused an increase in SWS,
a reduction in intermittent wakefulness, and an enhancement of the power density 
in the delta and theta frequencies during non-rapid eye movement (NREM) sleep.
Temazepam induced a reduction in the power density in the delta and theta
frequencies. It is concluded that the 5HT2 antagonist, seganserin, can induce
SWS. However, since the spectral results showed that the changes in the sleep EEG
were not identical to those induced by sleep deprivation it seems premature to
conclude that 5HT2 receptors are primarily involved in NREM sleep regulation.


PMID: 2576000  [PubMed - indexed for MEDLINE]

