
1. Arch Ital Biol. 2014 Jun-Sep;152(2-3):156-68. doi: 10.12871/0002982920142310.

Long-term history and immediate preceding state affect EEG slow wave
characteristics at NREM sleep onset in C57BL/6 mice.

Cui N(1), Mckillop LE(1), Fisher SP(1), Oliver PL(1), Vyazovskiy VV(1).

Author information: 
(1)University of Oxford, Department of Physiology, Anatomy and Genetics,
Sherrington Building, Parks Road, Oxford, OX1 3PT, United Kingdom.
Email:vladyslav.vyazovskiy@dpag.ox.ac.uk.

The dynamics of cortical activity across the 24-h day and at vigilance state
transitions is regulated by an interaction between global subcortical
neuromodulatory influences and local shifts in network synchrony and
excitability. To address the role of long-term and immediate preceding history in
local and global cortical dynamics, we investigated cortical EEG recorded from
both frontal and occipital regions during an undisturbed 24-h recording in mice. 
As expected, at the beginning of the light period, under physiologically
increased sleep pressure, EEG slow waves were more frequent and had higher
amplitude and slopes, compared to the rest of the light period. Within discrete
NREM sleep episodes, the incidence, amplitude and slopes of individual slow waves
increased progressively after episode onset in both derivations by approximately 
10-30%. Interestingly, at the beginning of NREM sleep episodes slow waves in the 
frontal and occipital derivations frequently occurred in isolation, as quantified
by longer latencies between consecutive slow waves in the two regions. Notably,
slow waves during the initial period of NREM sleep following REM sleep episodes
were significantly less frequent, lower in amplitude and exhibited shallower
slopes, compared to those that occurred in NREM episodes after prolonged waking. 
Moreover, the latencies between consecutive frontal and occipital NREM slow waves
were substantially longer when they occurred directly after REM sleep compared to
following consolidated wakefulness. Overall these data reveal a complex picture, 
where both time of day and preceding state contribute to the characteristics and 
dynamics of slow waves within NREM sleep. These findings suggest that NREM sleep 
initiates in a more "local" fashion when it occurs following REM sleep episodes
as opposed to sustained waking bouts. While the mechanisms and functional
significance of such a re-setting of brain state after individual REM sleep
episodes remains to be investigated, we suggest that it may be an essential
feature of physiological sleep regulation.


PMID: 25828687  [PubMed - indexed for MEDLINE]

