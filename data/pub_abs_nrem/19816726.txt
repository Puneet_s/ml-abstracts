
1. Sleep Breath. 2010 Sep;14(3):233-9. doi: 10.1007/s11325-009-0305-z. Epub 2009 Oct
9.

Validation of ECG-derived sleep architecture and ventilation in sleep apnea and
chronic fatigue syndrome.

Decker MJ(1), Eyal S, Shinar Z, Fuxman Y, Cahan C, Reeves WC, Baharav A.

Author information: 
(1)Chronic Viral Diseases Branch, National Center for Zoonotic, Vector-borne
Enteric Diseases, Centers for Disease Control and Prevention, Atlanta, GA 30333, 
USA. mdecker@cdc.gov

PURPOSE: Newly developed algorithms putatively derive measures of sleep,
wakefulness, and respiratory disturbance index (RDI) through detailed analysis of
heart rate variability (HRV). Here, we establish levels of agreement for one such
algorithm through comparative analysis of HRV-derived values of sleep-wake
architecture and RDI with those calculated from manually scored polysomnographic 
(PSG) recordings.
METHODS: Archived PSG data collected from 234 subjects who participated in a
3-day, 2-night study characterizing polysomnographic traits of chronic fatigue
syndrome were scored manually. The electrocardiogram and pulse oximetry channels 
were scored separately with a novel scoring algorithm to derive values for
wakefulness, sleep architecture, and RDI.
RESULTS: Four hundred fifty-four whole-night PSG recordings were acquired, of
which, 410 were technically acceptable. Comparative analyses demonstrated no
difference for total minutes of sleep, wake, NREM, REM, nor sleep efficiency
generated through manual scoring with those derived through HRV analyses. When
NREM sleep was further partitioned into slow-wave sleep (stages 3-4) and light
sleep (stages 1-2), values calculated through manual scoring differed
significantly from those derived through HRV analyses. Levels of agreement
between RDIs derived through the two methods revealed an R = 0.89. The
Bland-Altman approach for determining levels of agreement between RDIs generated 
through manual scoring with those derived through HRV analysis revealed a mean
difference of -0.7 +/- 8.8 (mean +/- two standard deviations).
CONCLUSION: We found no difference between values of wakefulness, sleep, NREM,
REM sleep, and RDI calculated from manually scored PSG recordings with those
derived through analyses of HRV.

DOI: 10.1007/s11325-009-0305-z 
PMID: 19816726  [PubMed - indexed for MEDLINE]

