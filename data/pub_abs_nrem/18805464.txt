
1. Neuroscience. 2008 Nov 11;157(1):238-53. doi: 10.1016/j.neuroscience.2008.08.040.
Epub 2008 Aug 27.

The role of cholinergic basal forebrain neurons in adenosine-mediated homeostatic
control of sleep: lessons from 192 IgG-saporin lesions.

Kalinchuk AV(1), McCarley RW, Stenberg D, Porkka-Heiskanen T, Basheer R.

Author information: 
(1)Laboratory of Neuroscience, Department of Psychiatry, Harvard Medical School
and VA Boston Healthcare System, 1400 V.F.W. Parkway, West Roxbury, MA 02132,
USA. anna_kalinchuk@hms.harvard.edu

A topic of high current interest and controversy is the basis of the homeostatic 
sleep response, the increase in non-rapid-eye-movement (NREM) sleep and
NREM-delta activity following sleep deprivation (SD). Adenosine, which
accumulates in the cholinergic basal forebrain (BF) during SD, has been proposed 
as one of the important homeostatic sleep factors. It is suggested that
sleep-inducing effects of adenosine are mediated by inhibiting the wake-active
neurons of the BF, including cholinergic neurons. Here we examined the
association between SD-induced adenosine release, the homeostatic sleep response 
and the survival of cholinergic neurons in the BF after injections of the
immunotoxin 192 immunoglobulin G (IgG)-saporin (saporin) in rats. We correlated
SD-induced adenosine level in the BF and the homeostatic sleep response with the 
cholinergic cell loss 2 weeks after local saporin injections into the BF, as well
as 2 and 3 weeks after i.c.v. saporin injections. Two weeks after local saporin
injection there was an 88% cholinergic cell loss, coupled with nearly complete
abolition of the SD-induced adenosine increase in the BF, the homeostatic sleep
response, and the sleep-inducing effects of BF adenosine infusion. Two weeks
after i.c.v. saporin injection there was a 59% cholinergic cell loss, correlated 
with significant increase in SD-induced adenosine level in the BF and an intact
sleep response. Three weeks after i.c.v. saporin injection there was an 87%
cholinergic cell loss, nearly complete abolition of the SD-induced adenosine
increase in the BF and the homeostatic response, implying that the time course of
i.c.v. saporin lesions is a key variable in interpreting experimental results.
Taken together, these results strongly suggest that cholinergic neurons in the BF
are important for the SD-induced increase in adenosine as well as for its
sleep-inducing effects and play a major, although not exclusive, role in sleep
homeostasis.

DOI: 10.1016/j.neuroscience.2008.08.040 
PMCID: PMC3678094
PMID: 18805464  [PubMed - indexed for MEDLINE]

