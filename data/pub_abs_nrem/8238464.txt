
1. Am J Physiol. 1993 Oct;265(4 Pt 2):R907-13.

An IL-1 receptor and an IL-1 receptor antagonist attenuate muramyl dipeptide- and
IL-1-induced sleep and fever.

Imeri L(1), Opp MR, Krueger JM.

Author information: 
(1)Department of Physiology and Biophysics, University of Tennessee, Memphis
38163.

It is hypothesized that the somnogenic and pyrogenic effects of muramyl dipeptide
(MDP) are mediated via enhanced interleukin-1 (IL-1) production. To test this
hypothesis the effects of intracerebroventricular (icv) administration of a
recombinant human soluble type I IL-1 receptor (sIL-1r) and of the IL-1 receptor 
antagonist (IL-1ra) on MDP-induced sleep and fever were evaluated in rabbits. The
sIL-1r recognized rabbit IL-1 beta, but it did not affect sleep or brain
temperature across the dose range tested (1-50 micrograms) when injected icv into
normal rabbits. Pretreatment of rabbits with 50 micrograms sIL-1r or 10
micrograms IL-1ra blocked human recombinant IL-1-enhanced nonrapid eye movement
(NREM) sleep and fever. Thus both the sIL-1r and the IL-1ra were effective
antagonists of IL-1 actions. When the animals were pretreated with either 50
micrograms sIL-1r or with 10 or 100 micrograms of the IL-1ra, the somnogenic
effects of 150 pmol MDP were attenuated. However, the sIL-1r had little effect on
MDP-induced febrile responses. These results suggest that the sIL-1r and the
IL-1ra can function as antagonists of IL-1 actions in vivo and that MDP-induced
sleep and fever are partially mediated by IL-1.


PMID: 8238464  [PubMed - indexed for MEDLINE]

