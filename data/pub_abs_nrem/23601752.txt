
1. Sleep Med. 2013 May;14(5):391-8. doi: 10.1016/j.sleep.2013.01.014. Epub 2013 Apr 
17.

Fight or flight? Dream content during sleepwalking/sleep terrors vs. rapid eye
movement sleep behavior disorder.

Uguccioni G(1), Golmard JL, de Fontréaux AN, Leu-Semenescu S, Brion A, Arnulf I.

Author information: 
(1)Sleep Disorders Unit, Pitié-Salpêtrière University Hospital, APHP, Paris,
France; Pierre and Marie Curie University, Paris, France.

Comment in
    Sleep Med. 2013 May;14(5):387-8.

OBJECTIVE: Dreams enacted during sleepwalking or sleep terrors (SW/ST) may differ
from those enacted during rapid eye movement sleep behavior disorder (RBD).
METHODS: Subjects completed aggression, depression, and anxiety questionnaires.
The mentations associated with SW/ST and RBD behaviors were collected over their 
lifetime and on the morning after video polysomnography (PSG). The reports were
analyzed for complexity, length, content, setting, bizarreness, and threat.
RESULTS: Ninety-one percent of 32 subjects with SW/ST and 87.5% of 24 subjects
with RBD remembered an enacted dream (121 dreams in a lifetime and 41 dreams
recalled on the morning). These dreams were more complex and less bizarre, with a
higher level of aggression in the RBD than in SW/ST subjects. In contrast, we
found low aggression, anxiety, and depression scores during the daytime in both
groups. As many as 70% of enacted dreams in SW/ST and 60% in RBD involved a
threat, but there were more misfortunes and disasters in the SW/ST dreams and
more human and animal aggressions in the RBD dreams. The response to these
threats differed, as the sleepwalkers mostly fled from a disaster (and 25% fought
back when attacked), while 75% of RBD subjects counterattacked when assaulted.
The dreams setting included their bedrooms in 42% SW/ST dreams, though this
finding was exceptional in the RBD dreams.
CONCLUSION: Different threat simulations and modes of defense seem to play a role
during dream-enacted behaviors (e.g., fleeing a disaster during SW/ST,
counterattacking a human or animal assault during RBD), paralleling and
exacerbating the differences observed between normal dreaming in nonrapid eye
movement (NREM) vs rapid eye movement (REM) sleep.

Copyright © 2013 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.sleep.2013.01.014 
PMID: 23601752  [PubMed - indexed for MEDLINE]

