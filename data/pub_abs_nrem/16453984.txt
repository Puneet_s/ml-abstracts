
1. Sleep. 2006 Jan;29(1):77-84.

Hypersynchronous delta waves and somnambulism: brain topography and effect of
sleep deprivation.

Pilon M(1), Zadra A, Joncas S, Montplaisir J.

Author information: 
(1)Centre d'tude du sommeil, Hôpital du Sacré-Coeur, Université de Montreal,
Montreal, Quebec, Canada.

Comment in
    Sleep. 2006 Jan;29(1):14-5.

STUDY OBJECTIVES: Hypersynchronous delta activity (HSD) is usually described as
several continuous high-voltage delta waves (> or = 150 microV) in the sleep
electroencephalogram of somnambulistic patients. However, studies have yielded
varied and contradictory results. The goal of the present study was to evaluate
HSD over different electroencephalographic derivations during the non-rapid eye
movement (NREM) sleep of somnambulistic patients and controls during normal sleep
and following 38 hours of sleep deprivation, as well as prior to sleepwalking
episodes.
DESIGN: N/A.
SETTING: Sleep disorders clinic.
PATIENTS: Ten adult sleepwalkers and 10 sex- and age-matched control subjects
were investigated polysomnographically during a baseline night and following 38
hours of sleep deprivation.
INTERVENTIONS: N/A.
MEASUREMENTS AND RESULTS: During normal sleep, sleepwalkers had a significantly
higher ratio of HSD over the time spent in stage 2, 3 and 4 on frontal and
central derivations when compared with controls. Sleep deprivation resulted in a 
significant increase in the ratio of the time in HSD over the time in stage 4 on 
the frontal lead in both groups and on the central lead in controls. There was no
evidence for a temporal accumulation of HSD prior to the episodes.
CONCLUSIONS: HSD shows a clear frontocentral gradient across all subjects during 
both baseline and recovery sleep and has relatively low specificity for the
diagnosis of NREM parasomnias. Increases in HSD after sleep deprivation may
reflect an enhancement of the homeostatic process underlying sleep regulation.


PMID: 16453984  [PubMed - indexed for MEDLINE]

