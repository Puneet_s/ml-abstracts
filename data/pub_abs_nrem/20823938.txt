
1. Cogn Neurosci. 2010 Sep;1(3):176-183.

Cortical reactivity and effective connectivity during REM sleep in humans.

Massimini M(1), Ferrarelli F, Murphy M, Huber R, Riedner B, Casarotto S, Tononi
G.

Author information: 
(1)Department of Clinical Sciences, University of Milan, via G.B. Grassi 74,
Milan 20157, Italy.

We recorded the electroencephalographic (EEG) responses evoked by transcranial
magnetic stimulation (TMS) during the first rapid eye movement (REM) sleep
episode of the night and we compared them with the responses obtained during
previous wakefulness and NREM sleep. Confirming previous findings, upon falling
into NREM sleep, cortical activations became more local and stereotypical,
indicating a significant impairment of the intracortical dialogue. During REM
sleep, a state in which subjects regain consciousness but are almost paralyzed,
TMS triggered more widespread and differentiated patterns of cortical activation,
that were similar to the ones observed in wakefulness. Similarly, TMS/hd-EEG may 
be used to probe the internal dialogue of the thalamocortical system in brain
injured patients that are unable to move and communicate.

DOI: 10.1080/17588921003731578 
PMCID: PMC2930263
PMID: 20823938  [PubMed]

