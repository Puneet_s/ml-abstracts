
1. J Appl Physiol (1985). 1993 Jul;75(1):397-404.

Ventilatory pattern after hypoxic stimulation during wakefulness and NREM sleep.

Gleeson K(1), Sweer LW.

Author information: 
(1)Division of Pulmonary and Critical Care Medicine, Milton S. Hershey Medical
Center, Pennsylvania State University, Hershey 17033.

The ventilatory after-discharge mechanism (VAD) may stabilize ventilation (VE)
after hyperventilation but has not been studied in detail in humans. Several
studies conducted during wakefulness suggest that VAD is present, although none
has been conducted during sleep, when disordered ventilation is most common. We
conducted two experiments during wakefulness and non-rapid-eye-movement (NREM)
sleep in 14 healthy young men to characterize the ventilatory response after
termination of a 45- to 60-s 10-12% O2 hypoxic stimulus. Eight subjects had
triplicate hypoxic trials terminated by 100% O2 during wakefulness and NREM
sleep. Hypoxia caused a drop in arterial O2 saturation to 78.5 +/- 0.5%, an
increase in VE of 4.4 +/- 0.6 l/min, and a decrease in end-tidal PCO2 of 4.4 +/- 
0.4 Torr during wakefulness, with no significant differences during sleep. When
the hypoxia was terminated with 100% O2, VE was variable within and between
subjects during wakefulness. During sleep, all subjects developed hypopnea (VE < 
67% baseline) with a mean decrease of 65.5 +/- 7.8% at the onset of hyperoxia (P 
< 0.05 compared with baseline VE). We hypothesized that this uniform decrease in 
VE might be due to the nonphysiological hyperoxia employed. We therefore studied 
six additional subjects, all during NREM sleep, with identical hypoxic
stimulation of breathing terminated by 100% O2 or room air. We again found that
termination of hypoxia with 100% O2 produced uniform hypoventilation. However,
when the identical stimulus was terminated with room air, no hypoventilation
occurred.(ABSTRACT TRUNCATED AT 250 WORDS)


PMID: 8376291  [PubMed - indexed for MEDLINE]

