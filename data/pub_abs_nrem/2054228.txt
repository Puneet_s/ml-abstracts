
1. No To Shinkei. 1991 Jan;43(1):81-6.

[Sodium regulation disorder, hypothermia, and circadian rhythm disturbances of
the body temperature and sleep-wakefulness as sequelae of acute subdural
hematoma].

[Article in Japanese]

Kubota M(1), Shinozaki M, Ishizaki A, Kurata K.

Author information: 
(1)Department of Pediatrics, Metropolitan Medical Center for the Severely
Handicapped, Tokyo, Japan.

We reported an 11-year-old boy who suffered from transient hypernatremia,
hypothermia, and circadian rhythm disturbances of sleep-wakefulness and body
temperature from the age of 4 years, as sequelae of acute subdural hematoma.
T1-weighted magnetic resonance imaging (MRI) of the brain revealed low intensity 
consistent with necrotic change in the whole left cerebral hemisphere,
hypothalamic region, and the right-sided brain stem including tegmentum, while
the pituitary structure was well preserved. Anterior pituitary function was
almost normal. ADH (antidiuretic hormone) was neither stimulated by
hyperosmolality nor suppressed by hyposmolality but continued to be secreted at
almost constant level approximating the normal basal state. This pattern seemed
to be due to complete destruction of the osmoreceptor located in the anterior
hypothalamus. He exhibited a dispersed-type sleep with differentiated stages of
NREM (non-rapid eye movement), although the percentage of sleep was higher at
night than in the daytime. It is suggested that circadian rhythm of
sleep-wakefulness and differentiation of NREM sleep stages are regulated by
different neuromechanisms. Brain stem lesion on MRI may be connected with the
pathogenesis of the dispersed-type sleep with special respect to amplitude
reduction of sleep-waking circadian rhythm. Circadian rhythm of body temperature 
(BT) was irregular in amplitude, phase, and period without synchronization with
sleep-wakefulness rhythm. Hypothermia was also demonstrated at the basal state,
while BT increased when he suffered from respiratory infection. It is likely that
hypothermia in our case is caused by the BT shift to the lower side due to
malfunction of BT integrating system including preoptic area and anterior
hypothalamus.(ABSTRACT TRUNCATED AT 250 WORDS)


PMID: 2054228  [PubMed - indexed for MEDLINE]

