
1. Sleep. 2006 May;29(5):619-23.

Mild hypoxia does not suppress auditory arousal from NREM sleep.

Catcheside PG(1), Orr RS, Chiong SC, Mercer J, Saunders NA, McEvoy RD.

Author information: 
(1)Adelaide Institute for Sleep Health, Repatriation General Hospital, Daws Road,
Daw Park, SA 5041, Australia. peter.catcheside@rgh.sa.gov.au

STUDY OBJECTIVES: The depressive effects of hypoxia on the central nervous system
are well known. The purpose of this study was to determine the influence of mild 
overnight hypoxia on the ability of healthy individuals to arouse from
non-rapid-eye-movement (NREM) sleep to auditory tones.
DESIGN: Randomized cross-over.
SETTING: Participants slept in a sound-insulated room with the physiologic
recordings and experimental interventions controlled from a separate room.
PARTICIPANTS: Eleven healthy men aged 18 to 24 years.
INTERVENTIONS: On separate nights, participants were exposed to mild overnight
hypoxia (SaO2 approximately 90%) or medical air in single-blind fashion. During
established sleep, subjects were administered 1 of 10 auditory tones (500 Hz,
54-90 dB, 5 seconds duration) via earphones, or a sham tone (recording period
with no tone).
MEASUREMENTS AND RESULTS: The probability and intensity of arousal responses in
the 30 seconds following tones or shams were compared between gas conditions and 
between stage 2 and slow-wave sleep. Arousal probability and intensity increased 
with tone intensity and were significantly lower during slow-wave compared with
stage 2 sleep but were not different between hypoxia and normoxia nights.
CONCLUSION: These data suggest that mild overnight hypoxia does not impair the
neural mechanisms involved in arousal from sleep to auditory stimuli.


PMID: 16774151  [PubMed - indexed for MEDLINE]

