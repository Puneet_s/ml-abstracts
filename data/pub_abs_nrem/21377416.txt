
1. Sleep Med. 2011 Apr;12(4):361-6. doi: 10.1016/j.sleep.2010.11.009. Epub 2011 Mar 
5.

Cyclic alternating pattern in sleep and its relationship to creativity.

Drago V(1), Foster PS, Heilman KM, Aricò D, Williamson J, Montagna P, Ferri R.

Author information: 
(1)IRCCS San Giovanni Di Dio Fatebenefratelli, Via Pilastroni, 4, 25125 Brescia, 
Italy. vdrago@fatebenefratelli.it

Comment in
    Sleep Med. 2011 Mar;12(3):203-4.
    Sleep Med. 2011 Apr;12(4):313-4.

BACKGROUND/OBJECTIVES: Sleep has been shown to enhance creativity, but the reason
for this enhancement is not entirely known. There are several different
physiologic states associated with sleep. In addition to rapid (REM) and
non-rapid eye movement (NREM) sleep, NREM sleep can be broken down into Stages
(1-4) that are characterized by the degree of EEG slow-wave activity. In
addition, during NREM sleep the cyclic alternating pattern (CAPs) of EEG activity
has been described which can also be divided into three subtypes (A1-A3)
according to the frequency of the EEG waves. Differences in CAP subtype ratios
have been previously linked to cognitive performances. The purpose of this study 
was to asses the relationship between CAP activity during sleep and creativity.
METHODS: The participants were eight healthy young adults (four women) who
underwent three consecutive nights of polysomnographic recording and took the
Abbreviated Torrance Test for Adults (ATTA) on the second and third mornings
after the recordings.
RESULTS: There were positive correlations between Stage 1 of NREM sleep and some 
measures of creativity such as fluency (R=.797; p=.029) and flexibility (R=.43;
p=.002), between Stage 4 of NREM sleep and originality (R=.779; p=.034) and a
global measure of figural creativity (R=.758; p=.040). There was also a negative 
correlation between REM sleep and originality (R=-.827; p=.042). During NREM
sleep the CAP rate, which in young people reflects primarily the A1 subtype, also
correlated with originality (R=.765; p=.038).
CONCLUSIONS: NREM sleep is associated with low levels of cortical arousal, and
low cortical arousal may enhance the ability of people to access to the remote
associations that are critical for creative innovations. In addition, A1 CAP
subtypes reflect frontal activity, and the frontal lobes are important for
divergent thinking, also a critical aspect of creativity.

Copyright © 2011 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.sleep.2010.11.009 
PMID: 21377416  [PubMed - indexed for MEDLINE]

