
1. J Appl Physiol (1985). 1986 Oct;61(4):1438-43.

Induction of periodic breathing during sleep causes upper airway obstruction in
humans.

Onal E, Burrows DL, Hart RH, Lopata M.

To test the hypothesis that occlusive apneas result from sleep-induced periodic
breathing in association with some degree of upper airway compromise, periodic
breathing was induced during non-rapid-eye-movement (NREM) sleep by administering
hypoxic gas mixtures with and without applied external inspiratory resistance (9 
cmH2O X l-1 X s) in five normal male volunteers. In addition to standard
polysomnography for sleep staging and respiratory pattern monitoring, esophageal 
pressure, tidal volume (VT), and airflow were measured via an esophageal catheter
and pneumotachograph, respectively, with the latter attached to a tight-fitting
face mask, allowing calculation of total pulmonary system resistance (Rp). During
stage I/II NREM sleep minimal period breathing was evident in two of the
subjects; however, in four subjects during hypoxia and/or relief from hypoxia,
with and without added resistance, pronounced periodic breathing developed with
waxing and waning of VT, sometimes with apneic phases. Resistive loading without 
hypoxia did not cause periodicity. At the nadir of periodic changes in VT, Rp was
usually at its highest and there was a significant linear relationship between Rp
and 1/VT, indicating the development of obstructive hypopneas. In one subject
without added resistance and in the same subject and in another during resistive 
loading, upper airway obstruction at the nadir of the periodic fluctuations in VT
was observed. We conclude that periodic breathing resulting in periodic
diminution of upper airway muscle activity is associated with increased upper
airway resistance that predisposes upper airways to collapse.


PMID: 3781958  [PubMed - indexed for MEDLINE]

