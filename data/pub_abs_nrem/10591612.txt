
1. J Androl. 1999 Nov-Dec;20(6):731-7.

Relationship between rapid eye movement sleep and testosterone secretion in
normal men.

Luboshitzky R(1), Herer P, Levi M, Shen-Orr Z, Lavie P.

Author information: 
(1)Endocrine Institute, Haemek Medical Center, Afula, Israel.

The relation between the pituitary-gonadal hormones' rhythm and sleep physiology 
in men is not fully elucidated. To examine whether the reproductive hormones are 
correlated with sleep architecture, we determined the nocturnal serum levels of
testosterone, luteinizing hormone (LH), and follicle-stimulating hormone (FSH) in
six healthy young men. Serum hormone levels were obtained every 15 minutes from
1900 to 0700 hours with simultaneous polysomnographic sleep recordings. Hourly
testosterone levels were lowest when subjects were awake (1900-2200 hours) than
during sleep (2300-0700 hours). Testosterone nocturnal rise antedated the first
REM by about 90 minutes. The rise in testosterone levels was slower when REM
latency was longer. Mean nocturnal testosterone levels did not correlate with the
number of rapid eye movement (REM) episodes. Also, pre-non-REM (NREM)
testosterone levels were higher as compared with the pre-REM periods and lower
during the first NREM period as compared with other nocturnal NREM periods. Serum
LH levels disclosed a nocturnal rise that preceeded a similar rise in
testosterone by about an hour. We conclude that in young adult men, testosterone 
levels begin to rise on falling asleep, peak at about the time of first REM, and 
remain at the same levels until awakening.


PMID: 10591612  [PubMed - indexed for MEDLINE]

