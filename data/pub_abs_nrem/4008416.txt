
1. J Appl Physiol (1985). 1985 Jun;58(6):1971-4.

Dynamics of respiratory drive and pressure during NREM sleep in patients with
occlusive apneas.

Onal E, Leech JA, Lopata M.

To study the dynamics of respiratory drive and pressure in patients with
occlusive apneas, diaphragmatic electromyogram (EMGdi), esophageal pressure
(Pes), and genioglossal electromyogram (EMGge) were monitored during nocturnal
sleep in five patients. Both EMGs were analyzed as peak moving time average, and 
Pes was quantitated as the peak inspiratory change from base line. During the
ventilatory phase both EMGs decreased proportionally. The decrease in Pes was
less than the decrease observed in EMGdi, and Pes generated for a given EMGdi
increased during the preapneic phase in spite of the proportional decrease in
EMGdi and EMGge during this period. We conclude that negative inspiratory
pressures which lead to the passive collapse of oropharyngeal walls are dependent
on both respiratory and upper airway muscle activity and that occlusive apneas of
non-rapid-eye-movement (NREM) sleep do occur in spite of proportional changes
observed in the activity of both muscle groups. The preapneic increase in
negative inspiratory pressures generated for a given respiratory muscle activity 
is most likely due to the decrease in upper airway muscle activity that is
associated with an increase in oropharyngeal resistance.


PMID: 4008416  [PubMed - indexed for MEDLINE]

