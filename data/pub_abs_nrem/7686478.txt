
1. Electroencephalogr Clin Neurophysiol. 1993 Jun;86(6):438-45.

The amplitude of elicited PGO waves: a correlate of orienting.

Sanford LD(1), Morrison AR, Ball WA, Ross RJ, Mann GL.

Author information: 
(1)Laboratory of Anatomy, School of Veterinary Medicine, University of
Pennsylvania, Philadelphia 19104-6045.

Ponto-geniculo-occipital (PGO) waves spontaneously occur in the pons, lateral
geniculate body (LGB), and occipital cortex during rapid eye movement sleep
(REM), and PGO-like waves (PGOE) may be elicited in LGB during sleep and waking. 
Because REM has been hypothesized to be a state of continual "orienting" or
"hyper-alertness," we tested whether the amplitudes of PGOE in "alerting"
situations (the abrupt onset of a loud sound or presentation of a novel stimulus 
within a series of stimuli) that evoke orienting responses (OR) would be greater 
than those following stimuli without OR. We also compared PGOE accompanying OR to
PGOE during REM and NREM when OR are absent. The amplitudes of PGOE in W were
greatest when OR were observed, and the amplitudes of PGOE accompanying OR were
not significantly different from PGOE amplitudes in REM. Likewise, the amplitudes
of PGOE during REM were not significantly different from those of the highest
amplitude spontaneous PGO waves. We propose that the presence of PGOE signals
registration of stimuli and that stimuli of sufficient significance to induce
behavioral OR in waking also elicit PGOE of significantly greater amplitudes in
all behavioral states. These findings support the hypothesis that the presence of
high-amplitude PGO waves in REM indicates that the brain is in a state of
more-or-less continual orienting.


PMID: 7686478  [PubMed - indexed for MEDLINE]

