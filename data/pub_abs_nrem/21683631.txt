
1. Eur J Paediatr Neurol. 2011 Nov;15(6):532-8. doi: 10.1016/j.ejpn.2011.04.014.
Epub 2011 Jun 16.

Levetiracetam reduces the frequency of interictal epileptiform discharges during 
NREM sleep in children with ADHD.

Bakke KA(1), Larsson PG, Eriksson AS, Eeg-Olofsson O.

Author information: 
(1)National Centre for Epilepsy, Oslo University Hospital, Oslo, Norway.
kristin_andersen_bakke@hotmail.com

BACKGROUND: Symptoms of attention deficit hyperactivity disorder (ADHD) are more 
common in children with epilepsy than in the general paediatric population.
Epileptiform discharges in EEG may be seen in children with ADHD also in those
without seizure disorders. Sleep enhances these discharges which may be
suppressed by levetiracetam.
AIM: To assess the effect of levetiracetam on focal epileptiform discharges
during sleep in children with ADHD.
METHOD: In this retrospective study a new semi-automatic quantitative method
based on the calculation of spike index in 24-h ambulatory EEG recordings was
applied. Thirty-five ADHD children, 17 with focal epilepsy, one with generalised 
epilepsy, and 17 with no seizure disorder were evaluated. Follow-up 24-h EEG
recordings were performed after a median time of four months.
RESULTS: Mean spike index was 50 prior to levetiracetam treatment and 21 during
treatment. Seventeen children had no focal interictal epileptiform discharges in 
EEG at follow-up. Five children had a more than 50% reduction in spike index.
Thus, a more than 50% reduction in spike index was found in 22/35 children (63%).
Out of these an improved behaviour was noticed in 13 children (59%).
CONCLUSION: This study shows that treatment with levetiracetam reduces interictal
epileptiform discharges in children with ADHD. There is a complex relationship
between epilepsy, ADHD and epileptiform activity, why it is a need for
prospective studies in larger sample sizes, also to ascertain clinical benefits.

Copyright © 2011 European Paediatric Neurology Society. Published by Elsevier
Ltd. All rights reserved.

DOI: 10.1016/j.ejpn.2011.04.014 
PMID: 21683631  [PubMed - indexed for MEDLINE]

