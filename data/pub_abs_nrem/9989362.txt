
1. Sleep. 1999 Feb 1;22(1):7, 9.

Adenosine, blood pressure and NREM delta.

Feinberg I, Campbell IG.

Comment on
    Sleep. 1997 Dec;20(12):1093-8.


PMID: 9989362  [PubMed - indexed for MEDLINE]

