
1. Neuroscience. 2014 Aug 22;274:357-68. doi: 10.1016/j.neuroscience.2014.05.050.
Epub 2014 Jun 6.

NREM sleep hypersomnia and reduced sleep/wake continuity in a neuroendocrine
mouse model of anxiety/depression based on chronic corticosterone administration.

Le Dantec Y(1), Hache G(2), Guilloux JP(2), Guiard BP(2), David DJ(2), Adrien
J(3), Escourrou P(4).

Author information: 
(1)Univ Paris-Sud, EA3544, Faculté de Pharmacie, 92296 Châtenay-Malabry cedex,
France. Electronic address: yannick.ledantec@gmail.com. (2)Univ Paris-Sud,
EA3544, Faculté de Pharmacie, 92296 Châtenay-Malabry cedex, France. (3)UMR975,
CRicm - INSERM/CNRS/UPMC, Neurotransmetteurs et Sommeil, Faculté de Médecine
Pitié-Salpêtrière, Université Pierre et Marie Curie - Paris VI, 91 boulevard de
l'Hôpital, 75013 Paris, France. (4)Univ Paris-Sud, EA3544, Faculté de Pharmacie, 
92296 Châtenay-Malabry cedex, France; Assistance Publique-Hôpitaux de Paris,
Hôpital Antoine Béclère, Département de Physiologie, Centre de Médecine du
Sommeil, 92141 Clamart cedex, France.

Sleep/wake disorders are frequently associated with anxiety and depression and to
elevated levels of cortisol. Even though these alterations are increasingly
sought in animal models, no study has investigated the specific effects of
chronic corticosterone (CORT) administration on sleep. We characterized
sleep/wake disorders in a neuroendocrine mouse model of anxiety/depression, based
on chronic CORT administration in the drinking water (35 μg/ml for 4 weeks, "CORT
model"). The CORT model was markedly affected during the dark phase by non-rapid 
eye movement sleep (NREM) increase without consistent alteration of rapid eye
movement (REM) sleep. Total sleep duration (SD) and sleep efficiency (SE)
increased concomitantly during both the 24h and the dark phase, due to the
increase in the number of NREM sleep episodes without a change in their mean
duration. Conversely, the total duration of wake decreased due to a decrease in
the mean duration of wake episodes despite an increase in their number. These
results reflect hypersomnia by intrusion of NREM sleep during the active period
as well as a decrease in sleep/wake continuity. In addition, NREM sleep was
lighter, with an increased electroencephalogram (EEG) theta activity. With regard
to REM sleep, the number and the duration of episodes decreased, specifically
during the first part of the light period. REM and NREM sleep changes correlated 
respectively with the anxiety and the anxiety/depressive-like phenotypes,
supporting the notion that studying sleep could be of predictive value for
altered emotional behavior. The chronic CORT model in mice that displays hallmark
characteristics of anxiety and depression provides an insight into understanding 
the changes in overall sleep architecture that occur under pathological
conditions.

Copyright © 2014 IBRO. Published by Elsevier Ltd. All rights reserved.

DOI: 10.1016/j.neuroscience.2014.05.050 
PMID: 24909899  [PubMed - indexed for MEDLINE]

