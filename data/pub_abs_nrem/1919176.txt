
1. Nihon Sanka Fujinka Gakkai Zasshi. 1991 Aug;43(8):843-52.

[Ontogeny of behavioral patterns in relation to the concurrent development of
central nervous system function, focusing on REM sleep, NREM sleep and waking
states in the human fetus].

[Article in Japanese]

Koyanagi T(1).

Author information: 
(1)Maternity and Perinatal Care Unit, Kyushu University Hospital, Fukuoka.

Behavior is considered a one to one match between the manifestation of a
particular individual motor activity (movement) and its correlated brain
function. Each motor activity can be seen to run on its own developmental course 
with advance in gestation, while various movements also develop in concurrence
with one another during intrauterine life, gradually integrating into complex and
accommodated movements. I emphasized the present study, under real-time
ultrasound observation, for investigating whether or not three states: REM (rapid
eye movement) sleep, NREM (non-rapid eye movement) sleep and the waking state
actually exist in utero, and if so, when the development of these states begins. 
1. Examined for 30-40 minutes were 21 fetuses, including 10 from 33 to 36 weeks
of gestation and 11 from 37 to 41 weeks of gestation. As parameters, the duration
of each eye movement unit and the cumulative duration of this movement, from the 
shortest to a given duration, per individual case, were observed. A scattergram
of cumulative duration vs, given duration obtained from all cases, for each
age-group, were analyzed using "piecewise linear regression". Critical points
were noted, with statistical significance, at 0.62 second for the earlier group
and at 0.76 second for the later group. These findings reveal two different types
of eye movement: rapid and slow eye movements. The fact that the rapid eye
movement coexists with the slow eye movement during REM period indicates that REM
sleep exists in utero at the latest at 33 weeks of gestation. 2. Observed for 60 
minutes were 26 fetuses from 28 to 41 weeks of gestation. Regular mouthing
movement every 300 to 600 msec concurred significantly only with the NREM period 
from 35 weeks of gestation onwards. Random mouthing movements were observed
predominantly during REM sleep and were unrelated to the advance in gestational
age. This concurrence between the NREM period and regular mouthing indicates the 
existence of NREM sleep in utero at this age of gestation. 3. To evaluate whether
the waking state is present, 10 fetuses at 36 weeks or more of gestation were
examined for a concurrence between miosis/mydriasis and the REM/NREM periods.
Using pupil diameter as a parameter, miosis and mydriasis were distinguished from
each other, statistically, by means of "least median of squares regression".
Accordingly, the NREM period was occupied only by miosis (41.0% of total
observation period), while the REM period was divided into two conditions: 52.6% 
with miosis and 6.4% with mydriasis.(ABSTRACT TRUNCATED AT 400 WORDS)


PMID: 1919176  [PubMed - indexed for MEDLINE]

