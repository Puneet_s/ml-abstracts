
1. Tuberk Toraks. 2006;54(3):213-21.

[Assessment of sleep with polysomnography in patients with interstitial lung
disease].

[Article in Turkish]

Aydoğdu M(1), Ciftçi B, Firat Güven S, Ulukavak Ciftçi T, Erdoğan Y.

Author information: 
(1)Atatürk Chest Disease and Chest Surgery Research and Training Hospital,
Ankara, Turkey. mugeaydogdu@yahoo.com

In our study we aimed to examine the sleep structure, oxygenation and breathing
pattern in interstitial lung disease (ILD) patients. We also aimed to determine
whether relevance between the advanced disease and the sleep disorders exists and
whether polysomnography is necessary in those patients. A total of 37 patients
were examined in the study and whole night standard polysomnography was performed
to all. Polysomnography results revealed that, total sleep time, time spent in
NREM sleep stage III and IV, and in REM sleep were decreased. The patients had
poor sleep efficiency and they spent more time as wake after sleep onset (WASO). 
Severe oxygen desaturations were detected during sleep and statistically
significant positive correlations were found between mean awake O2 saturation and
mean and lowest sleep O2 saturations. Clinical, Radiological and Physiological
(CRP) scoring system was used to assess the disease stage, whether advanced or
not, and statistically significant negative correlations were found between CRP
score and awake and sleep O2 saturations. Obstructive sleep apnea syndrome (OSAS)
was diagnosed in 24 (64.9%) patients. In those patients it was found that not the
apneas but the hypopneas predominate. No difference was found among body mass
indices (BMI) between the patients with and without OSAS. As a result it was
concluded that a sleep study should be considered as part of the overall
assessment in managing patients with ILD, and is especially indicated if there is
incipient pulmonary hypertension, cor pulmonale and nocturnal arrhythmia despite 
normal awake blood gas tensions and symptoms as snoring and excessive day time
sleepiness.


PMID: 17001537  [PubMed - indexed for MEDLINE]

