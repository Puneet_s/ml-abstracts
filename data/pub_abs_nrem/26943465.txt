
1. Sleep. 2016 Apr 1;39(4):801-12. doi: 10.5665/sleep.5632.

Regional Patterns of Elevated Alpha and High-Frequency Electroencephalographic
Activity during Nonrapid Eye Movement Sleep in Chronic Insomnia: A Pilot Study.

Riedner BA(1), Goldstein MR(1,)(2), Plante DT(1), Rumble ME(1), Ferrarelli F(1), 
Tononi G(1), Benca RM(1).

Author information: 
(1)University of Wisconsin School of Medicine and Public Health, Department of
Psychiatry, Madison, WI. (2)University of Arizona, Department of Psychology,
Tucson, AZ.

STUDY OBJECTIVES: To examine nonrapid eye movement (NREM) sleep in insomnia using
high-density electroencephalography (EEG).
METHODS: All-night sleep recordings with 256 channel high-density EEG were
analyzed for 8 insomnia subjects (5 females) and 8 sex and age-matched controls
without sleep complaints. Spectral analyses were conducted using unpaired t-tests
and topographical differences between groups were assessed using statistical
non-parametric mapping. Five minute segments of deep NREM sleep were further
analyzed using sLORETA cortical source imaging.
RESULTS: The initial topographic analysis of all-night NREM sleep EEG revealed
that insomnia subjects had more high-frequency EEG activity (> 16 Hz) compared to
good sleeping controls and that the difference between groups was widespread
across the scalp. In addition, the analysis also showed that there was a more
circumscribed difference in theta (4-8 Hz) and alpha (8-12 Hz) power bands
between groups. When deep NREM sleep (N3) was examined separately, the
high-frequency difference between groups diminished, whereas the higher regional 
alpha activity in insomnia subjects persisted. Source imaging analysis
demonstrated that sensory and sensorimotor cortical areas consistently exhibited 
elevated levels of alpha activity during deep NREM sleep in insomnia subjects
relative to good sleeping controls.
CONCLUSIONS: These results suggest that even during the deepest stage of sleep,
sensory and sensorimotor areas in insomnia subjects may still be relatively
active compared to control subjects and to the rest of the sleeping brain.

© 2016 Associated Professional Sleep Societies, LLC.

DOI: 10.5665/sleep.5632 
PMCID: PMC4791614 [Available on 2016-10-01]
PMID: 26943465  [PubMed - in process]

