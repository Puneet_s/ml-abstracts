
1. An Sist Sanit Navar. 2007;30 Suppl 1:7-17.

[Anatomical basis of sleep].

[Article in Spanish]

Velayos JL(1), Moleres FJ, Irujo AM, Yllanes D, Paternain B.

Author information: 
(1)Departamento de Anatomía, Facultad de Medicina, Universidad de Navarra,
Pamplona. jvelayos@unav.es

Sleep is an active and periodic biological state composed of NREM and REM phases,
which alternate during the night. Both biological clocks and specific
neurotransmitters are involved in the modulation of this system. It is a complex 
neuronal network in which several areas of the central nervous system are
involved. The oneiric processes are also controlled neurally. This work
summarises the history of the investigations on this topic from the 19th century 
to date. It is worth mentioning the recent findings of Lugaresi and colleages who
described fatal familial insomnia, a disease that helped to show the importance
of the mediodorsal thalamic nucleus in the genesis of slow-wave sleep. Reinoso s 
group found out that the paramedian ventral area of the oral pontine reticular
nucleus is the conductor in the establishment of REM sleep.


PMID: 17486144  [PubMed - indexed for MEDLINE]

