
1. Innov Clin Neurosci. 2016 Feb 1;13(1-2):40-2.

Sleep-isolated Trichotillomania (SITTM): A Case Report.

Goyal D(1), Surya S(1), Elder J(1), Mccall WV(1), Graham K(1).

Author information: 
(1)All authors are with the Department of Psychiatry and Health Behavior, Medical
College of Georgia, Georgia Regents University in Augusta, Georgia, USA.

We report a case of sleep-isolated trichotillomania admitted to the hospital for 
alcohol detoxification. It would be helpful for patients with sleep-isolated
trichotillomania to have diagnostic polysomnography to identify any other
sleep-related pathology and correlate sleep-isolated trichotillomania behaviors
with the sleep cycle to identify specific treatment for sleep-isolated
trichotillomania.


PMCID: PMC4896829
PMID: 27413587  [PubMed]

