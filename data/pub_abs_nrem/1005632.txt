
1. Psychosom Med. 1976 Nov-Dec;38(6):390-8.

Newborn heart rate and blood pressure: relation to race and to socioeconomic
class.

Schachter J, Lachin JM, Wimberly FC.

The effect of race and of socioeconomic class upon heart rate and systolic blood 
pressure distributions was examined in 247 full-term, appropriate birth weight
newborns. For each newborn, heart rate and blood pressure measurements obtained
during all of the non-rapid-eye-movement (NREM) periods of sleep in a single test
session were each averaged. Heart rate was significantly faster in black newborns
than in white newborns, and this racial difference in heart rate was similar in
upper socioeconomic class subjects as in lower socioeconomic class subjects.
Newborn systolic blood pressure did not vary as a function of race or of
socioeconomic class. Systolic blood pressure correlated positively with the
number of feedings from birth, the total fluid intake from birth, and the total
sodium ingested from birth.


PMID: 1005632  [PubMed - indexed for MEDLINE]

