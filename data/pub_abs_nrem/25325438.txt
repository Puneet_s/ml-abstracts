
1. Sleep. 2015 Jan 1;38(1):73-84. doi: 10.5665/sleep.4328.

Dexmedetomidine-induced sedation does not mimic the neurobehavioral phenotypes of
sleep in Sprague Dawley rat.

Garrity AG(1), Botta S(2), Lazar SB(2), Swor E(2), Vanini G(2), Baghdoyan HA(3), 
Lydic R(3).

Author information: 
(1)Neuroscience Program, University of Michigan, Ann Arbor, MI. (2)Department of 
Anesthesiology, University of Michigan, Ann Arbor, MI. (3)Department of
Anesthesiology, University of Michigan, Ann Arbor, MI: Neuroscience Program,
University of Michigan, Ann Arbor, MI.

STUDY OBJECTIVES: Dexmedetomidine is used clinically to induce states of sedation
that have been described as homologous to nonrapid eye movement (NREM) sleep. A
better understanding of the similarities and differences between NREM sleep and
dexmedetomidine-induced sedation is essential for efforts to clarify the
relationship between these two states. This study tested the hypothesis that
dexmedetomidine-induced sedation is homologous to sleep.
DESIGN: This study used between-groups and within-groups designs.
SETTING: University of Michigan.
PARTICIPANTS: Adult male Sprague Dawley rats (n = 40).
INTERVENTIONS: Independent variables were administration of dexmedetomidine and
saline or Ringer's solution (control). Dependent variables included time spent in
states of wakefulness, sleep, and sedation, electroencephalographic (EEG) power, 
adenosine levels in the substantia innominata (SI), and activation of pCREB and
c-Fos in sleep related forebrain regions.
MEASUREMENTS AND RESULTS: Dexmedetomidine significantly decreased time spent in
wakefulness (-49%), increased duration of sedation (1995%), increased EEG delta
power (546%), and eliminated the rapid eye movement (REM) phase of sleep for 16
h. Sedation was followed by a rebound increase in NREM and REM sleep.
Systemically administered dexmedetomidine significantly decreased (-39%) SI
adenosine levels. Dialysis delivery of dexmedetomidine into SI did not decrease
adenosine level. Systemic delivery of dexmedetomidine did not alter c-Fos or
pCREB expression in the horizontal diagonal band, or ventrolateral, median, and
medial preoptic areas of the hypothalamus.
CONCLUSIONS: Dexmedetomidine significantly altered normal sleep phenotypes, and
the dexmedetomidine-induced state did not compensate for sleep need. Thus, in the
Sprague Dawley rat, dexmedetomidine-induced sedation is characterized by
behavioral, electrographic, and immunohistochemical phenotypes that are
distinctly different from similar measures obtained during sleep.

© 2014 Associated Professional Sleep Societies, LLC.

DOI: 10.5665/sleep.4328 
PMCID: PMC4262959
PMID: 25325438  [PubMed - indexed for MEDLINE]

