
1. Front Hum Neurosci. 2015 Jun 3;9:323. doi: 10.3389/fnhum.2015.00323. eCollection 
2015.

Age-related changes in sleep spindles characteristics during daytime recovery
following a 25-hour sleep deprivation.

Rosinvil T(1), Lafortune M(1), Sekerovic Z(1), Bouchard M(1), Dubé J(1),
Latulipe-Loiselle A(2), Martin N(1), Lina JM(3), Carrier J(1).

Author information: 
(1)Center for Advanced Research in Sleep Medicine, Hôpital du Sacré-Coeur de
Montréal Montréal, QC, Canada ; Department of Psychology, Université de Montréal 
Montréal, QC, Canada ; Research Center, Institut Universitaire Gériatrique de
Montréal Montréal, QC, Canada. (2)Department of Psychology, Université de
Montréal Montréal, QC, Canada. (3)Center for Advanced Research in Sleep Medicine,
Hôpital du Sacré-Coeur de Montréal Montréal, QC, Canada ; Department of
Electrical Engineering, École de Technologie Supérieure Montréal, QC, Canada.

OBJECTIVES: The mechanisms underlying sleep spindles (~11-15 Hz; >0.5 s) help to 
protect sleep. With age, it becomes increasingly difficult to maintain sleep at a
challenging time (e.g., daytime), even after sleep loss. This study compared
spindle characteristics during daytime recovery and nocturnal sleep in young and 
middle-aged adults. In addition, we explored whether spindles characteristics in 
baseline nocturnal sleep were associated with the ability to maintain sleep
during daytime recovery periods in both age groups.
METHODS: Twenty-nine young (15 women and 14 men; 27.3 y ± 5.0) and 31 middle-aged
(19 women and 13 men; 51.6 y ± 5.1) healthy subjects participated in a baseline
nocturnal sleep and a daytime recovery sleep after 25 hours of sleep deprivation.
Spindles were detected on artifact-free Non-rapid eye movement (NREM) sleep
epochs. Spindle density (nb/min), amplitude (μV), frequency (Hz), and duration
(s) were analyzed on parasagittal (linked-ears) derivations.
RESULTS: In young subjects, spindle frequency increased during daytime recovery
sleep as compared to baseline nocturnal sleep in all derivations, whereas
middle-aged subjects showed spindle frequency enhancement only in the prefrontal 
derivation. No other significant interaction between age group and sleep
condition was observed. Spindle density for all derivations and centro-occipital 
spindle amplitude decreased whereas prefrontal spindle amplitude increased from
baseline to daytime recovery sleep in both age groups. Finally, no significant
correlation was found between spindle characteristics during baseline nocturnal
sleep and the marked reduction in sleep efficiency during daytime recovery sleep 
in both young and middle-aged subjects.
CONCLUSION: These results suggest that the interaction between homeostatic and
circadian pressure modulates spindle frequency differently in aging. Spindle
characteristics do not seem to be linked with the ability to maintain daytime
recovery sleep.

DOI: 10.3389/fnhum.2015.00323 
PMCID: PMC4452883
PMID: 26089788  [PubMed]

