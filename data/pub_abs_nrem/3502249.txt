
1. No To Shinkei. 1987 Dec;39(12):1131-7.

[Cerebral 11C-glucose metabolism during human sleep].

[Article in Japanese]

Uchida K(1).

Author information: 
(1)Department of 2nd Internal Medicine, Toho University School of Medicine,
Tokyo, Japan.

Some reports have suggested that there is no difference in cerebral metabolic
rates between wakefulness and sleep. However recently Kennedy et al. and Heiss et
al. reported a decrease in cerebral glucose utilization during NREM sleep
measured by the using deoxyglucose method. We used a 11C-glucose method for
positron emission tomography (PET) while estimating cerebral glucose metabolism
during human sleep with polysomnography (PSG). This PET and PSG study was carried
out on 11 healthy male volunteers ranging in age from 18 to 26 years. In order to
facilitate the onset of sleep, the subjects were deprived of sleep, under
observation in the lab, for a period of approximately 20 hours prior to the PSG
and PET examination. All experiments were performed in the early morning, most
often between 4 and 10 AM. The subjects' sleep was monitored by PSG, i.e.
electroencephalogram, electrooculogram and a submental electroencephalogram,
electrooculogram and a submental electromyogram. The 11C-glucose used in the
experiment was prepared by the biosynthetic method developed by Lifton and Welch,
using 11C produced by a baby cyclotron. The 11C-glucose solution, containing
about 20 mCi of 11C activity was administered orally to the subjects. The time
course of the 11C activity in the blood following the administration was
determined by drawing 1 ml of blood from the antecubital vein once every 10
minutes. These samples were assayed for 11C activity in a NaI well counter. The
PET images of a horizontal cross-section of the brain at 45 mm above the
orbito-meatal line, were used for the analysis of the glucose
metabolism.(ABSTRACT TRUNCATED AT 250 WORDS)


PMID: 3502249  [PubMed - indexed for MEDLINE]

