
1. Brain Dev. 2013 Jan;35(1):61-7. doi: 10.1016/j.braindev.2012.01.007. Epub 2012
Feb 8.

Ultrasound evaluation of fetal brain dysfunction based on behavioral patterns.

Morokuma S(1), Fukushima K, Otera Y, Yumoto Y, Tsukimori K, Ochiai M, Hara T,
Wake N.

Author information: 
(1)Department of Obstetrics and Gynecology, Graduate School of Medical Sciences, 
Kyushu University, Higashi-ku, Fukuoka, Japan. morokuma@med.kyushu-u.ac.jp

To identify fetuses at high risk of poor neurological outcomes using a novel
ultrasound evaluation system. We assessed an ultrasound evaluation system based
on our previous findings, consisting of screening for decreased or lack of fetal 
movements, abnormal patterns of fetal heart rate, congenital CNS malformations,
polyhydramnios of unknown cause, and a "brief ultrasound evaluation" of fetal
brain functions, including movement of extremities, breathing movements,
ultradian rhythm, REM period, and NREM period. We then assessed the correlation
between fetal brain functions and neurological outcomes in infancy (MR, CP, and
low Developmental Quotient). During screening, we prospectively evaluated 4978
fetuses receiving prenatal and intrapartum management between January 2000 and
December 2009 in our hospital that were later delivered between 32 and 41 weeks' 
gestation and identified 93 cases as suspicious for impairment. Of the 93
fetuses, 26 underwent the second step of brief ultrasound examination at 35-40
weeks' gestation. Our findings revealed that this method was adequately sensitive
(80%) and specific (88%) in identifying neurological impairment. We concluded
that this method was mainly useful in the clinical setting for establishing the
first indication for fetal CNS examination for functional impairment, rendering
it suitable for clinical application.

Copyright © 2012 The Japanese Society of Child Neurology. Published by Elsevier
B.V. All rights reserved.

DOI: 10.1016/j.braindev.2012.01.007 
PMID: 22321861  [PubMed - indexed for MEDLINE]

