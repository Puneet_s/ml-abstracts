
1. Clin Neurophysiol. 2010 Nov;121(11):1844-54. doi: 10.1016/j.clinph.2010.03.054.

Reduced fronto-cortical brain connectivity during NREM sleep in Asperger
syndrome: an EEG spectral and phase coherence study.

Lázár AS(1), Lázár ZI, Bíró A, Gyori M, Tárnok Z, Prekop C, Keszei A, Stefanik K,
Gádoros J, Halász P, Bódizs R.

Author information: 
(1)Institute of Behavioural Sciences, Semmelweis University, Budapest, Hungary.
a.lazar@surrey.ac.uk

OBJECTIVE: To investigate whether sleep macrostructure and EEG power spectral
density and coherence during NREM sleep are different in Asperger syndrome (AS)
compared to typically developing children and adolescents.
METHODS: Standard all night EEG sleep parameters were obtained from 18
un-medicated subjects with AS and 14 controls (age range: 7.5-21.5years) after
one adaptation night. Spectral, and phase coherence measures were computed for
multiple frequency bands during NREM sleep.
RESULTS: Sleep latency and wake after sleep onset were increased in AS. Absolute 
power spectrum density (PSD) was significantly reduced in AS in the alpha, sigma,
beta and gamma bands and in all 10 EEG derivations. Relative PSD showed a
significant increase in delta and a decrease in the sigma band for frontal, and
in beta for centro-temporal derivations. Intrahemispheric coherence measures were
markedly lower in AS in the frontal areas, and the right hemisphere over all EEG 
channels. The most prominent reduction in intrahemispheric coherence was observed
over the fronto-central areas in delta, theta, alpha and sigma EEG frequency
bands.
CONCLUSION: EEG power spectra and coherence during NREM sleep, in particular in
fronto-cortical derivations are different in AS compared to typically developing 
children and adolescents.
SIGNIFICANCE: Quantitative analysis of the EEG during NREM sleep supports the
hypothesis of frontal dysfunction in AS.

Copyright © 2010 International Federation of Clinical Neurophysiology. Published 
by Elsevier Ireland Ltd. All rights reserved.

DOI: 10.1016/j.clinph.2010.03.054 
PMID: 20434395  [PubMed - indexed for MEDLINE]

