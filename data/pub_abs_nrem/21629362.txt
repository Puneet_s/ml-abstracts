
1. Sleep. 2011 Jun 1;34(6):745-50. doi: 10.5665/SLEEP.1040.

The heterogeneity of obstructive sleep apnea (predominant obstructive vs pure
obstructive apnea).

Xie A(1), Bedekar A, Skatrud JB, Teodorescu M, Gong Y, Dempsey JA.

Author information: 
(1)Population Health Sciences, University of Wisconsin, Madison, WI, USA.
axie@wisc.edu

STUDY OBJECTIVES: To compare the breathing instability and upper airway
collapsibility between patients with pure OSA (i.e. 100% of apneas are
obstructive) and patients with predominant OSA (i.e., coexisting obstructive and 
central apneas).
DESIGN: A cross-sectional study with data scored by a fellow being blinded to the
subjects' classification. The results were compared between the 2 groups with
unpaired student t-test.
SETTING AND INTERVENTIONS: Standard polysomnography technique was used to
document sleep-wake state. Ventilator in pressure support mode was used to
introduce hypocapnic apnea during CO(2) reserve measurement. CPAP with both
positive and negative pressures was used to produce obstructive apnea during
upper airway collapsibility measurement.
PARTICIPANTS: 21 patients with OSA: 12 with coexisting central/mixed apneas and
hypopneas (28% ± 6% of total), and 9 had pure OSA.
MEASUREMENTS: The upper airway collapsibility was measured by assessing the
critical closing pressure (Pcrit). Breathing stability was assessed by measuring 
CO(2) reserve (i.e., ΔPCO(2) [eupnea-apnea threshold]) during NREM sleep.
RESULTS: There was no difference in Pcrit between the 2 groups (pure OSA vs.
predominant OSA: 2.0 ± 0.4 vs. 2.7 ± 0.4 cm H(2)O, P = 0.27); but the CO(2)
reserve was significantly smaller in predominant OSA group (1.6 ± 0.7 mm Hg) than
the pure OSA group (3.8 ± 0.6 mm Hg) (P = 0.02).
CONCLUSIONS: The present data indicate that breathing stability rather than upper
airway collapsibility distinguishes OSA patients with a combination of
obstructive and central events from those with pure OSA.

DOI: 10.5665/SLEEP.1040 
PMCID: PMC3099495
PMID: 21629362  [PubMed - indexed for MEDLINE]

