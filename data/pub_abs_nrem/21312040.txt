
1. Eur Arch Psychiatry Clin Neurosci. 2011 Dec;261(8):559-66. doi:
10.1007/s00406-011-0195-5. Epub 2011 Feb 11.

Sleep homeostasis in alcohol-dependent, depressed and healthy control men.

Brower KJ(1), Hoffmann R, Conroy DA, Arnedt JT, Armitage R.

Author information: 
(1)Department of Psychiatry, University of Michigan, 4250 Plymouth Rd, SPC 5740, 
Ann Arbor, MI 48109-2700, USA. kbrower@umich.edu

Visually scored and power spectral analyses (PSA) of polysomnography (PSG)
recordings reveal abnormalities in alcohol dependence (AD) and major depressive
disorder (MDD), including deficiencies in slow wave activity (SWA) during
non-rapid eye movement (NREM) sleep. SWA parameters reflect the integrity of the 
homeostatic sleep drive, which have not been compared in those with AD or MDD.
Ten men with AD were compared with 10 men with MDD and 10 healthy controls (HCs),
all aged 20-40 years. They maintained an 11 pm to 6 am sleep schedule for
5-7 days, followed by 3 consecutive nights of PSG in the laboratory: night 1 for 
adaptation/screening; night 2 for baseline recordings; and night 3 as the
challenge night, delaying sleep until 2 am. SWA was quantified with PSA across 4 
NREM periods. Men with AD generated the least SWA at baseline. In response to
sleep delay, HC men showed the expected SWA enhancement and a sharper exponential
decline across NREM periods. Both the MDD and the AD groups showed a
significantly blunted SWA response to sleep delay. Men with MDD had the least SWA
in the first NREM period (impaired accumulation of sleep drive), whereas men with
AD had the slowest SWA decay rate (impaired dissipation of sleep drive). These
results suggest that both SWA generation and its homeostatic regulation are
impaired in men with either AD or MDD. Finding interventions that selectively
improve these different components of sleep homeostasis should be a goal of
treatment for AD and MDD.

DOI: 10.1007/s00406-011-0195-5 
PMCID: PMC3156901
PMID: 21312040  [PubMed - indexed for MEDLINE]

