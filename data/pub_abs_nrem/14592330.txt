
1. Sleep Med. 2003 May;4(3):247-9.

Sleep disorder in alien hand syndrome.

Ortega-Albás JJ(1), de Entrambasaguas M, Montoya FJ, Serrano AL, Geffner D.

Author information: 
(1)Sleep Unit, Department of Clinical Neurophysiology, Hospital General de
Castellón, Avenida de Benicàssim s/n, E-12004 Castellón, Spain. ortega_jua@gva.es

A 63-year-old right-handed woman developed an alien hand syndrome (AHS) after an 
acute infarction in the territory of the left anterior cerebral artery. The
uncontrolled hand movements were present during the daytime and eventually
disturbed sleep. Polysomnography revealed that these motor actions only appeared 
when the patient was awake. These awakenings emerged mostly from NREM sleep stage
2 during the first half of the night. There was no evidence of any epileptiform
activity, dyssomnia or parasomnia. These movements were controlled making her
wear an oven mitt during sleep. The temporal distribution of this motor activity 
seems to follow the progressive hyperpolarization of anterior horn neurons that
occurs when sleep deepens. The accommodation of the grasp reflex in AHS probably 
helps control this unwanted motor activity.


PMID: 14592330  [PubMed - indexed for MEDLINE]

