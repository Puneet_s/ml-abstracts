
1. Obes Res Clin Pract. 2013 Jul-Aug;7(4):e251-7. doi: 10.1016/j.orcp.2013.01.005.

Hypothalamic prepro-orexin mRNA level is inversely correlated to the non-rapid
eye movement sleep level in high-fat diet-induced obese mice.

Tanno S(1), Terao A, Okamatsu-Ogura Y, Kimura K.

Author information: 
(1)Laboratory of Biochemistry, Department of Biomedical Sciences, Graduate School
of Veterinary Medicine, Hokkaido University, Sapporo 060-0818, Japan. Electronic 
address:terao@vetmed.hokudai.ac.jp.

Orexins are hypothalamic neuropeptides, which play important roles in the
regulation and maintenance of sleep/wakefulness states and energy homeostasis. To
evaluate whether alterations in orexin system is associated with the
sleep/wakefulness abnormalities observed in obesity, we examined the mRNA
expression of prepro-orexin, orexin receptor type 1 (orexin 1r), and orexin
receptor type 2 (oxexin 2r) in the hypothalamus in mice fed with a normal diet
(ND) and high-fat diet (HFD)-induced obese mice. We also compared their
relationships with sleep/wakefulness. Twenty-four, 4-week-old, male C57BL/6J mice
were divided randomly into three groups, which received the following: (1) ND for
17 weeks; (2) HFD for 17 weeks; and (3) ND for 7 weeks and HFD for a further 10
weeks. The body weights of mice fed the HFD for 10-17 weeks were 112-150% of the 
average body weight of the ND group. The daily amount of non-rapid eye movement
(NREM) sleep increased significantly in HFD-fed mice. These changes were
accompanied by increases in the number but decreases in the duration of each NREM
sleep episode. In addition, brief awakenings (<20 s epoch) during NREM sleep was 
nearly 2-fold more frequent. The mRNA level of prepro-orexin in the hypothalamus 
was significantly reduced in HFD-induced obese mice, whereas the levels of orexin
1r and orexin 2r were unaffected. The daily amount of NREM sleep was negatively
correlated with the hypothalamic prepro-orexin mRNA level, so these results
suggest that the increased NREM sleep levels in HFD-induced obese mice are
attributable to impaired orexin activity.

© 2013 Asian Oceanian Association for the Study of Obesity . Published by
Elsevier Ltd. All rights reserved.

DOI: 10.1016/j.orcp.2013.01.005 
PMID: 24306152  [PubMed - indexed for MEDLINE]

