
1. Brain Res. 2000 Jun 2;866(1-2):152-67.

Ultradian oscillations in cranial thermoregulation and electroencephalographic
slow-wave activity during sleep are abnormal in humans with annual winter
depression.

Schwartz PJ(1), Rosenthal NE, Kajimura N, Han L, Turner EH, Bender C, Wehr TA.

Author information: 
(1)Department of Psychiatry, University of Cincinnati College of Medicine and
Veterans Affairs Medical Center, Cincinnati, OH, USA. pjs4@ix.netcom.com

The level of core body, and presumably brain temperature during sleep varies with
clinical state in patients with seasonal affective disorder (SAD), becoming
elevated during winter depression and lowered during clinical remission induced
by either light treatment or summer. During sleep, brain temperatures are in part
determined by the level of brain cooling activity, which may be reflected by
facial skin temperatures. In many animals, the level of brain cooling activity
oscillates across the NREM-REM sleep cycle. Facial skin temperatures during sleep
in patients with winter depression are abnormally low and uncorrelated with
rectal temperatures, although their relationship to EEG-defined sleep stages
remains unknown. We therefore measured the sleep EEG, core body and facial skin
temperatures in 23 patients with winter depression and 23 healthy controls, and
tested the hypothesis that ultradian oscillations in facial skin temperatures
exist in humans and are abnormal in patients with winter depression. We found
that facial skin temperatures oscillated significantly across the NREM-REM sleep 
cycle, and were again significantly lower and uncorrelated with rectal
temperatures in patients with winter depression. Mean slow-wave activity and NREM
episode duration were significantly greater in patients with winter depression,
whereas the intraepisodic dynamics of slow-wave activity were normal in patients 
with winter depression. These results suggest that brain cooling activity
oscillates in an ultradian manner during sleep in humans and is reduced during
winter depression, and provide additional support for the hypothesis that brain
temperatures are elevated during winter depression.


PMID: 10825491  [PubMed - indexed for MEDLINE]

