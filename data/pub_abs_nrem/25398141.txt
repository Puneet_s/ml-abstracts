
1. PLoS One. 2014 Nov 14;9(11):e112849. doi: 10.1371/journal.pone.0112849.
eCollection 2014.

Enhanced slow-wave EEG activity and thermoregulatory impairment following the
inhibition of the lateral hypothalamus in the rat.

Cerri M(1), Del Vecchio F(1), Mastrotto M(1), Luppi M(1), Martelli D(1), Perez
E(1), Tupone D(1), Zamboni G(1), Amici R(1).

Author information: 
(1)Department of Biomedical and NeuroMotor Sciences, Alma Mater Studiorum -
University of Bologna, Bologna, Italy.

Neurons within the lateral hypothalamus (LH) are thought to be able to evoke
behavioural responses that are coordinated with an adequate level of autonomic
activity. Recently, the acute pharmacological inhibition of LH has been shown to 
depress wakefulness and promote NREM sleep, while suppressing REM sleep. These
effects have been suggested to be the consequence of the inhibition of specific
neuronal populations within the LH, i.e. the orexin and the MCH neurons,
respectively. However, the interpretation of these results is limited by the lack
of quantitative analysis of the electroencephalographic (EEG) activity that is
critical for the assessment of NREM sleep quality and the presence of aborted
NREM-to-REM sleep transitions. Furthermore, the lack of evaluation of the
autonomic and thermoregulatory effects of the treatment does not exclude the
possibility that the wake-sleep changes are merely the consequence of the
autonomic, in particular thermoregulatory, changes that may follow the inhibition
of LH neurons. In the present study, the EEG and autonomic/thermoregulatory
effects of a prolonged LH inhibition provoked by the repeated local delivery of
the GABAA agonist muscimol were studied in rats kept at thermoneutral (24°C) and 
at a low (10°C) ambient temperature (Ta), a condition which is known to depress
sleep occurrence. Here we show that: 1) at both Tas, LH inhibition promoted a
peculiar and sustained bout of NREM sleep characterized by an enhancement of
slow-wave activity with no NREM-to-REM sleep transitions; 2) LH inhibition caused
a marked transitory decrease in brain temperature at Ta 10°C, but not at Ta 24°C,
suggesting that sleep changes induced by LH inhibition at thermoneutrality are
not caused by a thermoregulatory impairment. These changes are far different from
those observed after the short-term selective inhibition of either orexin or MCH 
neurons, suggesting that other LH neurons are involved in sleep-wake modulation.

DOI: 10.1371/journal.pone.0112849 
PMCID: PMC4232523
PMID: 25398141  [PubMed - indexed for MEDLINE]

