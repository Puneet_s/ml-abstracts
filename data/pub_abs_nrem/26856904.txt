
1. Sleep. 2016 May 1;39(5):1069-82. doi: 10.5665/sleep.5758.

Responses in Rat Core Auditory Cortex are Preserved during Sleep Spindle
Oscillations.

Sela Y(1), Vyazovskiy VV(2), Cirelli C(3), Tononi G(3), Nir Y(1,)(4).

Author information: 
(1)Sagol School of Neuroscience, Tel Aviv University, Tel Aviv, Israel.
(2)Department of Physiology, Anatomy and Genetics, University of Oxford, Oxford, 
United Kingdom. (3)Department of Psychiatry, University of Wisconsin-Madison,
Madison, WI. (4)Department of Physiology and Pharmacology, Sackler School of
Medicine, Tel Aviv University, Tel Aviv, Israel.

STUDY OBJECTIVES: Sleep is defined as a reversible state of reduction in sensory 
responsiveness and immobility. A long-standing hypothesis suggests that a high
arousal threshold during non-rapid eye movement (NREM) sleep is mediated by sleep
spindle oscillations, impairing thalamocortical transmission of incoming sensory 
stimuli. Here we set out to test this idea directly by examining sensory-evoked
neuronal spiking activity during natural sleep.
METHODS: We compared neuronal (n = 269) and multiunit activity (MUA), as well as 
local field potentials (LFP) in rat core auditory cortex (A1) during NREM sleep, 
comparing responses to sounds depending on the presence or absence of sleep
spindles.
RESULTS: We found that sleep spindles robustly modulated the timing of neuronal
discharges in A1. However, responses to sounds were nearly identical for all
measured signals including isolated neurons, MUA, and LFPs (all differences <
10%). Furthermore, in 10% of trials, auditory stimulation led to an early
termination of the sleep spindle oscillation around 150-250 msec following
stimulus onset. Finally, active ON states and inactive OFF periods during slow
waves in NREM sleep affected the auditory response in opposite ways, depending on
stimulus intensity.
CONCLUSIONS: Responses in core auditory cortex are well preserved regardless of
sleep spindles recorded in that area, suggesting that thalamocortical sensory
relay remains functional during sleep spindles, and that sensory disconnection in
sleep is mediated by other mechanisms.

© 2016 Associated Professional Sleep Societies, LLC.

DOI: 10.5665/sleep.5758 
PMCID: PMC4835306 [Available on 2016-11-01]
PMID: 26856904  [PubMed - in process]

