
1. Can J Neurol Sci. 2013 Jul;40(4):536-9.

Sleep-laughing--hypnogely.

Trajanovic NN(1), Shapiro CM, Milovanovic S.

Author information: 
(1)Sleep Research Unit, University Health Network, Toronto, Ontario, Canada.
hypogely@gmail.com

OBJECTIVE: To explain relatively common phenomenon of laughing during sleep and
help to better define criteria for differentiating between physiological and
pathological sleep-laughing.
METHODS: Observational study of patients who underwent a sleep assessment in a
referential tertiary health facility.
RESULTS: A total of ten patients exhibited sleep laughing, nine of whom had
episodes associated with rapid eye movement (REM) sleep. Also, in one of the
patients sleep-laughing was one of the symptoms of REM sleep Behaviour Disorder, 
and in another patient sleep-laughing was associated with NREM sleep arousal
parasomnia.
CONCLUSION: The collected data and review of literature suggests that hypnogely
in majority of the cases presents as a benign physiological phenomenon related to
dreaming and REM sleep. Typically, these dreams are odd, bizarre or even unfunny 
for a person when awake. Nevertheless, they bring a sense of mirth and a genuine 
behavioural response. In a minority of cases, sleep-laughing appears to be a
symptom of neurological disorders affecting the central nervous system. In these 
patients the behavioural substrate differs when compared to physiological
laughing, and the sense of mirth is usually absent.


PMID: 23786736  [PubMed - indexed for MEDLINE]

