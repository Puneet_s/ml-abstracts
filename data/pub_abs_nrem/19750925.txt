
1. Sleep. 2009 Sep;32(9):1201-9.

Does the circadian modulation of dream recall modify with age?

Chellappa SL(1), Münch M, Blatter K, Knoblauch V, Cajochen C.

Author information: 
(1)CAPES Foundation/Ministry of Education of Brazil, Brasilia, Brazil.

STUDY OBJECTIVES: The ultradian NREM-REM sleep cycle and the circadian modulation
of REM sleep sum to generate dreaming. Here we investigated age-related changes
in dream recall, number of dreams, and emotional domain characteristics of
dreaming during both NREM and REM sleep.
DESIGN: Analysis of dream recall and sleep EEG (NREM/REM sleep) during a 40-h
multiple nap protocol (150 min of wakefulness and 75 min of sleep) under constant
routine conditions.
SETTING: Centre for Chronobiology, Psychiatric Hospital of the University of
Basel, Basel, Switzerland.
PARTICIPANTS: Seventeen young (20-31 years) and 15 older (57-74 years) healthy
volunteers
INTERVENTIONS: N/A.
MEASUREMENTS AND RESULTS: Dream recall and number of dreams varied significantly 
across the circadian cycle and between age groups, with older subjects exhibiting
fewer dreams (P < 0.05), particularly after naps scheduled during the biological 
day, closely associated with the circadian rhythm of REM sleep. No significant
age differences were observed for the emotional domain of dream content.
CONCLUSIONS: Since aging was associated with attenuated amplitude in the
circadian modulation of REM sleep, our data suggest that the age-related decrease
in dream recall can result from an attenuated circadian modulation of REM sleep.


PMCID: PMC2737578
PMID: 19750925  [PubMed - indexed for MEDLINE]

