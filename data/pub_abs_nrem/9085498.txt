
1. Sleep. 1996 Dec;19(10 Suppl):S157-9.

Accessing chemosensitivity and ventilatory stability from transient stimuli.

Bruce EN(1).

Author information: 
(1)Center for Biomedical Engineering, University of Kentucky, Lexington, USA.

The degree of ventilatory stability of human subjects is inferred from the
presence or absence of oscillations in ventilation in response to a brief CO2
disturbance using the method of pseudorandom stimulation. Simultaneously,
chemosensitivity is measured. Stability and chemosensitivity are compared in
hyperoxia between wakefulness and stage 2 non-rapid eye movement (NREM) sleep and
between normoxia and hyperoxia awake. Stability is unchanged between wakefulness 
and sleep but chemosensitivity decreases in sleep. In contrast, stability is
reduced in normoxia whereas chemosensitivity is larger than in hyperoxia. It is
concluded that chemosensitivity and ventilatory stability may change
independently, implying that chemosensitivity alone is not an adequate indicator 
of the likelihood of a subject to exhibit periodic breathing.


PMID: 9085498  [PubMed - indexed for MEDLINE]

