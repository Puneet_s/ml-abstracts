
1. Eur J Neurol. 2007 Aug;14(8):947-50.

Atypical presentation of NREM arousal parasomnia with repetitive episodes.

Trajanovic NN(1), Shapiro CM, Ong A.

Author information: 
(1)Sleep and Alertness Clinic, University Health Network, Toronto, ON, Canada.
somnolog@gmail.com

The case report describes a distinct variant of non-REM (Rapid Eye Movement)
arousal parasomnia, sleepwalking type, featuring repetitive abrupt arousals,
mostly from slow-wave sleep, and various automatisms and semi-purposeful
behaviours. The frequency of events and distribution throughout the night
presented as a continuous status of parasomnia ('status parasomnicus'). The
patient responded well to treatment typically administered for adult NREM
parasomnias, and after careful review of the clinical presentation, objective
findings and treatment outcome, sleep-related epilepsy was ruled out in favour of
parasomnia.

DOI: 10.1111/j.1468-1331.2007.01866.x 
PMID: 17662022  [PubMed - indexed for MEDLINE]

