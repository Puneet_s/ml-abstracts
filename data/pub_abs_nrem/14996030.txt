
1. J Sleep Res. 2004 Mar;13(1):1-23.

The nature of arousal in sleep.

Halász P(1), Terzano M, Parrino L, Bódizs R.

Author information: 
(1)Neurological Department, National Institute of Psychiatry and Neurology,
Budapest, Hungary. halasz@opni.hu

The role of arousals in sleep is gaining interest among both basic researchers
and clinicians. In the last 20 years increasing evidence shows that arousals are 
deeply involved in the pathophysiology of sleep disorders. The nature of arousals
in sleep is still a matter of debate. According to the conceptual framework of
the American Sleep Disorders Association criteria, arousals are a marker of sleep
disruption representing a detrimental and harmful feature for sleep. In contrast,
our view indicates arousals as elements weaved into the texture of sleep taking
part in the regulation of the sleep process. In addition, the concept of
micro-arousal (MA) has been extended, incorporating, besides the classical
low-voltage fast-rhythm electroencephalographic (EEG) arousals, high-amplitude
EEG bursts, be they like delta-like or K-complexes, which reflects a special kind
of arousal process, mobilizing parallely antiarousal swings. In physiologic
conditions, the slow and fast MA are not randomly scattered but appear
structurally distributed within sleep representing state-specific arousal
responses. MA preceded by slow waves occurs more frequently across the descending
part of sleep cycles and in the first cycles, while the traditional fast type of 
arousals across the ascending slope of cycles prevails during the last third of
sleep. The uniform arousal characteristics of these two types of MAs is supported
by the finding that different MAs are associated with an increasing magnitude of 
vegetative activation ranging hierarchically from the weaker slow EEG types
(coupled with mild autonomic activation) to the stronger rapid EEG types (coupled
with a vigorous autonomic activation). Finally, it has been ascertained that MA
are not isolated events but are basically endowed with a periodic nature
expressed in non-rapid eye movement (NREM) sleep by the cyclic alternating
pattern (CAP). Understanding the role of arousals and CAP and the relationship
between physiologic and pathologic MA can shed light on the adaptive properties
of the sleeping brain and provide insight into the pathomechanisms of sleep
disturbances. Functional significance of arousal in sleep, and particularly in
NREM sleep, is to ensure the reversibility of sleep, without which it would be
identical to coma. Arousals may connect the sleeper with the surrounding world
maintaining the selection of relevant incoming information and adapting the
organism to the dangers and demands of the outer world. In this dynamic
perspective, ongoing phasic events carry on the one hand arousal influences and
on the other elements of information processing. The other function of arousals
is tailoring the more or less stereotyped endogenously determined sleep process
driven by chemical influences according to internal and external demands. In this
perspective, arousals shape the individual course of night sleep as a variation
of the sleep program.


PMID: 14996030  [PubMed - indexed for MEDLINE]

