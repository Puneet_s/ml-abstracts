
1. Rheum Dis Clin North Am. 1989 Feb;15(1):91-103.

Sleep and fibrositis syndrome.

Moldofsky H(1).

Author information: 
(1)University of Toronto, Ontario, Canada.

Chronic diffuse myalgia, localized areas of tenderness, fatigue, and unrefreshing
sleep are related to a physiologic arousal disorder within sleep, that is, the
alpha EEG NREM sleep anomaly. This sleep physiologic disorder, nonrestorative
sleep, and symptoms of fibrositis syndrome are shown to occur with psychologic,
environmental, and physiologic distress conditions. Pathogenic mechanisms that
link nonrestorative sleep physiology to pain and fatigue may involve metabolic
dysfunction of the brain with sleep-related alteration in immunologic and
neurotransmitter functions (serotonin, substance P, endorphins). These
sleep-related mechanisms have important implications for the understanding and
treatment of fibrositis/fibromyalgia syndrome.


PMID: 2644681  [PubMed - indexed for MEDLINE]

