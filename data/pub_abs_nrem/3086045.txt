
1. Chest. 1986 Jun;89(6):800-5.

A possible mechanism for mixed apnea in obstructive sleep apnea.

Iber C, Davies SF, Chapman RC, Mahowald MM.

Hypopneas or pauses in respiratory effort frequently precede episodes of
obstructive sleep apnea resulting in mixed apneas. We studied five subjects after
chronic tracheostomy for obstructive sleep apnea. During stable non-REM (NREM)
sleep, subjects breathed entirely through the tracheostomy. Tracheostomy
occlusion caused experimental obstructive apnea which lasted 13.9 +/- 4.7 sec and
ended with transient arousal and pharyngeal opening. At the end of the apnea
there was marked hyperventilation (inspired minute ventilation rose 21.6 +/- 3.5 
L on the first breath) followed by hypocapnia, hypopnea, and pauses in
inspiratory effort as the subjects resumed NREM sleep. Hypocapnia was greater
before inspiratory pauses lasting at least 5 sec than before shorter pauses
(PETco2, 4.2 +/- 1.8 mm Hg below baseline vs 1.2 +/- 2.5 mm Hg below baseline).
In three patients, pauses in inspiratory effort following experimental
obstructive apnea were prevented by administration of 4 percent CO2 and 40
percent O2 inspired gas. This study suggests that: hyperventilation with
hypocapnia occurs at the termination of obstructive apneas, and hypocapnia may be
responsible for the attenuation or cessation of respiratory effort initiating the
subsequent cycle of obstruction.


PMID: 3086045  [PubMed - indexed for MEDLINE]

