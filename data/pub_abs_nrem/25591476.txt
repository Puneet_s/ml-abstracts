
1. Behav Brain Res. 2015 Apr 1;282:218-26. doi: 10.1016/j.bbr.2015.01.009. Epub 2015
Jan 13.

Relevance of the metabotropic glutamate receptor (mGluR5) in the regulation of
NREM-REM sleep cycle and homeostasis: evidence from mGluR5 (-/-) mice.

Ahnaou A(1), Raeymaekers L(2), Steckler T(2), Drinkenbrug WH(2).

Author information: 
(1)Department of Neuroscience, A Division of Janssen Pharmaceutica N.V.,
Turnhoutseweg 30, B-2340 Beerse, Belgium. Electronic address:
aahnaou@its.jnj.com. (2)Department of Neuroscience, A Division of Janssen
Pharmaceutica N.V., Turnhoutseweg 30, B-2340 Beerse, Belgium.

Sleep is a homeostatically regulated behavior and sleep loss evokes a
proportional increase in sleep time and delta slow wave activity. Glutamate and
pharmacological modulation of the metabotropic glutamate receptors (mGluR)
signaling have been implicated in the organization of vigilance states. Here, the
role of the mGluR5 on homeostatic regulation of sleep-wake cycle and
electroencephalographic (EEG) activity was examined in mGluR5 (-/-) mice. We
first characterized the sleep-wake EEG phenotype in mGluR5 (-/-) and wild-type
(WT) littermates mice by continuous recording for 72h of EEG, body temperature
(BT) and locomotor activity (LMA). Next, we investigated the influence of sleep
deprivation on the recovery sleep and EEG slow wave activity (1-4Hz) during NREM 
sleep to assess whether mGluR5 deletion affects the sleep homeostasis process.
Like the control animals, mGluR5 (-/-) mice exhibited a clear-cut circadian
sleep-wake architecture, however they showed reduced REM sleep time during the
light phase with shorter REM sleep bouts and reduced state transitions in the
NREM sleep-REM sleep cycle during the first and last 24h of the spontaneous 72h
recording period. In addition, mGluR5 (-/-) mice had decreased slow EEG delta
power during NREM sleep and enhanced LMA associated with elevated BT during the
dark phase. Moreover, mGluR5 (-/-) mice exhibited reduced slow wave activity and 
sleep drive after sleep deprivation, indicating altered sleep homeostatic
processes. The findings strongly indicate that mGluR5 is involved in shaping the 
stability of NREM sleep-REM sleep state transitions, NREM slow wave activity and 
homeostatic response to sleep loss.

Copyright © 2015 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.bbr.2015.01.009 
PMID: 25591476  [PubMed - indexed for MEDLINE]

