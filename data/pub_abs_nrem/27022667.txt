
1. Pharm Biol. 2016 Oct;54(10):2141-8. doi: 10.3109/13880209.2016.1148175. Epub 2016
Mar 29.

Hydroalcoholic extract of Myrtus communis can alter anxiety and sleep parameters:
a behavioural and EEG sleep pattern study in mice and rats.

Hajiaghaee R(1), Faizi M(2), Shahmohammadi Z(2), Abdollahnejad F(3), Naghdibadi
H(1), Najafi F(4), Razmi A(1).

Author information: 
(1)a Medicinal Plants Research Center, Institute of Medicinal Plants, ACECR ,
Karaj , Iran ; (2)b Department of Pharmacology and Toxicology, School of Pharmacy
, Shahid Beheshti University of Medical Sciences , Tehran , Iran ; (3)c School of
Traditional Medicine, Shahid Beheshti University of Medical Sciences , Tehran ,
Iran ; (4)d Biomedical Engineering Department, Faculty of Engineering , Shahed
University , Tehran , Iran.

CONTEXT: Myrtus communis L. (Myrtaceae), myrtle, is an evergreen shrub with
strong antibacterial, anti-inflammatory, antihyperglycemic and antioxidant
activities. Also, it is used as a sedative-hypnotic plant in Iranian traditional 
medicine.
OBJECTIVE: This study evaluates the effect of 80% ethanolic extract of M.
communis leaves on sleep and anxiety in mice and rats.
MATERIALS AND METHODS: Male NMRI mice were subjected to open field, righting
reflex, grip strength and pentylentetrazole-induced seizure tests. Male Wistar
rats were used to evaluate the alterations in rapid eye movement (REM) and
non-REM (NREM) sleep. They were treated with 25-400 mg/kg doses of the extract
intraperitoneally.
RESULTS: The applied doses (50-200 mg/kg) of M. communis extract increased
vertical (ED50 = 40.2 ± 6.6 mg/kg) and vertical and horizontal activity
(ED50 = 251 ± 55 mg/kg), while treatment with 200 and 400 mg/kg attenuated muscle
tone significantly compared to vehicle treated animals (p < 0.001 for all) in a
dose-independent manner. Also, a significant hypnotic and not anticonvulsant
effect was observed when animals were treated with 200 mg/kg of the extract
(p < 0.01). In this regard, electroencephalography results showed that REM sleep 
time was decreased (2.4 ± 0.5%), while total and NREM sleep times were increased 
significantly compared to the control group of mice (82.5 ± 7.6%).
DISCUSSION AND CONCLUSION: The data show the anxiolytic and muscle relaxant
effect of the extract without anticonvulsant activities. The anxiolytic,
myorelaxant and hypnotic effects without effect on seizure threshold are in line 
with the effect of a alpha 2 GABA receptor agonist.

DOI: 10.3109/13880209.2016.1148175 
PMID: 27022667  [PubMed - in process]

