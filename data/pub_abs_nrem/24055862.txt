
1. Brain Behav Immun. 2014 Jan;35:125-34. doi: 10.1016/j.bbi.2013.09.006. Epub 2013 
Sep 18.

Sleep and behavior during vesicular stomatitis virus induced encephalitis in
BALB/cJ and C57BL/6J mice.

Machida M(1), Ambrozewicz MA(1), Breving K(2), Wellman LL(1), Yang L(1), Ciavarra
RP(2), Sanford LD(3).

Author information: 
(1)Sleep Research Laboratory, Department of Pathology and Anatomy, Eastern
Virginia Medical School, Norfolk, VA, United States. (2)Department of Molecular
and Cellular Biology, Eastern Virginia Medical School, Norfolk, VA, United
States. (3)Sleep Research Laboratory, Department of Pathology and Anatomy,
Eastern Virginia Medical School, Norfolk, VA, United States. Electronic address: 
sanforld@evms.edu.

Intranasal application of vesicular stomatitis virus (VSV) produces a
well-characterized model of viral encephalitis in mice. Within one day
post-infection (PI), VSV travels to the olfactory bulb and, over the course of 7 
days, it infects regions and tracts extending into the brainstem followed by
clearance and recovery in most mice by PI day 14 (PI 14). Infectious diseases are
commonly accompanied by excessive sleepiness; thus, sleep is considered a
component of the acute phase response to infection. In this project, we studied
the relationship between sleep and VSV infection using C57BL/6 (B6) and BALB/c
mice. Mice were implanted with transmitters for recording EEG, activity and
temperature by telemetry. After uninterrupted baseline recordings were collected 
for 2 days, each animal was infected intranasally with a single low dose of VSV
(5×10(4) PFU). Sleep was recorded for 15 consecutive days and analyzed on PI 0,
1, 3, 5, 7, 10, and 14. Compared to baseline, amounts of non-rapid eye movement
sleep (NREM) were increased in B6 mice during the dark period of PI 1-5, whereas 
rapid eye movement sleep (REM) was significantly reduced during the light periods
of PI 0-14. In contrast, BALB/c mice showed significantly fewer changes in NREM
and REM. These data demonstrate sleep architecture is differentially altered in
these mouse strains and suggests that, in B6 mice, VSV can alter sleep before
virus progresses into brain regions that control sleep.

Copyright © 2013 Elsevier Inc. All rights reserved.

DOI: 10.1016/j.bbi.2013.09.006 
PMCID: PMC3959631
PMID: 24055862  [PubMed - indexed for MEDLINE]

