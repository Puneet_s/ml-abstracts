
1. J Hypertens. 2003 Aug;21(8):1555-61.

Sleep-related changes in baroreflex sensitivity and cardiovascular autonomic
modulation.

Legramante JM(1), Marciani MG, Placidi F, Aquilani S, Romigi A, Tombini M,
Massaro M, Galante A, Iellamo F.

Author information: 
(1)Dipartimento di Medicina Interna-Centro di Riabilitazione Cardiologica San
Raffaele-Pisana, Université di Roma, Rome, Italy. legramante@med.uniroma2.it

Comment in
    J Hypertens. 2003 Aug;21(8):1455-7.

OBJECTIVE: We examined the effects of the various sleep stages on baroreflex
sensitivity (BRS), and heart rate and blood pressure (BP) variability, and tested
the hypothesis that there is a different behavior of the baroreflex control of
the sinus node in response to hypertensive and hypotensive stimuli and in
relation to different cycles of the overnight sleep.
DESIGN: Polygraphic sleep recordings were performed in 10 healthy males. The BP
and the RR interval were continuously recorded during sleep.
METHODS: BRS was calculated by the sequences method. Autoregressive power
spectral analysis was used to investigate the RR-interval and BP variabilities.
RESULTS: During rapid eye movement (REM) sleep BRS significantly increased in
response to hypertensive stimuli in comparison with non-rapid eye movement (NREM)
sleep and the awake state, whereas it did not change in response to hypotensive
stimuli. In the first sleep cycle, BRS significantly increased during NREM in
comparison with wakefulness, whereas during REM BRS in response to hypertensive
stimuli did not show significant changes as compared with the awake state and/or 
with NREM. During REM occurring in the sleep cycle before morning awakening, BRS 
showed a significant increase in response to hypertensive stimuli in comparison
with both NREM and the awake state.
CONCLUSIONS: During sleep, arterial baroreflex modulation of the sinus node is
different in response to hypotensive and hypertensive stimuli particularly during
REM. Furthermore, baroreflex control of the sinus node shows a non-uniform
behavior during REM occurring in different nocturnal sleep cycles. These findings
suggest that the arterial baroreflex is more effective in buffering the increased
sympathetic activation associated with REM at the end of sleep than in the early 
night.

DOI: 10.1097/01.hjh.0000084700.87421.fb 
PMID: 12872051  [PubMed - indexed for MEDLINE]

