
1. Lung. 2014 Feb;192(1):185-90. doi: 10.1007/s00408-013-9510-4. Epub 2013 Oct 1.

Correlates of obstructive apnea duration.

Koo BB(1), Mansour A.

Author information: 
(1)Department of Neurology, University Hospitals Case Medical Center, Case
Western Reserve University School of Medicine, 11100 Euclid Avenue, Cleveland,
OH, 44106, USA, koobri@gmail.com.

PURPOSE: The duration of an obstructive apnea is dependent on gender, age, body
position, and state. Termination of an obstructive respiratory event is
attributed as the end result of an achieved specific negative inspiratory force, 
sufficient to result in cortical arousal. We used polysomnographic measures of
tendency for arousal, hypothesizing that long apnea would be associated with a
smaller tendency for arousal.
METHODS: From a clinical sleep laboratory sample, 140 subjects (82 men) with an
obstructive apnea index >5 were selected. Subjects were split into those with
long and short apnea by stratifying around median obstructive apnea duration.
Those with long duration apnea were compared to short apnea for age, gender,
apnea-hypopnea index (AHI), and a measure of tendency to arouse, respiratory
event-related arousal to AHI ratio (RespArI:AHI).
RESULTS: Obstructive apnea duration (mean ± SD) was 20.7 ± 5.6. Apnea duration
trended toward being longer in men than women and was significantly longer in
supine than nonsupine sleep (19.2 ± 7.3 vs. 16.1 ± 10; p = 0.001) and REM than
NREM sleep (24.3 ± 9.0 vs. 19.7 ± 5.1; p < 0.001). Those with long compared with 
short apnea were significantly older; AHI was similar. RespArI:AHI was
significantly higher for those with long compared with short apnea (0.58 ± 0.24
and 0.43 ± 0.19; p < 0.0001). This difference was most apparent for women in whom
RespArI:AHI for those with long and short apnea was 0.6 ± 0.24 and 0.41 ± 0.17
(p = 0.001). For men, RespArI:AHI for those with long and short obstructive apnea
was 0.56 ± 0.24 and 0.44 ± 0.2 (p = 0.02).
CONCLUSIONS: Long obstructive apnea duration occurs to a greater degree in older 
individuals, in supine, and REM sleep. Women more than men with long apnea had a 
greater tendency toward arousal.

DOI: 10.1007/s00408-013-9510-4 
PMID: 24081878  [PubMed - indexed for MEDLINE]

