
1. Clin Neurophysiol. 2007 Nov;118(11):2512-8. Epub 2007 Sep 25.

Impaired cortical and autonomic arousal during sleep in multiple system atrophy.

Vetrugno R(1), D'Angelo R, Cortelli P, Plazzi G, Vignatelli L, Montagna P.

Author information: 
(1)Department of Neurological Sciences, University of Bologna, Via Ugo Foscolo 7,
40123 Bologna, Italy. vetrugno@neuro.unibo.it

OBJECTIVE: Periodic limb movements during sleep (PLMS) in Restless Legs Syndrome 
(RLS) are associated with arousals and stereotyped EEG and heart rate (HR)
changes. We investigated PLMS-related EEG and HR variations in multiple system
atrophy (MSA) in order to detect possible abnormalities in cortical and autonomic
arousal responses.
METHODS: Ten patients with MSA were contrasted against ten patients with primary 
RLS. Cortical (EEG) and autonomic (HR) variations associated with PLMS during
NREM sleep were analysed by means of Fast Fourier Transform and HR analysis. In
addition, we analysed the cyclic alternating pattern (CAP) during sleep, CAP
representing a measure of the spontaneous arousal oscillations during NREM sleep.
RESULTS: PLMS in RLS were associated with tachycardia and spectral EEG
variations, beginning about 2s before the onset of PLMS, and peaking 1-4s after. 
The HR and spectral EEG variations were strikingly reduced or absent in MSA. MSA 
patients also had significantly lower CAP rate compared to RLS patients.
CONCLUSIONS: Blunted HR and EEG spectral changes adjacent to PLMS indicated
impaired cortical and autonomic arousal responses during sleep in MSA patients.
SIGNIFICANCE: PLMS, when present, may represent a useful means to study the
arousal responses during sleep.

DOI: 10.1016/j.clinph.2007.08.014 
PMID: 17897876  [PubMed - indexed for MEDLINE]

