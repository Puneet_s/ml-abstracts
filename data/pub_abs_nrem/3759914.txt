
1. J Clin Psychiatry. 1986 Oct;47(10):499-503.

Sleep-disordered breathing in normal and pathologic aging.

Hoch CC, Reynolds CF 3rd, Kupfer DJ, Houck PR, Berman SR, Stack JA.

In a study of sleep-disordered breathing among 139 elderly individuals, sleep
apnea (defined as 5 or more apneas per hour) occurred in 34 (41.7%) Alzheimer's
subjects compared with 56 (5.4%) healthy controls, 35 (11.4%) depressive
subjects, and 24 (16.7%) patients with mixed symptoms of both cognitive
impairment and depression (p less than .001). Alzheimer's patients had a
significantly higher proportion of NREM-related than REM-related apnea. Moreover,
a significant (p less than .01) positive correlation between the apnea index and 
severity of dementia, as measured by the Blessed Dementia Rating Scale, was found
in apnea-positive Alzheimer's patients, as well as in the entire sample of
Alzheimer's patients (p less than .05). No such correlation was found in the
mixed-symptoms group. Possible clinical and neuropathologic implications are
discussed.


PMID: 3759914  [PubMed - indexed for MEDLINE]

