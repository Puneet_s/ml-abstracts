
1. Am J Physiol Regul Integr Comp Physiol. 2010 Jan;298(1):R34-42. doi:
10.1152/ajpregu.00205.2009. Epub 2009 Oct 21.

Heterogeneous activity level of jaw-closing and -opening muscles and its
association with arousal levels during sleep in the guinea pig.

Kato T(1), Masuda Y, Kanayama H, Nakamura N, Yoshida A, Morimoto T.

Author information: 
(1)Osaka Univ. Graduate School of Dentistry, Dept. of Oral Anatomy and
Neurobiology, 1-8 Yamadaoka Suita, Osaka 565-0871, Japan.
takafumi@dent.osaka-u.ac.jp

Exaggerated jaw motor activities during sleep are associated with muscle symptoms
in the jaw-closing rather than the jaw-opening muscles. The intrinsic activity of
antagonistic jaw muscles during sleep remains unknown. This study aims to assess 
the balance of muscle activity between masseter (MA) and digastric (DG) muscles
during sleep in guinea pigs. Electroencephalogram (EEG), electroocculogram, and
electromyograms (EMGs) of dorsal neck, MA, and DG muscles were recorded with
video during sleep-wake cycles. These variables were quantified for each 10-s
epoch. The magnitude of muscle activity during sleep in relation to mean EMG
activity of total wakefulness was up to three times higher for MA muscle than for
DG muscle for nonrapid eye movement (NREM) and rapid-eye-movement (REM) sleep.
Although the activity level of the two jaw muscles fluctuated during sleep, the
ratio of activity level for each epoch was not proportional. Epochs with a high
activity level for each muscle were associated with a decrease in deltaEEG power 
and/or an increase in heart rate in NREM sleep. However, this association with
heart rate and activity levels was not observed in REM sleep. These results
suggest that in guinea pigs, the magnitude of muscle activity for antagonistic
jaw muscles is heterogeneously modulated during sleep, characterized by a high
activity level in the jaw-closing muscle. Fluctuations in the activity are
influenced by transient arousal levels in NREM sleep but, in REM sleep, the
distinct controls may contribute to the fluctuation. The above intrinsic
characteristics could underlie the exaggeration of jaw motor activities during
sleep (e.g., sleep bruxism).

DOI: 10.1152/ajpregu.00205.2009 
PMID: 19846747  [PubMed - indexed for MEDLINE]

