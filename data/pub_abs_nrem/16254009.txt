
1. J Physiol. 2006 Jan 15;570(Pt 2):385-96. Epub 2005 Oct 27.

Catecholamine neurones in rats modulate sleep, breathing, central chemoreception 
and breathing variability.

Li A(1), Nattie E.

Author information: 
(1)Department of Physiology, Dartmouth Medical School, Lebanon, NH 03756-0001,
USA.

Brainstem catecholamine (CA) neurones have wide projections and an
arousal-state-dependent activity pattern. They are thought to modulate the
processing of sensory information and also participate in the control of
breathing. Mice with lethal genetic defects that include CA neurones have
abnormal respiratory control at birth. Also the A6 region (locus coeruleus),
which contains CA neurones sensitive to CO(2) in vitro, is one of many putative
central chemoreceptor sites. We studied the role of CA neurones in the control of
breathing during sleep and wakefulness by specifically lesioning them with
antidopamine beta-hydroxylase-saporin (DBH-SAP) injected via the 4th ventricle.
After 3 weeks there was a 73-84% loss of A5, A6 and A7 tyrosine hydroxylase (TH) 
immunoreactive (ir) neurones along with 56-60% loss of C1 and C2 phenyl
ethanolamine-N-methyltransferase (PNMT)-ir neurones. Over the 3 weeks, breathing 
frequency decreased significantly during air and 3 or 7% CO(2) breathing in both 
wakefulness and non-REM (NREM) sleep. The rats spent significantly less time
awake and more time in NREM sleep. REM sleep time was unaffected. The ventilatory
response to 7% CO(2) was reduced significantly in wakefulness at 7, 14 and 21
days (-28%) and in NREM sleep at 14 and 21 days (-26%). Breathing variability
increased in REM sleep but not in wakefulness or NREM sleep. We conclude that CA 
neurones (1) promote wakefulness, (2) participate in central respiratory
chemoreception, (3) stimulate breathing frequency, and (4) minimize breathing
variability in REM sleep.

DOI: 10.1113/jphysiol.2005.099325 
PMCID: PMC1464315
PMID: 16254009  [PubMed - indexed for MEDLINE]

