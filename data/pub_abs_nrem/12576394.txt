
1. Chest. 2003 Feb;123(2):632-9.

Sleep-disordered breathing associated with long-term opioid therapy.

Farney RJ(1), Walker JM, Cloward TV, Rhondeau S.

Author information: 
(1)Intermountain Sleep Disorders Center, LDS Hospital, Salt Lake City, UT 84143, 
USA. rifmd@msn.com

Three patients are described who illustrate distinctive patterns of
sleep-disordered breathing that we have observed in patients who are receiving
long-term, sustained-release opioid medications. Polysomnography shows
respiratory disturbances occur predominantly during non-rapid eye movement (NREM)
sleep and are characterized by ataxic breathing, central apneas, sustained
hypoxemia, and unusually prolonged obstructive "hypopneas" secondary to delayed
arousal responses. In contrast to what is usually observed in subjects with
obstructive sleep apnea (OSA), oxygen desaturation is more severe and respiratory
disturbances are longer during NREM sleep compared to rapid eye movement sleep.
Further studies are needed regarding the effects of opioids on respiration during
sleep as well as the importance of interaction with other medications and
associated risk factors for OSA.


PMID: 12576394  [PubMed - indexed for MEDLINE]

