
1. Biol Psychol. 2006 Mar;71(3):341-9. Epub 2005 Sep 6.

Sleep changes vary by odor perception in young adults.

Goel N(1), Lao RP.

Author information: 
(1)Department of Psychology, 207 High Street, Judd Hall, Wesleyan University,
Middletown, CT 06459, USA. ngoel@wesleyan.edu

Peppermint, a stimulating odor, increases alertness while awake and therefore may
inhibit sleep. This study examined peppermint's effects on polysomnographic (PSG)
sleep, alertness, and mood when presented before bedtime. Twenty-one healthy
sleepers (mean age +/- S.D., 20.1 +/- 2.0 years) completed three consecutive
laboratory sessions (adaptation, control, and stimulus nights). Peppermint
reduced fatigue and improved mood and was rated as more pleasant, intense,
stimulating, and elating than water. These perceptual qualities associated with
sleep measures: subjects rating peppermint as very intense had more total sleep
than those rating it as moderately intense, and also showed more slow-wave sleep 
(SWS) in the peppermint than control session. Furthermore, subjects who found
peppermint stimulating showed more NREM and less REM sleep while those rating it 
as sedating took longer to reach SWS. Peppermint did not affect PSG sleep,
however, when these perceptual qualities were not considered. Peppermint also
produced gender-differentiated responses: it increased NREM sleep in women, but
not men, and alertness in men, but not women, compared with the control. Thus,
psychological factors, including individual differences in odor perception play
an important role in physiological sleep and self-rated mood and alertness
changes.

DOI: 10.1016/j.biopsycho.2005.07.004 
PMID: 16143443  [PubMed - indexed for MEDLINE]

