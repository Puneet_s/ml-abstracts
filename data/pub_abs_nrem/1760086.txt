
1. Epilepsy Res Suppl. 1991;2:165-76.

Sleep and prolonged epileptic activity (status epilepticus).

Fröscher W(1).

Author information: 
(1)Neurologische Abteilung, Psychiatrisches Landeskrankenhaus Weissenau,
Ravensburg-Weissenau, F.R.G.

The correlations between sleep and prolonged epileptic activity are discussed on 
the basis of the status classification of Gastaut (1983). Little information is
available on the interrelation of sleep and the status of tonic-clonic seizures
(grand mal status). Most important is the therapeutical management of these
cases. Tonic seizures have been reported to occur in large numbers during NREM
sleep in patients with Lennox-Gastaut syndrome. A status-like increase is
possible. Tonic seizures occur almost exclusively during sleep. Myoclonic status 
epilepticus arising (a) in the course of primary generalized epilepsy and (b) in 
the course of encephalopathies, are usually markedly attenuated during sleep. In 
absence status (petit mal status) synchronized sleep generally fragments the
continuous discharge which is replaced by isolated bursts of polyspikes, or
polyspike and wave complexes. The absence status can recur upon awaking during
the night or in the morning. The abnormal EEG activity of a petit mal status can,
however, occasionally persist during the whole night. Improvement as well as
activation during sleep have been observed in elementary (= simple) partial
status epilepticus; improvement seems to be more frequent. Epilepsia partialis
continua may persist or decrease during sleep. An increase as well as decrease of
motor phenomena has been observed during the REM stages. 'Epileptic aphasia' of
childhood is associated with subclinical bioelectric status epilepticus during
sleep. The electrical status epilepticus must be delineated as a separate group. 
The term encephalopathy related to electrical status epilepticus during slow
sleep (ESES) has been proposed on the basis of associated psychic syndromes. This
form of status epilepticus disappears during the waking state and during REM
sleep. Cases with hypsarrhythmia without clinical signs may also be classified
under the group of electrical or bioelectrical status. In some cases, a
continuous hypsarrhythmia is observed only during sleep. In this context, one
must also mention those patients who demonstrate continuous activation of spikes,
or spike and wave potentials (without clinical seizures) during eye closure.


PMID: 1760086  [PubMed - indexed for MEDLINE]

