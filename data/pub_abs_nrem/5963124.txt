
1. Percept Mot Skills. 1966 Jun;22(3):927-42.

Auditory awakening thresholds in REM and NREM sleep stages.

Rechtschaffen A, Hauri P, Zeitlin M.

DOI: 10.2466/pms.1966.22.3.927 
PMID: 5963124  [PubMed - indexed for MEDLINE]

