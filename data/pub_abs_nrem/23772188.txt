
1. J Clin Sleep Med. 2013 Jun 15;9(6):553-7. doi: 10.5664/jcsm.2748.

Comparison of polysomnographic and clinical presentations and predictors for
cardiovascular-related diseases between non-obese and obese obstructive sleep
apnea among Asians.

Chirakalwasan N(1), Teerapraipruk B, Simon R, Hirunwiwatkul P, Jaimchariyatam N, 
Desudchit T, Charakorn N, Wanlapakorn C.

Author information: 
(1)Excellence Center for Sleep Disorders, King Chulalongkorn Memorial
Hospital/Thai Red Cross Society, Bangkok, Thailand. narichac@hotmail.com

INTRODUCTION: Unlike Caucasians, many Asians with obstructive sleep apnea (OSA)
are non-obese but are affected by the disease due to predisposing craniofacial
structure. Therefore, non-obese and obese OSA may represent different disease
entities. The associated risk factors for developing cardiovascular-related
diseases, consequently, may be considered separately for the two types of OSA.
METHOD: We reviewed polysomnographic studies performed in adults (aged ≥ 18
years) diagnosed with OSA (respiratory disturbance index [RDI] ≥ 5). We divided
the patients into obese (body mass index [BMI] ≥ 25) and non-obese (BMI < 25)
groups. We aimed to determine the differences between these two groups in terms
of clinical presentations, polysomno-graphic findings, and association with
cardiovascular-related diseases including hypertension, diabetes mellitus,
coronary artery disease, and/or cerebrovascular disease.
RESULTS: Among 194 patients with OSA (RDI ≥ 5), 63.4% were non-obese and 36.6%
were obese. Compared with obese OSA patients, non-obese OSA patients were noted
to have smaller neck size, less prevalence of hypertension, and less history of
frequent nocturia (> 3-4/week), with equal prevalence of excessive daytime
sleepiness. Overall, non-obese OSA patients were noted to have milder disease
indicated by lower total, supine, and non-supine, NREM RDI and higher mean and
nadir oxygen saturations. In the non-obese group, only total obstructive apnea
index (OAI) was noted to be a predictor for developing any of the
cardiovascular-related diseases after controlling for age, sex, and RDI (odds
ratio = 9.7). However, in the obese OSA group, frequent snoring (> 50% of total
sleep time), low sleep efficiency (≤ 90%), and low mean oxygen saturation (< 95%)
were noted to be significant predictors of cardiovascular-related diseases (odds 
ratios = 12.3, 4.2, and 5.2, respectively).
CONCLUSION: Among Asians, most OSA patients were not obese. Compared to obese OSA
patients, non-obese OSA patients were noted to have less prevalence of
hypertension and less history of nocturia. They were also noted to have overall
milder OSA. Only OAI was noted to be a significant predictor for
cardiovascular-related disease in the non-obese OSA group.

DOI: 10.5664/jcsm.2748 
PMCID: PMC3659375
PMID: 23772188  [PubMed - indexed for MEDLINE]

