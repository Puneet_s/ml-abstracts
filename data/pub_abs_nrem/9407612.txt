
1. Respir Physiol. 1997 Nov;110(2-3):199-210.

Life without ventilatory chemosensitivity.

Shea SA(1).

Author information: 
(1)Harvard Medical School, Neuroendocrine, Circadian and Sleep Disorders Section,
Brigham and Women's Hospital, Boston, MA 02115, USA. SShea@GCRC.BwH.Harvard.Edu

In healthy humans ventilatory chemoreception results in exquisite regulation of
arterial blood gases during NREM sleep, but during wakefulness other behavioral
and arousal-related influences on breathing compete with chemoreceptive
respiratory control. This paper examines the extent of chemoreceptive control of 
breathing within the normal physiological range in awake and sleeping humans and 
explores the consequences upon breathing of absent chemoreceptive function.
Recent studies of subjects with congenital central hypoventilation syndrome
(CCHS) demonstrate the extent of behavioral and arousal-related influences on
breathing in the absence of arterial blood gas homeostasis. CCHS subjects lack
chemoreceptor control of breathing and seriously hypoventilate during NREM sleep,
requiring mechanical ventilation. Many CCHS subjects breathe adequately during
many waking behaviors associated with arousal, cognitive activity or
exercise--presumably reflecting input to the brainstem respiratory complex from
the reticular activating system, the forebrain or mechanoreceptor afferents. In
most situations, and despite changes in metabolism, the non-chemoreceptive inputs
to breathing result in surprisingly well controlled arterial blood gases in CCHS 
patients.


PMID: 9407612  [PubMed - indexed for MEDLINE]

