
1. Mov Disord. 2010 Jun 15;25(8):985-90. doi: 10.1002/mds.22990.

Complex paroxysmal nocturnal behaviors in Parkinson's disease.

Manni R(1), Terzaghi M, Repetto A, Zangaglia R, Pacchetti C.

Author information: 
(1)Sleep Unit, IRCCS C. Mondino Institute of Neurology Foundation, Pavia, Italy. 
raffaele.manni@mondino.it

Complex paroxysmal nocturnal motor behavioral disorders (CPNBs) are frequently
reported in patients with Parkinson's disease (PD). REM sleep behavior disorder
(RBD) is reported in at least a third of PD patients, although CPNB episodes can 
also occur on arousal from NREM sleep. It is important to establish the nature of
CPNBs occurring in PD, as the different types have different neurobiological
significance and clinical importance, and also different treatments. Ninety-six
PD patients with and without CPNBs were submitted to overnight in-hospital
video-polysomnography. Of these, 76 (47 men) were included in the study analysis:
these were patients in whom it was possible to establish the presence or absence 
of CPNBs and to obtain a clear-cut diagnosis of the nature of the CPNBs reported.
The CPNBs were found to be RBD episodes in 39 cases (87%) and nonRBD episodes in 
6 (13%) (arousal-related episodes arising from NREM sleep in 3 cases and from REM
sleep in 2 cases, parasomnia overlapping syndrome in 1 case). In 4 of the 6
subjects with nonRBD episodes, these occurred upon arousal at the end of an
obstructive apnoeic event. Our data confirm that CPNBs in PD are, in most cases, 
RBD episodes. However, arousal-related episodes accounted for 13% of the CPNBs
observed in our sample and occurred in close temporal association with
sleep-disordered breathing (SDB). The arousal system is defective in
extrapyramidal diseases due to neurodegenerative changes involving the brain stem
reticular network; against this background, a trigger effect of SDB on CPNBs,
through induction of abrupt arousal, may be hypothesized.

(c) 2010 Movement Disorder Society.

DOI: 10.1002/mds.22990 
PMID: 20198716  [PubMed - indexed for MEDLINE]

