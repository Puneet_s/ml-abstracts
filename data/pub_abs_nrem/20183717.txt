
1. Dev Neuropsychol. 2009;34(5):539-51. doi: 10.1080/87565640903133418.

Slow EEG amplitude oscillations during NREM sleep and reading disabilities in
children with dyslexia.

Bruni O(1), Ferri R, Novelli L, Finotti E, Terribili M, Troianiello M, Valente D,
Sabatello U, Curatolo P.

Author information: 
(1)Department of Developmental Neurology and Psychiatry, Sapienza University,
Rome, Italy.

STUDY OBJECTIVES: To analyze non-rapid eye movement (NREM) sleep microstructure
of children with dyslexia, by means of cyclic alternating pattern (CAP) analysis 
and to correlate CAP parameters with neuropsychological measures.
DESIGN: Cross-sectional study using polysomnographic recordings and
neuropsychological assessments.
SETTING: Sleep laboratory in academic center.
PARTICIPANTS: Sixteen subjects with developmental dyslexia (mean age 10.8 years) 
and 11 normally reading children (mean age 10.1 years) underwent overnight
polysomnographic recording.
INTERVENTION: N/A.
MEASUREMENTS AND RESULTS: Sleep architecture parameters only showed some
statistically significant differences: number of sleep stage shifts per hour of
sleep, percentage of N3, and number of R periods were significantly lower in
dyslexic children versus controls. CAP analysis revealed a higher total CAP rate 
and A1 index in stage N3. A2% and A2 index in stage N2 and N3 were lower in
dyslexic children while no differences were found for A3 CAP subtypes. The
correlation analysis between CAP parameters and cognitive-behavioral measures
showed a significant positive correlation between A1 index in N3 with Verbal IQ, 
full-scale IQ, and Memory and Learning Transfer reading test; while CAP rate in
N3 was positively correlated with verbal IQ.
CONCLUSIONS: To overcome reading difficulties, dyslexic subjects overactivate
thalamocortical and hippocampal circuitry to transfer information between
cortical posterior and anterior areas. The overactivation of the ancillary
frontal areas could account for the CAP rate modifications and mainly for the
increase of CAP rate and of A1 index in N3 that seem to be correlated with IQ and
reading abilities.

DOI: 10.1080/87565640903133418 
PMID: 20183717  [PubMed - indexed for MEDLINE]

