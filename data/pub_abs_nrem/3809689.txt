
1. Rev Electroencephalogr Neurophysiol Clin. 1986 Oct;16(3):229-37.

[Regional cerebral blood flow in partial complex epilepsy with and without the
presence of lesions].

[Article in French]

Valmier J, Touchon J, Baldy-Moulinier M.

Regional cerebral blood flow (r CBF) was measured by the I.V. 133 Xenon method
and use of 27 detectors in 91 patients with complex partial epilepsy in
interictal periods (at least 48 h over a complex partial seizure). Some were also
examined less than 48 h before or after seizures. All were studied with ictal and
interictal electroencephalography (EEG), polysomnography, computed tomography
(CT), some had nuclear magnetic resonance scans (MR). The blood flow values were 
compared with a group of a 20 normal subjects matching for age. A significant
decrease of r CBF ranged from 15% to 25% was found in the temporal region in
three groups of epileptic patients: with repeated normal CT scans and lateralized
EEG abnormalities (N = 46); with cortical atrophy in CT scan (N = 12); with
neurosurgical focal lesions on CT and or MR scans glioma, arteriovenous
malformation) (N = 10). r CBF was normal or decreased by less than 15% in the
other regions of the brain. Patients with repeated normal CT scans and bilateral 
EEG abnormalities either asynchronous or alternatively observed in the right side
or left side on waking EEG or during NREM sleep and REM sleep, did not show
reduction in r CBF. In a previous study, r CBF distribution was also found normal
during interictal phase in patients with primary generalized epilepsy.(ABSTRACT
TRUNCATED AT 250 WORDS)


PMID: 3809689  [PubMed - indexed for MEDLINE]

