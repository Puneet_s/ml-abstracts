
1. Respir Physiol. 2000 Feb;119(2-3):155-61.

Respiratory-related activation and mechanical effects of the pharyngeal
constrictor muscles.

Kuna ST(1).

Author information: 
(1)Pulmonary, Critical Care and Sleep Section, Department of Internal Medicine,
Philadelphia Veterans Affairs Medical Center (111P), University of Pennsylvania, 
University and Woodland Avenue, Philadelphia, PA, USA. skuna@mail.med.upenn.edu

We have examined the respiratory-related activation of pharyngeal constrictor
(PC) muscles in decerebrate cats, normal adult humans and patients with
obstructive sleep apnea. In decerebrate cats and awake normal adult humans,
phasic expiratory PC activity is uniformly present under hypercapnic and hypoxic 
conditions. While the PC muscles are electrically silent during quiet breathing
in normal adult humans in NREM sleep, an activation pattern very similar to that 
of other upper airway dilators, such as the genioglossus muscle, is present
during spontaneous and induced apneas in patients with obstructive sleep apnea.
Experiments using an isolated, sealed upper airway preparation in decerebrate cat
show that selective activation of the PC muscles stiffens the pharyngeal airway. 
The results also show that activation of the PC muscles constricts the airway at 
relatively high airway volumes but dilates the airway at relatively low airway
volumes. These results suggest that PC muscle activation at the end of an apneic 
episode, when airway volume is relatively low, may help restore airway patency in
patients with obstructive sleep apnea.


PMID: 10722858  [PubMed - indexed for MEDLINE]

