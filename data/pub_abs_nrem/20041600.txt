
1. Sleep. 2009 Dec;32(12):1637-44.

NREM arousal parasomnias and their distinction from nocturnal frontal lobe
epilepsy: a video EEG analysis.

Derry CP(1), Harvey AS, Walker MC, Duncan JS, Berkovic SF.

Author information: 
(1)Epilepsy Research Centre, Department of Medicine, University of Melbourne,
Victoria, Australia. cderry@nhs.net

Comment in
    Sleep. 2009 Dec;32(12):1544-5.

STUDY OBJECTIVES: To describe the semiological features of NREM arousal
parasomnias in detail and identify features that can be used to reliably
distinguish parasomnias from nocturnal frontal lobe epilepsy (NFLE).
DESIGN: Systematic semiologial evaluation of parasomnias and NFLE seizures
recorded on video-EEG monitoring.
PATIENTS: 120 events (57 parasomnias, 63 NFLE seizures) from 44 subjects (14
males). Interventions. The presence or absence of 68 elemental clinical features 
was determined in parasomnias and NFLE seizures. Qualitative analysis of behavior
patterns and ictal EEG was undertaken. Statistical analysis was undertaken using 
established techniques.
RESULTS: Elemental clinical features strongly favoring parasomnias included:
interactive behavior, failure to wake after event, and indistinct offset (all P <
0.001). Cluster analysis confirmed differences in both the frequency and
combination of elemental features in parasomnias and NFLE. A diagnostic decision 
tree generated from these data correctly classified 94% of events. While sleep
stage at onset was discriminatory (82% of seizures occurred during stage 1 or 2
sleep, with 100% of parasomnias occurring from stage 3 or 4 sleep), ictal EEG
features were less useful. Video analysis of parasomnias identified three
principal behavioral patterns: arousal behavior (92% of events); non-agitated
motor behavior (72%); distressed emotional behavior (51%).
CONCLUSIONS: Our results broadly support the concept of confusion arousals,
somnambulism and night terrors as prototypical behavior patterns of NREM
parasomnias, but as a hierarchical continuum rather than distinct entities. Our
observations provide an evidence base to assist in the clinical diagnosis of NREM
parasomnias, and their distinction from NFLE seizures, on semiological grounds.


PMCID: PMC2786048
PMID: 20041600  [PubMed - indexed for MEDLINE]

