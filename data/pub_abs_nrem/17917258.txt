
1. Biol Pharm Bull. 2007 Oct;30(10):1895-7.

Effects of urocortin, corticotropin-releasing factor (CRF) receptor agonist, and 
astressin, CRF receptor antagonist, on the sleep-wake pattern: analysis by
radiotelemetry in conscious rats.

Uchida M(1), Suzuki M, Shimizu K.

Author information: 
(1)Food Science Institute, Division of Research and Development, Meiji Dairies
Corporation, Odawara, Kanagawa, Japan. MASAYUKI_UCHIDA@MEIJI-MILK.COM

Stress has been known to release corticotropin-releasing factor (CRF) and have an
affect on sleep-wake patterns. However, there is no direct evidence of CRF
receptor agonist and antagonist on sleep-wake patterns. Therefore, this study
aimed to clarify this point by using radiotelemetry system in conscious rats.
Wake, non-rapid eye-moving (NREM) sleep and rapid eye-moving (REM) sleep were
analyzed by computer software, simultaneously measuring electroencephalogram and 
electromyogram. In the light period, urocortin (CRF receptor agonist: i.v.)
significantly increased wake duration, and decreased NREM sleep duration. REM
sleep was not affected. Astressin (CRF receptor antagonist: i.p.) significantly
attenuated the changes induced by urocortin, although astressin itself did not
affect the sleep-wake pattern in the light period at this dosage. These findings 
show that urocortin changes the sleep-wake pattern in the light period. Moreover,
urocortin was found to change the sleep-wake pattern by acting on CRF receptor,
as astressin significantly attenuated the urocortin-induced changes on sleep-wake
patterns.


PMID: 17917258  [PubMed - indexed for MEDLINE]

