
1. Sb Lek. 2002;103(1):79-83.

[Nocturnal cardiac dysrhythmias associated with obstructive sleep apnea].

[Article in Slovak]

Szabóová E(1), Donic V, Albertová D, Turciansky M, Tomori Z.

Author information: 
(1)Ustav patofyziológie, Lekárskej fakulty Univerzity P.J. Safárika, Tr. SNP 1,
040 66 Kosice, Slovak Republic.

The occurrence of cardiac dysrhythmias have been analysed in 16 adult patients
suffering from obstructive sleep apnea syndrome of various severity randomly
selected from more than 300 persons examined in our sleep laboratory from 1996
with a complex polysomnography Alice 3 (Healthdyne). The number of apneic
episodes emerging in the first, second and third part of sleep was practically
the same although their duration prolonged during the night culminating with an
average of 25 sec (p < 0.02). OSA episodes caused a decrease of oxyhaemoglobin
saturation to lower values during REM compared to NREM sleep (76.1% versus 81.7%,
p < 0.05). Cardiac dysrhythmias occurred more frequently during and immediately
after, than before OSA episodes demonstrating their causal relations.


PMID: 12448941  [PubMed - indexed for MEDLINE]

