
1. Psychoneuroendocrinology. 1998 Jul;23(5):427-37.

Endotoxin-induced changes in sleep and sleepiness during the day.

Hermann DM(1), Mullington J, Hinze-Selch D, Schreiber W, Galanos C, Pollmächer T.

Author information: 
(1)Max Planck Institute of Psychiatry, Munich, Germany.

Sleepiness is a common symptom of infectious diseases. However, the peculiarities
and causes of impaired vigilance during host defense activation are largely
unknown. It has been shown earlier that mild host defense activation by endotoxin
does not affect daytime sleepiness and non-rapid eye movement (NREM) sleep in
humans. In the present study we investigated the effects of a more intensive
stimulation of the host defense by Salmonella abortus equi endotoxin (0.8 ng/kg),
administered 12 h following host response priming by granulocyte
colony-stimulating factor (300 micrograms s.c.), on daytime sleep and sleepiness 
in a placebo-controlled design in ten healthy men. Six equidistant
polysomnographically monitored naps were scheduled across the day and the time
course of subjective sleepiness was assessed. Endotoxin induced prominent
increases in rectal temperature, and in the plasma levels of tumor necrosis
factor-alpha, interleukin-6, interleukin-1 receptor antagonist, and cortisol. In 
the first nap, 1 h following endotoxin administration, total sleep time and NREM 
sleep stage 2 were reduced, whereas wakefulness and sleep onset latency were
increased. Following this nap sleepiness transiently increased peaking prior to
the second nap. However, this nap and the following ones were not influenced by
endotoxin. These results suggest that prominent host defense activation reduces
daytime NREM sleep and increases sleepiness. One cause of daytime sleepiness
during infections may be prior sleep disruption and this kind of sleepiness may
not necessarily be associated with an increased sleep pressure.


PMID: 9802118  [PubMed - indexed for MEDLINE]

