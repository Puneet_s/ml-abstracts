
1. Sleep. 2000 May 1;23(3):401-7.

Influence of ambient temperature on sleep characteristics and autonomic nervous
control in healthy infants.

Franco P(1), Szliwowski H, Dramaix M, Kahn A.

Author information: 
(1)Pediatric Sleep Unit, Erasmus Hospital, Brussels, Belgium.
Patricia.Franco@skynet.be

OBJECTIVE: To evaluate the influence of ambient temperature on infant's sleep and
cardiorespiratory parameters during sleep.
PATIENTS AND METHOD: 20 healthy infants with a median age of 11.5 weeks (range 7 
to 18 weeks) were recorded polygraphically for one night. They were exposed to 3 
different ambient temperatures (20 degrees C-25 degrees C-30 degrees C). Ambient 
and core temperatures were measured throughout the procedure.
RESULTS: Influence of ambient temperature was seen in: RESPIRATORY PARAMETERS:
The frequency of central apneas increased significantly with increasing
temperatures in REM sleep, but not in NREM sleep. HEART RATE (HR) PARAMETERS AND 
HR SPECTRAL ANALYSIS: Elevation of temperature was characterized by significantly
higher basal HR, shorter RR intervals, and lower parasympathetic activity in REM 
and NREM sleep. SATURATION IN OXYGEN: During total sleep time, rise in
temperature induced a decrease in basal oxygen saturation. During REM sleep, a
greater frequency of oxygen saturation drops was associated with central apneas. 
CORE TEMPERATURE: With increasing ambient temperature, the rise of rectal
temperature was mild. Despite this lack of significant increase, similar results 
were found when sleep and cardiorespiratory parameters were evaluated according
to rectal temperatures.
CONCLUSION: Changes in ambient temperatures associated with mild increases in
body temperature significantly modified cardiorespiratory parameters and
autonomic controls in healthy infants. The changes associated with increases in
temperature were mainly seen during REM sleep.


PMID: 10811384  [PubMed - indexed for MEDLINE]

