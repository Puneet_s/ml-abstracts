
1. J Clin Neurophysiol. 2009 Feb;26(1):39-44. doi: 10.1097/WNP.0b013e318196046f.

Antidepressant medications, neuroleptics, and prominent eye movements during NREM
sleep.

Geyer JD(1), Carney PR, Dillard SC, Davis L, Ward LC.

Author information: 
(1)Neurology and Sleep Medicine, College of Community Health Sciences, The
University of Alabama, Tuscaloosa, Alabama 35406, USA. jgeyer@nctpc.com

Eye movements during stage 2, 3, and 4 sleep have been associated with the use of
several selective serotonin reuptake inhibitor (SSRI) medications. This activity 
has been postulated to be a serotonin effect. The authors identified all cases of
nonrapid eye movement (NREM) eye movements observed over a 36-month period in an 
accredited hospital-based sleep center and then correlated the findings with the 
patient's medications. The polysomnogram (PSG) studies of 2,959 consecutive
adults were evaluated prospectively to identify all patients with atypical eye
movements which occurred during NREM sleep. Standard recording, staging and
arousal scoring methods were used. The use of antidepressants and neuroleptic
medications was recorded for each patient. Eye movements in NREM sleep were
detected in 94 PSGs. Of these, 73 patients (78%) were taking a SSRI at the time
of the study, and 6 (6%) had taken a SSRI in the past. Thirty-six percent of
patients (73 of 201) taking a SSRI had abnormal NREM eye movements on PSG. Other 
classes of antidepressants, neuroleptics, and benzodiazepines showed a much lower
incidence of NREM eye movements. Mirtazapine was rarely related to NREM eye
movements. Clonazepam and zolpidem were not associated with atypical eye
movements unless used in combination with SSRI medications. Selective serotonin
reuptake inhibitors was associated with atypical NREM eye movements, even when
the medication had been discontinued months to years before the PSG. Atypical
NREM eye movements appear to be related primarily to serotonin and less
prominently to dopaminergic medication effects.

DOI: 10.1097/WNP.0b013e318196046f 
PMID: 19151617  [PubMed - indexed for MEDLINE]

