
1. J Neurosci Methods. 2009 Dec 15;185(1):29-38. doi:
10.1016/j.jneumeth.2009.09.002. Epub 2009 Sep 10.

The hypnospectrogram: an EEG power spectrum based means to concurrently overview 
the macroscopic and microscopic architecture of human sleep.

Kokkinos V(1), Koupparis A, Stavrinou ML, Kostopoulos GK.

Author information: 
(1)Department of Physiology, Medical School, University of Patras, Patras,
Greece.

This study introduces a complementary tool for the description and evaluation of 
human sleep. The nocturnal sleep electroencephalographic (EEG) time-frequency
analysis (TFA) plot (hypnospectrogram for short) is hereby proposed as a means to
visualize both the macroscopic and the microscopic architecture of human sleep.
It provides the ability to concurrently visually inspect the coarse sleep
architecture, that is, the time-course of non-rapid eye movement (NREM) and REM
stages, along with finer sleep elements such as slow and fast spindles, NREM
delta distribution, REM alpha and beta, microarousals (MAs), and NREM cyclic
alternating patterns (CAPs). Furthermore, the hypnospectrogram has the potential 
to provide visual quality of sleep (QoS) evaluation, as well as reveal the
dominant rhythms and their transitions for every cerebral locus - as represented 
at the electrode space - during the night.

DOI: 10.1016/j.jneumeth.2009.09.002 
PMID: 19747945  [PubMed - indexed for MEDLINE]

