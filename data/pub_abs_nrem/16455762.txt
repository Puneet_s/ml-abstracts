
1. Am J Physiol Regul Integr Comp Physiol. 2006 Jul;291(1):R197-204. Epub 2006 Feb
2.

Inhibition of caspase-1 in rat brain reduces spontaneous nonrapid eye movement
sleep and nonrapid eye movement sleep enhancement induced by lipopolysaccharide.

Imeri L(1), Bianchi S, Opp MR.

Author information: 
(1)Institute of Human Physiology II, University of Milan Medical School, Milan,
Italy luca.imeri@unimi.it

Evidence suggests that IL-1beta is involved in promoting physiological nonrapid
eye movement (NREM) sleep. IL-1beta has also been proposed to mediate NREM sleep 
enhancement induced by bacteria or their components. Mature and biologically
active IL-1beta is cleaved from an inactive precursor by a cysteinyl
aspartate-specific protease (caspase)-1. This study aimed to test the hypothesis 
that inhibition in brain of the cleavage of biologically active IL-1beta will
reduce in rats both spontaneous NREM sleep and NREM sleep enhancement induced by 
the peripheral administration of components of the bacterial cell wall. To test
this hypothesis, rats were intracerebroventricularly administered the caspase-1
inhibitor Ac-Tyr-Val-Ala-Asp chloromethyl ketone (YVAD; 3, 30, 300, and 1,500 ng)
or were pretreated intracerebroventricularly with YVAD (300 ng) and then
intraperitoneally injected with the gram-negative bacterial cell wall component
LPS (250 microg/kg). Subsequent sleep-wake behavior was determined by standard
polygraphic recordings. YVAD administration at the beginning of the light phase
of the light-dark cycle significantly reduced time spontaneously spent in NREM
sleep during the first 12 postinjection hours. YVAD pretreatment also completely 
prevented NREM sleep enhancement induced by peripheral LPS administration at the 
beginning of the dark phase. These results, in agreement with previous evidence, 
support the involvement of brain IL-1beta in physiological promotion of NREM
sleep and in mediating NREM sleep enhancement induced by peripheral immune
challenge.

DOI: 10.1152/ajpregu.00828.2005 
PMID: 16455762  [PubMed - indexed for MEDLINE]

