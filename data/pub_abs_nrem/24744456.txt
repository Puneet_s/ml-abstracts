
1. Sleep. 2014 Apr 1;37(4):785-93, 793A-793C. doi: 10.5665/sleep.3590.

The circadian clock gene Csnk1e regulates rapid eye movement sleep amount, and
nonrapid eye movement sleep architecture in mice.

Zhou L(1), Bryant CD(2), Loudon A(3), Palmer AA(4), Vitaterna MH(1), Turek FW(1).

Author information: 
(1)Center for Sleep and Circadian Biology, Northwestern University, Evanston, IL 
; Department of Neurobiology, Northwestern University, Evanston, IL.
(2)Departments of Pharmacology and Experimental Therapeutics and Psychiatry,
Boston University School of Medicine, Boston, MA. (3)Faculty of Life Sciences,
University of Manchester, Manchester, UK. (4)Department of Human Genetics,
University of Chicago, Chicago, IL ; Department of Psychiatry and Behavioral
Neuroscience, University of Chicago, Chicago, IL.

STUDY OBJECTIVES: Efforts to identify the genetic basis of mammalian sleep have
included quantitative trait locus (QTL) mapping and gene targeting of known core 
circadian clock genes. We combined three different genetic approaches to identify
and test a positional candidate sleep gene - the circadian gene casein kinase 1
epsilon (Csnk1e), which is located in a QTL we identified for rapid eye movement 
(REM) sleep on chromosome 15.
MEASUREMENTS AND RESULTS: Using electroencephalographic (EEG) and
electromyographic (EMG) recordings, baseline sleep was examined in a 12-h
light:12-h dark (LD 12:12) cycle in mice of seven genotypes, including
Csnk1e(tau/tau) and Csnk1e(-/-) mutant mice, Csnk1e (B6.D2) and Csnk1e (D2.B6)
congenic mice, and their respective wild-type littermate control mice.
Additionally, Csnk1e(tau/tau) and wild-type mice were examined in constant
darkness (DD). Csnk1e(tau/tau) mutant mice and both Csnk1e (B6.D2) and Csnk1e
(D2.B6) congenic mice showed significantly higher proportion of sleep time spent 
in REM sleep during the dark period than wild-type controls - the original
phenotype for which the QTL on chromosome 15 was identified. This phenotype
persisted in Csnk1e(tau/tau) mice while under free-running DD conditions. Other
sleep phenotypes observed in Csnk1e(tau/tau) mice and congenics included a
decreased number of bouts of nonrapid eye movement (NREM) sleep and an increased 
average NREM sleep bout duration.
CONCLUSIONS: These results demonstrate a role for Csnk1e in regulating not only
the timing of sleep, but also the REM sleep amount and NREM sleep architecture,
and support Csnk1e as a causal gene in the sleep QTL on chromosome 15.

DOI: 10.5665/sleep.3590 
PMCID: PMC3972422
PMID: 24744456  [PubMed - indexed for MEDLINE]

