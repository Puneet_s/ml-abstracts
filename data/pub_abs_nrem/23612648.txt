
1. Rev Neurosci. 2013;24(3):279-91. doi: 10.1515/revneuro-2013-0002.

Sleep disorders in Parkinson's disease: a narrative review of the literature.

Raggi A(1), Bella R, Pennisi G, Neri W, Ferri R.

Author information: 
(1)Morgagni-Pierantoni Hospital, Forli, Italy. alberto.raggi@ausl.fo.it

Parkinson's disease (PD) is classically considered to be a motor system
affliction; however, also non-motor alterations, including sleep disorders, are
important features of the disease. The aim of this review is to provide data on
sleep disturbances in PD in the following grouping: difficulty initiating sleep, 
frequent night-time awakening and sleep fragmentation, nocturia, restless legs
syndrome/periodic limb movements, sleep breathing disorders, drug induced
symptoms, parasomnias associated with rapid eye movements (REM) sleep, sleep
attacks, reduced sleep efficiency and excessive daytime sleepiness. Research has 
characterized some of these disturbances as typical examples of dissociated
states of wakefulness and sleep that are admixtures or incomplete declarations of
wakefulness, REM sleep, and non-REM (NREM) sleep. Moreover, sleep disorders may
precede the typical motor system impairment of PD and their ability to predict
disease has important implications for development of neuroprotective treatment; 
in particular, REM sleep behavior disorder may herald any other clinical
manifestation of PD by more than 10 years.

DOI: 10.1515/revneuro-2013-0002 
PMID: 23612648  [PubMed - indexed for MEDLINE]

