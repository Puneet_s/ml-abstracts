
1. Adv Neuroimmunol. 1995;5(2):155-69.

Influence of host defense activation on sleep in humans.

Pollmächer T(1), Mullington J, Korth C, Hinze-Selch D.

Author information: 
(1)Max-Planck-Institute of Psychiatry, Clinical Institute, Munich, Germany.

Despite considerable progress in our understanding of the phenomenology of sleep 
and wakefulness, their regulation and peculiar functions are poorly understood.
Recent animal research has revealed considerable evidence for interactions
between host defense and sleep. Therefore, it has been hypothesized that host
response mediators, mainly cytokines like interleukin-1 (IL-1), are involved in
physiological sleep regulation. Furthermore, it has been suggested that sleep,
and non rapid eye movement (NREM) sleep in particular, has an immuno-supportive
function. In humans, sleep-host defense interactions are just starting to be
understood. There is quite good evidence that some viral diseases cause excessive
sleepiness. Other infectious diseases induce, however, serious disturbances of
the distribution of sleep and wakefulness rather than excessive sleep. In
addition, some disorders with excessive sleep, daytime fatigue or disturbed night
sleep as prominent symptoms are thought to involve, at least in part,
immuno-pathophysiological mechanisms. Experimental settings have only recently
been used to elucidate host defense-sleep interactions in humans. The effects of 
endotoxin, a cell-wall lipopolysaccharide of gram-negative bacteria, on sleep
have been tested in different settings in healthy volunteers. Endotoxin
transiently suppresses rapid eye movement (REM) sleep independently of the time
of the day of administration. Only low doses, given in the evening, promote NREM 
sleep. Electorencephalogram (EEG) power in higher frequency bands is enhanced
during NREM sleep, whereas delta activity is not affected. In rats and rabbits,
on the other hand, the effects of endotoxin and of the mediators of its activity 
on REM sleep are variable. Enhanced NREM sleep is a common finding and most
pronounced during the active part of the nycthemeron and, in general, EEG delta
activity is augmented. In view of these species differences, hypotheses regarding
the underlying mechanisms and the biological significance of host defense-sleep
interactions, primarily derived from the results of animal studies, may not
entirely fit human physiology. They should therefore be re-evaluated and probably
modified, through the use of additional experimental approaches in humans.


PMID: 7496610  [PubMed - indexed for MEDLINE]

