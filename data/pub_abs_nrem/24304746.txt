
1. Behav Brain Sci. 2013 Dec;36(6):589-607. doi: 10.1017/S0140525X12003135.

Such stuff as dreams are made on? Elaborative encoding, the ancient art of
memory, and the hippocampus.

Llewellyn S(1).

Author information: 
(1)Faculty of Humanities, University of Manchester, Manchester M15 6PB, United
Kingdom. http://www.humanities.manchester.ac.uk sue.llewellyn@mbs.ac.uk.

Comment in
    Behav Brain Sci. 2013 Dec;36(6):611-2; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):612-3; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):613-4; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):614; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):615; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):615-6; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):616-7; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):617-8; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):618-9; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):619-20; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):620-1; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):621; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):622-3; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):624-5; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):625-6; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):610-1; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):609-10; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):608-9; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):607-8; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):626-8; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):628-9; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):623-4; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):621-2; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):633; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):631-3; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):630-1; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):629-30; discussion 634-59.
    Behav Brain Sci. 2013 Dec;36(6):629; discussion 634-59.

This article argues that rapid eye movement (REM) dreaming is elaborative
encoding for episodic memories. Elaborative encoding in REM can, at least
partially, be understood through ancient art of memory (AAOM) principles:
visualization, bizarre association, organization, narration, embodiment, and
location. These principles render recent memories more distinctive through novel 
and meaningful association with emotionally salient, remote memories. The AAOM
optimizes memory performance, suggesting that its principles may predict aspects 
of how episodic memory is configured in the brain. Integration and segregation
are fundamental organizing principles in the cerebral cortex. Episodic memory
networks interconnect profusely within the cortex, creating omnidirectional
"landmark" junctions. Memories may be integrated at junctions but segregated
along connecting network paths that meet at junctions. Episodic junctions may be 
instantiated during non-rapid eye movement (NREM) sleep after hippocampal
associational function during REM dreams. Hippocampal association involves
relating, binding, and integrating episodic memories into a mnemonic
compositional whole. This often bizarre, composite image has not been present to 
the senses; it is not "real" because it hyperassociates several memories. During 
REM sleep, on the phenomenological level, this composite image is experienced as 
a dream scene. A dream scene may be instantiated as omnidirectional neocortical
junction and retained by the hippocampus as an index. On episodic memory
retrieval, an external stimulus (or an internal representation) is matched by the
hippocampus against its indices. One or more indices then reference the relevant 
neocortical junctions from which episodic memories can be retrieved. Episodic
junctions reach a processing (rather than conscious) level during normal wake to 
enable retrieval. If this hypothesis is correct, the stuff of dreams is the stuff
of memory.

DOI: 10.1017/S0140525X12003135 
PMID: 24304746  [PubMed - indexed for MEDLINE]

