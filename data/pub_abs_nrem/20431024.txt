
1. J Appl Physiol (1985). 2010 Jul;109(1):159-70. doi:
10.1152/japplphysiol.00933.2009. Epub 2010 Apr 29.

A role for the Kolliker-Fuse nucleus in cholinergic modulation of breathing at
night during wakefulness and NREM sleep.

Bonis JM(1), Neumueller SE, Krause KL, Kiner T, Smith A, Marshall BD, Qian B, Pan
LG, Forster HV.

Author information: 
(1)Department of Physiology, Medical College of Wisconsin, Milwaukee, WI, USA.

For many years, acetylcholine has been known to contribute to the control of
breathing and sleep. To probe further the contributions of cholinergic rostral
pontine systems in control of breathing, we designed this study to test the
hypothesis that microdialysis (MD) of the muscarinic receptor antagonist atropine
into the pontine respiratory group (PRG) would decrease breathing more in animals
while awake than while in NREM sleep. In 16 goats, cannulas were bilaterally
implanted into rostral pontine tegmental nuclei (n = 3), the lateral (n = 3) or
medial (n = 4) parabrachial nuclei, or the Kölliker-Fuse nucleus (KFN; n = 6).
After >2 wk of recovery from surgery, the goats were studied during a 45-min
period of MD with mock cerebrospinal fluid (mCSF), followed by at least 30 min of
recovery and a second 45-min period of MD with atropine. Unilateral and bilateral
MD studies were completed during the day and at night. MD of atropine into the
KFN at night decreased pulmonary ventilation and breathing frequency and
increased inspiratory and expiratory time by 12-14% during both wakefulness and
NREM sleep. However, during daytime studies, MD of atropine into the KFN had no
effect on these variables. Unilateral and bilateral nighttime MD of atropine into
the KFN increased levels of NREM sleep by 63 and 365%, respectively. MD during
the day or at night into the other three pontine sites had minimal effects on any
variable studied. Finally, compared with MD of mCSF, bilateral MD of atropine
decreased levels of acetylcholine and choline in the effluent dialysis fluid. Our
data support the concept that the KFN is a significant contributor to
cholinergically modulated control of breathing and sleep.

DOI: 10.1152/japplphysiol.00933.2009 
PMCID: PMC2904195
PMID: 20431024  [PubMed - indexed for MEDLINE]

