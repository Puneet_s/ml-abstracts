
1. J Cogn Neurosci. 1999 Mar;11(2):182-93.

Sleep-induced changes in associative memory.

Stickgold R(1), Scott L, Rittenhouse C, Hobson JA.

Author information: 
(1)Massachusetts Medical Health Center, Harvard Medical School, 74 Fenwood Road, 
Boston, MA 02115, USA. rstickgold@hms.harvard.edu

The notion that dreaming might alter the strength of associative links in memory 
was first proposed almost 200 years ago. But no strong evidence of such altered
associative links has been obtained. Semantic priming can be used to quantify the
strength of associative links between pairs of words; it is thought to measure
the automatic spread of activation from a "node" representing one word to nodes
representing semantically related words. Semantic priming could thus be used to
test for global alterations in the strengths of associative links across the
wake-sleep cycle. Awakenings from REM and nonREM (NREM) sleep produce a period of
state carry-over during which performance is altered as a result of the brain's
slow transition to full wakefulness, and cognitive testing in this period can
provide information about the functioning of the brain during the prior sleep
period. When subjects were tested across the night--before and after a night's
sleep as well as immediately following forced awakenings from REM and NREM
sleep--weak priming (e. g., thief-wrong) was found to be state dependent (p =
0.016), whereas strong priming (e.g., hot-cold) was not (p = 0.89). Weak primes
were most effective in the presleep and REM sleep conditions and least effective 
in NREM and postsleep conditions. Most striking are analyses comparing weak and
strong priming within each wake-sleep state. Contrary to the normal pattern of
priming, subjects awakened from REM sleep showed greater priming by weak primes
than by strong primes (p = 0.01). This result was seen in each of three
protocols. In contrast, strong priming exceeded weak priming in NREM sleep. The
shift in weak priming seen after REM sleep awakenings suggests that cognition
during REM sleep is qualitatively different from that of waking and NREM sleep
and may reflect a shift in associative memory systems, a shift that we
hypothesize underlies the bizarre and hyperassociative character of REM-sleep
dreaming. Known changes in brainstem activity that control the transition into
and maintenance of REM sleep provide a possible explanation of this shift.


PMID: 10198133  [PubMed - indexed for MEDLINE]

