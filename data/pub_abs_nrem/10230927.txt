
1. Pediatr Pulmonol. 1999 Apr;27(4):273-7.

Effect of sleep stages on measurements of passive respiratory mechanics in
infants with bronchiolitis.

Pratl B(1), Steinbrugger B, Weinhandl E, Zach MS.

Author information: 
(1)Pediatric Department, University of Graz, Austria.

The measurement of passive respiratory mechanics by the single-breath occlusion
technique is one of the more frequently used tests of infant lung function.
Measurements are routinely done under chloral hydrate sedation, and a possible
influence of sleep stages on these measurements has not been evaluated so far. We
combined the assessment of passive respiratory mechanics with sleep stage
monitoring in 44 infants and toddlers with mild to moderately severe
bronchiolitis. In 31 infants, only nonrapid eye movement (NREM) sleep was
recorded. In 13 patients who showed both NREM and rapid eye movement (REM) sleep,
compliance of the respiratory system was significantly lower during REM than NREM
sleep (73.2 +/- 19.7 vs. 81.2 +/- 21.3 mL/kPa, P = 0.0007), while resistance
remained essentially unchanged. This finding was explained by an unchanged airway
opening pressure in combination with a significantly decreased extrapolated
volume. As tidal volume did not change from NREM to REM, this indicates reduced
dynamic elevation of lung volume during REM sleep and thus supports previous
observations of decreased lung volume in this sleep stage. From a practical
perspective, these findings argue for the monitoring of sleep stage during
measurements of passive respiratory mechanics, thereby increasing the complexity 
of these measurements significantly.


PMID: 10230927  [PubMed - indexed for MEDLINE]

