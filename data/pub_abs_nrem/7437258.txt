
1. Br J Clin Pharmacol. 1980 Nov;10(5):465-72.

Wakefullness and reduced rapid eye movement sleep: studies with prolintane and
pemoline.

Nicholson AN, Stone BM, Jones MM.

1 Effects of prolintane (15 and 30 mg) and pemoline (60 and 100 mg) on sleep were
studied in six healthy adult males. Sleep was assessed by electroencephalography 
and by analogue scales. 2 Prolintane (15 and 30 mg) reduced rapid eye movement
(REM) sleep both by delaying the first period (P < 0.05 and < 0.001 respectively)
and by reducing total REM sleep (P < 0.05 and < 0.001 respectively). In some
subjects there were increased awakenings during the early part of the night, and 
in two subjects long periods of wakefulness occurred. 3 With pemoline (60 and 100
mg) sleep duration was marked reduced (P < 0.001). There was evidence in some
subjects of delay to the first REM period, and reduced percentage REM sleep (P < 
0.01). Shortened and fragmented sleep with 60 and 100 mg were associated with
reduced sleep efficiency indices (P < 0.001), and shorter sleep period times led 
to reduced REM/NREM ratios. Absence of an effect on REM latency for the subjects 
as a group may be related to relatively slow absorption. 4 The heterocyclic
amphetamine derivatives have variable effects on sleep. The differences may be
dose related, and wakefulness and reduced REM sleep may be seen together or
separately. Alterations in sleep occur with doses of pemoline and prolintane
which also modify performance.


PMCID: PMC1430138
PMID: 7437258  [PubMed - indexed for MEDLINE]

