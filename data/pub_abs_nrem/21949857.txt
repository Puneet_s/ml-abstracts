
1. PLoS One. 2011;6(9):e25076. doi: 10.1371/journal.pone.0025076. Epub 2011 Sep 16.

Orexin neurons receive glycinergic innervations.

Hondo M(1), Furutani N, Yamasaki M, Watanabe M, Sakurai T.

Author information: 
(1)Department of Molecular Neuroscience and Integrative Physiology, Faculty of
Medicine, Kanazawa University, Kanazawa, Ishikawa, Japan.

Glycine, a nonessential amino-acid that acts as an inhibitory neurotransmitter in
the central nervous system, is currently used as a dietary supplement to improve 
the quality of sleep, but its mechanism of action is poorly understood. We
confirmed the effects of glycine on sleep/wakefulness behavior in mice when
administered peripherally. Glycine administration increased non-rapid eye
movement (NREM) sleep time and decreased the amount and mean episode duration of 
wakefulness when administered in the dark period. Since peripheral administration
of glycine induced fragmentation of sleep/wakefulness states, which is a
characteristic of orexin deficiency, we examined the effects of glycine on orexin
neurons. The number of Fos-positive orexin neurons markedly decreased after
intraperitoneal administration of glycine to mice. To examine whether glycine
acts directly on orexin neurons, we examined the effects of glycine on orexin
neurons by patch-clamp electrophysiology. Glycine directly induced
hyperpolarization and cessation of firing of orexin neurons. These responses were
inhibited by a specific glycine receptor antagonist, strychnine. Triple-labeling 
immunofluorescent analysis showed close apposition of glycine transporter 2
(GlyT2)-immunoreactive glycinergic fibers onto orexin-immunoreactive neurons.
Immunoelectron microscopic analysis revealed that GlyT2-immunoreactive terminals 
made symmetrical synaptic contacts with somata and dendrites of orexin neurons.
Double-labeling immunoelectron microscopy demonstrated that glycine receptor
alpha subunits were localized in the postsynaptic membrane of symmetrical
inhibitory synapses on orexin neurons. Considering the importance of glycinergic 
regulation during REM sleep, our observations suggest that glycine injection
might affect the activity of orexin neurons, and that glycinergic inhibition of
orexin neurons might play a role in physiological sleep regulation.

DOI: 10.1371/journal.pone.0025076 
PMCID: PMC3174993
PMID: 21949857  [PubMed - indexed for MEDLINE]

