
1. Neurol Neurochir Pol. 1986 Sep-Oct;20(5):410-4.

[Physiologic all-night sleep in patients with severe forms of epilepsy].

[Article in Polish]

Bacia T, Czarkwini-Pacak L, Fryze C.

100 all night sleep records in 90 patients with various forms of epilepsy and 10 
patients with syncope were analyzed. There were 10 patients with generalized
epilepsy, 41-with partial epilepsy with complex symptomatology and temporal foci,
23--with mixed seizures and frontal focal changes and 16 patients with partial
epilepsy with simple seizures and various location of EEG foci. Normal sleep
pattern was present in 21% of cases. The most frequent changes of sleep pattern
were: prolongation of sleep onset and the latency of the first episode of REM,
instability of sleep stages and absence of sleep spindles. Interictal discharges 
appeared mostly in all sleep stages of NREM. 50% of epileptic patients showed
focal spikes in REM. Nocturnal seizures occurred in 18 patients, in several of
them very frequently.


PMID: 3587510  [PubMed - indexed for MEDLINE]

