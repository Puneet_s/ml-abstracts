
1. Am J Respir Crit Care Med. 2010 May 1;181(9):997-1002. doi:
10.1164/rccm.200908-1304OC. Epub 2010 Jan 21.

Sleepiness, quality of life, and sleep maintenance in REM versus non-REM
sleep-disordered breathing.

Chami HA(1), Baldwin CM, Silverman A, Zhang Y, Rapoport D, Punjabi NM, Gottlieb
DJ.

Author information: 
(1)The Pulmonary Center, Boston University School of Medicine, 72 East Concord
Street, R-304, Boston, MA 02118, USA. hchami@bu.edu

Erratum in
    Am J Respir Crit Care Med. 2011 Sep 1;184(5):622.

Comment in
    Am J Respir Crit Care Med. 2011 Sep 1;184(5):622.

RATIONALE: The impact of REM-predominant sleep-disordered breathing (SDB) on
sleepiness, quality of life (QOL), and sleep maintenance is uncertain.
OBJECTIVE: To evaluate the association of SDB during REM sleep with daytime
sleepiness, health-related QOL, and difficulty maintaining sleep, in comparison
to their association with SDB during non-REM sleep in a community-based cohort.
METHODS: Cross-sectional analysis of 5,649 Sleep Heart Health Study participants 
(mean age 62.5 [SD = 10.9], 52.6% women, 22.6% ethnic minorities). SDB during REM
and non-REM sleep was quantified using polysomnographically derived
apnea-hypopnea index in REM (AHI(REM)) and non-REM (AHI(NREM)) sleep. Sleepiness,
sleep maintenance, and QOL were respectively quantified using the Epworth
Sleepiness Scale (ESS), the Sleep Heart Health Study Sleep Habit Questionnaire,
and the physical and mental composites scales of the Medical Outcomes Study Short
Form (SF)-36.
MEASUREMENTS AND MAIN RESULTS: AHI(REM) was not associated with the ESS scores or
the physical and mental components scales scores of the SF-36 after adjusting for
demographics, body mass index, and AHI(NREM) x AHI(REM) was not associated with
frequent difficulty maintaining sleep or early awakening from sleep. AHI(NREM)
was associated with the ESS score (beta = 0.25; 95% confidence interval [CI],
0.16 to 0.34) and the physical (beta = -0.12; 95% CI, -0.42 to -0.01) and mental 
(beta = -0.20; 95% CI, -0.20 to -0.01) components scores of the SF-36 adjusting
for demographics, body mass index, and AHI(REM).
CONCLUSIONS: In a community-based sample of middle-aged and older adults,
REM-predominant SDB is not independently associated with daytime sleepiness,
impaired health-related QOL, or self-reported sleep disruption.

DOI: 10.1164/rccm.200908-1304OC 
PMCID: PMC3269234
PMID: 20093641  [PubMed - indexed for MEDLINE]

