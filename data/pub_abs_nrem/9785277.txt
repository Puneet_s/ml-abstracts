
1. J Sleep Res. 1998 Sep;7(3):217-23.

Sleep fragmentation and daytime vigilance in patients with OSA treated by
surgical maxillomandibular advancement compared to CPAP therapy.

Conradt R(1), Hochban W, Heitmann J, Brandenburg U, Cassel W, Penzel T, Peter JH.

Author information: 
(1)Schlafmedizinisches Labor, Gesichtschirurgie, Philipps-Universität Marburg,
Germany.

Impaired vigilance is a frequent daytime complaint of patients with obstructive
sleep apnoea (OSA). To date, continuous positive airway pressure (CPAP) is a well
established therapy for OSA. Nevertheless, in patients with certain craniofacial 
characteristics, maxillomandibular advancement osteotomy (MMO) is a promising
surgical treatment. Twenty-four male patients with OSA (pretreatment respiratory 
disturbance index (RDI) 59.3 SD +/- 24.1 events/h) participated in this
investigation. The mean age was 42.7 +/- 10.7 years and the mean body mass index 
was 26.7 +/- 2.9 kg/m2. According to cephalometric evaluation, all patients had a
narrow posterior airway space, more or less due to severe maxillary and
mandibular retrognathia. All patients except two were treated first with CPAP for
at least 3 months and afterwards by MMO. Two patients only tolerated a CPAP trial
for 2 nights. Polysomnographic investigation and daytime vigilance were assessed 
before therapy, with CPAP therapy and 3 months after surgical treatment.
Patients' reports of impaired daytime performance were confirmed by a
pretreatment vigilance test using a 90-min, four-choice reaction-time test. The
test was repeated with effective CPAP therapy and postoperatively. Daytime
vigilance was increased with CPAP and after surgical treatment in a similar
manner. Respiratory and polysomnographic patterns clearly improved, both with
CPAP and after surgery, and showed significant changes compared to the
pretreatment investigation. The RDI decreased significantly, both with CPAP (5.3 
+/- 6.0) and postoperatively (5.6 +/- 9.6 events/h). The percentages of non-rapid
eye movement Stage 1 (NREM 1) sleep showed a marked decrease (with CPAP 8.2 +/-
3.6% and after MMO 8.2 +/- 4.4% vs. 13.3 +/- 7.4% before treatment), whereas
percentages of slow wave sleep increased significantly from 8.0 +/- 6.1% before
therapy to 18.2 +/- 12.8 with CPAP and 14.4 +/- 7.3% after MMO. The number of
awakenings per hour time in bed (TIB) was significantly reduced after surgery
(2.8 +/- 1.3), compared to both preoperative investigation (baseline 4.2 +/- 2.0 
and CPAP 3.4 +/- 1.5). Brief arousals per hour TIB were reduced to half with CPAP
(19.3 +/- 20.0) and after MMO (19.7 +/- 13.6), compared to baseline (54.3 +/-
20.0). We conclude that the treatment of OSA by MMO in carefully selected cases
has positive effects on sleep, respiration and daytime vigilance, which are
comparable to CPAP therapy.


PMID: 9785277  [PubMed - indexed for MEDLINE]

