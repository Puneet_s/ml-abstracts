
1. Dev Med Child Neurol. 1984 Apr;26(2):169-76.

Development of consolidated sleep and wakeful periods in relation to the
day/night cycle in infancy.

Coons S, Guilleminault C.

Periods of sustained sleep were analyzed to assess the development of sleep-state
organization and structure during the first six months of life. Infants first
establish consolidated sleep and wakeful periods, which then become oriented to
the 24-hour day/night cycle. As infants mature they gradually show greater sleep 
'efficiency', and at onset of sleep, REM periods become less likely. The longest 
sleep period progressively becomes associated with the dark period of the 24-hour
cycle. This study also assessed the sequences of NREM-REM sleep-cycle
organization during the first six months of life. Although individual infants may
have a significant correlation between duration of sleep and latency at onset of 
sleep, the over-all direction is not constant, nor is an 'age effect' apparent
during the first six months. The data support the hypothesis that states of
alterness , which develop independently in the perinatal period, become
integrated in early infancy.


PMID: 6724155  [PubMed - indexed for MEDLINE]

