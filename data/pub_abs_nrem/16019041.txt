
1. Physiol Behav. 2005 Jul 21;85(4):419-29.

Differential effects of two types of environmental novelty on activity and sleep 
in BALB/cJ and C57BL/6J mice.

Tang X(1), Xiao J, Parris BS, Fang J, Sanford LD.

Author information: 
(1)Sleep Research Laboratory, Department of Pathology and Anatomy, Eastern
Virginia Medical School, P.O. Box 1980, Norfolk, VA 23501, USA. tangx@evms.edu

Change in the sleeping environment can produce significant alterations in sleep. 
To determine how these alterations may vary with the amount of change and the
relative reactivity of the sleeper, we examined the influences of environmental
novelty on sleep in two mouse strains that differ in behavioral anxiety. Mice
[BALB/cJ (n=7) and C57BL/6J (n=8)] were implanted for recording EEG and activity 
via telemetry. Following baseline data collection, activity and sleep were
examined over 46 h after routine cage change, after placing a simple novel object
(PVC Tee) in the home cage, and after handling controls. Mice of both strains
showed immediate increases in activity and decreases in rapid eye movement sleep 
(REM) and non-REM (NREM) after cage change and novel object. Within strain,
changes in activity and sleep were greater after cage change than after novel
object. Changes in activity and sleep time were significantly correlated in each 
strain. Compared to C57BL/6J mice, BALB/cJ mice exhibited greater and longer
duration initial reductions in sleep time, and greater increases in EEG slow wave
activity power after cage change and novel object, but these changes were not
followed with subsequent increases in sleep time. In contrast, C57BL/6J mice
showed significantly greater subsequent increases in sleep time following the
initial reductions induced by both manipulations. The results suggest that
initial decreases and subsequent increases in sleep time are related to putative 
differences in the intensity of environmental novelty (cage change>novel object) 
and to previously described strain differences in anxiety (BALB/cJ>C57BL/6J).

DOI: 10.1016/j.physbeh.2005.05.008 
PMID: 16019041  [PubMed - indexed for MEDLINE]

