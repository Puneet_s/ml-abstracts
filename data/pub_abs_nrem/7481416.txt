
1. Sleep. 1995 Jul;18(6):446-50.

Slow wave sleep and cyclic alternating pattern (CAP) in HIV-infected asymptomatic
men.

Ferini-Strambi L(1), Oldani A, Tirloni G, Zucconi M, Castagna A, Lazzarin A,
Smirne S.

Author information: 
(1)Sleep Disorders Center, State University, Milan, Italy.

Alterations of sleep structure have been reported in asymptomatic human
immunodeficiency virus (HIV)-infected subjects. In these patients some authors
have found an increased percentage of slow wave sleep (SWS) and a SWS
preponderance in the second half of the night, as well as subjective sleep
complaints. Other authors have found an increased stage 1 non-rapid eye movement 
(NREM) and reduced stage 2 NREM percentages in asymptomatic subjects. We
evaluated the macrostructure and the microstructure (cyclic alternating pattern, 
CAP) of sleep in nine HIV-infected asymptomatic men without sleep complaints or
psychiatric illness, in comparison with nine age-matched controls. Our study
showed a decreased amount of SWS and a significantly higher CAP rate in
HIV-subjects, suggesting an altered organization of the sleep process in these
patients.


PMID: 7481416  [PubMed - indexed for MEDLINE]

