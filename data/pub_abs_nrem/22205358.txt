
1. Sleep Breath. 2012 Dec;16(4):1181-91. doi: 10.1007/s11325-011-0630-x. Epub 2011
Dec 29.

Sleep-related epilepsy in a Long-Evans hooded rat model of depression.

McDowell AL(1), Strohl KP, Feng P.

Author information: 
(1)Division of Pulmonary, Critical Care and Sleep Medicine, Case Western Reserve 
University, Cleveland, OH 44106, USA.

INTRODUCTION: Neonatal treatment with clomipramine (CLI) has been shown to have
reliable behavioral and biological changes that mimic major symptomatic and
biochemical changes found in depression. This paper further explores a common
feature of depression, the comorbidity of seizure activity and depressive
behaviors in this mode.
METHODS: Rat pups were neonatally treated with 40 mg/kg/day of CLI from postnatal
day 8 through 21. In adulthood, they were instrumented with
electroencephalographic (EEG) and electromyographic (EMG) electrodes for 24 h of 
polysomnogram (PSG) recordings. PSG data were analyzed for: (1) sleep-wake cycle;
(2) spectral power; and (3) epileptiform activity, including NREM-to-REM
transition (NRT) bursts.
RESULTS: Neonatal treatment with CLI reliably produces enhanced levels of REM
(p < 0.01) and reduced sexual activity (p < 0.05). Theta power was enhanced
during NREM sleep in the CLI group (p = 0.02). CLI-treated animals experienced
increased frequency at the NRT (p < 0.01), as well as additional epileptiform
activity of continuous (CTS; p < 0.05) and petite-continuous (P-CTS; p < 0.01)
types, across the sleep-wake cycle. There is a strong temporal correlation with
increased REM sleep duration, increased frequency of NRT bursts, and increased
theta power during NREM sleep in CLI-treated animals.
DISCUSSION: Neonatal CLI-treated animals experienced significantly more
epileptiform activity as a whole, in addition to comorbid features of depression 
in adulthood. Neonatal exposure to CLI will not only produce depressive phenotype
but may also enhance risk for epilepsy in some individuals. This warrants further
investigation into currently acceptable medicinal use in humans.

DOI: 10.1007/s11325-011-0630-x 
PMID: 22205358  [PubMed - indexed for MEDLINE]

