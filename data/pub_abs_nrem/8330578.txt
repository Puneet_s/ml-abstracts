
1. Epilepsia. 1993 Jul-Aug;34(4):679-85.

Reappraisal of interictal electroencephalograms in infantile spasms.

Watanabe K(1), Negoro T, Aso K, Matsumoto A.

Author information: 
(1)Department of Pediatrics, Nagoya University School of Medicine, Japan.

To delineate interictal electroencephalographic (EEG) features before treatment
of patients with clinically defined infantile spasms, EEGs of 82 infants having
tonic spasms in clusters were analyzed by type of paroxysmal abnormalities,
continuity, interhemispheric synchrony, topography, and wave component of
hypsarrhythmia during wakefulness and sleep. Hypsarrhythmia occurred less
frequently in wakefulness than in non-rapid eye movement (NREM) sleep at any age,
least frequently in wakefulness after 1 year of age, and disappeared in rapid eye
movement (REM) sleep at any age. The continuity of hypsarrhythmia changed with
states, but did not change with age, and was greatest in wakefulness and stage 1 
and decreased in stage 2-3. Interhemispheric synchrony increased with increasing 
age but decreased with advancing sleep stage. The term modified hypsarrhythmia
should be discarded, and unusual features, if present, should be specified.


PMID: 8330578  [PubMed - indexed for MEDLINE]

