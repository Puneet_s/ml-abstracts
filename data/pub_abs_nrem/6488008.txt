
1. Brain Res. 1984 Aug 20;309(1):17-26.

Changes in the thermal characteristics of hypothalamic neurons during sleep and
wakefulness.

Glotzbach SF, Heller HC.

The characteristics of the mammalian thermoregulatory system are dependent upon
arousal state. During NREM sleep thermoregulatory mechanisms are intact but body 
temperature is regulated at a lower level than during wakefulness. In REM sleep
thermoregulatory effector mechanisms are inhibited and thermal homeostasis is
severely disrupted. Thermosensitivity of neurons in the preoptic/anterior
hypothalamus (POAH) was determined for behaving kangaroo rats (Dipodomys deserti)
during electrophysiologically defined wakefulness, NREM sleep and REM sleep to
elucidate possible neural mechanisms for previous findings of state-dependent
changes in thermoregulation. Thirty cells were tested during at least two arousal
states. During wakefulness, 70% of the recorded cells were sensitive to changes
in local temperature, with the number of warm-sensitive (W) cells outnumbering
cold-sensitive (C) cells by 1.6:1. In NREM sleep, 43% of the cells were thermally
sensitive, with the ratio of W:C remaining the same as in wakefulness. In REM
sleep only two cells were thermosensitive (both W). The decrease in neuronal
thermosensitivity of POAH cells during REM sleep parallels findings of inhibition
of thermoregulatory effector responses during REM, although further work is
necessary to determine the source and nature of the inhibition.


PMID: 6488008  [PubMed - indexed for MEDLINE]

