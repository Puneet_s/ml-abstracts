
1. Indian J Physiol Pharmacol. 2012 Oct-Dec;56(4):295-300.

Sleep architecture at 4300 m altitude in a sample of Indian lowlanders.

Thakur L(1), Anand JP, Malhotra AS, Sharma YK, Panjwani U.

Author information: 
(1)Neurophysiology Division, Defence Institute of Physiology and Allied Sciences,
Lucknow Road, Timarpur, Delhi, Zip--110 054, India.

The present study aimed to evaluate sleep architecture at 4300 m in a sample of
10 healthy Indian lowlanders, mean age 25.7 +/- 5.1 yrs. Polysomnography on two
consecutive nights each was performed at sea level and 4300 m, the first night
for adaptation and the second one for actual recording. Total sleep time reduced 
from 433.33 +/- 8.95 to 412.06 +/- 13.13 minutes (P < 0.0005), sleep latency
increased from 11.56 +/- 6.85 to 22.22 +/- 7.95 minutes (P < 0.0025), deep NREM
sleep (S3 + S4) reduced from 79.56 +/- 28.45 to 45.39 +/- 25.32 minutes (P <
0.01), light NREM sleep (S1 + S2) increased from 272.94 +/- 20.63 to 296.72
+/-23.24 minutes (P < 0.05), REM decreased from 80.89 +/- 7.65 to 69.94 +/- 11.30
minutes (P < 0.02) and periodic breathing was present in 4 of 10 participants on 
the second night at 4300 m. Decreased sleep quality (P < 0.0005) and increased
sleep disturbances (P < 0.0005) were reported in subjective ratings at high
altitude. Changes in sleep architecture similar to but of a greater magnitude are
present on the second night of staged induction to 4300 m, than reported at 3500 
m in our earlier study.


PMID: 23781648  [PubMed - indexed for MEDLINE]

