
1. Biol Psychiatry. 1997 Oct 1;42(7):560-7.

Maintenance nortriptyline effects on electroencephalographic sleep in elderly
patients with recurrent major depression: double-blind, placebo- and
plasma-level-controlled evaluation.

Reynolds CF 3rd(1), Buysse DJ, Brunner DP, Begley AE, Dew MA, Hoch CC, Hall M,
Houck PR, Mazumdar S, Perel JM, Kupfer DJ.

Author information: 
(1)Mental Health Clinical Research Center for the Study of Late-Life Mood
Disorders, University of Pittsburgh Medical Center, Pennsylvania, USA.

Our aim was to contrast the effects of maintenance nortriptyline and placebo on
electroencephalographic sleep measures in elderly recurrent depressives who
survived 1-year without recurrence of depression. Patients on nortriptyline took 
longer to fall asleep and did not maintain sleep better than patients on placebo;
however, maintenance nortriptyline was associated with more delta-wave production
and higher delta-wave density in the first non-REM (NREM) period relative to the 
second. Nortriptyline levels were positively but weakly related to all-night
delta-wave production during maintenance (accounting for 6.6% of the variance in 
delta-wave counts). Total phasic REM activity increased 100% under chronic
nortriptyline relative to placebo, with a robust increase in the rate of REM
activity generation across the night. Effective long-term pharmacotherapy of
recurrent major depression is associated with enhancement in the rate of
delta-wave production in the first NREM period (i.e., delta sleep ratio) and of
REM activity throughout the night.

DOI: 10.1016/S0006-3223(96)00424-6 
PMID: 9376452  [PubMed - indexed for MEDLINE]

