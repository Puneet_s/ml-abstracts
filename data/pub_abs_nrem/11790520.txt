
1. Neuropsychopharmacology. 2002 Feb;26(2):246-58.

Fluoxetine and sleep EEG: effects of a single dose, subchronic treatment, and
discontinuation in healthy subjects.

Feige B(1), Voderholzer U, Riemann D, Dittmann R, Hohagen F, Berger M.

Author information: 
(1)Department of Psychiatry and Psychotherapy, Albert-Ludwigs-University,
Freiburg, Germany.

The goals of the current study were to evaluate whether a single dose of
fluoxetine causes qualitatively different changes in sleep architecture and NREM 
sleep EEG than subchronic administration in healthy subjects and to determine
degree and duration of such changes after the single dose and after
discontinuation from subchronic administration. Our hypothesis was that
subchronic intake should cause changes qualitatively different from the single
dose and that such changes could be sufficiently long-lived to suggest the
possibility of a dosing in intervals of several days. Ten healthy volunteers
first took one single 60-mg dose of fluoxetine and a week later started to take a
40-mg dose every morning for three weeks. Sleep laboratory nights included two
nights before and four nights after the single dose and every second night for
two weeks after discontinuation from subchronic administration. The single dose
caused only a slight increase in drug plasma concentrations but relatively clear 
changes in sleep structure. After discontinuation from subchronic administration,
sleep quality indices normalized quickly (within 2-4 days), whereas REM latency
and spectral power effects correlated with total SSRI plasma concentration and
normalized more slowly, corresponding to the drug plasma half-life of about 10
days. The REM fraction of the sleep period showed a rebound, whereas the delta
sleep ratio did not correlate with drug plasma levels and yet remained increased 
after the medication interval. Thus, the only qualitative difference seen between
acute and subchronic medication was the initial sleep disturbance. REM latency
and especially the delta sleep ratio remained increased for several days after
discontinuation from subchronic administration, indicating the possibility of a
less-than-daily maintenance medication after an initial daily interval. Finally, 
the pattern of change observed for the delta sleep ratio indicates that it may be
due to secondary, adaptive effects possibly linked to the antidepressant effect
of fluoxetine in depressed patients.

DOI: 10.1016/S0893-133X(01)00314-1 
PMID: 11790520  [PubMed - indexed for MEDLINE]

