
1. Respir Physiol. 1998 Oct;114(1):37-48.

Variability of vigilance and ventilation: studies on the control of respiration
during sleep.

Schäfer T(1).

Author information: 
(1)Department of Applied Physiology, Ruhr-Universität Bochum, Germany.
thorsten.schaefer@ruhr-uni-bochum.de

Ventilation is under metabolic as well as under behavioural control. This causes 
a complex interaction between states of 'vigilance' and respiration. This paper
briefly summarizes sleep-related changes of respiration and presents an
experimental study on the course of respiratory CO2-sensitivity during a whole
night's sleep in ten healthy volunteers. The feedback control of breathing was
challenged by continuous step changes of inspired CO2 every 7 min, resulting in
60, 3-step steady-state hypercapnic ventilatory responses (HCVR) per night in
each subject. We analysed the variability of baseline ventilation and the effects
of hypercapnia on ventilation with respect to sleep stages. There were only small
differences in baseline PCO2 and ventilation between sleep stages, but a high
variability of the slope of the CO2-response curves in the course of the night,
ranging from 0.5 to 3.0 L min(-1) Torr(-1). The HCVR was significantly lower
during REM sleep than during all stages of NREM sleep. Due to a compensatory left
shift of the flattened CO2-response curves, however, ventilation at baseline CO2 
as well as during slight hypercapnia varied much less than would be expected from
the high variability of slopes. We conclude that the characteristics of the
CO2-sensitive feedback control system of respiration, are highly variable during 
sleep, but due to offsetting effects, PCO2 and ventilation remain quite stable in
the physiological range.


PMID: 9858049  [PubMed - indexed for MEDLINE]

