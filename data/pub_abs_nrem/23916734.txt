
1. Brain Res. 2013 Sep 19;1531:9-16. doi: 10.1016/j.brainres.2013.07.048. Epub 2013 
Aug 2.

Sleep allostasis in chronic sleep restriction: the role of the norepinephrine
system.

Kim Y(1), Chen L, McCarley RW, Strecker RE.

Author information: 
(1)VA Boston Healthcare System, Research Service and Harvard Medical School,
Department of Psychiatry, 940 Belmont St., Brockton, MA 02301-5596, USA.
youngsoo_kim@hms.harvard.edu

Sleep responses to chronic sleep restriction may be very different from those
observed after acute total sleep deprivation. Specifically, when sleep
restriction is repeated for several consecutive days, animals express attenuated 
compensatory increases in sleep time and intensity during daily sleep
opportunities. The neurobiological mechanisms underlying these adaptive, or more 
specifically, allostatic, changes in sleep homeostasis are unknown. Several lines
of evidence indicate that norepinephrine may play a key role in modulating
arousal states and NREM EEG delta power, which is widely recognized as a marker
for sleep intensity. Therefore, we investigated time course changes in brain
adrenergic receptor mRNA levels in response to chronic sleep restriction using a 
rat model. Here, we observed that significantly altered mRNA levels of the α1-
adrenergic receptor in the basal forebrain as well as α2- and β1-adrenergic
receptor in the anterior cingulate cortex only on the first sleep restriction
day. On the other hand, the frontal cortex α1-, α2-, and β1-adrenergic receptor
mRNA levels were reduced throughout the period of sleep restriction. Combined
with our earlier findings on EEG that sleep time and intensity significantly
increased only on the first sleep restriction days, these results suggest that
alterations in the brain norepinephrine system in the basal forebrain and
cingulate cortex may mediate allostatic changes in sleep time and intensity
observed during chronic sleep restriction.

© 2013 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.brainres.2013.07.048 
PMCID: PMC3807856
PMID: 23916734  [PubMed - indexed for MEDLINE]

