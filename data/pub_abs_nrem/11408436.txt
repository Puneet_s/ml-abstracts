
1. J Appl Physiol (1985). 2001 Jul;91(1):239-48.

Effect of REM sleep on retroglossal cross-sectional area and compliance in normal
subjects.

Rowley JA(1), Sanders CS, Zahn BR, Badr MS.

Author information: 
(1)Medical Service, John D. Dingell Veterans Affairs Medical Center, and the
Division of Pulmonary, Critical Care and Sleep Medicine, Department of Medicine, 
Wayne State University School of Medicine, Detroit, Michigan 48201, USA.
jrowley@intmed.wayne.edu

It has been proposed that the upper airway compliance should be highest during
rapid eye movement (REM) sleep. Evidence suggests that the increased compliance
is secondary to an increased retroglossal compliance. To test this hypothesis, we
examined the effect of sleep stage on the relationship of retroglossal
cross-sectional area (CSA; visualized with a fiber-optic scope) to pharyngeal
pressure measured at the level of the oropharynx during eupneic breathing in
subjects without significant sleep-disordered breathing. Breaths during REM sleep
were divided into phasic (associated with eye movement, PREM) and tonic (not
associated with eye movements, TREM). Retroglossal CSA decreased with non-REM
(NREM) sleep and decreased further in PREM [wake 156.8 +/- 48.6 mm(2), NREM 104.6
+/- 65.0 mm(2) (P < 0.05 wake vs. NREM), TREM 83.1 +/- 46.4 mm(2) (P = not
significant NREM vs. TREM), PREM 73.9 + 39.2 mm(2) (P < 0.05 TREM vs. PREM)].
Retroglossal compliance, defined as the slope of the regression CSA vs.
pharyngeal pressure, was the same between all four conditions (wake -0.7 + 2.1
mm(2)/cmH(2)O, NREM 0.6 +/- 3.0 mm(2)/cmH(2)O, TREM -0.2 +/- 3.3 mm(2)/cmH(2)O,
PREM -0.6 +/- 5.1 mm(2)/cmH(2)O, P = not significant). We conclude that the
intrinsic properties of the airway wall determine retroglossal compliance
independent of changes in the neuromuscular activity associated with changes in
sleep state.


PMID: 11408436  [PubMed - indexed for MEDLINE]

