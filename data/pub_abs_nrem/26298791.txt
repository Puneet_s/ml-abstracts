
1. Sleep Med. 2015 Sep;16(9):1139-45. doi: 10.1016/j.sleep.2015.04.027. Epub 2015
Jun 25.

Non-rapid eye movement sleep instability in mild cognitive impairment: a pilot
study.

Maestri M(1), Carnicelli L(2), Tognoni G(2), Di Coscio E(2), Giorgi FS(2), Volpi 
L(2), Economou NT(3), Ktonas P(3), Ferri R(4), Bonuccelli U(2), Bonanni E(2).

Author information: 
(1)Neurology Unit, Department of Clinical and Experimental Medicine, University
of Pisa, Pisa, Italy. Electronic address: m.maestri@ao-pisa.toscana.it.
(2)Neurology Unit, Department of Clinical and Experimental Medicine, University
of Pisa, Pisa, Italy. (3)Sleep Study Unit, Eginition Hospital, University of
Athens Medical School, Athens, Greece. (4)Department of Neurology IC, Oasi
Institute for Research on Mental Retardation and Brain Aging (IRCCS), Troina,
Italy.

OBJECTIVE: Polysomnographic (PSG) studies in mild cognitive impairment (MCI) are 
not conclusive and are limited only to conventional sleep parameters. The aim of 
our study was to evaluate sleep architecture and cyclic alternating pattern (CAP)
parameters in subjects with MCI, and to assess their eventual correlation with
cognition.
METHODS: Eleven subjects with MCI (mean age 68.5 ± 7.0 years), 11 patients with
mild probable Alzheimer's disease (AD; mean age 72.7 ± 5.9 years), referred to
the Outpatient Cognitive Disorders Clinic, and 11 cognitively intact healthy
elderly individuals (mean age 69.2 ± 12.6 years) underwent ambulatory PSG for the
evaluation of nocturnal sleep architecture and CAP parameters.
RESULTS: Rapid eye movement sleep, CAP rate, and CAP slow components (A1 index)
were decreased in MCI subjects and to a greater extent in AD patients, compared
to cognitively intact controls. AD showed also decreased slow wave sleep (SWS)
relative to healthy elderly individuals. MCI nappers showed decreased nocturnal
SWS and A1 subtypes compared to non-nappers. Several correlations between sleep
variables and neuropsychological tests were found.
CONCLUSIONS: MCI and AD subjects showed a decreased sleep instability correlated 
with their cognitive decline. Such a decrease may be considered as a potential
biomarker of underlying neurodegeneration.

Copyright © 2015 Elsevier B.V. All rights reserved.

DOI: 10.1016/j.sleep.2015.04.027 
PMID: 26298791  [PubMed - indexed for MEDLINE]

