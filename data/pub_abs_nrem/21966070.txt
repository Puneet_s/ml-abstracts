
1. Sleep. 2011 Oct 1;34(10):1385-93. doi: 10.5665/SLEEP.1284.

Sleep EEG provides evidence that cortical changes persist into late adolescence.

Tarokh L(1), Van Reen E, LeBourgeois M, Seifer R, Carskadon MA.

Author information: 
(1)E.P. Bradley Sleep Research Laboratory, Providence, RI 02906, USA.
Leila_Tarokh@brown.edu

Comment in
    Sleep. 2011 Oct;34(10):1287-8.

STUDY OBJECTIVES: To examine developmental changes in the human sleep
electroencephalogram (EEG) during late adolescence.
SETTING: A 4-bed sleep laboratory.
PARTICIPANTS: Fourteen adolescents (5 boys) were studied at ages 15 or 16
(initial) and again at ages 17 to 19 (follow-up).
INTERVENTIONS: N/A.
MEASUREMENTS AND RESULTS: All-night polysomnography was recorded at each
assessment and scored according to the criteria of Rechtschaffen and Kales. A 27%
decline in duration of slow wave sleep, and a 22% increase of stage 2 sleep was
observed from the initial to the follow-up session. All-night spectral analysis
of 2 central and 2 occipital leads revealed a significant decline of NREM and REM
sleep EEG power with increasing age across frequencies in both states.
Time-frequency analysis revealed that the decline in power was consistent across 
the night for all bands except the delta band. The decreases in power were most
pronounced over the left central (C3/A2) and right occipital (O2/A1) derivations.
CONCLUSIONS: Using longitudinal data, we show that the developmental changes to
the sleeping EEG that begin in early adolescence continue into late adolescence. 
As with early adolescents, we observed hemispheric asymmetry in the decline of
sleep EEG power. This decline was state and frequency nonspecific, suggesting
that it may be due to the pruning of synapses known to occur during adolescence.

DOI: 10.5665/SLEEP.1284 
PMCID: PMC3174840
PMID: 21966070  [PubMed - indexed for MEDLINE]

