
1. Nat Neurosci. 2013 Mar;16(3):357-64. doi: 10.1038/nn.3324. Epub 2013 Jan 27.

Prefrontal atrophy, disrupted NREM slow waves and impaired hippocampal-dependent 
memory in aging.

Mander BA(1), Rao V, Lu B, Saletin JM, Lindquist JR, Ancoli-Israel S, Jagust W,
Walker MP.

Author information: 
(1)Sleep and Neuroimaging Laboratory, University of California, Berkeley,
California, USA. bamander@berkeley.edu

Aging has independently been associated with regional brain atrophy, reduced slow
wave activity (SWA) during non-rapid eye movement (NREM) sleep and impaired
long-term retention of episodic memories. However, whether the interaction of
these factors represents a neuropatholgical pathway associated with cognitive
decline in later life remains unknown. We found that age-related medial
prefrontal cortex (mPFC) gray-matter atrophy was associated with reduced NREM SWA
in older adults, the extent to which statistically mediated the impairment of
overnight sleep-dependent memory retention. Moreover, this memory impairment was 
further associated with persistent hippocampal activation and reduced
task-related hippocampal-prefrontal cortex functional connectivity, potentially
representing impoverished hippocampal-neocortical memory transformation.
Together, these data support a model in which age-related mPFC atrophy diminishes
SWA, the functional consequence of which is impaired long-term memory. Such
findings suggest that sleep disruption in the elderly, mediated by structural
brain changes, represents a contributing factor to age-related cognitive decline 
in later life.

DOI: 10.1038/nn.3324 
PMCID: PMC4286370
PMID: 23354332  [PubMed - indexed for MEDLINE]

