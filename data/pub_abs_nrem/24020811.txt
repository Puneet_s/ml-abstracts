
1. Physiol Res. 2013;62(5):569-75. Epub 2013 Sep 10.

Does obstructive sleep apnea worsen during REM sleep?

Peregrim I(1), Grešová S, Pallayová M, Fulton BL, Štimmelová J, Bačová I,
Mikuľaková A, Tomori Z, Donič V.

Author information: 
(1)Department of Medical Physiology, Faculty of Medicine, P. J. Šafárik
University, Košice, Slovakia. igor.peregrim@upjs.sk.

Although it is thought that obstructive sleep apnea (OSA) is worse during rapid
eye movement (REM) sleep than in non-REM (NREM) sleep there are some
uncertainties, especially about apnoe-hypopnoe-index (AHI). Several studies found
no significant difference in AHI between both sleep stages. However, REM sleep is
associated more with side sleeping compared to NREM sleep, which suggests that
body position is a possible confounding factor. The main purpose of this study
was to compare the AHI in REM and NREM sleep in both supine and lateral body
position. A retrospective study was performed on 422 consecutive patients who
underwent an overnight polysomnography. Women had higher AHI in REM sleep than
NREM sleep in both supine (46.05+/-26.26 vs. 23.91+/-30.96, P<0.01) and lateral
(18.16+/-27.68 vs. 11.30+/-21.09, P<0.01) body position. Men had higher AHI in
REM sleep than NREM sleep in lateral body position (28.94+/-28.44 vs.
23.58+/-27.31, P<0.01), however, they did not reach statistical significance in
supine position (49.12+/-32.03 in REM sleep vs. 45.78+/-34.02 in NREM sleep,
P=0.50). In conclusion, our data suggest that REM sleep is a contributing factor 
for OSA in women as well as in men, at least in lateral position.


PMID: 24020811  [PubMed - indexed for MEDLINE]

