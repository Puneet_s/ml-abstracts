
1. Br J Rheumatol. 1995 Jul;34(7):629-35.

Sleep intensity in fibromyalgia: focus on the microstructure of the sleep
process.

Drewes AM(1), Nielsen KD, Taagholt SJ, Bjerregård K, Svendsen L, Gade J.

Author information: 
(1)Department of Rheumatology, Aalborg Hospital, Denmark.

Alpha electroencephalography (EEG) predominance has been described during sleep
in patients suffering from the fibromyalgia syndrome (FMS). However, EEG power
density in the lower frequency bands probably better reflects the restorative
functions of sleep. This study was conducted to describe the energy in all
frequency bands in the sleep EEG. Ambulatory sleep recordings were performed on
12 women with FMS and 14 control women. Epochs were classified according to
standard criteria. Moreover, all 2-s segments (n = 287,355) of the EEG in
non-rapid-eye-movement (NREM) 2-4 sleep were subjected to frequency analysis
using autoregressive modelling. Frequency bands were: delta (0.5-3.5 Hz), theta
(3.5-8 Hz), alpha (8-12 Hz), sigma (12-14.5 Hz) and beta (14.5-25 Hz). In
patients with FMS, there was a predominance of EEG power in the higher frequency 
bands [two-way analysis of variance (ANOVA), alpha: P = 0.043; sigma: P = 0.004] 
at the expense of the lower frequencies (ANOVA, delta: P = 0.005; theta: P =
0.008). The same trends were obtained for the individual sleep cycles. The
calculations of total delta power in the time domain showed an exponentially
declining curve in healthy subjects, but a flatter decline in FMS. The decreased 
power in the low-frequency range might reflect a disorder in homoeostatic and
circadian mechanisms during sleep and may contribute to daytime symptoms in
patients with fibromyalgia.


PMID: 7670781  [PubMed - indexed for MEDLINE]

