
1. Electroencephalogr Clin Neurophysiol. 1978 Feb;44(2):202-13.

Period and amplitude analysis of 0.5-3 c/sec activity in NREM sleep of young
adults.

Feinberg I, March JD, Fein G, Floyd TC, Walker JM, Price L.


PMID: 75093  [PubMed - indexed for MEDLINE]

