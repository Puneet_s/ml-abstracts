
1. Sleep. 2016 Aug 1;39(8):1501-5. doi: 10.5665/sleep.6006.

Effects of Zolpidem CR on Sleep and Nocturnal Ventilation in Patients with Heart 
Failure.

Gatti RC(1), Burke PR(1), Otuyama LJ(1), Almeida DR(2), Tufik S(1), Poyares D(1).

Author information: 
(1)Department of Psychobiology, Sleep Division, Universidade Federal de Sao
Paulo, Brazil. (2)Department of Medicine, Cardiology Division, Universidade
Federal de Sao Paulo, Brazil.

STUDY OBJECTIVE: This study aimed to evaluate the effects of zolpidem CR
(controlled release) on sleep and nocturnal ventilation in patients with
congestive heart failure, a population at risk for insomnia and poor sleep
quality.
METHODS: Fifteen patients with heart failure (ischemic cardiomyopathy) and
ejection fraction ≤ 45% in NYHA functional class I or II were evaluated with full
polysomnography in a placebo-controlled, double-blind, randomized trial. Patients
underwent three tests: (1) baseline polysomnography and, after randomization, (2)
a new test with zolpidem CR 12.5 mg or placebo, and after 1 week, (3) a new
polysomnography, crossing the "medication" used.
RESULTS: A 16% increase in total sleep time was found with the use of zolpidem CR
and an increase in stage 3 NREM sleep (slow wave sleep). The apnea hypopnea index
(AHI) did not change with zolpidem CR even after controlling for supine position;
however, a slight but significant decrease was observed in lowest oxygen
saturation compared with baseline and placebo conditions (83.60 ± 5.51; 84.43 ±
3.80; 80.71 ± 5.18, P = 0.002).
CONCLUSION: Zolpidem CR improved sleep structure in patients with heart failure, 
did not change apnea hypopnea index, but slightly decreased lowest oxygen
saturation.

© 2016 Associated Professional Sleep Societies, LLC.

DOI: 10.5665/sleep.6006 
PMCID: PMC4945308 [Available on 2017-02-01]
PMID: 27166233  [PubMed - in process]

