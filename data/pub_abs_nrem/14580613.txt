
1. Clin Neurophysiol. 2003 Nov;114(11):2146-55.

A study of the dynamic interactions between sleep EEG and heart rate variability 
in healthy young men.

Jurysta F(1), van de Borne P, Migeotte PF, Dumont M, Lanquart JP, Degaute JP,
Linkowski P.

Author information: 
(1)Sleep Laboratory, Department of Psychiatry, Erasme Academic Hospital, Free
University of Brussels, 1070 Brussels, Belgium. fajuryst@ulb.ac.be

OBJECTIVE: We investigated the interactions between heart rate variability and
sleep electroencephalogram power spectra.
METHODS: Heart rate and sleep electroencephalogram signals were recorded in 8
healthy young men. Spectral analysis was applied to electrocardiogram and
electroencephalogram recordings. Spectral components of RR intervals were studied
across sleep stages. The cross-spectrum maximum was determined as well as
coherencies, gains and phase shifts between normalized high frequency of RR
intervals and all electroencephalographic frequency bands, calculated over the
first 3 NREM-REM cycles.
RESULTS: RR intervals increased from awake to NREM and decreased during REM.
Normalized low frequency decreased from awake to NREM and increased during REM
while normalized high frequency evolved conversely. Low to high frequency ratio
developed in opposition to RR intervals. Coherencies between normalized high
frequency and power spectra were high for all bands. The gain was highest for
delta band. Phase shift between normalized high frequency and delta differed from
zero and modifications in normalized high frequency preceded changes in delta by 
41+/-14 degrees.
CONCLUSIONS: Our study demonstrates that: (1) all electroencephalographic power
bands are linked to normalized high frequency; (2) modifications in cardiac vagal
activity show predominantly parallel changes and precede changes in delta band by
a phase shift corresponding to a lead of 12+/-5 min.


PMID: 14580613  [PubMed - indexed for MEDLINE]

