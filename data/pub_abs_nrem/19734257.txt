
1. Evid Based Complement Alternat Med. 2011;2011:361054. doi: 10.1093/ecam/nep133.
Epub 2011 May 26.

Electroacupuncture treatment normalized sleep disturbance in morphine withdrawal 
rats.

Li YJ(1), Zhong F, Yu P, Han JS, Cui CL, Wu LZ.

Author information: 
(1)Neuroscience Research Institute, Department of Neurobiology, School of Basic
Medical Sciences, Peking University, Key Lab for Neuroscience, the Ministry of
Education and Key Lab for Neuroscience, the Ministry of Public Health, Beijing
10019 1, China.

Sleep disturbance is considered as an important symptom of acute and protracted
opiate withdrawal. Current results suggest that sleep disturbance may be taken as
a predictor of relapse. Appropriate sleep enhancement therapy will be in favor of
the retention in treatment for opiate addicts. Our previous studies have shown
that electroacupuncture (EA) is effective in suppressing morphine withdrawal
syndrome. The aim of the present study is to investigate the effect of 2 and
100 Hz EA on the sleep disturbance during morphine withdrawal. Rats were made
dependent on morphine by repeated morphine injections (escalating doses of
5-80 mg kg(-1), subcutaneously, twice a day) for 5 days. EA of 2 or 100 Hz was
given twice a day for 3 days, starting at 48 h after the last morphine injection.
Electroencephalogram and electromyogram were monitored at the end of the first
and the last EA treatments, respectively. Results showed that non-rapid eye
movement (NREM) sleep, REM sleep and total sleep time decreased dramatically,
while the sleep latency prolonged significantly during acute morphine withdrawal.
Both 2 and 100 Hz EA produced a significant increase in NREM sleep, REM sleep and
total sleep time. It was suggested that EA could be a potential treatment for
sleep disturbance during morphine withdrawal.

DOI: 10.1093/ecam/nep133 
PMCID: PMC3137251
PMID: 19734257  [PubMed]

