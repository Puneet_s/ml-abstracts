
1. Proc Natl Acad Sci U S A. 2012 Dec 11;109(50):20673-8. doi:
10.1073/pnas.1217897109. Epub 2012 Nov 20.

Optogenetically induced sleep spindle rhythms alter sleep architectures in mice.

Kim A(1), Latchoumane C, Lee S, Kim GB, Cheong E, Augustine GJ, Shin HS.

Author information: 
(1)Center for Cognition and Sociality, Institute for Basic Science, Yusung-gu,
Daejeon 305-811, Korea.

Sleep spindles are rhythmic patterns of neuronal activity generated within the
thalamocortical circuit. Although spindles have been hypothesized to protect
sleep by reducing the influence of external stimuli, it remains to be confirmed
experimentally whether there is a direct relationship between sleep spindles and 
the stability of sleep. We have addressed this issue by using in vivo
photostimulation of the thalamic reticular nucleus of mice to generate spindle
oscillations that are structurally and functionally similar to spontaneous sleep 
spindles. Such optogenetic generation of sleep spindles increased the duration of
non-rapid eye movement (NREM) sleep. Furthermore, the density of sleep spindles
was correlated with the amount of NREM sleep. These findings establish a causal
relationship between sleep spindles and the stability of NREM sleep, strongly
supporting a role for the thalamocortical circuit in sleep regulation.

DOI: 10.1073/pnas.1217897109 
PMCID: PMC3528529
PMID: 23169668  [PubMed - indexed for MEDLINE]

