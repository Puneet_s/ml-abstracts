
1. Exp Neurol. 2006 Feb;197(2):486-94. Epub 2005 Nov 11.

Manipulating REM sleep in older adults by selective REM sleep deprivation and
physiological as well as pharmacological REM sleep augmentation methods.

Hornung OP(1), Regen F, Schredl M, Heuser I, Danker-Hopfe H.

Author information: 
(1)Department of Psychiatry and Psychotherapy, Charité-University Medicine
Berlin, Campus Benjamin Franklin, Eschenallee 3, 14050 Berlin, Germany.
orla.hornung@charite.de

Experimental approaches to manipulate REM sleep within the cognitive neuroscience
of sleep are usually based on sleep deprivation paradigms and focus on younger
adults. In the present study, a traditional selective REM sleep deprivation
paradigm as well as two alternative manipulation paradigms targeting REM sleep
augmentation were investigated in healthy older adults. The study sample
consisted of 107 participants, male and female, between the ages of 60 and 82
years, who had been randomly assigned to five experimental groups. During the
study night, a first group was deprived of REM sleep by selective REM sleep
awakenings, while a second group was woken during stage 2 NREM sleep in matched
frequency. Physiological REM sleep augmentation was realized by REM sleep rebound
after selective REM sleep deprivation, pharmacological REM sleep augmentation by 
administering an acetylcholinesterase inhibitor in a double-blind,
placebo-controlled design. Deprivation and augmentation paradigms manipulated REM
sleep significantly, the former affecting more global measures such as REM sleep 
minutes and percentage, the latter more organizational aspects such as stage
shifts to REM sleep, REM latency, REM density (only pharmacological augmentation)
and phasic REM sleep duration. According to our findings, selective REM sleep
deprivation seems to be an efficient method of REM sleep manipulation in healthy 
older adults. While physiological rebound-based and pharmacological cholinergic
REM sleep augmentation methods both failed to affect global measures of REM
sleep, their efficiency in manipulating organizational aspects of REM sleep
extends the traditional scope of REM sleep manipulation methods within the
cognitive neuroscience of sleep.

DOI: 10.1016/j.expneurol.2005.10.013 
PMID: 16289171  [PubMed - indexed for MEDLINE]

