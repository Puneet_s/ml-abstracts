
1. Regul Pept. 2004 Jun 15;119(1-2):133-8.

Circulating endothelin parallels arterial blood pressure during sleep in healthy 
subjects.

Charloux A(1), Piquard F, Geny B, Ehrhart J, Brandenberger G.

Author information: 
(1)Laboratoire des Régulations Physiologiques et des Rythmes Biologiques chez
l'Homme, Institut de Physiologie, 4 rue Kirschleger, 67085, Strasbourg cedex,
France. Anne.Charloux@chru-strasbourg.fr

OBJECTIVE: To characterize plasma endothelin 1 (ET-1) and arterial blood pressure
(ABP) time courses during the first complete non-rapid eye movement (NREM)-REM
sleep cycle in healthy subjects, together with plasma renin activity (PRA) and
plasma atrial natriuretic peptide (ANP).
METHODS: Heart rate (HR), intra-arterial blood pressure and sleep
electroencephalographic activity were recorded continuously during the night in
eight healthy 20-28-year-old males. Blood was sampled every 10 min during their
first complete sleep cycle for simultaneous measurements of plasma ET-1, PRA and 
ANP.
RESULTS: Circulating ET-1 demonstrated significant variations during the sleep
cycle (p<0.0001) that paralleled those of ABP (p<0.05) and HR (p<0.005), with a
minimum during NREM sleep and a maximum during REM sleep. ET-1 time course
opposed that of PRA which increases during NREM sleep and decreases during REM
sleep (p<0.0005). Plasma ANP did not demonstrate systematic variation in relation
with the sleep cycle.
CONCLUSION: Circulating ET-1, which parallels variations of ABP, may participate 
in ABP regulation during sleep in healthy subjects, in association with the
renin-angiotensin system.

DOI: 10.1016/j.regpep.2004.01.006 
PMID: 15093707  [PubMed - indexed for MEDLINE]

