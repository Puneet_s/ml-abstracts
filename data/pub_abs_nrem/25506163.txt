
1. Ann Indian Acad Neurol. 2014 Oct;17(4):416-9. doi: 10.4103/0972-2327.144016.

Sleep disturbances in drug naïve Parkinson's disease (PD) patients and effect of 
levodopa on sleep.

Ferreira T(1), Prabhakar S(1), Kharbanda PS(1).

Author information: 
(1)Department of Neurology, Nehru Hospital, Postgraduate Institute of Medical
Education and Research, Chandigarh, India.

CONTEXT: Parkinson's disease (PD) is associated with sleep disturbances,
attributed to the neurodegenerative process and therapeutic drugs. Studies have
found levodopa to increase wakefulness in some patients while increasing
sleepiness in others.
AIMS: To confirm sleep disturbances in drug naïve PD patients and understand the 
impact of levodopa on their sleep.
MATERIALS AND METHODS: Twenty-three drug naïve PD patients and 31 age-gender
matched controls were compared using the Parkinson's Disease Sleep Scale (PDSS)
and Epworth Sleepiness Scale (ESS). A polysomnogram objectively compared sleep
quality. Of the 23 patients, the 12 initiated on levodopa were reassessed
subjectively and through polysomnography after 2 months of therapy.
STATISTICAL ANALYSIS: Data was expressed as mean ± standard deviation, median,
and range. Continuous variables were analyzed by Student's T test for normally
distributed data and Mann-Whitney U test for skewed data. Discrete variables were
compared by Chi Square tests (Pearson Chi square Test or Fisher's Exact Test).
Wilcoxon signed ranks test was applied in the analysis of paired data pre- and
post-levodopa. A P value < 0.05 was considered as statistically significant.
Statistical analysis of the data was done using the Statistical Package for the
Social Sciences (SPSS) version 12.
RESULTS: Drug naïve PD patients had lower PDSS scores than controls. The sleep
architecture changes observed on polysomnogram were reduced NREM Stage III and
REM sleep and increased sleep latency and wake after sleep onset time. Following 
levodopa, improved sleep efficiency with reduced sleep latency and wake after
sleep onset time was noted, coupled with improved PDSS scores. However, NREM
Stage III and REM sleep duration did not increase.
DISCUSSION: PD patients take longer to fall asleep and have difficulty in sleep
maintenance. Sleep maintenance is affected by nocturia, REM behavioral disorder, 
nocturnal cramps, akinesia, and tremors, as observed in PDSS scores. Levodopa
improves sleep efficiency by improving motor scores without altering sleep
architecture.
CONCLUSIONS: Poor sleep quality and sleep architecture changes occur secondary to
the neurodegenerative process in PD patients. Though levodopa improves sleep
quality by reducing rigidity and tremor, it does not reverse sleep architecture
changes.

DOI: 10.4103/0972-2327.144016 
PMCID: PMC4251015
PMID: 25506163  [PubMed]

