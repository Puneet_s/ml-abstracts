
1. Sleep. 2012 Oct 1;35(10):1325-34.

GABA-to-ACh ratio in basal forebrain and cerebral cortex varies significantly
during sleep.

Vanini G(1), Lydic R, Baghdoyan HA.

Author information: 
(1)Department of Anesthesiology, University of Michigan, Ann Arbor, MI, USA.

STUDY OBJECTIVES: GABAergic and cholinergic transmission within the basal
forebrain and cerebral cortex contribute to the regulation of sleep and
wakefulness. In contrast to levels of acetylcholine (ACh), levels of endogenous
GABA in basal forebrain and cortex during sleep and wakefulness have not
previously been quantified. This study (1) tested the hypothesis that there are
differential, state-specific changes in GABA levels within the substantia
innominata (SI) region of the basal forebrain and somatosensory cortex; and (2)
quantified the ratio of GABAergic to cholinergic transmission in the SI, cortex, 
and pontine reticular formation during rapid eye movement sleep (REM), non-REM
sleep (NREM), and wakefulness.
DESIGN: Within/between subjects.
SETTING: University of Michigan.
PATIENTS OR PARTICIPANTS: Adult, male, purpose bred cats (n = 5).
INTERVENTIONS: In vivo microdialysis, high performance liquid chromatography,
electrophysiological recordings.
MEASUREMENTS AND RESULTS: In the SI, GABA levels were significantly greater
during NREM (17%) than during REM. In the cortex, GABA levels were significantly 
greater during NREM than during wakefulness (39%) and REM (63%). During prolonged
wakefulness, there was a linear increase in cortical GABA levels, and the amount 
of time spent awake accounted for 87% of the variance in GABA. The GABA-to-ACh
ratio was largest during NREM for all brain regions. REM was characterized by a
68% decrease in the GABA-to-ACh ratio across brain regions, always due to a
decrease in GABA levels.
CONCLUSION: Three of the brain regions that comprise the anatomically
distributed, sleep-generating network have in common a GABA-mediated,
sleep-dependent decrease in the GABA-to-ACh ratio.

DOI: 10.5665/sleep.2106 
PMCID: PMC3443758
PMID: 23024430  [PubMed - indexed for MEDLINE]

