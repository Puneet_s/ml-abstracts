
1. Sleep. 1979 Summer;1(4):369-91.

Effects of sleep deprivation on sleepiness, sleep intensity, and subsequent sleep
in the rat.

Friedman L, Bergmann BM, Rechtschaffen A.

The effects of 24 hr of sleep deprivation on cortical EEG and ventral hippocampus
EEG recordings, ventral hippocampus spike rates, sleep stages percentages, and
bout length measures were studied in rats. Two groups, differing only in the rate
and distance they were forced to walk during deprivation by the water wheel
method, were recorded continuously (23 hr per day) for one baseline, one
deprivation, and two recovery days. During deprivation, microsleeps, increased
hippocampal spike rates, and increased amplitude of the EEG recordings all
suggested the intrusion of sleep processes. Nonetheless, there was no evidence to
support the idea that these animals were not substantially deprived of sleep. No 
important differences were found in the recovery data of the two groups, even
though one group walked three times as far as the other during deprivation. This 
supports the idea that, in conjunction with large amounts of sleep deprivation,
changes in exercise and energy depletion may have little effect on sleep
measures. During recovery, increased hippocampal spike rates and bout lengths, as
well as increases in EEG amplitude, were interpreted in terms of increased sleep 
"intensity." High amplitude NREM sleep rebounded first, followed by rebounds in
both paradoxical sleep and low amplitude NREM sleep. This pattern was compared to
patterns previously reported for humans, cats, and rats. Finally, the tendency
for some measures to fall below their baseline levels after an initial rebound
was discussed in terms of "sleep inhibition" and servomechanism theory.


PMID: 504877  [PubMed - indexed for MEDLINE]

