
1. Neurosci Lett. 2002 Apr 19;323(1):17-20.

Non-rapid-eye-movement sleep propensity after sleep deprivation in human
subjects.

Tagaya H(1), Uchiyama M, Shibui K, Kim K, Suzuki H, Kamei Y, Okawa M.

Author information: 
(1)Department of Psychophysiology, National Institute of Mental Health, National 
Center of Neurology and Psychiatry, 1-7-3 Kohnodai, Ichikawa, 272-0827, Chiba,
Japan. hirokuni.tagaya@nifty.ne.jp

The circadian modulation of occurrence of non-rapid-eye-movement sleep (NREM) was
investigated in 37 volunteers under dim-light conditions after 24-h total sleep
deprivation using a 26-h 10/20-min ultra-short sleep-wake schedule. The
propensity of NREM showed rapid increase followed by gradual decrease during the 
subjective day and nocturnal bouts during the subjective night coinciding with
melatonin production. The mean propensity of NREM during the subjective day was
smaller than that during the subjective night, even though the sleep debt due to 
the 24-h sleep deprivation would have enhanced NREM more strongly during the
subjective day than that during the subjective night. These results suggest that 
the occurrence of NREM sleep is modulated by a circadian pacemaker.


PMID: 11911980  [PubMed - indexed for MEDLINE]

