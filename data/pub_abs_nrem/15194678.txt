
1. J Appl Physiol (1985). 2004 Oct;97(4):1343-8. Epub 2004 Jun 11.

Cerebral blood flow response to isocapnic hypoxia during slow-wave sleep and
wakefulness.

Meadows GE(1), O'Driscoll DM, Simonds AK, Morrell MJ, Corfield DR.

Author information: 
(1)Clinical and Academic Unit of Sleep and Breathing, National Heart and Lung
Institute, Imperial College, London SW3 6LY, UK.

Nocturnal hypoxia is a major pathological factor associated with
cardiorespiratory disease. During wakefulness, a decrease in arterial O2 tension 
results in a decrease in cerebral vascular tone and a consequent increase in
cerebral blood flow; however, the cerebral vascular response to hypoxia during
sleep is unknown. In the present study, we determined the cerebral vascular
reactivity to isocapnic hypoxia during wakefulness and during stage 3/4 non-rapid
eye movement (NREM) sleep. In 13 healthy individuals, left middle cerebral artery
velocity (MCAV) was measured with the use of transcranial Doppler ultrasound as
an index of cerebral blood flow. During wakefulness, in response to isocapnic
hypoxia (arterial O2 saturation -10%), the mean (+/-SE) MCAV increased by 12.9
+/- 2.2% (P < 0.001); during NREM sleep, isocapnic hypoxia was associated with a 
-7.4 +/- 1.6% reduction in MCAV (P <0.001). Mean arterial blood pressure was
unaffected by isocapnic hypoxia (P >0.05); R-R interval decreased similarly in
response to isocapnic hypoxia during wakefulness (-21.9 +/- 10.4%; P <0.001) and 
sleep (-20.5 +/- 8.5%; P <0.001). The failure of the cerebral vasculature to
react to hypoxia during sleep suggests a major state-dependent vulnerability
associated with the control of the cerebral circulation and may contribute to the
pathophysiologies of stroke and sleep apnea.

DOI: 10.1152/japplphysiol.01101.2003 
PMID: 15194678  [PubMed - indexed for MEDLINE]

