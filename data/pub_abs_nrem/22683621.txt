
1. Pharmacol Biochem Behav. 2012 Sep;102(3):450-7. doi: 10.1016/j.pbb.2012.06.002.
Epub 2012 Jun 7.

NHBA isolated from Gastrodia elata exerts sedative and hypnotic effects in sodium
pentobarbital-treated mice.

Zhang Y(1), Li M, Kang RX, Shi JG, Liu GT, Zhang JJ.

Author information: 
(1)State Key Laboratory of Bioactive Substance and Function of Natural Medicines,
Institute of Materia Medica, Chinese Academy of Medical Sciences and Peking Union
Medical College, Beijing 100050, China.

Erratum in
    Pharmacol Biochem Behav. 2012 Oct;102(4):593.

The rhizomes of Gastrodia elata have been used for the treatment of insomnia in
oriental countries. N⁶-(4-hydroxybenzyl) adenine riboside (NHBA) was originally
isolated from G. elata. For the first time we report a detailed study on the
effects and mechanisms of NHBA on its sedative and hypnotic activity. Adenosine, 
an endogenous sleep factor, regulates sleep-wake cycle via interacting with
adenosine A₁/A(2A) receptors. Using radioligand binding studies and cAMP
accumulation assays, our results show that NHBA may be a functional ligand for
the adenosine A₁ and A(2A) receptors. NHBA significantly decreases spontaneous
locomotor activity and potentiates the hypnotic effect of sodium pentobarbital in
mice. Sleep architecture analyses reveal that NHBA significantly decreases
wakefulness time and increases NREM sleep times. However, NHBA does not affect
the amount of REM sleep. Pretreatment with the adenosine A₁ receptor antagonist
DPCPX or the A(2A) receptor antagonist SCH 58261 significantly reverses the
increase in sleeping time induced by NHBA in sodium pentobarbital treated mice.
Immunohistochemical studies show that NHBA increases c-Fos expression in
GABAergic neurons of the ventrolateral preoptic area (VLPO), which suggests that 
NHBA activates the sleep center in the anterior hypothalamus. Altogether, these
results indicate that NHBA produces significant sedative and hypnotic effects.
Such effects might be mediated by the activation of adenosine A₁/A(2A) receptors 
and stimulation of the sleep center VLPO.

Copyright © 2012 Elsevier Inc. All rights reserved.

DOI: 10.1016/j.pbb.2012.06.002 
PMID: 22683621  [PubMed - indexed for MEDLINE]

