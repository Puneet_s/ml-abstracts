
1. Sleep. 1980;3(1):1-12.

Respiratory activity and sleep--wakefulness in the deafferented, paralyzed cat.

Netick A, Foutz AS.

The purpose of this study was to assess the role, if any, that peripheral
feedback plays in the distinct respiratory patterns which are characteristic of
the various states of consciousness. Those states include wakefulness, non-rapid 
eye movement (NREM) sleep, and rapid eye movement (REM) sleep. Eight adult cats, 
implanted with electrodes and skull bolts for sleep recordings and head
restraint, sustained extensive deafferentations (bilateral vagotomy and
pneumothorax, spinal transection at T-1 level, bilateral section of the phrenic
nerves), were paralyzed with gallamine, artificially ventilated (ensuring stable 
blood gases), and held at a constant temperature. Central respiratory activity
was determined by phrenic nerve recordings. During NREM sleep, respiratory
activity slowed as in intact cats. During REM sleep without phasic events,
phrenic activity did not differ from that in NREM sleep. During REM sleep with
phasic phenomena, fast and irregular "breathing" was observed. It is concluded
that states of consciousness have a direct effect on central respiratory
activity. Possible mechanisms for this effect are discussed.


PMID: 6936747  [PubMed - indexed for MEDLINE]

