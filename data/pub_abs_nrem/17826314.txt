
1. Sleep Med. 2007 Dec;9(1):33-41. Epub 2007 Sep 7.

Non-REM sleep instability in patients with major depressive disorder: subjective 
improvement and improvement of non-REM sleep instability with treatment
(Agomelatine).

Lopes MC(1), Quera-Salva MA, Guilleminault C.

Author information: 
(1)Stanford University Sleep Medicine Program, Sleep Disorders Clinic, Garches,
France.

OBJECTIVE: To assess the importance of non-rapid eye movement (NREM) sleep
disturbance in major depressive disorder (MDD) patients using cyclic alternating 
pattern (CAP) analysis, and to determine the usefulness of CAP analysis in
evaluating treatment effect.
METHODS: Baseline sleep-staging data and CAP analysis of NREM sleep was compared 
in 15 MDD patients (Hamilton depression scale score>20) and normal controls.
Longitudinal evaluation of sleep changes using similar analysis during a
treatment trial was also performed.
ANALYSIS: A single-blinded researcher scored and analyzed the sleep of MDD and
age-matched normal controls at baseline and during a treatment trial using the
international scoring system as well as CAP analysis.
RESULTS: MDD patients had evidence of disturbed sleep with both analyses, but CAP
analysis revealed more important changes in NREM sleep of MDD patients at
baseline than did conventional sleep staging. There was a significant decrease in
CAP rate, time, and cycle and disturbances of phase A subtype of CAP. NREM
abnormalities, observed by CAP analysis, during the treatment trial paralleled
subjective responses. Analysis of subtype A phase of CAP demonstrated better
sleep improvement.
CONCLUSION: CAP analysis demonstrated the presence of more important NREM sleep
disturbances in MDD patients than did conventional sleep staging, suggesting the 
involvement of slow wave sleep (SWS) in the sleep impairment of MDD patients.
Improvement of NREM sleep paralleled subjective mood improvement and preceded REM
sleep improvement. CAP analysis allowed objective investigation of the effect of 
treatment on sleep disturbances.

DOI: 10.1016/j.sleep.2007.01.011 
PMID: 17826314  [PubMed - indexed for MEDLINE]

