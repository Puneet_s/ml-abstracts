
1. Brain Res. 1997 Mar 28;752(1-2):81-9.

Thermosensitive neurons of the diagonal band in rats: relation to wakefulness and
non-rapid eye movement sleep.

Alam MN(1), McGinty D, Szymusiak R.

Author information: 
(1)Department of Veterans Affairs Medical Center, Sepulveda, CA 91343, USA.

The thermosensitivity of the neurons in the diagonal band of Broca (DBB) was
studied in 12 freely moving rats by determining responses to local cooling or
warming with a water perfused thermode. Of 151 neurons studied, 37 (25%) neurons 
met the criterion for thermosensitivity including 17 warm-sensitive (WSNs) and 20
cold-sensitive neurons (CSNs). The spontaneous discharge rates of WSNs and CSNs
were recorded through 1-3 sleep-waking cycles. The discharge of WSNs and CSNs
during waking and non-rapid eye movement (NREM) sleep were different. Of 17 WSNs,
10 exhibited increased discharge rates during NREM sleep as compared with waking 
(NREM/Wake discharge ratio, > 1.2). Of 20 CSNs, 14 discharged more slowly during 
NREM sleep as compared with waking (NREM/Wake discharge ratio, < 0.8). In both
WSNs and CSNs, changes in discharge rate preceded EEG changes at the waking-NREM 
transition. These results support a hypothesis that the activation of
sleep-related WSNs and the deactivation of wake-related CSNs play a role in the
onset and regulation of NREM sleep.


PMID: 9106443  [PubMed - indexed for MEDLINE]

