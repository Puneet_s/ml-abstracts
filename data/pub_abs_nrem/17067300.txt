
1. Eur J Neurosci. 2006 Oct;24(7):2039-48.

REM sleep changes in rats induced by siRNA-mediated orexin knockdown.

Chen L(1), Thakkar MM, Winston S, Bolortuya Y, Basheer R, McCarley RW.

Author information: 
(1)Department of Psychiatry, Harvard Medical School, Boston VA Healthcare System,
940 Belmont Street, Brockton, MA 02301, USA, and Department of Neurology, Harry
Truman Memorial VA Hospital, Columbia, MO 65203, USA.

Short interfering RNAs (siRNA) targeting prepro-orexin mRNA were microinjected
into the rat perifornical hypothalamus. Prepro-orexin siRNA-treated rats had a
significant (59%) reduction in prepro-orexin mRNA compared to scrambled
siRNA-treated rats 2 days postinjection, whereas prodynorphin mRNA was
unaffected. The number of orexin-A-positive neurons on the siRNA-treated side
decreased significantly (23%) as compared to the contralateral control (scrambled
siRNA-treated) side. Neither the colocalized dynorphin nor the neighbouring
melanin-concentrating hormone neurons were affected. The number of
orexin-A-positive neurons on the siRNA-treated side did not differ from the
number on the control side 4 or 6 days postinjection. Behaviourally, there was a 
persistent (approximately 60%) increase in the amount of time spent in rapid eye 
movement (REM) sleep during the dark (active) period for 4 nights postinjection, 
in rats treated with prepro-orexin siRNA bilaterally. This increase occurred
mainly because of an increased number of REM episodes and decrease in REM-to-REM 
interval. Cataplexy-like episodes were also observed in some of these animals.
Wakefulness and NREM sleep were unaffected. The siRNA-induced increase in REM
sleep during the dark cycle reverted to control values on the 5th day
postinjection. In contrast, the scrambled siRNA-treated animals only had a
transient increase in REM sleep for the first postinjection night. Our results
indicate that siRNA can be usefully employed in behavioural studies to complement
other loss-of-function approaches. Moreover, these data suggest that the orexin
system plays a role in the diurnal gating of REM sleep.

DOI: 10.1111/j.1460-9568.2006.05058.x 
PMCID: PMC2394504
PMID: 17067300  [PubMed - indexed for MEDLINE]

