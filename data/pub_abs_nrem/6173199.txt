
1. Electroencephalogr Clin Neurophysiol. 1982 Jan;53(1):36-47.

Daytime sleep stage organization in three-month-old infants.

Crowell DH, Kapuniai LE, Boychuk RB, Light MJ, Hodgman JE.

Sleep monitoring at 3 months post term of clinically normal term infants,
pre-term infants with no demonstrable pathologic clinical signs, and pre-term and
term infants with a history of metabolic disorders show that these infants have
EEG sleep stages resembling those seen in adults, as well as the adult pattern of
sleep stage organization. The presence of NREM stage organization and stage
sequencing suggest that sleep regulatory mechanisms are approaching a level of
functional maturity in the human infant at 3 months post term. There is a
significant relationship between sleep staging at 3 months post term and mental
and motor performance at 12 months post term. In light of this, it is
hypothesized that early bioelectric maturation may reflect the development of
neural mechanisms which are also the substrate for later cognitive and behavioral
functioning. Sleep stage organization at 3 months post term may be utilized as a 
benchmark of CNS development and for research on the pathophysiology of sleep
disorders.


PMID: 6173199  [PubMed - indexed for MEDLINE]

