
1. Sleep. 1979 Summer;1(4):357-67.

Motoneuronal excitability during wakefulness and non-REM sleep: H-reflex recovery
function in man.

Pivik RT, Mercier L.

Comparisons were made in 13 normal young adults of alpha motoneuronal
excitability during non-rapid-eye-movement (NREM) sleep (stages 2--4) and
wakefulness based on a study of the recovery cycle of the H-reflex after a
conditioning stimulus. During sleep, excitability was generally decreased and a
peak of secondary facilitation, which characteristically occurs 100--300 msec
after the conditioning stimulus during wakefulness, was reduced or absent. A
variety of mechanisms, including long-loop reflexes and afferent discharges from 
reflex muscle contraction and cutaneous fibers, has been proposed to account for 
this phase of facilitation in the waking recovery curve. However, since studies
to date indicate the absence of tonic presynaptic or postsynpatic inhibitory
influences acting on the monosynatpic reflex pathway during NREM sleep, but
report tonic reduction in fusimotor activity at this time, it is suggested that a
primary factor underlying the absence of facilitation during sleep is a reduction
in central excitability deriving from depressed fusimotor activity.


PMID: 504876  [PubMed - indexed for MEDLINE]

