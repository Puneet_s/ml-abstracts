
1. Front Neurol. 2012 Jul 11;3:109. doi: 10.3389/fneur.2012.00109. eCollection 2012.

The effects of pre-sleep learning on sleep continuity, stability, and
organization in elderly individuals.

Conte F(1), Carobbi G, Errico BM, Ficca G.

Author information: 
(1)Department of Psychology, University of Naples II Caserta, Italy.

Several studies have consistently shown that pre-sleep learning is associated to 
changes of sleep structure. Whereas previous research has mainly focused on sleep
states, namely REM and NREM amount, very little attention has been paid to the
hypothesis that pre-sleep learning might improve sleep continuity, stability, and
cyclic organization, which are often impaired in aging. Thus, aim of this
research was to assess, in a sample of 18 healthy elderly subjects, whether a
memory task administered at bedtime would determine changes in any sleep
parameter, with special regard to sleep continuity, stability, and organization. 
To this purpose, a baseline sleep (BL), i.e., a normal sleep with 9-h time in bed
(TIB), was compared to a post-training sleep (TR), with the same TIB but preceded
by an intensive training session. For the latter, a verbal declarative task was
used, consisting in learning paired-word lists, rehearsed, and recalled for three
times in a row. To control for individual learning abilities, subjects were
administered several sets of lists with increasing difficulty, until they reached
an error rate ≥20% at third recall. Relative to BL, TR shows a significant
reduction in the frequency of brief awakenings, arousals, state transitions,
"functional uncertainty" (FU) periods, and in the percentage of time in FU over
total sleep time (TST). A significant increase in the number of complete cycles, 
total cycle time (TCT), and TCT/TST proportion was also found. All these changes 
are evenly distributed over the sleep episode. No sleep stage measure display
significant changes, apart from a slight reduction in the percentage of Stage 1. 
Scores at retest are negatively correlated with both the frequency of arousals
and of state transitions. Our data suggest that pre-sleep learning can yield a
beneficial re-organizing effect on elderlies' sleep quality. The inverse
correlation between recall scores and the measures of sleep continuity and
stability provides further support to the role of these features in memory
processes.

DOI: 10.3389/fneur.2012.00109 
PMCID: PMC3394199
PMID: 22798956  [PubMed]

