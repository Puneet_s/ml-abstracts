
1. Sleep. 1991 Feb;14(1):48-55.

Real-time automated sleep scoring: validation of a microcomputer-based system for
mice.

Van Gelder RN(1), Edgar DM, Dement WC.

Author information: 
(1)Sleep Disorders and Research Center, Stanford University School of Medicine,
California 94304.

Long-term circadian studies of sleep and wakefulness in rodents have been
hindered by the labor required to analyze long polygraph records. To expedite
such studies, we have designed and implemented SCORE, a microcomputer-based
real-time sleep scoring system for rodents. The electroencephalograph is
digitized in 10-s epochs at 100 Hz. Frequency and amplitude information from the 
waveform are extracted into a 48-dimension vector that is then compared to
previously taught vectors representing the canonical features of four arousal
states: wakefulness, theta-dominated wakefulness, rapid eye movement (REM) sleep,
and nonREM (NREM) sleep. Match values are assigned for each state to each epoch; 
after excluding states based on wheel-running or drinking activity data, the
nonexcluded state with the best match value for the epoch is scored. Analysis of 
over 23,000 epochs for four mice yielded an overall agreement of 94.0% between
two human scorers and the program, compared with a 94.5% agreement between the
two human scorers. The SCORE algorithm matched the human concensus best for
wakefulness (97.8%) and NREM sleep (94.7%), but was lower for REM sleep (75.2%)
and theta-dominated wakefulness (83.3%). Most errors in scoring of REM sleep were
in close temporal proximity to human-scored REM epochs. SCORE is capable of
scoring arousal states for eight animals simultaneously in real time on a
standard IBM PC equipped with a commercially available analog-to-digital
conversion board, and should considerably facilitate the performance of long-term
studies of sleep and wakefulness in the rodent.


PMID: 1811319  [PubMed - indexed for MEDLINE]

