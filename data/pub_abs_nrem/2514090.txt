
1. EEG EMG Z Elektroenzephalogr Elektromyogr Verwandte Geb. 1989 Dec;20(4):302-9.

[Spectral power density and coherence in sleep EEG in patients with acquired
immunodeficiency syndrome].

[Article in German]

Terstegge K(1), Henkes H, Kubicki S, Scholz G, Hansen ML, Ruf B, Müller R.

Author information: 
(1)Abteilung für Klinische Neurophysiologie, Frien Universität Berlin.

15 male AIDS-patients from 26 to 55 years (mean 41.8 +/- 8.5) with various
cerebral manifestations had a whole-night-sleep-EEG registration. As control the 
recordings of 15 age-matched volunteers (26-55 years, mean 41.8 +/- 9.8) were
examined. Spectral characteristics of elementary EEG-epochs of 40 s length were
computed, and sleep staging was performed visually for these intervals. The
spectral power density of eight EEG-derivations (left and right frontopolar,
frontal, central and occipital electrodes, reference montage to the ipsilateral
Cb) were measured (sampling rate 64(-1) s, spectral resolution .25 Hz, frequency 
range from .25 to 24 Hz). Interhemispherical coherences of the frontal and
occipital derivation pairs, and intrahemispherical fronto-occipital coherences of
the left and right hemisphere, were computed. In the patients the frontal power
density of NREM sleep showed lower values in the frequency range of 10 to 14 Hz. 
In central and in occipital derivations the power density between 12.5 and 15 Hz 
was lower in the patients, but the difference was less accentuated. The spectral 
power density of REM sleep showed similar characteristics in both groups. The
interhemispherical frontal coherence of the whole frequency range below 13 Hz was
markedly lower in the patient group. This was true for the NREM sleep, and,
slightly less, for the REM sleep, too. The interoccipital spectral coherence was 
generally slightly lower in the patient group; the difference was most clearly in
the 12.5 to 15 Hz range of NREM sleep.


PMID: 2514090  [PubMed - indexed for MEDLINE]

