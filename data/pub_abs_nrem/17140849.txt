
1. Clin Neurophysiol. 2007 Feb;118(2):438-48. Epub 2006 Nov 30.

Heart rate and spectral EEG changes accompanying periodic and non-periodic leg
movements during sleep.

Ferri R(1), Zucconi M, Rundo F, Spruyt K, Manconi M, Ferini-Strambi L.

Author information: 
(1)Sleep Research Centre, Department of Neurology I.C., Oasi Institute (IRCCS),
94018 Troina, Italy. rferri@oasi.en.it

OBJECTIVE: To evaluate the changes in heart rate (HR) and EEG spectra
accompanying periodic (PLM) and non-periodic leg movements (NPLM) during sleep in
patients with restless legs syndrome (RLS).
METHODS: Sixteen patients with RLS underwent one polysomnographic night
recording; leg movements (LMs) during sleep were detected and classified as PLM
or NPLM; up to 10 PLM and NPLM were chosen from NREM and REM sleep, for each
patient and for each type (mono- or bilateral). EEG spectral analysis and HR were
evaluated for 20s preceding and 30s following the onset of each LM.
RESULTS: EEG activation preceded LMs, particularly in the delta band which
increased before the other frequency bands, in NREM sleep but not in REM sleep
for PLM, and in both stages for NPLM. A similar difference was seen between mono-
and bilateral LMs.
CONCLUSIONS: Sleep EEG, HR, and leg motor activity seems to be modulated by a
complex dynamically interacting system of cortical and subcortical mechanisms,
which influence each other.
SIGNIFICANCE: Future studies on the clinical significance of leg motor events
during sleep need to take into account events classifiable as "isolated" and to
integrate the autonomic and EEG changes accompanying them.

DOI: 10.1016/j.clinph.2006.10.007 
PMID: 17140849  [PubMed - indexed for MEDLINE]

