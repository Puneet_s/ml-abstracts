
1. Electroencephalogr Clin Neurophysiol. 1987 Feb;66(2):175-84.

Evolution of sleep structure following brief intervals of wakefulness.

Campbell SS.

Structural components of sleep following brief waking intervals were examined.
Following an uninterrupted baseline night, 10 subjects were awakened on the next 
5 nights for either 20, 40, 60, 90 or 120 min. All awakenings occurred at 02.30
h. During waking intervals subjects sat quietly in an illuminated room. Following
each waking interval subjects returned to bed and slept uninterrupted until the
next morning. Such awakenings had the effect of increasing the mean duration of
the first SWS episode following return to sleep, and increasing the mean duration
of first and second REM episodes following return to sleep. Sleep stage
sequencing was also affected, with REM sleep preceding the first epoch of SWS in 
38% of the sleep episodes following return to sleep. Eye movement density was
essentially unaffected by controlled awakenings and timing of the REM/NREM cycle 
bridging awakenings appeared to be sleep dependent.


PMID: 2431882  [PubMed - indexed for MEDLINE]

