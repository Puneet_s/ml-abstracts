
1. Zhonghua Er Bi Yan Hou Tou Jing Wai Ke Za Zhi. 2010 Feb;45(2):105-10.

[Rapid eye movement-related and none rapid eye movement-related classification in
obstructive sleep apnea hypopnea syndrome].

[Article in Chinese]

Chai LP(1), Xie X, Zeng YH, Wang ZF, Tu XP.

Author information: 
(1)Department of Otorhinolaryngology, The First Affiliated Hospital of SUN
Yat-sen University, Otorhinolaryngology Institute of SUN Yat-sen University,
Guangzhou 510080, China. banana1212@163.com

OBJECTIVE: To study the value of a new measurement that divided obstructive sleep
apnea-hypopnea syndrome (OSAHS) into rapid-eye-movement (REM) related and
non-rapid-eye-movement (NREM) related subgroups.
METHODS: According to Siddiqui classification, 137 adult patients with OSHAS were
diagnosed as REM-related OSAHS [REM apnea hypopnea index (AHI)/NREM AHI > 1] or
NREM-related OSAHS (REM AHI/NREM AHI < 1). Polysomnographic data were compared
and discussed.
RESULTS: (1) There were 72 cases defined as REM-related OSAHS (52.6%) and 65
cases defined as NREM-related OSAHS (47.4%). (2) In all cases, total AHI and NREM
AHI in REM-related OSAHS were significantly lower than those in NREM-related
OSAHS, while lowest arterial oxygen saturation (LSaO₂), REM LSaO₂ and NREM LSaO₂ 
were significantly higher than those in NREM-related OSAHS (t were -6.466,
-7.638, 3.426, 2.472, 4.873 respectively, P < 0.05). No significance was found in
sleep structure, REM AHI and REM LSaO₂ between REM-related and NREM-related OSAHS
(P > 0.05). (3) Given the severity of OSHAS, the constituent ratio of REM-related
OSAHS decreased (77.8%, 61.5%, 37.3%) from mild to severe OSAHS, while that of
NREM-related OSAHS rose (22.7%, 38.5%, 62.7%; chi² = 16.996, P < 0.01). In mild
and moderate groups, REM LSaO₂ of REM-related OSAHS was significantly lower than 
those in NREM-related OSAHS (t were -4.273 and -2.136, P < 0.05), while the
differences of total AHI and LSaO₂, NREM LSaO₂ between these two types were not
significant. In severe group, AHI in NREM-related OSAHS was significantly higher 
than that in REM-related OSAHS, while LSaO₂, REM LSaO₂ and NREM LSaO₂ was
significantly lower than those in REM-related OASHS (t were -4.943, 2.574, 1.996,
3.571, P ≤ 0.05). (4) There was no significance in sleeping latency and
efficiency between REM-related and NREM-related OSHAS.
CONCLUSIONS: REM-related OSHAS mainly exists in mild and moderate OSHAS, while
NREM-related one mainly exists in severe OSHAS. NREM-related OSAHS may be more
severe in AHI and hypoxia than REM-related one. Whenever obstructive apnea
happened in REM or NREM period, its impacts on sleep structure, efficiency and
latency have no difference.


PMID: 20398503  [PubMed - indexed for MEDLINE]

