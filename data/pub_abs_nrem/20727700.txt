
1. Brain Dev. 2011 May;33(5):421-7. doi: 10.1016/j.braindev.2010.07.008. Epub 2010
Aug 19.

Sleep cyclic alternating pattern analysis in healthy children during the first
year of life: a daytime polysomnographic study.

Miano S(1), Peraita-Adrados R, Montesano M, Castaldo R, Forlani M, Villa MP.

Author information: 
(1)Department of Pediatrics, Sleep Disorder Centre, University of Rome La
Sapienza-Sant'Andrea Hospital, Rome, Italy.

We evaluated the cyclic alternating pattern (CAP) during the first year of life
in order to obtain information on the maturation of arousal mechanisms during
NREM sleep and to provide normative data for CAP parameters in this age range
(5-16months). Eleven healthy children (mean age 7.9±3.3months, seven boys) were
studied while they slept in the morning. They underwent a 3-h
video-EEG-polysomnographic recording at the Pediatric Sleep Unit of Sant'Andrea
Hospital in Rome, Italy. Sleep was scored visually for sleep architecture and CAP
analysis using standard criteria. Our results were complemented by CAP data from 
a previous sample of healthy infants (2-4months), studied when they slept during 
the morning, in order to correlate CAP parameters with age. The total sample
comprised 24 children. The sleep period was approximately 2h, with a first REM
latency of about 30min, and a clear distinction between stages N1, N2, and N3.
The arousal index was 12±2.1 events/hour of sleep. The total CAP rate was
23.7±7.6%, and it increased progressively with the deepness of sleep; the highest
values were observed during stage N3 and the lowest values during stage N1. A1
phases were the most numerous (78.2%), followed by A2 (14%) and A3 (7.7%) phases.
The A1 index was higher than the A2 and A3 indices, whereas the mean duration of 
B was higher than that of A. The correlation showed that the CAP rate, A1, A2, A3
indices, A2, A3 percentages, and the average duration of B increased with age,
whereas the A1 percentage decreased. We provide the first data on CAP analysis in
children aged 5-16months, studied when they slept during the morning. Our results
confirm the trend toward an increase in CAP rate during the first year of life.
In addition, we observed a progressive increase in CAP rate with deepness of
sleep, and with age, reflecting maturation of slow-wave activity. The decreased
percentage of A1 subtypes may reflect the maturation of arousability.

Copyright © 2010 The Japanese Society of Child Neurology. Published by Elsevier
B.V. All rights reserved.

DOI: 10.1016/j.braindev.2010.07.008 
PMID: 20727700  [PubMed - indexed for MEDLINE]

