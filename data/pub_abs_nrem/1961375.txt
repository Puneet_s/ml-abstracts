
1. Neurol Neurochir Pol. 1991 May-Jun;25(3):301-6.

[Electrophysiological recording of nocturnal sleep and the multiple sleep latency
test in obstructive sleep apnea syndrome].

[Article in Polish]

Brzecka A(1), Rudkowska-Brzecka A.

Author information: 
(1)I Kliniki Chorób Płuc.

Polysomnographic studies were performed in 6 patients with obstructive sleep
apnoea syndrome (OSA). The sleep study consisted of: electroencephalography,
electromyography, electrooculography, electrocardiography, pulse oximetry and
observation of respiration. During day multiple sleep latency tests were
performed. In all patients fragmentation of sleep with prevalent stages 1. and 2.
of NREM and occasionally deep sleep and REM phase were observed. Concomitantly
with the appearance of electrophysiologic sleep stages the muscle tone decreased 
and episodes of obstructive apnoea occurred. The periods of sleep and apnoea
alternated with wakefulness and breathing. In MSLF the mean latency was 3 +/- 2
min. In OSA syndrome episodes of obstructive sleep apnoea cause sleep
fragmentation and prevalence of light sleep stages. Excessive daytime somnolence 
observed in this syndrome is caused by sleep disturbances. MSLT demonstrated
pathologic hypersomnolence in OSA syndrome.


PMID: 1961375  [PubMed - indexed for MEDLINE]

