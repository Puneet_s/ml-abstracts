
1. J Appl Physiol (1985). 1997 Dec;83(6):1954-61.

Cardiopulmonary control in sleeping Sprague-Dawley rats treated with hydralazine.

Carley DW(1), Trbovic SM, Bozanich A, Radulovacki M.

Author information: 
(1)Section of Respiratory and Critical Care Medicine, University of Illinois
College of Medicine at Chicago, Chicago, Illinois 60612, USA.

To test the hypothesis that hydralazine can suppress spontaneous sleep-related
central apnea, respiratory pattern, blood pressure, and heart period were
monitored in Sprague-Dawley rats. In random order and on separate days, rats were
recorded after intraperitoneal injection of 1) saline or 2) 2 mg/kg hydralazine. 
Normalized minute ventilation (NVI) declined significantly with transitions from 
wake to non-rapid-eye-movement (NREM) sleep (-5.1%; P = 0.01) and
rapid-eye-movement (REM) sleep (-4.2%; P = 0.022). Hydralazine stimulated
respiration (NVI increased by 21%; P < 0.03) and eliminated the effect of state
on NVI. Blood pressure decreased by 17% after hydralazine, and the correlation
between fluctuations in mean blood pressure and NVI changed from strongly
positive during control recordings to weakly negative after hydralazine (P <
0.0001 for each). Postsigh and spontaneous apneas were reduced during NREM and
REM sleep after hydralazine (P < 0.05 for each). This suppression was strongly
correlated with the reduction in blood pressure and with the degree of
respiratory stimulation. We conclude that mild hydralazine-induced hypotension
leads to respiratory stimulation and apnea suppression.


PMID: 9390968  [PubMed - indexed for MEDLINE]

