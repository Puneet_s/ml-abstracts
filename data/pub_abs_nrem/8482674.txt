
1. J Appl Physiol (1985). 1993 Mar;74(3):1325-36.

Effect of inspiratory muscle unloading on arousal responses to CO2 and hypoxia in
sleeping dogs.

Kimoff RJ(1), Kozar LF, Yasuma F, Bradley TD, Phillipson EA.

Author information: 
(1)Department of Medicine, University of Toronto, Ontario, Canada.

Chemical respiratory stimuli can induce arousal from sleep, but the specific
mechanisms involved have not been established. Therefore, we tested the
hypothesis that mechanoreceptor stimuli arising in the ventilatory apparatus have
a role in the arousal responses to progressive hypercapnia and hypoxia by
comparing arousal responses during spontaneous ventilation with those obtained
when the inspiratory muscles were unloaded by mechanical ventilatory assistance. 
Studies were performed in three trained dogs in which the adequacy of inspiratory
muscle unloading was verified by diaphragmatic electromyographic (EMG)
recordings. In rapid-eye-movement (REM) sleep the arousal threshold during
progressive hypercapnia increased from 68.4 +/- 0.5 (SE) mmHg during spontaneous 
runs to 72.3 +/- 0.8 mmHg during mechanically assisted runs (P < 0.01). In
contrast there were no changes in arousal responses to hypercapnia during non-REM
(NREM) sleep or to hypoxia in either NREM or REM sleep. However, during the
assisted hypoxic runs, EMG activity of the transversus abdominis muscle was
increased compared with the unassisted runs; therefore, the effects on arousal
threshold of unloading the inspiratory muscles may have been offset by increased 
loading of the expiratory muscles. The findings indicate that even in the absence
of added mechanical loads, mechanoreceptor stimuli probably arising in the
respiratory muscles contribute to the arousal response to hypercapnia during REM 
sleep.


PMID: 8482674  [PubMed - indexed for MEDLINE]

