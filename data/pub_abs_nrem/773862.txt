
1. Int J Psychiatry Med. 1975;6(1-2):43-62.

Nocturnal psychophysiological correlates of somatic conditions and sleep
disorders.

Kales JD, Kales A.

Modern sleep research studies have provided the practicing physician with
considerable new information concerning the basic psychophysiology of sleep, the 
effects of medical conditions on sleep and the role of maturational and emotional
factors in producing certain sleep disorders. Medical and psychiatric disorders, 
sleep disorders and drug-induced sleep stage alterations are studied in the sleep
laboratory using the same techniques developed to analyze sleep patterns in
normal subjects. After initial sleep laboratory adaptation, a profile of the
sleep characteristics of various clinical conditions is obtained. This profile
can be compared to sleep profiles of normal subjects as well as to the effects on
sleep of subsequent experimental or therapeutic procedures. Various studies have 
shown that coronary artery, duodenal ulcer and nocturnal headache patients
experience angina, increased gastric acid secretion and migraine or cluster
headaches, respectively during REM sleep. Adult nocturnal asthamtic episodes
occur out of all sleep stages while attacks of dyspnea in asthmatic children
occur in all stages except stage 4 sleep. Hypothyroid patients show decreases in 
stages 3 and 4 sleep, while in hyperthyroid patients the percentage of time spent
in stages 3 and 4 sleep is markedly increased. Enuretic episodes occur
predominantly in non-rapid eye movement (NREM) sleep. Sleepwalking and night
terror episodes occur exclusively out of NREM sleep, particularly from stages 3
and 4 sleep. Most child somnambulists and children with night terrors "outgrow"
this disorder, suggesting a delayed maturation of the central nervous system.
Stimulant drugs are effective in the treatment of the sleep attacks of narcolepsy
and in treating certain cases of hypersomnia, while imipramine is an effective
treatment for the auxillary symptoms of narcolepsy. Psychological disturbances
are frequent in adult somnambulism and night terrors as well as in hypersomnia
and insomnia. Proper pharmacologic treatment to provide symptomatic relief for
insomnia is recommended to enhance the psychotherapeutic process.


PMID: 773862  [PubMed - indexed for MEDLINE]

