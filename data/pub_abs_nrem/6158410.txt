
1. Electroencephalogr Clin Neurophysiol. 1980 Aug;49(3-4):337-44.

Nocturnal sleep in severely mentally retarded children: abnormal EEG patterns in 
sleep cycle.

Shibagaki M, Kiyono S, Watanabe K.

Sleep EEG patterns in 43 mentally retarded children (from 4 months to 80 years of
age) were studied throughout nocturnal sleep and the following results were
obtained. (1) Twenty-two cases evidenced normal sleep patterns that could be
classified into 6 stages. (2) The 21 other cases showed some abnormal sleep EEG
patterns as follows: (a) absence of sleep spindles (n = 18) which included cases 
of high voltage fast activity (n = 2) and cases of low voltage activity
throughout nocturnal sleep (n = 3); (b) indistinguishable delta and theta
activities (n = 1), extreme spindle-like pattern (n = 1), absence of REM sleep (n
= 1). (3) The severely mentally retarded children under 18 months had definitely 
decreased spindle activity in comparison to the values obtained by other authors 
from normal children. (4) The majority of the abnormal sleep EEG patterns
occurred during light sleep. (5) A significant decrease in DQ was found in
children with abnormal sleep patterns throughout NREM sleep as compared with
those in whom abnormal EEGs occurred only during stages 1 and 2. (6) It was
concluded that the EEG recorded in nocturnal sleep may serve as a useful
indicator of abnormality in mentally retarded children.


PMID: 6158410  [PubMed - indexed for MEDLINE]

