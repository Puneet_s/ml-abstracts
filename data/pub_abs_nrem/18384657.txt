
1. Evolution. 2008 Jul;62(7):1764-76. doi: 10.1111/j.1558-5646.2008.00392.x.

Phylogenetic analysis of the ecology and evolution of mammalian sleep.

Capellini I(1), Barton RA, McNamara P, Preston BT, Nunn CL.

Author information: 
(1)Evolutionary Anthropology Research Group, Department of Anthropology, Durham
University, DH1 3HN Durham, UK. Isabella.Capellini@durham.ac.uk

The amount of time asleep varies greatly in mammals, from 3 h in the donkey to 20
h in the armadillo. Previous comparative studies have suggested several
functional explanations for interspecific variation in both the total time spent 
asleep and in rapid-eye movement (REM) or "quiet" (non-REM) sleep. In support of 
specific functional benefits of sleep, these studies reported correlations
between time in specific sleep states (NREM or REM) and brain size, metabolic
rate, and developmental variables. Here we show that estimates of sleep duration 
are significantly influenced by the laboratory conditions under which data are
collected and that, when analyses are limited to data collected under more
standardized procedures, traditional functional explanations for interspecific
variation in sleep durations are no longer supported. Specifically, we find that 
basal metabolic rate correlates negatively rather than positively with sleep
quotas, and that neither adult nor neonatal brain mass correlates positively with
REM or NREM sleep times. These results contradict hypotheses that invoke energy
conservation, cognition, and development as drivers of sleep variation. Instead, 
the negative correlations of both sleep states with basal metabolic rate and diet
are consistent with trade-offs between sleep and foraging time. In terms of
predation risk, both REM and NREM sleep quotas are reduced when animals sleep in 
more exposed sites, whereas species that sleep socially sleep less. Together with
the fact that REM and NREM sleep quotas correlate strongly with each other, these
results suggest that variation in sleep primarily reflects ecological constraints
acting on total sleep time, rather than the independent responses of each sleep
state to specific selection pressures. We propose that, within this ecological
framework, interspecific variation in sleep duration might be compensated by
variation in the physiological intensity of sleep.

DOI: 10.1111/j.1558-5646.2008.00392.x 
PMCID: PMC2674385
PMID: 18384657  [PubMed - indexed for MEDLINE]

