
1. Biol Psychiatry. 1986 Jul;21(8-9):710-6.

Sleep disturbance produced by electrical stimulation of the locus coeruleus in a 
human subject.

Kaitin KI, Bliwise DL, Gleason C, Nino-Murcia G, Dement WC, Libet B.

A 25-year-old man with a chronically implanted stimulating electrode placed in
the region of the locus coeruleus (LC) was monitored for 5 nights in a sleep
laboratory to study the role of the LC in sleep. Sleep patterns were compared
between the 2 nights in which the stimulation was applied periodically every 90
min and the 2 nights in which no stimulation was applied. In contrast to the
normal sleep patterns that occurred during the 2 nonstimulation nights,
electrical stimulation of the LC produced a profound disruption of sleep and
significant reductions in the total amounts of NREM sleep, REM sleep, REM sleep
as a percent of total sleep (NREM + REM sleep), and total sleep. Results suggest 
that the LC has a role in maintaining normal sleep patterns.


PMID: 3730455  [PubMed - indexed for MEDLINE]

