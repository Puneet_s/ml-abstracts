
1. Respir Physiol Neurobiol. 2008 Mar 20;161(1):23-8. doi:
10.1016/j.resp.2007.11.008. Epub 2007 Dec 23.

Expression of TASK-1 in brainstem and the occurrence of central sleep apnea in
rats.

Wang J(1), Zhang C, Li N, Su L, Wang G.

Author information: 
(1)Department of Pulmonary Medicine, Peking University First Hospital, Beijing
100034, China.

Recent studies revealed that unstable ventilation control is one of mechanisms
underlying the occurrence of sleep apnea. Thus, we investigated whether TASK-1,
an acid-sensitive potassium channel, plays a role in the occurrence of sleep
apnea. First, the expression of TASK-1 transcriptions on brainstem was checked by
in situ hybridization. Then, the correlation between the central apneic episodes 
and protein contents of TASK-1 measured by western blot was analyzed from 27 male
rats. Results showed that TASK-1 mRNAs were widely distributed on the putative
central chemoreceptors such as locus coeruleus, nucleus tractus solitarius and
medullary raphe, etc. Both the total spontaneous apnea index (TSAI) and
spontaneous apnea index in NREM sleep (NSAI) were positively correlated with
TASK-1 protein contents (r=0.547 and 0.601, respectively, p<0.01). However, the
post-sigh sleep apnea index (PAI) had no relationship with TASK-1 protein. Thus, 
we concluded that TASK-1 channels may function as central chemoreceptors that
play a role in spontaneous sleep apneas in rats.

DOI: 10.1016/j.resp.2007.11.008 
PMID: 18272437  [PubMed - indexed for MEDLINE]

