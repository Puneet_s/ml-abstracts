
1. Epilepsia. 2000 Feb;41(2):128-31.

Effects of vigabatrin on sleep-wakefulness cycle in amygdala-kindled rats.

Raol YH(1), Meti BL.

Author information: 
(1)Department of Neurophysiology, National Institute of Mental Health and Neuro
Sciences, Bangalore, India.

PURPOSE: Our aim was to study the effect of prolonged administration of
vigabatrin (VGB) on sleep-wakefulness cycle in kindled seizure-induced rats.
METHODS: Adult male Wistar rats were implanted stereotaxically with electrodes
for kindling and polysomnography. The rats were divided into two groups, kindled 
and VGB-treated kindled rats. VGB was administered intraperitonially every day
for 21 days, and polysomnographic recordings were taken after doses 1, 7, 14, and
21. The drug effects were evaluated by comparing the records of kindled and
drug-treated kindled rats.
RESULTS: The VGB-administered kindled rats showed an increase in total sleep time
(TST) due to an increase in total non-rapid eye movement (NREM) and light
slow-wave sleep stage I (SI) with a decrease in wakefulness. The number of
episodes and REM onset latencies were found to be decreased after drug treatment.
CONCLUSIONS: It can therefore be concluded that VGB has a somnolence-inducing
effect and that it might mediate its anticonvulsant effect by altering sleep
architecture through sleep-regulating areas.


PMID: 10691108  [PubMed - indexed for MEDLINE]

