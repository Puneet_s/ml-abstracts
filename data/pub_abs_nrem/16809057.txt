
1. Sleep Med Rev. 2006 Aug;10(4):267-85. Epub 2006 Jun 30.

CAP, epilepsy and motor events during sleep: the unifying role of arousal.

Parrino L(1), Halasz P, Tassinari CA, Terzano MG.

Author information: 
(1)Sleep Disorders Center, Department of Neuroscience, University of Parma, Via
Gramsci, 14, 43100 Parma, Italy.

Arousal systems play a topical neurophysiologic role in protecting and tailoring 
sleep duration and depth. When they appear in NREM sleep, arousal responses are
not limited to a single EEG pattern but are part of a continuous spectrum of EEG 
modifications ranging from high-voltage slow rhythms to low amplitude fast
activities. The hierarchic features of arousal responses are reflected in the
phase A subtypes of CAP (cyclic alternating pattern) including both slow arousals
(dominated by the <1Hz oscillation) and fast arousals (ASDA arousals). CAP is an 
infraslow oscillation with a periodicity of 20-40s that participates in the
dynamic organization of sleep and in the activation of motor events. Physiologic,
paraphysiologic and pathologic motor activities during NREM sleep are always
associated with a stereotyped arousal pattern characterized by an initial
increase in EEG delta power and heart rate, followed by a progressive activation 
of faster EEG frequencies. These findings suggest that motor patterns are already
written in the brain codes (central pattern generators) embraced with an
automatic sequence of EEG-vegetative events, but require a certain degree of
activation (arousal) to become visibly apparent. Arousal can appear either
spontaneously or be elicited by internal (epileptic burst) or external (noise,
respiratory disturbance) stimuli. Whether the outcome is a physiologic movement, 
a muscle jerk or a major epileptic attack will depend on a number of ongoing
factors (sleep stage, delta power, neuro-motor network) but all events share the 
common trait of arousal-activated phenomena.

DOI: 10.1016/j.smrv.2005.12.004 
PMID: 16809057  [PubMed - indexed for MEDLINE]

