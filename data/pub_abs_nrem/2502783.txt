
1. Physiologie. 1989 Jan-Mar;26(1):39-49.

Autonomic control of the heart in some vagal maneuvers and normal sleep.

Negoescu RM(1), Csiki IE.

Author information: 
(1)Bioengineering Group, Institute of Hygiene and Public Health, Bucharest,
Romania.

Components of the autonomic control of the heart were studied in 5 young normals 
subjected to: 1) sustained lung inflation (SLI) in various postures; (2) mental
loading by concentration of attention, 3) face immersion in cold water under SLI 
or snorkel breathing, when sitting; 4) superposition of above experimental
interventions. We recorded cardiotachogram (RR interval) and T-amplitude from
ECG, ear lobe photoplethysmogram, digital impedance rheogram, thermistor
respiration. Cardiac data were explored by means of Fourier spectral analysis on 
a M 118 B microcomputer. Results caution that allegedly unilateral stimulation by
some "vagal" maneuvers triggers in fact amphotropic heart rate control menus
involving sympathetic nonreciprocal response. Subsequent bradycardia should be
interpreted as an absolute indicator of the parasympathetic function only when a 
separate measure of the sympathetic counterpart is available. In vigil young
adult at physical rest the medullary cardiovascular centers appear as subjected
to "sliding capture" by their prominent input signal--as determined by various
receptor stimulation or cortical status, the latter preserving a higher priority 
at competing task demands. In a coupled study 9 normals were polygraphically
monitored (EEG, EOG, ear duct temperature and the above variables as well) during
2 nonconsecutive nights and cardiac data were spectrally examined along various
sleep stages. Results suggest an increased dual autonomic tone during REM that
could be related to reported stage-specific arrhythmias. Reciprocal dynamics of
the respiratory versus lower frequency variability of RR interval from NREM to
REM sleep points to a useful discriminant for computer sleep stage scoring.


PMID: 2502783  [PubMed - indexed for MEDLINE]

