
1. Sleep. 2012 Sep 1;35(9):1269-75. doi: 10.5665/sleep.2084.

Cardiac and sympathetic activation are reduced in children with Down syndrome and
sleep disordered breathing.

O'Driscoll DM(1), Horne RS, Davey MJ, Hope SA, Anderson V, Trinder J, Walker AM, 
Nixon GM.

Author information: 
(1)The Ritchie Centre, Monash Institute of Medical Research, Monash University,
Victoria, Australia. denise.odriscoll@monash.edu

STUDY OBJECTIVES: Sleep disordered breathing (SDB) occurs at an increased
incidence in children with Down Syndrome (DS) compared to the general pediatric
population. We hypothesized that, compared with typically developing (TD)
children with SDB, children with DS have a reduced cardiovascular response with
delayed reoxygenation after obstructive respiratory events, and reduced
sympathetic drive, providing a potential explanation for their increased risk of 
pulmonary hypertension.
DESIGN: Beat-by-beat heart rate (HR) was analyzed over the course of obstructive 
events (pre, early, late, post-event) and compared between groups. Also compared 
were the time for oxygen resaturation post-event and overnight urinary
catecholamines.
SETTING: Pediatric sleep laboratory.
PATIENTS: Sixty-four children aged 2-17 y referred for investigation of SDB (32
DS; 32 TD) matched for age and obstructive apnea/hypopnea index.
MEASUREMENT AND RESULTS: Children underwent overnight polysomnography with
overnight urine collection. Compared to TD children, those with DS had
significantly reduced HR changes post-event during NREM (DS: 21.4% ± 1.8%, TD:
26.6% ± 1.6%, change from late to post-event, P < 0.05). The time to resaturation
post-event was significantly increased in the DS group (P < 0.05 for both NREM
and REM sleep). Children with DS had significantly reduced overnight urinary
noradrenaline (P < 0.01), adrenaline (P < 0.05) and dopamine levels (P < 0.01)
compared with TD children.
CONCLUSION: Children with DS and SDB exhibit a compromised acute
cardio-respiratory response and dampened sympathetic response to SDB compared
with TD children with SDB. These data may reflect autonomic dysfunction in
children with DS that may place them at increased risk for cardiovascular
complications such as pulmonary hypertension.

DOI: 10.5665/sleep.2084 
PMCID: PMC3413804
PMID: 22942505  [PubMed - indexed for MEDLINE]

