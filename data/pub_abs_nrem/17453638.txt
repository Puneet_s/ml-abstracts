
1. Amyotroph Lateral Scler. 2007 Apr;8(2):101-5.

Sleep characteristics of amyotrophic lateral sclerosis in patients with preserved
diaphragmatic function.

Atalaia A(1), De Carvalho M, Evangelista T, Pinto A.

Author information: 
(1)Laboratory of Clinical Neurophysiology, British Hospital--Lisbon XXI, Lisbon. 
Portugal. antonio.atalaia@mail.telelpac.pt

There are a number of sleep studies in amyotrophic lateral sclerosis (ALS), in
general including a heterogeneous population of patients. We aimed to study sleep
in a population of selected ALS patients by investigating nocturnal
polysomnography (PSG) characteristics in ALS patients with normal respiratory
function tests and preserved diaphragmatic innervation. Ninety-two ALS patients
were screened by percutaneous nocturnal oximetry (PNO). Eleven ALS patients with 
normal respiratory function tests, phrenic motor responses and preserved motor
units on needle electromyography of the diaphragm, but abnormal PNO, were
selected for PSG. REM was present in eight patients, but normal in only three.
Three patients had mixed apnoea-hypopnoea, severe in one. Seven showed a pattern 
of periodic mild O(2) desaturation, which occurred in REM 3, REM and NREM 3 and
in NREM sleep 1. One patient studied six months later had more severe changes in 
the second evaluation. In conclusion, the most common sleep disordered breathing 
was periodic mild O(2) desaturation independent of the sleep stage (REM, NREM).
This might represent central drive dysfunction or respiratory muscle fatigue in
ALS.

DOI: 10.1080/17482960601029883 
PMID: 17453638  [PubMed - indexed for MEDLINE]

