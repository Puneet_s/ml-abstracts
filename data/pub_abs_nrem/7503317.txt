
1. Am J Physiol. 1995 Nov;269(5 Pt 2):R1250-7.

Preoptic/anterior hypothalamic neurons: thermosensitivity in rapid eye movement
sleep.

Alam MN(1), McGinty D, Szymusiak R.

Author information: 
(1)Department of Psychology, University of California, Los Angeles 90024, USA.

The thermosensitivity of 15 warm-sensitive neurons (WSNs) and 19 cold-sensitive
neurons (CSNs) from the medial preoptic/anterior hypothalamus (POAH) was tested
during wakefulness, non-rapid eye movement (NREM) sleep and rapid eye movement
(REM) sleep by local POAH warming and cooling in freely moving cats.
Thermosensitivity was quantified by three criteria, Q10, impulses per second per 
degree Celsius, and percent change per degree Celsius. Irrespective of the
criterion used, WSNs did not exhibit a significant change in thermosensitivity
during REM sleep compared with wakefulness and NREM sleep. In contrast, CSNs
exhibited decreased mean thermosensitivity during REM sleep compared with
wakefulness. CSNs as a group did not retain significant thermosensitivity in REM 
sleep. These findings are consistent with evidence that thermoeffector responses 
to cooling are lost in REM sleep, whereas some responses to warming are
preserved.


PMID: 7503317  [PubMed - indexed for MEDLINE]

