
1. Eur Neuropsychopharmacol. 2014 Apr;24(4):585-94. doi:
10.1016/j.euroneuro.2013.09.002. Epub 2013 Sep 12.

GABA transporter-1 inhibitor NO-711 alters the EEG power spectra and enhances
non-rapid eye movement sleep during the active phase in mice.

Xu XH(1), Qiu MH(2), Dong H(2), Qu WM(3), Urade Y(4), Huang ZL(5).

Author information: 
(1)Department of Pharmacology, Shanghai Medical College, Fudan University,
Shanghai, China. (2)State Key Laboratory of Medical Neurobiology, Fudan
University, Shanghai, China. (3)Department of Pharmacology, Shanghai Medical
College, Fudan University, Shanghai, China; Institutes of Brain Science, Fudan
University, Shanghai, China. Electronic address: quweimin@fudan.edu.cn.
(4)Department of Molecular Behavioral Biology, Osaka Bioscience Institute, Suita,
Osaka, Japan. (5)Department of Pharmacology, Shanghai Medical College, Fudan
University, Shanghai, China; State Key Laboratory of Medical Neurobiology, Fudan 
University, Shanghai, China; Institutes of Brain Science, Fudan University,
Shanghai, China. Electronic address: huangzl@fudan.edu.cn.

GABA transporter subtype 1 (GAT1) constructs high affinity reuptake sites for
GABA in the CNS and regulates GABAergic transmission. Compounds that inhibit GAT1
are targets often used for the treatment of epilepsy; however sedation has been
reported as a side effect of these agents, indicating potential sedative and/or
hypnotic uses for these compounds. In the current study, we observed the sleep
behaviors of mice treated with NO-711, a selective GAT1 inhibitor, in order to
elucidate the role of GAT1 in sleep-wake regulation during the active phase. The 
data revealed that NO-711 at a high dose of 10 mg/kg caused a marked enhancement 
of EEG activity in the frequency ranges of 3-25 Hz during wakefulness as well as 
rapid eye movement (REM) sleep. During the non-REM (NREM) sleep, NO-711 (10
mg/kg) elevated EEG activity in the frequency ranges of 1.5-6.75 Hz. Similar
changes were found in mice treated with a low dose of 3 mg/kg. NO-711
administered i.p. at a dose of 1, 3 or 10 mg/kg significantly shortened the sleep
latency of NREM sleep, increased the amount of NREM sleep and the number of NREM 
sleep episodes. NO-711 did not affect the sleep latency and the amount of REM
sleep. NO-711 dose-dependently increased c-Fos expression in sleep-promoting
nucleus of the ventrolateral preoptic area and median preoptic area. However,
c-Fos expression was decreased in the wake-promoting nuclei, tuberomammillary
nucleus and lateral hypothalamus. These results indicate that NO-711 can increase
NREM sleep in mice.

© 2013 Published by Elsevier B.V. and ECNP.

DOI: 10.1016/j.euroneuro.2013.09.002 
PMID: 24080505  [PubMed - indexed for MEDLINE]

